package com.igt.psr.ui.view;

import java.util.Date;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PsrEventModelView {
	private static final long serialVersionUID = 1L;

	private String modelName;
	private String eventCode;
	private String description;
	private Date planDate;
	private Date actualDate;
	private Date revisedDate;
	private String triggerEvent;
	private String triggerDays;
	private Boolean disableEvent;

}
