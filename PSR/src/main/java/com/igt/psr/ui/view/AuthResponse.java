package com.igt.psr.ui.view;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.igt.psr.common.util.PSRResponse;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Thippeswamy
 *
 */
@Data
@Accessors(chain = true)
public class AuthResponse extends PSRResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private String loginUserId;
	private String brUserId;
	private String tokenId;
	private Map<String, String> roleMap;
	private String roleName;
	private String queryName;
	private Integer queryId;
	private List<String> milestone;
	private Integer gridRowsLimit;
}