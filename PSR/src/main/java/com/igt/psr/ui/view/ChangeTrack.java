package com.igt.psr.ui.view;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Yashwanth
 *
 */
@Data
@Accessors(chain = true)
public class ChangeTrack implements Serializable {
	private static final long serialVersionUID = 1L;

	private String vpo;
	private Date logDate;
	private Integer psrOrder;
	private String modifiedBy;
	private Date timeStamp;
	private String timeStampStr;
	private String type;
	private String field;
	private String before;
	private String after;

	private String timeStampKey;
	private String hrWithMins;
	private String timeFormate;
}