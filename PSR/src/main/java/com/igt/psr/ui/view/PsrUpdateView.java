package com.igt.psr.ui.view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.igt.psr.db.model.PsrOrder;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Thippeswamy
 *
 */
@Data
@Accessors(chain = true)
public class PsrUpdateView implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<PsrOrder> orders = new ArrayList<>();
	private List<PsrEventsView> events = new ArrayList<>();
}