package com.igt.psr.ui.view;

import java.io.Serializable;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Thippeswamy
 *
 */
@Data
@Accessors(chain = true)
public class PsrHeaderColumns implements Serializable {
	private static final long serialVersionUID = 1L;

	private String text;
	private String columngroup;
	private String datafield;
	private String editable;
	private String width;
	private String cellsformat;
	private String initeditor;
	private String cellsalign;
	private String align;
	private String columntype;
	private String cellbeginedit;
	private String cellsrenderer;
	private String hidden;
	private String clipboard;
	private String cellclassname;
}
