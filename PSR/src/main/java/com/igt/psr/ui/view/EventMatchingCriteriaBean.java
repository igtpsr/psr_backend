package com.igt.psr.ui.view;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Thippeswamy
 *
 */
@Data
@Accessors(chain = true)
@JsonDeserialize
public class EventMatchingCriteriaBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String modelName;
	private List<EventModel> eventModel;
}
