package com.igt.psr.ui.view;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Thippeswamy
 *
 */
@Data
@Accessors(chain = true)
public class PsrEventsWRTVpo implements Serializable {
	private static final long serialVersionUID = 1L;
	private String vpo;
	private String eventDesc;
	private Date actualDate;
	private Date planneDate;
	private Date revisedDate;

}
