package com.igt.psr.ui.view;

import java.io.Serializable;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Thippeswamy
 *
 */
@Data
@Accessors(chain = true)
public class SearchForm implements Serializable {

	private static final long serialVersionUID = 1L;

	private String loginUserId;
	private String brUserId;
	private String vpo;
	private String productionOffice;
	private String vendorId;
	private String vendorName;
	private String factoryId;
	private String factoryName;
	private String countryOrigin;
	private String brand;
	private String style;
	private String styleDescription;

	private String cpo;
	private String dept;
	private String season;
	private String colorWashName;
	private String ndcdate;
	private String vpoNltGacDate;
	private String vpoFtyGacDate;
	private String ndcWk;
	private String vpoStatus;
	private String vpoLineStatus;
	private String ocCommit;
	private String category;
	private String cpoColorDesc;
	private String cpoDept;
	private String initFlow;

	private String vposelect;
	private String vendorIdSelect;
	private String vendorNameSelect;
	private String factoryIdSelect;
	private String factoryNameSelect;
	private String brandSelect;
	private String styleSelect;
	private String styleDescSelect;
	private String prodOfficeSelect;
	private String countryOriginSelect;
	private String milestoneselect;

	private String cpoSelect;
	private String deptSelect;
	private String seasonSelect;
	private String colorWashNameSelect;
	private String ndcdateSelect;
	private String vpoNltGacDateSelect;
	private String vpoFtyGacDateSelect;
	private String ndcWkSelect;
	private String vpoStatusSelect;
	private String vpoLineStatusSelect;

	private String ocCommitSelect;
	private String categorySelect;
	private String cpoColorDescSelect;
	private String cpoDeptSelect;
	private String initFlowSelect;

	private int pageNo;
	private int pageSize = 100;
	private String roleName;

}