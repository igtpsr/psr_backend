package com.igt.psr.ui.view;

public class PsrUniqueExcelData {
	private String loginUserId;
	private String excelUniqueId;

	public String getLoginUserId() {
		return loginUserId;
	}

	public void setLoginUserId(String loginUserId) {
		this.loginUserId = loginUserId;
	}

	public String getExcelUniqueId() {
		return excelUniqueId;
	}

	public void setExcelUniqueId(String excelUniqueId) {
		this.excelUniqueId = excelUniqueId;
	}

}
