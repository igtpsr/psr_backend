package com.igt.psr.ui.view;

import java.io.Serializable;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Thippeswamy
 *
 */
@Data
@Accessors(chain = true)
public class PsrEventsView implements Serializable {
	private static final long serialVersionUID = 1L;
	private int psrOrderId;
	private String eventCode;
	private String actualDateStr;
	private String revisedDateStr;

}
