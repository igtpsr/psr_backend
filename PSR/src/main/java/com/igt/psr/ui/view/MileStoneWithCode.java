package com.igt.psr.ui.view;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Thippeswamy
 *
 */
@Data
@Accessors(chain = true)
public class MileStoneWithCode {
	private String eventcode;
	private String eventDesc;
}
