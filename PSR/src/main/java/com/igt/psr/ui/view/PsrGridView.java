package com.igt.psr.ui.view;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Size;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Thippeswamy
 *
 */
@Data
@Accessors(chain = true)
public class PsrGridView implements Serializable {

	private static final long serialVersionUID = 1L;

	private int psrOrderId;

	private String vpo;

	private String colorWashName;

	private String shipTo;

	private String soId;

	private String eventCode;

	private String prodOffice;

	private String vendorId;

	private String vendorName;

	private String factoryId;

	private String factoryName;

	private String countryOrigin;

	private String brand;

	private String dept;

	private String category;

	private String merchant;

	private String style;

	private String styleDesc;

	private BigDecimal fob;

	private BigDecimal grossSellPrice;

	private BigDecimal margin;

	private String productType;

	private String programName;

	private String shipMode;

	private String gender;

	private BigDecimal orderQty;

	private BigDecimal shippedQty;

	private String ocCommit;

	private String cpo;

	private String season;

	private Date ndcDate;

	private Date vpoNltGacDate;

	private Date vpoFtyGacDate;

	private String keyItem;

	private String vpoStatus;

	private String vpoLineStatus;

	private Date interfaceTs;

	private BigDecimal cpoOrderQty;

	private String cpoShipMode;

	private String cpoColorDesc;

	private String cpoPackType;

	private Date modifyTs;

	private String modifyUser;

	private String profomaPo;

	private BigDecimal actualPoundageAvailable;

	private String apprFabricYarnLot;

	private String asn;

	private Integer balanceShip;

	private String careLabelCode;

	private String colorCodePatrn;

	@Size(min = 2, message = "Comment should have atleast 2 characters")
	private String comment;

	private Date createDate;

	private String custFtyId;

	private String cutApprLetter;

	private Integer dailyOutputPerLine;

	private String extraProcess;

	private String fabTrimOrder;

	private String fabricCode;

	private Date fabricEstDate;

	private String fabricMill;

	private String fabricTestReportNumeric;

	private String factoryRef;

	private String fiberContent;

	private Date plActualGac;

	private Date cpoGacDate;

	private Date garmentTestActual;

	private String gauge;

	private String graphicName;

	private String image;

	private BigDecimal knitting;

	private Integer knittingMachines;

	private Integer knittingPcs;

	private BigDecimal linking;

	private Integer linkingMachines;

	private Integer linkingPcs;

	private String ndcWk;

	private String orderType;

	private String packType;

	private Date packagingReadyDate;

	private BigDecimal salesAmount;

	private Integer sewingLines;

	private String ticketColorCode;

	private String ticketStyleNumeric;

	private Date vpoFtyGacDateTemp;

	private Date vpoHeadStatus;

	// private String eventDesc;

	private Date actualDate;

	private Date plannedDate;

	private Date revisedDate;

}