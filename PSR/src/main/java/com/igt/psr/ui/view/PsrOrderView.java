package com.igt.psr.ui.view;

import java.io.File;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Thippeswamy
 *
 */
@Data
@Accessors(chain = true)
public class PsrOrderView implements Serializable {

	private static final long serialVersionUID = 1L;

	private String vpo;
	private File imageFile;
	private String prodOffice;
	private String vendorId;
	private String vendorName;
	private String factoryId;
	private String factoryName;
	private String countryOrigin;
	private String brand;
	private String style;
	private String styleDesc;
	private Date ndcDate;
	private Date vpoNltGacDate;
	private Timestamp planDate;
	private Timestamp actualDate;
	private Timestamp cpoPlanDate;
	private Timestamp cpoActualDate;
	private int uid;
	private int boundindex;
	private int visibleindex;
	private String uniqueid;
	private String model = "--Select--";
	private List<PsrOrderView> locations = new ArrayList<PsrOrderView>();

}