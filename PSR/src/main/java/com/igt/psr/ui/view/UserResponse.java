package com.igt.psr.ui.view;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserResponse implements Serializable {
	private static final long serialVersionUID = 1L;

	private List<PtlUserProfileForm> PtlUserProfileDetails;
	private Map<Integer, String> allQueryIds;
	private Map<String, String> allCntryNames;
	private Map<Integer, String> ptlRoleProfileDetails;
}