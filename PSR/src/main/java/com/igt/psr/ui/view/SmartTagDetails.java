package com.igt.psr.ui.view;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)

public class SmartTagDetails {
	
	private String itemNumber;
	private String fabricationCategory;
	
}
