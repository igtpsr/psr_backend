package com.igt.psr.ui.view;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PsrDynamicData implements Serializable {

	public Integer TotalRows;
	public List<Map> map;
	public String searchTs;
}
