package com.igt.psr.ui.view;

import java.io.Serializable;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CustomisedViewForm implements Serializable {
	private static final long serialVersionUID = 1L;
	private String queryId;
	private String selectedColumn;
}
