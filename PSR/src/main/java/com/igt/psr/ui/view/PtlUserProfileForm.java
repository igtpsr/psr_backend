package com.igt.psr.ui.view;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PtlUserProfileForm implements Serializable {
	private static final long serialVersionUID = 1L;

	private String userId;
	private String brUserId;
	private String userName;
	private String pswd;
	private String email;
	private List<Integer> roleIds;
	private String country;
	private Date pswEffectiveDate;
	private Integer pswdEffectiveDays;
	private Integer pswdNotifyDays;
	private Date pswdExpireDate;
	private String pswdNeverExpires;
	private String disableUser;
	private Date pswdChangeDate;
	private String pswdChgNxtLogin;
	private String pswdResetDigest;
	private String roleName;
	private String createDate;
	private Integer queryId;
	private String queryName;
}
