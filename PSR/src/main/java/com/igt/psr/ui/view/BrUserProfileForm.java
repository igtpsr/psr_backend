
package com.igt.psr.ui.view;

import java.io.Serializable;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class BrUserProfileForm implements Serializable {

	private static final long serialVersionUID = 1L;

	private String brUserID;
	private String brUserName;
	private Integer brRoleId;
	private String brRoleName;
	private String brAttrName;
	private String brAttrValue;

}