package com.igt.psr.ui.view;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class FileTrack implements Serializable {
	private static final long serialVersionUID = 1L;

	private String loginUserId;
	private String fileName;
	private String desc;
	private String userId;
	private Date startTime;
	private Date endTime;
	private String startTimeStr;
	private String endTimeStr;
	private Date modifyTs;
}
