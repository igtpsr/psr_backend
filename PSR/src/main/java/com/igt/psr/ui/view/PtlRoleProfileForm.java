package com.igt.psr.ui.view;

import java.io.Serializable;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PtlRoleProfileForm implements Serializable {
	private static final long serialVersionUID = 1L;

	private int roleId;
	private String roleName;
	private String description;
	private String appName;
	private String disableRole;
	private Boolean brUserIdStatus;
}
