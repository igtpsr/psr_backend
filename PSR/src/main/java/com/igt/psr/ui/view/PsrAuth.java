package com.igt.psr.ui.view;

public class PsrAuth {
	private static final long serialVersionUID = 1L;

	private String tokenId;
	private String uniqueId;

	public String getTokenId() {
		return tokenId;
	}

	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}
	
	public String getUniqueId() {
		return uniqueId;
	}


	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}
	
}
