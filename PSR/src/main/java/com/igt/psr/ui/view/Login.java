package com.igt.psr.ui.view;

import java.io.Serializable;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Thippeswamy
 *
 */
@Data
@Accessors(chain = true)
public class Login implements Serializable {
	private static final long serialVersionUID = 1L;

	private String username;
	private String password;
	private String credentials;
}