package com.igt.psr.ui.view;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Thippeswamy
 *
 */
@Data
@Accessors(chain = true)
public class PostXmlInputParams implements Serializable {

	private static final long serialVersionUID = 1L;

	private String shipTo;
	private String colorWashName;
	private String profomaPo;
	private Date revisedDate;
	private String revisedDateStr;
	private String planId;
	private int psrOrderId;

}
