package com.igt.psr.dao;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.igt.psr.common.constants.ApplicationConstants;
import com.igt.psr.common.dao.QueryGenerator;
import com.igt.psr.common.util.DAOHelper;
import com.igt.psr.common.util.PSRHelper;
import com.igt.psr.db.dto.PsrSaveSearchDto;
import com.igt.psr.db.model.PsrEvents;
import com.igt.psr.db.model.PsrOrder;
import com.igt.psr.db.model.PsrOrderPK;
import com.igt.psr.db.model.PsrSaveSearch;
import com.igt.psr.db.repo.PsrEventsRepository;
import com.igt.psr.db.repo.PsrOrderRepository;
import com.igt.psr.db.repo.PsrSaveSearchRepository;
import com.igt.psr.portal.model.BrUserProfile;
import com.igt.psr.ui.view.ChangeTrack;
import com.igt.psr.ui.view.FileTrack;
import com.igt.psr.ui.view.PostXmlInputParams;
import com.igt.psr.ui.view.PsrEventsView;
import com.igt.psr.ui.view.PsrGridView;
import com.igt.psr.ui.view.PsrOrderView;
import com.igt.psr.ui.view.PsrUpdateView;
import com.igt.psr.ui.view.SearchForm;

@Service
public class PsrDao {

	private static final Logger log = LoggerFactory.getLogger(PsrDao.class);

	@Autowired
	private PsrOrderRepository psrRepository;

	@Autowired
	private PsrEventsRepository psrEventsRepository;

	@Autowired
	private PsrSaveSearchRepository psrSaveSearchRepository;

	@PersistenceUnit(unitName = "psrdb")
	private EntityManagerFactory emf;

	public PsrOrder save(PsrOrder psr) {
		return psrRepository.save(psr);
	}

	public void delete(Long psrOrderId) {
		psrRepository.delete(psrOrderId);
	}

	public PsrOrder saveOrUpdate(PsrOrder psr) {
		return psrRepository.save(psr);
	}

	public List<PsrOrder> findAll() {
		return psrRepository.findAll();
	}

	public PsrOrder findOne(PsrOrderPK psrOrderPK) {
		return null;
	}

	public void eventsData(Long eventCode) {
		EntityManager em = emf.createEntityManager();
		Query q = em.createNativeQuery(
				"SELECT  em.EVENT_CODE FROM PSR_EVENT_MASTER em ,PSR_EVENT e where em.EVENT_CODE = e.EVENT_CODE and e.EVENT_CODE= ? ");
		q.setParameter(1, eventCode);
		log.info(String.format(" events size %d", q.getResultList().size()));
	}

	public List<PsrOrderView> getMatchingCriteriaModelNames() {

		EntityManager em = emf.createEntityManager();
		Query q = em.createNativeQuery("SELECT DISTINCT em.MODEL_NAME FROM PSR_MATCH_CRITERIA em");
		List<PsrOrderView> psrSeacrh = q.getResultList();
		return psrSeacrh;
	}

	public List<Map> getSearchInfo(SearchForm searchForm) throws JsonProcessingException {
		log.info(" enetered into the method getSearchInfo with SearchForm as argument ");
		List<String> eventDescList = this.getEventDescriptions(searchForm.getBrUserId(), searchForm.getLoginUserId(),
				searchForm.getRoleName());
		EntityManager em = emf.createEntityManager();
		Query q = em.createNativeQuery(QueryGenerator.getSearchQueryStr(searchForm));
		List<Object[]> psrSerachList = q.getResultList();
		return DAOHelper.getPsrDataResultSetUtility(psrSerachList, eventDescList);
	}

	public int getPsrDataTotalCount(SearchForm searchForm) {
		log.info(" enetered into the method getTotalCountPsrData with SearchForm as argument ");
		EntityManager em = emf.createEntityManager();
		return (int) em.createNativeQuery(QueryGenerator.getPsrOrdersCount(searchForm)).getSingleResult();
	}

	public List<Map> getPsrDynamicGridData(SearchForm searchForm) throws Exception {
		log.info(" enetered into the method getPsrDynamicGridData with SearchForm as argument ");
		List<String> eventDescList = this.getEventDescriptions(searchForm.getBrUserId(), searchForm.getLoginUserId(),
				searchForm.getRoleName());
		EntityManager em = emf.createEntityManager();
		Query q = em.createNativeQuery(QueryGenerator.getSearchQueryStr(searchForm));
		List<Object[]> list = q.getResultList();
		return DAOHelper.getPsrDataResultSetUtility(list, eventDescList);
	}

	public List<String> getEventDescriptions(String brUserId, String loginUserId, String roleName) {
		EntityManager em = emf.createEntityManager();
		Query q = em.createNativeQuery(QueryGenerator.getPsrGridEvents(brUserId, loginUserId, roleName));
		List<Object[]> eventObj = q.getResultList();
		LinkedList<String> psrEventDescList = new LinkedList<>();
		for (Object[] object : eventObj) {
			psrEventDescList.add((String) object[0]);
		}
		log.info(String.format(" Size of PSR Headers Object : %d", psrEventDescList.size()));
		return psrEventDescList;
	}

	public Map<String, String> getEventMileStones(String brUserId, String loginUserId, String roleName) {
		EntityManager em = emf.createEntityManager();
		Query q = em.createNativeQuery(QueryGenerator.getPsrGridEvents(brUserId, loginUserId, roleName));
		List<Object[]> psrEventList = q.getResultList();
		Map<String, String> psrEvent = new LinkedHashMap<>();
		for (Object[] object : psrEventList) {
			psrEvent.put((String) object[0], (String) object[1]);
		}
		return psrEvent;
	}

	public List<String> getSelectedColumnView(String queryName, String loginUserId) {
		EntityManager em = emf.createEntityManager();
		if (queryName != null && queryName.contains(" (Public)")) {
			queryName = queryName.substring(0, queryName.lastIndexOf(" (Public)")).trim();
		}
		String selectedCols = (String) em
				.createNativeQuery("select em.Selected_Columns from PSR_USER_CUSTIMOZED_VIEW em where em.Query_Name ='"
						+ queryName + "' and em.LOGIN_USER ='" + loginUserId
						+ "' or em.PUBLIC_VIEW = 1 and LTRIM(RTRIM(em.Query_Name)) = LTRIM(RTRIM('" + queryName
						+ "')) ")
				.getSingleResult();
		return Arrays.asList(selectedCols.split("\\s*,\\s*"));
	}
	
	public List<String> getReOrderColumn(String queryName, String loginUserId) {
		EntityManager em = emf.createEntityManager();
		if (queryName != null && queryName.contains(" (Public)")) {
			queryName = queryName.substring(0, queryName.lastIndexOf(" (Public)")).trim();
		}
		String selectedCols = (String) em
				.createNativeQuery("select em.COLUMNS_ORDER from PSR_USER_CUSTIMOZED_VIEW em where em.Query_Name ='"
						+ queryName + "' and em.LOGIN_USER ='" + loginUserId
						+ "' or em.PUBLIC_VIEW = 1 and LTRIM(RTRIM(em.Query_Name)) = LTRIM(RTRIM('" + queryName
						+ "')) ")
				.getSingleResult();
		
		if(selectedCols !=null) {
			return Arrays.asList(selectedCols.split("\\s*,\\s*"));
		} else {
			return null;
		}
		
	}

	public List getCustomisedViewNames(String logInUserId) throws Exception {
		EntityManager em = emf.createEntityManager();
		return em.createNativeQuery(
				" SELECT QUERY_ID, CASE WHEN PUBLIC_VIEW = 1 THEN QUERY_NAME+' (Public)' ELSE QUERY_NAME END AS QueryName,PUBLIC_VIEW as pubView FROM PSR_USER_CUSTIMOZED_VIEW  where LOGIN_USER ='"
						+ logInUserId + "' or PUBLIC_VIEW = 1  ORDER BY QUERY_NAME ASC ")
				.getResultList();
	}

	public void doAutoAttach(PsrOrder psrOrder) throws Exception {
		log.info(String.format("  psrOrder.getBrand() %s", psrOrder.getBrand()));
		List<Object[]> autoAttachEvents = this.getAutoAttach(psrOrder.getBrand(), psrOrder.getCategory(),
				psrOrder.getProdOffice());
		if (autoAttachEvents != null && autoAttachEvents.size() > 0) {
			PsrEvents events = DAOHelper.getPsrEvents(autoAttachEvents, psrOrder);
			psrEventsRepository.save(events);
		}
	}

	private List<Object[]> getAutoAttach(String brand, String category, String prodOffice) throws Exception {
		EntityManager em = emf.createEntityManager();
		return em.createNativeQuery(QueryGenerator.getAutoAttachQuery(brand, category, prodOffice)).getResultList();
	}

	public PsrOrder getSingleRow() throws Exception {
		return psrRepository.findAll().get(0);
	}

	public List<PsrGridView> getPsrData(SearchForm searchForm) throws Exception {
		EntityManager em = emf.createEntityManager();
		Query q = em.createNativeQuery(QueryGenerator.getSearchQueryStr(searchForm));
		List<Object[]> list = q.getResultList();
		List<PsrGridView> gridViews = new ArrayList<>();
		for (Object[] objects : list) {
			PsrGridView psrGridView = DAOHelper.getObject(objects);
			gridViews.add(psrGridView);
		}
		return gridViews;
	}

	public String updateOrders(List<PsrUpdateView> updateOrders, String imageurl, String loginUserId, String roleName,
			String searchTs) throws Exception {
		try {
			String psrOrderStr = null;
			for (PsrUpdateView psrUpdateView : updateOrders) {
				List<PsrOrder> orders = psrUpdateView.getOrders();
				List<PsrEventsView> events = psrUpdateView.getEvents();
				Map<Integer, List<PsrEventsView>> eventsWRTOrder = DAOHelper.getEventsWRTOrder(events);
				psrOrderStr = DAOHelper.constructXmlStr(orders, eventsWRTOrder, imageurl);
			}
			log.info(String.format(" Stored Procedure String Starts %s", psrOrderStr));
			return this.callStoredProcedureToSavePsrData(psrOrderStr, loginUserId, roleName, searchTs);
		} catch (Exception e) {
			log.error(" An error has been occured --> " + e.getMessage());
			throw e;
		}
	}

	public String callStoredProcedureToSavePsrData(String psrOrderStr, String loginUserId, String roleName,
			String searchTs) throws Exception {
		log.info(" psrOrderStr --> \n " + psrOrderStr.toString());
		if (psrOrderStr != null && psrOrderStr.trim().length() > 0) {
			EntityManager em = emf.createEntityManager();
			try {
				String message = (String) em.createStoredProcedureQuery("PSR_SAVEORDERS")
						.registerStoredProcedureParameter("XMLSTR", String.class, ParameterMode.IN)
						.registerStoredProcedureParameter("ModifiedBy", String.class, ParameterMode.IN)
						.registerStoredProcedureParameter("userrole", String.class, ParameterMode.IN)
						.registerStoredProcedureParameter("searchdate", String.class, ParameterMode.IN)
						.registerStoredProcedureParameter("result", String.class, ParameterMode.OUT)
						.setParameter("XMLSTR", psrOrderStr).setParameter("ModifiedBy", loginUserId)
						.setParameter("userrole", roleName).setParameter("searchdate", searchTs)
						.getOutputParameterValue("result");
				log.info(String.format("  message  %s", message));
				if (PSRHelper.checkEmptyOrNull(message) && message.contains("FAILED DUE TO DEADLOCK.")) {
					throw new Exception("This is failed due to deadlock situation, please try after sometime");
				}
				return message;
			} catch (Exception e) {
				log.error(String.format(" Orders data has not been updated %s", e.getMessage()));
				throw e;
			} finally {
				em.close();
			}
		}
		return null;
	}

	public void bulkUpdate() {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			int i = em.createNativeQuery("").executeUpdate();
			em.getTransaction().commit();
			if (i > 0) {
				log.info(String.format(" No of Orders has been Updated, count --> %s", i));
			}
		} catch (Exception e) {
			log.error(String.format("  Orders are not Updated %s", e.getMessage()));
			em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}
	}

	public void doReCalculate(String loginUserId, String commaSepratedOrderIds) throws Exception {
		if (commaSepratedOrderIds != null && commaSepratedOrderIds.trim().length() > 0) {
			EntityManager em = emf.createEntityManager();
			try {
				String message = (String) em.createStoredProcedureQuery("PSR_RECALCDATES")
						.registerStoredProcedureParameter("ORDER_ID", String.class, ParameterMode.IN)
						.registerStoredProcedureParameter("ModifiedBy", String.class, ParameterMode.IN)
						.registerStoredProcedureParameter("result", String.class, ParameterMode.OUT)
						.setParameter("ORDER_ID", commaSepratedOrderIds).setParameter("ModifiedBy", loginUserId).getOutputParameterValue("result");
				log.info(String.format(" recalculated  planned date & revised date has updated, total count --> %s",
						message));
				if (PSRHelper.checkEmptyOrNull(message) && message.contains("FAILED DUE TO DEADLOCK.")) {
					throw new Exception("This is failed due to deadlock situation, please try after sometime");
				}
			} catch (Exception e) {
				log.error(
						String.format(" recalculated  planned date & revised date has not updated %s", e.getMessage()));
				throw e;
			} finally {
				em.close();
			}
		}
	}

	public String getRevisedDateForGacByVendor(int psrOrderId) {
		EntityManager em = emf.createEntityManager();
		Date dbRevisedDate = null;
		try {
			dbRevisedDate = (Date) em.createNativeQuery(
					"select revised_date from psr_events where psr_order_id=" + psrOrderId + " and event_code='E000'")
					.getSingleResult();

		} catch (Exception e) {

			log.error(String.format(" Error in getRevisedDateForGacByVendor %s ", e.getMessage()));
		} finally {
			em.close();
		}
		return PSRHelper.getDateStrForSP(dbRevisedDate);
	}

	public String deleteImage(String psrOrderId, String loginUserId, String roleName, String searchTs)
			throws Exception {
		log.info(" enetered into the method deleteImage ");
		String delStr = DAOHelper.constructDelImageXmlStr(psrOrderId);
		this.callStoredProcedureToSavePsrData(delStr, loginUserId, roleName, searchTs);
		return "SUCCESS";
	}

	public Map<String, LinkedHashSet<ChangeTrack>> getChangeTrackDetails(String psrOrderIds, String from,
			String searchKeyword, String fromDate, String toDate) throws Exception {
		log.info(" enetered into the method getChangeTrackDetails ");
		List<Object[]> changeTrackList = null;
		LinkedHashSet<ChangeTrack> gridViews = null;
		ChangeTrack chgTrack = null;
		int i = 0;
		Map<String, LinkedHashSet<ChangeTrack>> map = new LinkedHashMap<>();
		String[] searchKeywords = null;
		String finalSearchKeyword = "";
		List<String> listOfSearchKeyword = new ArrayList<>();
		if (searchKeyword != null) {
			searchKeywords = searchKeyword.split(" ");
		}
		if (searchKeywords.length > 1) {
			finalSearchKeyword = searchKeyword.replace(" ", "_");
		} else{
			finalSearchKeyword = searchKeyword;
		}
		listOfSearchKeyword.add(finalSearchKeyword);
		//to fetch psr orderids
		EntityManager em2 = emf.createEntityManager();
		try {
			if (searchKeyword != null && !searchKeyword.equalsIgnoreCase("")) {
				Query q = em2.createNativeQuery(
						"Select PSR_ORDER_ID from PSR_ORDER where VPO like '%" + searchKeyword.trim()+ "%'");
				List<Integer> list = q.getResultList();
				for (Integer str : list) {
					listOfSearchKeyword.add(String.valueOf(str));
				}
			}
		} catch (Exception e) {
			log.error(String.format(" Error occured while getting psr order ids in PSR Dao : %s", e.getMessage()));
			throw e;
		} finally {
			em2.close();
		}
		log.info(Arrays.toString(listOfSearchKeyword.toArray()));
		
		//to fetch event codes
		EntityManager em1 = emf.createEntityManager();
		try {
			if (searchKeyword != null && !searchKeyword.equalsIgnoreCase("")) {
				Query q = em1.createNativeQuery(
						"Select EVENT_CODE from PSR_EVENT_MASTER where EVENT_DESC like '%" + searchKeyword.replace("Actual Date", "").replace("Revised Date", "").trim()+ "%'");
				List<String> list = q.getResultList();
				for (String str : list) {
					listOfSearchKeyword.add(str);
				}
			}
		} catch (Exception e) {
			log.error(String.format(" Error occured while getting event codes in PSR Dao : %s", e.getMessage()));
			throw e;
		} finally {
			em1.close();
		}
		log.info(Arrays.toString(listOfSearchKeyword.toArray()));
		for (String search : listOfSearchKeyword) {
			EntityManager em = emf.createEntityManager();
			if (psrOrderIds != null && !psrOrderIds.trim().isEmpty()) {
				try {

					Integer since = 0;
					String type = "ALL";
					if (from != null) {
						if (from.equalsIgnoreCase("Today")) {
							since = 0;
							type = "";
						}
						if (from.equalsIgnoreCase("Yesterday")) {
							since = -1;
							type = "";
						} else if (from.equalsIgnoreCase("Last Week")) {
							since = -7;
							type = "";
						} else if (from.equalsIgnoreCase("Last Month")) {
							since = -30;
							type = "";
						} else if (from.equalsIgnoreCase("All")) {
							type = "ALL";
						} else if (from.equalsIgnoreCase("Custom")) {
							type = "CUSTOM";
							if (fromDate == null || fromDate.equalsIgnoreCase("") || toDate == null
									|| toDate.equalsIgnoreCase("")) {
								throw new Exception("Dates must be provide in order to search.");
							}
							fromDate = fromDate.trim() + " 00:00:00.000";
							toDate = toDate.trim() + " 23:59:59.999";
						}
					}

					log.info(type);
					log.info(since.toString());
					log.info(fromDate);
					log.info(toDate);
					log.info(search);

					changeTrackList = em.createStoredProcedureQuery("PSR_LOG_EXTRACT")
							.registerStoredProcedureParameter("PSRLIST", String.class, ParameterMode.IN)
							.registerStoredProcedureParameter("SINCE", Integer.class, ParameterMode.IN)
							.registerStoredProcedureParameter("TYPE", String.class, ParameterMode.IN)
							.registerStoredProcedureParameter("stringToFind", String.class, ParameterMode.IN)
							.registerStoredProcedureParameter("fromDate", String.class, ParameterMode.IN)
							.registerStoredProcedureParameter("toDate", String.class, ParameterMode.IN)
							.setParameter("PSRLIST", psrOrderIds).setParameter("SINCE", since)
							.setParameter("TYPE", type).setParameter("stringToFind", search.trim())
							.setParameter("fromDate", fromDate).setParameter("toDate", toDate).getResultList();

					for (Object[] object : changeTrackList) {
						i++;
						chgTrack = new ChangeTrack().setVpo((String) object[0]).setLogDate((Date) object[1])
								.setPsrOrder((Integer) object[2]).setModifiedBy((String) object[3])
								.setTimeStamp((Date) object[4]).setType((String) object[5]).setField((String) object[6])
								.setBefore((String) object[7]).setAfter((String) object[8]);
						if (chgTrack.getType() != null && !chgTrack.getType().isEmpty()
								&& chgTrack.getType().equalsIgnoreCase("EVENT")) {
							String eventDesc = ApplicationConstants.getLabel(
									chgTrack.getField().substring(0, chgTrack.getField().trim().indexOf(' ')).trim());
							chgTrack.setField(eventDesc + chgTrack.getField().substring(4, chgTrack.getField().length())
									.replace("ACTUAL_DATE", "Actual Date").replace("REVISED_DATE", "Revised Date"));
							chgTrack.setBefore(PSRHelper.getDateWithCustomFormat((String) object[7]));
							chgTrack.setAfter(PSRHelper.getDateWithCustomFormat((String) object[8]));
						} else {
							chgTrack.setField(ApplicationConstants.getLabel(chgTrack.getField()));
							if (chgTrack.getField().trim().equalsIgnoreCase("Tech Pack Received Date")
									|| chgTrack.getField().trim().equalsIgnoreCase("Packaging Ready Date")
									|| chgTrack.getField().trim().equalsIgnoreCase("Fabric Pp Date")
									|| chgTrack.getField().trim().equalsIgnoreCase("Cpo Acc Date By Vendor")
									|| chgTrack.getField().trim().equalsIgnoreCase("Sample Merch Eta")
									|| chgTrack.getField().trim().equalsIgnoreCase("Sample Floor Set Eta")
									|| chgTrack.getField().trim().equalsIgnoreCase("Sample Dcom Eta")
									|| chgTrack.getField().trim().equalsIgnoreCase("Sample Mailer Eta")
									|| chgTrack.getField().trim().equalsIgnoreCase("TOP sample ETA Date")
									|| chgTrack.getField().trim().equalsIgnoreCase("Photo/Merchant Sample Send date")
									|| chgTrack.getField().trim().equalsIgnoreCase("Photo/Merchant Sample ETA date")
									|| chgTrack.getField().trim().equalsIgnoreCase("Marketing Sample Send date")
									|| chgTrack.getField().trim().equalsIgnoreCase("Marketing Sample ETA date")
									|| chgTrack.getField().trim().equalsIgnoreCase("Visual Sample Send date")
									|| chgTrack.getField().trim().equalsIgnoreCase("Visual Sample ETA date")
									|| chgTrack.getField().trim().equalsIgnoreCase("Copyright Sample Send date")
									|| chgTrack.getField().trim().equalsIgnoreCase("Copyright Sample ETA date")
									|| chgTrack.getField().trim().equalsIgnoreCase("Additional Bulk Lot Approve")) {
								chgTrack.setBefore(PSRHelper.getDateWithCustomFormat((String) object[7]));
								chgTrack.setAfter(PSRHelper.getDateWithCustomFormat((String) object[8]));
							}
						}
						chgTrack = setChangeTrackValues(chgTrack);
						if (map.containsKey(chgTrack.getTimeStampStr())) {
							gridViews = map.get(chgTrack.getTimeStampStr());
							gridViews.add(chgTrack);
							map.put(chgTrack.getTimeStampStr(), gridViews);
						} else {
							gridViews = new LinkedHashSet<>();
							gridViews.add(chgTrack);
							map.put(chgTrack.getTimeStampStr(), gridViews);
						}
					}
				} catch (Exception e) {
					log.error(String.format(" An error has been occured Error --> %s", e.getMessage()));
					throw e;
				} finally {
					em.close();
				}
			}
		}
		log.info(String.format(" No of records extracted is  ----> %d", map.size()));
		return map;
	}

	private ChangeTrack setChangeTrackValues(ChangeTrack chgTrack) throws Exception {
		DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
		String strDate = formatter.format(chgTrack.getTimeStamp());
		String formatedDate = null;
		try {
			Date date = formatter.parse(strDate);
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			formatedDate = (PSRHelper.getMonthName(cal.get(Calendar.MONTH) + 1)) + " " + (cal.get(Calendar.DATE)) + " "
					+ cal.get(Calendar.YEAR) + " " + cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE)
					+ ":" + cal.get(Calendar.SECOND);
			chgTrack.setHrWithMins((cal.get(Calendar.HOUR) < 10 ? "0" + cal.get(Calendar.HOUR) : cal.get(Calendar.HOUR))
					+ ":" + (cal.get(Calendar.MINUTE) < 10 ? "0" + cal.get(Calendar.MINUTE) : cal.get(Calendar.MINUTE))
					+ ":"
					+ (cal.get(Calendar.SECOND) < 10 ? "0" + cal.get(Calendar.SECOND) : cal.get(Calendar.SECOND)));
			chgTrack.setTimeFormate(cal.getDisplayName(Calendar.AM_PM, Calendar.SHORT, Locale.getDefault()));
			chgTrack.setTimeStampKey(formatedDate);
			chgTrack.setTimeStampStr((PSRHelper.getMonthName(cal.get(Calendar.MONTH) + 1)) + " "
					+ (cal.get(Calendar.DATE)) + " " + cal.get(Calendar.YEAR));
		} catch (Exception e) {
			log.error(String.format("Error in dateFormatter method : %s", e.getMessage()));
			throw e;
		}
		return chgTrack;
	}

	public int updateTheColorChangeFlag(List<PostXmlInputParams> postXmlInputParams) throws Exception {
		log.info(" enetered into the method updateTheColorChangeFlag ");
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			String qryForUpdateColorChangeFlagStr = this.getQryForUpdateColorChgFlagStr(postXmlInputParams);
			int i = em.createNativeQuery(qryForUpdateColorChangeFlagStr).executeUpdate();
			em.getTransaction().commit();
			if (i > 0) {
				log.info(" updateTheColorChangeFlag has been updated the highlight revised date flag");
				return i;
			} else {
				log.info(" updateTheColorChangeFlag has not been updated the highlight revised date flag");
				return 0;
			}
		} catch (Exception e) {
			log.error(String.format(" Error while updating highlight revised date flag  %s", e.getMessage()));
			em.getTransaction().rollback();
			throw e;
		} finally {
			em.close();
		}
	}

	private String getQryForUpdateColorChgFlagStr(List<PostXmlInputParams> postXmlInputParams) throws Exception {
		StringBuilder sb = new StringBuilder();
		for (PostXmlInputParams postXmlInputParam : postXmlInputParams) {
			sb.append(" update psr_events set highlight_revised_date = 0 where psr_order_id = '"
					+ postXmlInputParam.getPsrOrderId() + "' and event_code ='E000' ");
		}
		return sb.toString();
	}

	public String getQueryName(int queryId) throws Exception {
		if (queryId != 0) {
			return (String) emf.createEntityManager().createNativeQuery(
					"SELECT CASE WHEN public_view = 1 THEN QUERY_NAME+' (Public)' ELSE QUERY_NAME END AS QueryName  from [dbo].[PSR_USER_CUSTIMOZED_VIEW] where QUERY_ID = '"
							+ queryId + "'")
					.getSingleResult();
		} else {
			return null;
		}
	}

	public Integer getQueryId(String loginUserId, String queryName) throws Exception {
		try {
			return (Integer) emf.createEntityManager()
					.createNativeQuery("SELECT QUERY_ID FROM [DBO].[PSR_USER_CUSTIMOZED_VIEW] WHERE QUERY_NAME = '"
							+ queryName.trim() + "'")
					.getSingleResult();
		} catch (NoResultException e) {
			log.info(String.format("No result Found with this Query Name = %s", queryName));
			return null;
		} catch (Exception e) {
			log.error(String.format("An error occured %s", e.getMessage()));
			throw e;
		}
	}

	public List<Map> getPsrExportAllData(SearchForm searchForm, List<String> eventDescList, String selectedColoumns,  String checkedOrderId)
			throws Exception {
		log.info(" enetered into the method getPsrExportAllData with SearchForm as argument ");
		try {
			EntityManager em = emf.createEntityManager();
			Query q = em.createNativeQuery(QueryGenerator.getSearchQueryStrAllData(searchForm, checkedOrderId));
			List<Object[]> list = q.getResultList();
			return DAOHelper.getExportData(searchForm, list, eventDescList, selectedColoumns);
		} catch (Exception e) {
			log.error(String.format(" Error occured in getPsrExportAllData : %s", e.getMessage()));
			throw e;
		}
	}

	public void deleteAvailableView(String queryId) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			int availableViewDeleteQuery = em
					.createNativeQuery(" DELETE FROM PSR_USER_CUSTIMOZED_VIEW where Query_Id ='" + queryId + "' ")
					.executeUpdate();
			em.getTransaction().commit();
			if (availableViewDeleteQuery > 0) {
				log.info(String.format(" Available view is removed successfully for Query Id %s", queryId));
			} else {
				log.error(String.format(
						" Database operation is failed while removing the available view with Query Id %s", queryId));
			}
		} catch (Exception e) {
			em.getTransaction().rollback();
			log.error(String.format(" Error occured in deleteAvailableView : %s", e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
	}

	public String updateAvailableView(String queryId, String selectedColumns, String checkBoxValue, String columnReorder,
			String loginUserId) {
		log.info(" Enetered into the method updateAvailableView ");
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			int availableViewUpdateQuery = em.createNativeQuery(
					" UPDATE PSR_USER_CUSTIMOZED_VIEW set SELECTED_COLUMNS = '" + selectedColumns + "' , PUBLIC_VIEW ='"
							+ checkBoxValue + "', COLUMNS_ORDER = '" + columnReorder + "', MODIFY_TS='" + new Timestamp(new Date().getTime()) + "',MODIFY_USER ='"
							+ loginUserId + "' WHERE QUERY_ID ='" + queryId + "' ")
					.executeUpdate();
			em.getTransaction().commit();
			if (availableViewUpdateQuery > 0) {
				log.info(String.format("Available view is updated successfully for Query Id %s", queryId));
			} else {
				log.error(String.format(
						" Database operation is failed while updating the available view with Query Id %s", queryId));
			}
			return (String) em.createNativeQuery(
					" SELECT CASE WHEN public_view = 1 THEN QUERY_NAME+' (Public)' ELSE QUERY_NAME END AS QueryName FROM PSR_USER_CUSTIMOZED_VIEW WHERE QUERY_ID ='"
							+ queryId + "' ")
					.getSingleResult();
		} catch (Exception e) {
			em.getTransaction().rollback();
			log.error(String.format(" Error occured in updateAvailableView : %s", e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
	}

	public Map<String, String> getEventCodeDescriptions() {
		Map<String, String> eventCodeMap = new HashMap<>();
		try {
			List<Object[]> eventCodeList = emf.createEntityManager()
					.createNativeQuery(" SELECT distinct [EVENT_CODE],[EVENT_DESC] FROM [PSR_EVENT_MASTER] ")
					.getResultList();
			for (Object[] objects : eventCodeList) {
				String eventCode = (String) objects[0];
				String eventDesc = (String) objects[1];
				eventCodeMap.put(eventCode, eventDesc);
			}
			return eventCodeMap;
		} catch (Exception e) {
			log.error(String.format("An error occured %s", e.getMessage()));
			throw e;
		}
	}

	public List<BrUserProfile> getBrUserProfile(String brUserId) {
		log.info(" entered into the method getBrUserProfile PSR DAO service ");
		List<BrUserProfile> brUserProfileList = new ArrayList<>();
		try {
			List<Object[]> brUserProfile = emf.createEntityManager()
					.createNativeQuery(
							" SELECT DISTINCT * FROM BR_USER_PROFILE where  BR_USER_ID = '" + brUserId + "' ")
					.getResultList();
			for (Object[] object : brUserProfile) {
				brUserProfileList
						.add(new BrUserProfile().setBrUserId((String) object[0]).setBrUserName((String) object[1])
								.setBrRoleId((Integer) object[2]).setBrRoleName((String) object[3])
								.setBrAttrName((String) object[4]).setBrAttrValue((String) object[5]));
			}
		} catch (Exception e) {
			log.error(String.format(" Error occured in getBrUserProfile : %s", e.getMessage()));
		}
		return brUserProfileList;
	}

	public Map<Integer, String> getQueryNames() {
		log.info(" entered into the method getQueryNames Portal DAO service ");
		Map<Integer, String> queryNameWithIds = new LinkedHashMap<>();
		try {
			EntityManager em = emf.createEntityManager();
			List<Object[]> queryStatus = em.createNativeQuery(QueryGenerator.getQueryNamesQuery()).getResultList();
			for (Object[] object : queryStatus) {
				queryNameWithIds.put((Integer) object[0], (String) object[1]);
			}
		} catch (Exception e) {
			log.error(String.format(" Error occured in getQueryNames : %s", e.getMessage()));
		}
		return queryNameWithIds;
	}

	public void updateAcknowledge(String psrOrderIds) {
		log.info(String.format(" entered into the method updateAcknowledge PSR DAO service psrUserIds  %s",
				psrOrderIds));
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			int userProfileUpdateStatus = em.createNativeQuery(QueryGenerator.updateAcknowledge(psrOrderIds))
					.executeUpdate();
			em.getTransaction().commit();
			if (userProfileUpdateStatus > 0) {
				log.info(String.format(" Update Acknowledge is updated successfully for psrUserIds %s", psrOrderIds));
			} else {
				log.error(" Database operation is failed while updating the Update Acknowledge with psrUserIds "
						+ psrOrderIds);
			}
		} catch (Exception e) {
			em.getTransaction().rollback();
			log.error(String.format(" Error occured in updateUserProfile : %s", e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
	}

	public String getSelectedColumnsWRTQueryId(String queryId) {
		EntityManager em = emf.createEntityManager();
		String selectedCols = (String) em.createNativeQuery(
				"select em.Selected_Columns from PSR_USER_CUSTIMOZED_VIEW em where em.Query_Id ='" + queryId + "' ")
				.getSingleResult();
		return (selectedCols != null ? selectedCols : null);
	}

	public void saveExportDataForExcel(FileTrack fileTrack) {
		log.info(" entered into the method saveExportDataForExcel ");
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			Query addQuery = em.createNativeQuery(
					" INSERT INTO PSR_FILE_EXPORT([FILE_NAME],[DESC],[EXPORT_USER_ID],[EXPORT_START_DATE],[EXPORT_END_DATE],"
							+ "[MODIFY_TS]) VALUES ('" + fileTrack.getFileName() + "','" + fileTrack.getDesc() + "','"
							+ fileTrack.getUserId() + "','" + fileTrack.getStartTime() + "','" + fileTrack.getEndTime()
							+ "'," + "getdate()" + ')');
			int addQueryCount = addQuery.executeUpdate();
			em.getTransaction().commit();
			if (addQueryCount > 0) {
				log.info(" saved the saveExportDataForExcel ");
			}
		} catch (Exception e) {
			em.getTransaction().rollback();
			log.error(String.format("Error occured in saveExportDataForExcel ", e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
	}

	public void saveImportDataForExcel(FileTrack fileTrack) {
		log.info(" entered into the method saveImportDataForExcel ");
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			Query addQuery = em.createNativeQuery(
					" INSERT INTO PSR_FILE_IMPORT([FILE_NAME],[DESC],[IMPORT_USER_ID],[IMPORT_START_DATE],[IMPORT_END_DATE],"
							+ "[MODIFY_TS]) VALUES ('" + fileTrack.getFileName() + "','" + fileTrack.getDesc() + "','"
							+ fileTrack.getUserId() + "','" + fileTrack.getStartTime() + "','" + fileTrack.getEndTime()
							+ "'," + "getdate()" + ')');
			int addQueryCount = addQuery.executeUpdate();
			em.getTransaction().commit();
			if (addQueryCount > 0) {
				log.info(" updated the saveImportDataForExcel ");
			}
		} catch (Exception e) {
			em.getTransaction().rollback();
			log.error(String.format("Error occured in saveImportDataForExcel ", e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
	}

	public int checkFileName(String loginUserId, String excelFileName) {
		log.info(" entered into the method checkFileName psrdao " + excelFileName);
		try {
			return (Integer) emf.createEntityManager()
					.createNativeQuery("select count(*) as count from PSR_FILE_TRACK where file_name='"
							+ excelFileName.trim() + "' and login_user_id='" + loginUserId.trim() + "'")
					.getSingleResult();
		} catch (NoResultException e) {
			log.info(String.format("No result Found with this excelFileName = %s", excelFileName));
			return 0;
		} catch (Exception e) {
			log.error(String.format("An error occured %s", e.getMessage()));
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public Map<String, Set<FileTrack>> getFileTrackInfo(String loginUserId, String roleName, String from, String fromDate, String toDate) throws Exception {
		log.info(" entered into the method getFileTrackInfo PSR DAO service ");
		Map<String, Set<FileTrack>> fileTrackInfo = new LinkedHashMap<String, Set<FileTrack>>();
		Set<FileTrack> fileTrackList = null;
		FileTrack fileTrack = null;
		EntityManager em = emf.createEntityManager();
		try {
			
			Integer since = 0;
			String type = "ALL";
			if (from != null) {
				if (from.equalsIgnoreCase("Today")) {
					since = 0;
					type = "";
				}
				if (from.equalsIgnoreCase("Yesterday")) {
					since = -1;
					type = "";
				} else if (from.equalsIgnoreCase("Last Week")) {
					since = -7;
					type = "";
				} else if (from.equalsIgnoreCase("Last Month")) {
					since = -30;
					type = "";
				} else if (from.equalsIgnoreCase("All")) {
					type = "ALL";
				} else if (from.equalsIgnoreCase("Custom")) {
					type = "CUSTOM";
					if (fromDate == null || fromDate.equalsIgnoreCase("") || toDate == null
							|| toDate.equalsIgnoreCase("")) {
						throw new Exception("Dates must be provide in order to search.");
					}
					fromDate = fromDate.trim() + " 00:00:00.000";
					toDate = toDate.trim() + " 23:59:59.999";
				}
			}

			log.info(type);
			log.info(since.toString());
			log.info(fromDate);
			log.info(toDate);
			
			List<Object[]> fileTrackData = em.createNativeQuery(QueryGenerator.getFileTrackQuery(loginUserId, roleName, type, since, fromDate, toDate))
					.getResultList();
			for (Object[] object : fileTrackData) {
				fileTrack = new FileTrack().setFileName((String) object[0]).setDesc((String) object[1])
						.setUserId((String) object[2]).setStartTime((Date) object[3]).setEndTime((Date) object[4])
						.setModifyTs((Date) object[5]);

				if (fileTrack != null && fileTrack.getModifyTs() != null) {
					fileTrack.setFileName(fileTrack.getFileName().substring(0, fileTrack.getFileName().lastIndexOf('_'))
							+ ApplicationConstants.FILE_TYPE);
					fileTrack = this.setFileTrackValues(fileTrack);
					if (!fileTrackInfo.containsKey(fileTrack.getFileName())) {
						fileTrackList = new LinkedHashSet<>();
						fileTrackList.add(fileTrack);
						fileTrackInfo.put(fileTrack.getFileName(), fileTrackList);
					} else {
						fileTrackList = fileTrackInfo.get(fileTrack.getFileName());
						fileTrackList.add(fileTrack);
						fileTrackInfo.put(fileTrack.getFileName(), fileTrackList);
					}
					// fileTrackInfo.put(fileTrack.getFileName(), fileTrackInfo);
				}
			}
		} catch (Exception e) {
			log.error(String.format(" Error occured in getFileTrackList : %s", e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
		return fileTrackInfo;
	}

	private FileTrack setFileTrackValues(FileTrack fileTrack) throws Exception {
		DateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss:SSS");
		try {
			if (fileTrack != null && PSRHelper.checkEmptyOrNull(fileTrack.getUserId())) {
				fileTrack.setStartTimeStr(formatter.format(fileTrack.getStartTime()));
				fileTrack.setEndTimeStr(formatter.format(fileTrack.getEndTime()));
			}
		} catch (Exception e) {
			log.error("an error has been occured in setFileTrackValues method ");
			throw e;
		}
		return fileTrack;
	}

	public Date getESTDateFromDB() {
		return (Date) emf.createEntityManager().createNativeQuery("select getDate()").getSingleResult();
	}

	public String saveSearchForReferance(PsrSaveSearchDto psrSaveSearchDto, String loginUserId) throws Exception {
		log.info(" enetered into the method saveSearchForReferance with PsrSaveSearchDto as argument ");
		PsrSaveSearch entity = new PsrSaveSearch();
		try {
			if (loginUserId != null) {
				entity = PSRHelper.saveSearchResult(psrSaveSearchDto, loginUserId);
				psrSaveSearchRepository.save(entity);
			}
		} catch (Exception e) {
			log.error(String.format(" Error occured in save search : %s", e.getMessage()));
			throw e;
		}
		return entity.getLoginUserId();
	}

	public PsrSaveSearchDto getSavedSearchData(String loginUserId) throws Exception {
		log.info(" enetered into the method getSavedSearchData with loginUserId as argument ");
		PsrSaveSearchDto savedSearchData = new PsrSaveSearchDto();
		PsrSaveSearch result = psrSaveSearchRepository.findByLoginUserId(loginUserId);
		try {
			if (result != null) {
				savedSearchData = PSRHelper.getSavedResult(result);
			}
		} catch (Exception e) {
			log.error(String.format(" Error occured in save search : %s", e.getMessage()));
			throw e;
		}
		return savedSearchData;
	}

	public String updateOrdersByImportExcel(List<PsrUpdateView> updateOrders, String loginUserId, String roleName,
			String[] availableOrder) throws Exception {
		try {
			String psrOrderStr = null;
			for (PsrUpdateView psrUpdateView : updateOrders) {
				List<PsrOrder> orders = psrUpdateView.getOrders();
				List<PsrEventsView> events = psrUpdateView.getEvents();
				Map<Integer, List<PsrEventsView>> eventsWRTOrder = DAOHelper.getEventsWRTOrder(events);
				psrOrderStr = DAOHelper.constructXmlStrExcel(orders, eventsWRTOrder, null, availableOrder);
			}
			log.info(String.format(" Stored Procedure String Starts %s", psrOrderStr));
			return this.callStoredProcedureToSavePsrData(psrOrderStr, loginUserId, roleName,
					ApplicationConstants.getCurrentDateWithOutTimeDelay());
		} catch (Exception e) {
			log.error(" An error has been occured --> " + e.getMessage());
			throw e;
		}
	}

	public void recalcModel(String loginUserId, String modelName) throws Exception {
		if (loginUserId.trim().length() > 0 && modelName.trim().length() > 0) {
			EntityManager em = emf.createEntityManager();
			try {
				String message = (String) em.createStoredProcedureQuery("PSR_RECALC_MODEL")
						.registerStoredProcedureParameter("MODEL_NAME", String.class, ParameterMode.IN)
						.registerStoredProcedureParameter("ModifiedBy", String.class, ParameterMode.IN)
						.registerStoredProcedureParameter("result", String.class, ParameterMode.OUT)
						.setParameter("MODEL_NAME", modelName).setParameter("ModifiedBy", loginUserId)
						.getOutputParameterValue("result");
				log.info(String.format("recalculated Model", message));
				if (PSRHelper.checkEmptyOrNull(message) && message.contains("FAILED DUE TO DEADLOCK.")) {
					throw new Exception("This is failed due to deadlock situation, please try after sometime");
				}
			} catch (Exception e) {
				log.error(String.format(" recalculated Model has not updated %s", e.getMessage()));
				throw e;
			} finally {
				em.close();
			}
		}
	}
	
	public String getReOrderColumnsWRTQueryId(String queryId) {
		EntityManager em = emf.createEntityManager();
		String selectedCols = (String) em.createNativeQuery(
				"select em.COLUMNS_ORDER from PSR_USER_CUSTIMOZED_VIEW em where em.Query_Id ='" + queryId + "' ")
				.getSingleResult();
		return (selectedCols != null ? selectedCols : null);
	}
	
}
