package com.igt.psr.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.igt.psr.common.constants.ApplicationConstants;
import com.igt.psr.common.dao.QueryGenerator;
import com.igt.psr.common.util.DAOHelper;
import com.igt.psr.db.model.PsrEventMaster;
import com.igt.psr.db.model.PsrEventModel;
import com.igt.psr.db.model.PsrMatchCriteria;
import com.igt.psr.db.model.PsrMatchCriteriaPK;
import com.igt.psr.db.model.PsrModelMaster;
import com.igt.psr.db.model.PsrUserCustimozedView;
import com.igt.psr.db.repo.PsrEventMasterRepository;
import com.igt.psr.db.repo.PsrEventModelRepository;
import com.igt.psr.db.repo.PsrMatchCriteriaRepository;
import com.igt.psr.db.repo.PsrModelMasterRepository;
import com.igt.psr.db.repo.PsrUserCustimozedViewRepository;
import com.igt.psr.ui.view.EventModelName;
import com.igt.psr.ui.view.PsrEventForm;

@Service
public class EventDao {
	private static final Logger log = LoggerFactory.getLogger(EventDao.class);

	@Autowired
	private PsrEventMasterRepository eventRepository;
	@Autowired
	private PsrMatchCriteriaRepository matchCriteriaRepository;
	@Autowired
	private PsrUserCustimozedViewRepository custimozedViewRepository;
	@Autowired
	private PsrEventModelRepository psrEventModelRepository;
	@Autowired
	private PsrModelMasterRepository modelMasterRepository;

	@PersistenceUnit(unitName = "psrdb")
	private EntityManagerFactory emf;

	public PsrEventMaster addEventMaster(PsrEventMaster event) {
		return eventRepository.save(event);
	}

	public void addEventModel(PsrEventModel eventModel) {
		psrEventModelRepository.save(eventModel);
	}

	public PsrEventMaster findOne(Long eventCode) {
		return eventRepository.findOne(eventCode);
	}

	public void deleteEventMaster(String eventCode) {
		eventRepository.delete(new PsrEventMaster().setEventCode(eventCode));
	}

	// Note: please dont delete below code may be it will be use ful for future
	// purpose
	/*
	 * public void addSingleEventModelsToCriteria(PsrMatchCriteria matchCriteria) {
	 * EntityManager em = emf.createEntityManager(); em.getTransaction().begin();
	 * Query q = em.createNativeQuery(
	 * "insert into PSR_MATCH_CRITERIA (FIELD_VALUE, MATCH_CASE, MODEL_NAME, MODIFY_USER) values (?, ?, ?,?)"
	 * ); q.setParameter(1, matchCriteria.getFieldValue()); q.setParameter(2,
	 * matchCriteria.getMatchCase()); q.setParameter(3,
	 * matchCriteria.getModelName()); q.setParameter(4,
	 * matchCriteria.getModifyUser()); int i = q.executeUpdate();
	 * em.getTransaction().commit(); em.close(); log.info(" updated count size " +
	 * i); }
	 */

	/**
	 * Jagadish Method to save Match Criteria -- Save in PSR_MODEL_MASTER AND
	 * PSR_MATCH_CRITERIA
	 **/
	public void addSingleEventModelsToCriteria(PsrModelMaster pmm, PsrMatchCriteria matchCriteria) {
		modelMasterRepository.save(pmm);
		int modelId = getModelId(pmm.getModelName());
		Integer rowNo = getMatchCriteriaRowNo();
		matchCriteria.setId(new PsrMatchCriteriaPK().setModelId(modelId).setMcRowNo(rowNo));
		matchCriteriaRepository.save(matchCriteria);
	}

	public int deleteEventModel(String modelName) {
		EntityManager em = emf.createEntityManager();
		int pmmCount = 0;
		try {
			em.getTransaction().begin();
			int modelId = getModelId(modelName);
			Query pmc = em.createNativeQuery(
					"delete mc from psr_match_criteria mc where not exists (select model_name from psr_events where model_name = mc.model_name) and mc.model_id = "
							+ modelId + "");
			int pmcCount = pmc.executeUpdate();
			if (pmcCount > 0) {
				Query pem = em.createNativeQuery("delete from PSR_EVENT_MODEL where MODEL_ID = " + modelId + " ");
				int pemCount = pem.executeUpdate();
			}
			Query pmm = em.createNativeQuery(
					"delete mm from PSR_MODEL_MASTER  mm where not exists (select model_name from psr_events where model_name = mm.model_name) and mm.model_id = "
							+ modelId + "");
			pmmCount = pmm.executeUpdate();
			em.getTransaction().commit();
			if (pmcCount > 0 && pmmCount > 0) {
				log.info(
						" Deleted records in PSR_MODEL_MASTER, PSR_MATCH_CRITERIA  and PSR_EVENT_MODEL Total Count of PSR_MATCH_CRITERIA = "
								+ pmcCount + " and PSR_MODEL_MASTER = " + pmmCount);
			}
		} catch (Exception e) {
			em.getTransaction().rollback();
			log.error(String.format(ApplicationConstants.ERROR_STRING, e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
		return pmmCount;
	}

	// Added viewExists logic to restrict user to create duplicate views
	public int addCustomizedView(PsrUserCustimozedView customizedView) {
		EntityManager em = emf.createEntityManager();
		int viewExists = -1;
		try {
			em.getTransaction().begin();
			viewExists = em.createNativeQuery(
					"INSERT INTO PSR_USER_CUSTIMOZED_VIEW (QUERY_NAME,SELECTED_COLUMNS,LOGIN_USER,MODIFY_TS,MODIFY_USER,CREATE_DATE,PUBLIC_VIEW, COLUMNS_ORDER) VALUES('"
							+ customizedView.getQueryName() + "','" + customizedView.getSelectedColumns() + "','"
							+ customizedView.getLoginUser() + "','" + new Timestamp(new Date().getTime()) + "','"
							+ customizedView.getModifyUser() + "','" + new Timestamp(new Date().getTime()) + "','"
							+ customizedView.getPublicView() + "','"
							+customizedView.getColumnReorder()+ "') ")
					.executeUpdate();
			em.getTransaction().commit();
			log.info(String.format("Modified Rows in PSR_USER_CUSTIMOZED_VIEW table --> count = %s", viewExists));
		} catch (Exception e) {
			em.getTransaction().rollback();
			log.error(String.format(ApplicationConstants.ERROR_STRING, e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
		return viewExists;
	}

	public List<EventModelName> getModelNames(String model, String eventModelSearch) {
		List<Object[]> eventModelNames = emf.createEntityManager()
				.createNativeQuery(QueryGenerator.getModelNamesQuery(model, eventModelSearch)).getResultList();
		return DAOHelper.getModelNames(eventModelNames);

	}

	public List<PsrEventMaster> getEventMaster(String event, String eventcodeselect) {
		List<PsrEventMaster> psrEventForms = new ArrayList<>();
		List<Object[]> eventModelNames = emf.createEntityManager()
				.createNativeQuery(QueryGenerator.getEventCodeQuery(event, eventcodeselect)).getResultList();
		for (Iterator iterator = eventModelNames.iterator(); iterator.hasNext();) {
			Object[] objects = (Object[]) iterator.next();
			psrEventForms.add(new PsrEventMaster().setEventCode((String) objects[0]).setEventDesc((String) objects[1])
					.setEventCategory((String) objects[2]));
		}
		return psrEventForms;
	}

	// Jagadish EVENT MODEL
	public List<PsrEventModel> getModelDetails(String modeldetails) {
		EntityManager em = emf.createEntityManager();
		Query q = em.createNativeQuery(QueryGenerator.getModelNamesdetailsQuery(modeldetails));
		q.setParameter(1, modeldetails);
		List<Object[]> eventModelNamedetails = q.getResultList();
		return DAOHelper.constructPsrEventModel(eventModelNamedetails);
	}

	// Event Match Criteria
	public List<PsrMatchCriteria> getEventMatchCriteria(String modelName) {
		EntityManager em = emf.createEntityManager();
		int modelId = getModelId(modelName);
		Query q = em.createNativeQuery(QueryGenerator.getEventModelNameMatchCriteria(modelId));
		List<Object[]> eventMatchCriteria = q.getResultList();
		return DAOHelper.constructPsrEventMatchCriteria(eventMatchCriteria);
	}

	public List<PsrEventMaster> getEventCodeDetails(String eventCodeDetails) {
		EntityManager em = emf.createEntityManager();
		Query q = em.createNativeQuery(QueryGenerator.getEventCodedetailsQuery(eventCodeDetails));
		q.setParameter(1, eventCodeDetails);
		List<Object[]> eventCodeListDetails = q.getResultList();
		return DAOHelper.constructEventCodeDetails(eventCodeListDetails);
	}

	public int getModelId(String modelName) {
		EntityManager em = emf.createEntityManager();
		return (int) em
				.createNativeQuery(" select model_id from PSR_MODEL_MASTER where MODEL_NAME ='" + modelName + "'")
				.getSingleResult();
	}

	// PsrMatchCriteria psrMatchCriteria
	public Integer getMatchCriteriaRowNo() {
		EntityManager em = emf.createEntityManager();
		Integer mcRowNo = (Integer) em.createNativeQuery(" SELECT MAX(MC_ROW_NO)+1 MC_ROW_NO FROM PSR_MATCH_CRITERIA")
				.getSingleResult();
		return (mcRowNo != null) ? mcRowNo : 0;

	}

	public void addEventModelsToCriteria(List<PsrMatchCriteria> pmcList, PsrModelMaster pmm) throws Exception {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			String bulkInsertStr = QueryGenerator.getAddEventModelsToCriteria(pmcList, pmm);
			Query q = em.createNativeQuery(bulkInsertStr);
			int i = q.executeUpdate();
			em.getTransaction().commit();
			if (i > 0) {
				log.info(String.format(" Added Event Model , Total Count = %s", i));
			}
		} catch (Exception e) {
			em.getTransaction().rollback();
			log.error(String.format("Error : %s", e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
	}

	public void deleteModelFromMatchCriteria(String modelName) throws Exception {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			String deleteMatchCriteriaStr = " delete from PSR_MATCH_CRITERIA  where  model_name = '" + modelName
					+ "' delete from PSR_MODEL_MASTER  where model_name = '" + modelName + "'";
			Query q = em.createNativeQuery(deleteMatchCriteriaStr);
			int i = q.executeUpdate();
			em.getTransaction().commit();
			if (i > 0) {
				log.info(String.format(" Deleted Event Model from matchCriteria , Total Count = %s", i));
			}
		} catch (Exception e) {
			em.getTransaction().rollback();
			log.error(String.format(ApplicationConstants.ERROR_STRING, e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
	}

	public boolean isModelNameAlreadyExists(String modelName) throws Exception {
		EntityManager em = emf.createEntityManager();
		Integer count = (Integer) em.createNativeQuery(
				"select count(*) from PSR_MODEL_MASTER pmm,PSR_MATCH_CRITERIA pmc,PSR_EVENT_MODEL pem  where pmm.model_id=pmc.model_id and pem.model_id=pmc.model_id and pmm.model_name = '"
						+ modelName + "'")
				.getSingleResult();
		return count == 0 ? true : false;
	}

	public Integer isEventCodeAlreadyExists(String eventCode) throws Exception {
		EntityManager em = emf.createEntityManager();
		return (Integer) em
				.createNativeQuery(
						"select Count(EVENT_CODE) from PSR_EVENT_MODEL where EVENT_CODE = '" + eventCode + "' ")
				.getSingleResult();
	}

	public void addPsrEventModel(PsrEventForm psrEvent) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		int eventCodeStatus = em.createNativeQuery("UPDATE PSR_EVENT_MODEL SET DESCRIPTION= '" + psrEvent.getDesc()
				+ "' WHERE EVENT_CODE='" + psrEvent.getEventcode() + "'").executeUpdate();
		em.getTransaction().commit();
		if (eventCodeStatus > 0) {
			log.info(String.format(" Event Code has been updated successfully for Event Code %s",
					psrEvent.getEventcode()));
		} else {
			log.error(
					String.format(" Database operation is failed while updating the Psr Event Model with Event Code %s",
							psrEvent.getEventcode()));
		}
	}
	
	
	public void updateDisableModel(Boolean status, String modelName) {
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		int eventCodeStatus = em.createNativeQuery("UPDATE PSR_MODEL_MASTER SET DISABLE_MODEL= '" + status
				+ "' WHERE MODEL_NAME='" + modelName + "'").executeUpdate();
		em.getTransaction().commit();
		if (eventCodeStatus > 0) {
			log.info(String.format(" Disable Model has been updated successfully for Model Name %s",
					modelName));
		} else {
			log.error(
					String.format(" Database operation is failed while updating the Disable Model for Model Name %s",
							modelName));
		}
	}
	

}
