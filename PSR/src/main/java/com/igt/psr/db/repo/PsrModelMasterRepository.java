package com.igt.psr.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.igt.psr.db.model.PsrModelMaster;

public interface PsrModelMasterRepository extends JpaRepository<PsrModelMaster, Long> {

}
