package com.igt.psr.db.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * The primary key class for the PSR_ORDER database table.
 * 
 */
@Embeddable
@Data
@Accessors(chain = true)
public class PsrOrderPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "VPO", unique = true, nullable = false, length = 35)
	private String vpo;

	@Column(name = "COLOR_WASH_NAME", unique = true, nullable = false, length = 35)
	private String colorWashName;

	@Column(name = "SHIP_TO", unique = true, nullable = false, length = 17)
	private String shipTo;

	@Column(name = "SO_ID", unique = true, nullable = false, precision = 30, scale = 2)
	private BigDecimal soId;

}