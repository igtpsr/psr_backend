package com.igt.psr.db.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "PSR_EVENT_MODEL")
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class PsrEventModel implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PsrEventModelIdentity id;

	@Column(name = "DESCRIPTION", nullable = true)
	private String description;

	@Column(name = "PLAN_DATE", nullable = true)
	private Date planDate;

	@Column(name = "ACTUAL_DATE", nullable = true)
	private Date actualDate;

	@Column(name = "REVISED_DATE", nullable = true)
	private Date revisedDate;

	@Column(name = "TRIGGER_EVENT", nullable = true)
	private String triggerEvent;

	@Column(name = "TRIGGER_DAYS", nullable = true)
	private String triggerDays;

	@Column(name = "MODIFY_TS", nullable = true)
	private Date modifyTs;

	@Column(name = "MODIFY_USER", nullable = true)
	private String modifyUser;

	@Column(name = "CREATE_DATE", nullable = true)
	private Date createDate;
	
	@Column(name = "DISABLE_EVENT", nullable = true)
	private Boolean disableEvent;
}
