package com.igt.psr.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.igt.psr.db.model.PsrOrder;

public interface PsrOrderRepository extends JpaRepository<PsrOrder, Long> {

}
