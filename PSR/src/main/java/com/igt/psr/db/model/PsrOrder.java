package com.igt.psr.db.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * The persistent class for the PSR_ORDER database table.
 * 
 */
@Entity
@Table(name = "PSR_ORDER")
@Data
@Accessors(chain = true)
@NoArgsConstructor
public class PsrOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "PSR_ORDER_ID")
	private Integer psrOrderId;

	@Column(name = "VPO", unique = true, nullable = false, length = 35)
	private String vpo;

	@Column(name = "COLOR_WASH_NAME", unique = true, nullable = false, length = 35)
	private String colorWashName;

	@Column(name = "SHIP_TO", unique = true, nullable = false, length = 17)
	private String shipTo;

	@Column(name = "SO_ID", unique = true, nullable = false, precision = 30, scale = 2)
	private BigDecimal soId;

	@Column(name = "PROD_OFFICE", length = 17)
	private String prodOffice;

	@Column(name = "VENDOR_ID", nullable = false, length = 17)
	private String vendorId;

	@Column(name = "VENDOR_NAME", nullable = false, length = 80)
	private String vendorName;

	@Column(name = "FACTORY_ID", nullable = false, length = 17)
	private String factoryId;

	@Column(name = "FACTORY_NAME", nullable = false, length = 80)
	private String factoryName;

	@Column(name = "COUNTRY_ORIGIN", length = 3)
	private String countryOrigin;

	@Column(name = "BRAND", nullable = false, length = 17)
	private String brand;

	@Column(name = "DEPT", length = 17)
	private String dept;

	@Column(name = "CATEGORY", length = 35)
	private String category;

	@Column(name = "MERCHANT", length = 18)
	private String merchant;

	@Column(name = "STYLE", length = 35)
	private String style;

	@Column(name = "STYLE_DESC", length = 80)
	private String styleDesc;

	@Column(name = "FOB", precision = 10, scale = 2)
	private BigDecimal fob;

	@Column(name = "GROSS_SELL_PRICE", precision = 6, scale = 2)
	private BigDecimal grossSellPrice;

	@Column(name = "MARGIN", precision = 10, scale = 6)
	private BigDecimal margin;

	@Column(name = "PRODUCT_TYPE", length = 17)
	private String productType;

	@Column(name = "PROGRAM_NAME", length = 80)
	private String programName;

	@Column(name = "SHIP_MODE", length = 3)
	private String shipMode;

	@Column(name = "GENDER", length = 17)
	private String gender;

	@Column(name = "ORDER_QTY")
	private BigDecimal orderQty;

	@Column(name = "SHIPPED_QTY")
	private BigDecimal shippedQty;

	@Column(name = "OC_COMMIT", length = 35)
	private String ocCommit;

	@Column(name = "CPO", length = 35)
	private String cpo;

	@Column(name = "SEASON", length = 35)
	private String season;

	@Column(name = "NDC_DATE")
	private Date ndcDate;

	@Column(name = "VPO_NLT_GAC_DATE")
	private Date vpoNltGacDate;

	@Column(name = "VPO_FTY_GAC_DATE")
	private Date vpoFtyGacDate;

	@Column(name = "KEY_ITEM", length = 35)
	private String keyItem;

	@Column(name = "VPO_STATUS", length = 18)
	private String vpoStatus;

	@Column(name = "VPO_LINE_STATUS", length = 1)
	private String vpoLineStatus;

	@Column(name = "INTERFACE_TS")
	private Date interfaceTs;

	@Column(name = "CPO_ORDER_QTY")
	private BigDecimal cpoOrderQty;

	@Column(name = "CPO_SHIP_MODE", length = 35)
	private String cpoShipMode;

	@Column(name = "CPO_COLOR_DESC", length = 50)
	private String cpoColorDesc;

	@Column(name = "CPO_PACK_TYPE", length = 6)
	private String cpoPackType;

	@Column(name = "MODIFY_TS", length = 1)
	private Date modifyTs;

	@Column(name = "MODIFY_USER", length = 20)
	private String modifyUser;

	@Column(name = "PROFOMA_PO", nullable = true)
	private String profomaPo;

	@Column(name = "ACTUAL_POUNDAGE_AVAILABLE", precision = 10, scale = 2)
	private BigDecimal actualPoundageAvailable;

	@Column(name = "APPR_FABRIC_YARN_LOT", length = 20)
	private String apprFabricYarnLot;

	@Column(name = "ASN", length = 35)
	private String asn;

	@Column(name = "BALANCE_SHIP")
	private BigDecimal balanceShip;

	@Column(name = "CARE_LABEL_CODE", length = 10)
	private String careLabelCode;

	@Column(name = "COLOR_CODE_PATRN", length = 20)
	private String colorCodePatrn;

	@Lob
	@Column(name = "[COMMENT]")
	private String comment;

	@Column(name = "CREATE_DATE", length = 1)
	private Date createDate;

	@Column(name = "CUST_FTY_ID", length = 20)
	private String custFtyId;

	@Column(name = "CUT_APPR_LETTER", length = 1)
	private String cutApprLetter;

	@Column(name = "DAILY_OUTPUT_PER_LINE")
	private BigDecimal dailyOutputPerLine;

	@Column(name = "EXTRA_PROCESS")
	private String extraProcess;

	@Column(name = "FAB_TRIM_ORDER", length = 20)
	private String fabTrimOrder;

	@Column(name = "FABRIC_CODE", length = 50)
	private String fabricCode;

	@Column(name = "FABRIC_EST_DATE")
	private Date fabricEstDate;

	private String fabricEstDateStr;

	@Column(name = "FABRIC_MILL", length = 80)
	private String fabricMill;

	@Column(name = "FABRIC_TEST_REPORT_NUMERIC", length = 20)
	private String fabricTestReportNumeric;

	@Column(name = "FACTORY_REF", length = 20)
	private String factoryRef;

	@Column(name = "FIBER_CONTENT", length = 80)
	private String fiberContent;

	@Column(name = "PL_ACTUAL_GAC")
	private Date plActualGac;

	@Column(name = "CPO_GAC_DATE")
	private Date cpoGacDate;

	@Column(name = "GARMENT_TEST_ACTUAL")
	private Date garmentTestActual;

	private String garmentTestActualStr;

	@Column(name = "GAUGE", length = 20)
	private String gauge;

	@Column(name = "GRAPHIC_NAME", length = 50)
	private String graphicName;

	private String imageType;

	@Column(name = "IMAGE", length = 250)
	private String image;

	@Column(name = "KNITTING", precision = 10, scale = 2)
	private BigDecimal knitting;

	@Column(name = "KNITTING_MACHINES")
	private BigDecimal knittingMachines;

	@Column(name = "KNITTING_PCS")
	private BigDecimal knittingPcs;

	@Column(name = "LINKING", precision = 10, scale = 2)
	private BigDecimal linking;

	@Column(name = "LINKING_MACHINES")
	private BigDecimal linkingMachines;

	@Column(name = "LINKING_PCS")
	private BigDecimal linkingPcs;

	@Column(name = "NDC_WK", length = 10)
	private String ndcWk;

	@Column(name = "ORDER_TYPE", length = 20)
	private String orderType;

	@Column(name = "PACK_TYPE", length = 10)
	private String packType;

	@Column(name = "PACKAGING_READY_DATE")
	private Date packagingReadyDate;

	private String packagingReadyDateStr;

	@Column(name = "SALES_AMOUNT", precision = 10, scale = 2)
	private BigDecimal salesAmount;

	@Column(name = "SEWING_LINES")
	private BigDecimal sewingLines;

	@Column(name = "TICKET_COLOR_CODE", length = 20)
	private String ticketColorCode;

	@Column(name = "TICKET_STYLE_NUMERIC", length = 20)
	private String ticketStyleNumeric;

	@Column(name = "VPO_FTY_GAC_DATE_TEMP")
	private Date vpoFtyGacDateTemp;

	@Column(name = "VPO_HEAD_STATUS")
	private Date vpoHeadStatus;

	@Column(name = "PLAN_ID", length = 3)
	private String palnId;

	@Column(name = "INIT_FLOW")
	private String initFlow;

	@Column(name = "VPO_ORDER_TYPE")
	private String vpoOrderType;

	@Column(name = "SPEED_INDICATOR")
	private String speedIndicator;

	@Column(name = "GARMENT_TEST_REPORT_NO")
	private String garmentTestReportNo;

	@Column(name = "PL_SHIP_DATE")
	private Date plShipDate;

	@Column(name = "CPO_STYLE_NO")
	private String cpoStyleNo;

	@Column(name = "REQUEST_NO")
	private String requestNo;

	@Column(name = "LADING_POINT")
	private String ladingPoint;

	@Column(name = "TECH_DESIGNER")
	private String techDesigner;

	@Column(name = "PLM_NO")
	private String plmNo;

	@Column(name = "WASH_YN")
	private String washYn;

	@Column(name = "WASH_FACILITY")
	private String washFacility;

	@Column(name = "FABRIC_SHIP_MODE")
	private String fabricShipMode;

	@Column(name = "TECH_PACK_RECEIVED_DATE")
	private Date techPackReceivedDate;

	private String techPackReceivedDateStr;

	@Column(name = "SAMPLE_MATERIAL_STATUS")
	private String sampleMaterialStatus;

	@Column(name = "RETAIL_PRICE")
	private BigDecimal retailPrice;

	@Column(name = "FABRIC_PP_DATE")
	private Date fabricPpDate;

	private String fabricPpDateStr;

	@Column(name = "DELIVERY_MONTH")
	private String deliveryMonth;

	@Column(name = "CPO_ACC_DATE_BY_VENDOR")
	private Date cpoAccDateByVendor;

	private String cpoAccDateByVendorStr;

	@Column(name = "DIST_CHANNEL")
	private String distChannel;

	@Column(name = "FLOOR_SET")
	private String floorSet;

	@Column(name = "STYLE_AT_RISK")
	private String styleAtRisk;

	@Column(name = "SAMPLE_MERCH_ETA")
	private Date sampleMerchEta;

	private String sampleMerchEtaStr;

	@Column(name = "SAMPLE_FLOOR_SET_ETA")
	private Date sampleFloorSetEta;

	private String sampleFloorSetEtaStr;

	@Column(name = "SAMPLE_DCOM_ETA")
	private Date sampleDcomEta;

	private String sampleDcomEtaStr;

	@Column(name = "SAMPLE_MAILER")
	private String sampleMailer;

	@Column(name = "SAMPLE_MAILER_ETA")
	private Date sampleMailerEta;

	private String sampleMailerEtaStr;
	
	@Column(name = "TOP_SAMPLE_ETA_DATE")
	private Date topSampleEtaDate;
	
	private String topSampleEtaDateStr;
	
	@Column(name = "TOP_SAMPLE_COMMENT")
	private String topSampleComment;
	
	@Column(name = "PHOTO_MERCHANT_SAMPLE_SEND_DATE")
	private Date photoMerchantSampleSendDate;
	
	private String photoMerchantSampleSendDateStr;
	
	@Column(name = "PHOTO_MERCHANT_SAMPLE_ETA_DATE")
	private Date photoMerchantSampleEtaDate;
	
	private String photoMerchantSampleEtaDateStr;
	
	@Column(name = "PHOTO_MERCHANT_SAMPLE_COMMENT")
	private String photoMerchantSampleComment;
	
	@Column(name = "MARKETING_SAMPLE_SEND_DATE")
	private Date marketingSampleSendDate;
	
	private String marketingSampleSendDateStr;
	
	@Column(name = "MARKETING_SAMPLE_ETA_DATE")
	private Date marketingSampleEtaDate;
	
	private String marketingSampleEtaDateStr;
	
	@Column(name = "MARKETING_SAMPLE_COMMENT")
	private String marketingSampleComment;
	
	@Column(name = "VISUAL_SAMPLE_SEND_DATE")
	private Date visualSampleSendDate;
	
	private String visualSampleSendDateStr;
	
	@Column(name = "VISUAL_SAMPLE_ETA_DATE")
	private Date visualSampleEtaDate;
	
	private String visualSampleEtaDateStr;
	
	@Column(name = "VISUAL_SAMPLE_COMMENT")
	private String visualSampleComment;
	
	@Column(name = "COPYRIGHT_SAMPLE_SEND_DATE")
	private Date copyrightSampleSendDate;
	
	private String copyrightSampleSendDateStr;
	
	@Column(name = "COPYRIGHT_SAMPLE_ETA_DATE")
	private Date copyrightSampleEtaDate;
	
	private String copyrightSampleEtaDateStr;
	
	@Column(name = "COPYRIGHT_SAMPLE_COMMENT")
	private String copyrightSampleComment;

	@Column(name = "REASON_FOR_LATE_GAC")
	private String reasonForLateGac;
	
	@Column(name = "CPO_DEPT")
	private String cpoDept;
		
	@Column(name = "ADDITIONAL_BULK_LOT_APPROVE")
	private Date additionalBulkLotApprove;
	
	private String additionalBulkLotApproveStr;
	
	@Column(name = "COLOR_COMMENT")
	private String colorComment;
	
	@Column(name = "FABRIC_COMMENT")
	private String fabricComment;
	
	@Column(name = "FABRIC_TEST_COMMENT")
	private String fabricTestComment;
	
	@Column(name = "GARMENT_TEST_COMMENT")
	private String garmentTestComment;
	
	@Column(name = "TRIM_COMMENT")
	private String trimComment;
	
	@Column(name = "SAMPLE_COMMENT")
	private String sampleComment;
	
	@Column(name = "FIT_SAMPLE_COMMENT")
	private String fitSampleComment;
	
	@Column(name = "PP_SAMPLE_COMMENT")
	private String ppSampleComment;
	
	@Column(name = "GARMENT_TREATEMENT_PCS")
	private BigDecimal garmentTreatmentPcs;
	
	@Column(name = "FINISHED_GARMENT_PCS")
	private BigDecimal finishedGarmentPcs;
	
	@Column(name = "PACKED_UNITS_PCS")
	private BigDecimal packedUnitsPcs;

}