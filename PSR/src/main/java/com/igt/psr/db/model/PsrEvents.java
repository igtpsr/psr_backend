package com.igt.psr.db.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "PSR_EVENTS")
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class PsrEvents implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PsrEventsPK id;

	@Column(name = "EVENT_DESC", nullable = true)
	private String eventDesc;

	@Column(name = "EVENT_CATEGORY", nullable = true)
	private String eventCategory;

	@Column(name = "ACTUAL_DATE", nullable = true)
	private Date actualDate;

	@Column(name = "PLANNED_DATE", nullable = true)
	private Date plannedDate;

	@Column(name = "REVISED_DATE", nullable = true)
	private Date revisedDate;

	@Column(name = "MODEL_NAME", nullable = true)
	private String modelName;

	@Column(name = "MODIFY_TS", nullable = true)
	private Date modifyTs;

	@Column(name = "MODIFY_USER", nullable = true)
	private String modifyUser;

	@Column(name = "CREATE_DATE", nullable = true)
	private Date createDate;

	@Column(name = "HIGHLIGHT_REVISED_DATE", nullable = true)
	private Date highlightRevisedDate;
}
