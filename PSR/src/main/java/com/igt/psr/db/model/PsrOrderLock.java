package com.igt.psr.db.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.Data;
import lombok.experimental.Accessors;

@Entity
@Data
@Accessors(chain = true)
@Table(name = "[PSR_ORDER_LOCK]")
@NamedQuery(name = "PsrOrderLock.findAll", query = "SELECT p FROM PsrOrderLock p")
public class PsrOrderLock implements Serializable {
	private static final long serialVersionUID = 1L;//

	@Id
	@Column(name = "USER_TOKEN_ID")
	private String userTokenId;

	@Column(name = "PSR_ORDER_IDS")
	private String psrOrderIds;

	@Column(name = "SEARCH_TIMESTAMP", nullable = true)
	private Date searchTimeStamp;

	@Column(name = "SAVE_TIMESTAMP", nullable = true)
	private Date saveTmeStamp;

}
