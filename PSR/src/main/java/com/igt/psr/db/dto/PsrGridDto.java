package com.igt.psr.db.dto;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.springframework.jdbc.core.RowMapper;

import lombok.Data;
import lombok.experimental.Accessors;

/**
 * @author Thippeswamy
 *
 */
@Data
@Accessors(chain = true)
public class PsrGridDto implements Serializable, RowMapper<PsrGridDto> {

	private static final long serialVersionUID = 1L;

	private int vendorId;
	private int eventCode;
	private int vpo;
	private String prodOffice;
	private String vendorName;
	private String factoryName;
	private String brand;
	private String dept;
	private String merchant;
	private String styleDesc;
	private String eventDesc;
	private Date actualDate;
	private Date planneDate;
	private Date revisedDate;

	@Override
	public PsrGridDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new PsrGridDto().setVpo(rs.getInt("VPO"));
	}
}
