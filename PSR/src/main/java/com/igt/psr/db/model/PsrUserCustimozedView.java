package com.igt.psr.db.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.Data;
import lombok.experimental.Accessors;

@Entity
@Data
@Accessors(chain = true)
@Table(name = "PSR_USER_CUSTIMOZED_VIEW")
@NamedQuery(name = "PsrUserCustimozedView.findAll", query = "SELECT p FROM PsrUserCustimozedView p")
public class PsrUserCustimozedView implements Serializable {
	private static final long serialVersionUID = 1L;//

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Query_Id")
	private Integer queryId;

	@Column(name = "Query_Name", nullable = true)
	private String queryName;

	@Column(name = "Selected_Columns", nullable = true)
	private String selectedColumns;

	@Column(name = "LOGIN_USER", nullable = true)
	private String loginUser;

	@Column(name = "CREATE_DATE")
	private Date createDate;

	@Column(name = "MODIFY_TS")
	private Date modifyTs;

	@Column(name = "MODIFY_USER", nullable = true)
	private String modifyUser;

	@Column(name = "PUBLIC_VIEW", nullable = true)
	private String publicView;
	
	@Column(name = "COLUMNS_ORDER", nullable = true)
	private String columnReorder;
}
