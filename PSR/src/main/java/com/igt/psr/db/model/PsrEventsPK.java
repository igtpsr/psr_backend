package com.igt.psr.db.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;
import lombok.experimental.Accessors;

@Embeddable
@Data
@Accessors(chain = true)
public class PsrEventsPK implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "PSR_ORDER_ID")
	private int psrOrderId;

	@Column(name = "EVENT_CODE", unique = true, nullable = false)
	private String eventCode;
}
