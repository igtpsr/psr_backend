package com.igt.psr.db.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "PSR_EVENT_MASTER")
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class PsrEventMaster implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "EVENT_CODE")
	private String eventCode;
	@Column(name = "EVENT_DESC", nullable = true)
	private String eventDesc;
	@Column(name = "EVENT_CATEGORY", nullable = true)
	private String eventCategory;
	@Column(name = "MODIFY_TS", nullable = true)
	private Date modifyTs;
	@Column(name = "CREATE_DATE", nullable = true)
	private Date createDate;

}
