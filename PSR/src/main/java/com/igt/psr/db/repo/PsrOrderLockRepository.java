package com.igt.psr.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.igt.psr.db.model.PsrOrderLock;

public interface PsrOrderLockRepository extends JpaRepository<PsrOrderLock, String> {

}
