package com.igt.psr.db.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author Thippeswamy
 *
 */
@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "psrdbEntityManagerFactory", transactionManagerRef = "psrdbTransactionManager", basePackages = {
		"com.igt.psr.db.repo" })
public class PsrDataSource extends CustomDataSource {

	@Primary
	@Bean(name = "psrdbDataSource")
	@ConfigurationProperties(prefix = "psrdb.datasource")
	public DataSource dataSource() {
		return DataSourceBuilder.create().build();
	}

	@Primary
	@Bean(name = "psrdbEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean psrdbEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		return builder.dataSource(dataSource()).packages("com.igt.psr.db.model").persistenceUnit("psrdb").build();
	}

	@Primary
	@Bean(name = "psrdbTransactionManager")
	public PlatformTransactionManager psrdbTransactionManager(
			@Qualifier("psrdbEntityManagerFactory") EntityManagerFactory psrdbEntityManagerFactory) {
		return new JpaTransactionManager(psrdbEntityManagerFactory);
	}

}
