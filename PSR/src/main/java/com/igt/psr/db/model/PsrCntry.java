
package com.igt.psr.db.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "PSR_CNTRY")
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class PsrCntry implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CODE")
	private String code;
	@Column(name = "DESCRIPTION")
	private String description;
	@Column(name = "CURRENCY")
	private String currency;
	@Column(name = "LANG_CODE")
	private String langCode;
	@Column(name = "SPECIAL_PROGRAMS")
	private String specialPrograms;
	@Column(name = "DUTY_COLUMN_IND")
	private String dutyColumnInd;
	@Column(name = "COUNTRY_GROUP")
	private String countryGroup;
	@Column(name = "MEMO1")
	private String memo1;
	@Column(name = "MEMO2")
	private String memo2;
	@Column(name = "MEMO3")
	private String memo3;
	@Column(name = "VAT_PCT")
	private BigDecimal vetPat;
	@Column(name = "MODIFY_USER")
	private String modifyUser;
	@Column(name = "MODIFY_TS")
	private Date modifyTs;
	@Column(name = "LATITUDE1")
	private BigDecimal latitude1;
	@Column(name = "LONGITUDE1")
	private BigDecimal longitude1;

}
