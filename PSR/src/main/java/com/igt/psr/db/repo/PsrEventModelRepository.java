package com.igt.psr.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.igt.psr.db.model.PsrEventModel;

public interface PsrEventModelRepository extends JpaRepository<PsrEventModel, Long> {

}
