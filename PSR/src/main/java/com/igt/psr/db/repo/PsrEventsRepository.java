package com.igt.psr.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.igt.psr.db.model.PsrEvents;

public interface PsrEventsRepository extends JpaRepository<PsrEvents, Long> {

}
