package com.igt.psr.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.igt.psr.db.model.PsrUserCustimozedView;

public interface PsrUserCustimozedViewRepository extends JpaRepository<PsrUserCustimozedView, Long> {

}
