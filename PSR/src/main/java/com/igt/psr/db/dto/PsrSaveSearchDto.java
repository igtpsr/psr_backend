package com.igt.psr.db.dto;

import java.io.Serializable;
import java.sql.Date;

import lombok.Data;
@Data
public class PsrSaveSearchDto implements Serializable{
	/**
	 * @author rgoswami
	 */
	private static final long serialVersionUID = -6415656178210912668L;
	private String vpo;
	private String productionOffice;
	private String vendorId;
	private String ocCommit;
	private String factoryId;
	private String factoryName;
	private String countryOrigin;
	private String brand;
	private String style;
	private String styleDescription;
	private String cpo;
	private String dept;
	private String season;
	private String colorWashName;
	private String ndcdate;
	private String vpoNltGacDate;
	private String vpoFtyGacDate;
	private String ndcWk;
	private String vpoStatus;
	private String vpoLineStatus;
	private String category;
	private String cpoColorDesc;
	private String cpoDept;
	private String initFlow;
	private String vpoSelect;
	private String vendorIdSelect;
	private String ocCommitSelect;
	private String factoryIdSelect;
	private String factoryNameSelect;
	private String brandSelect;
	private String styleSelect;
	private String styleDescSelect;
	private String prodOfficeSelect;
	private String countryOriginSelect;
	private String milestoneselect;
	private String cpoSelect;
	private String deptSelect;
	private String seasonSelect;
	private String colorWashNameSelect;
	private String ndcdateSelect;
	private String vpoNltGacDateSelect;
	private String vpoFtyGacDateSelect;
	private String ndcWkSelect;
	private String vpoStatusSelect;
	private String vpoLineStatusSelect;
	private String categorySelect;
	private String cpoColorDescSelect;
	private String cpoDeptSelect;
	private String initFlowSelect;

}
