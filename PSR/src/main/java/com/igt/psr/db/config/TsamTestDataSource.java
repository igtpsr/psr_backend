package com.igt.psr.db.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "tsamEntityManagerFactory", transactionManagerRef = "tsamTransactionManager", basePackages = {
		"com.igt.psr.tsam.repo" })
public class TsamTestDataSource {

	@Bean(name = "tsamDataSource")
	@ConfigurationProperties(prefix = "tsam.datasource")
	public DataSource dataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "tsamEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean tsamEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		return builder.dataSource(dataSource()).packages("com.igt.psr.tsam.model").persistenceUnit("tsam").build();
	}

	@Bean(name = "tsamTransactionManager")
	public PlatformTransactionManager tsamTransactionManager(
			@Qualifier("tsamEntityManagerFactory") EntityManagerFactory tsamEntityManagerFactory) {
		return new JpaTransactionManager(tsamEntityManagerFactory);
	}

}
