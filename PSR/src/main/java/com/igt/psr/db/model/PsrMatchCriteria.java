package com.igt.psr.db.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "PSR_MATCH_CRITERIA")
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class PsrMatchCriteria implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PsrMatchCriteriaPK id;

	@Column(name = "MODEL_NAME", nullable = true)
	private String modelName;
	@Column(name = "MATCH_CASE", nullable = true)
	private String matchCase;
	@Column(name = "FIELD_VALUE", nullable = true)
	private String fieldValue;
	@Column(name = "MODIFY_USER", nullable = true)
	private String modifyUser;

}
