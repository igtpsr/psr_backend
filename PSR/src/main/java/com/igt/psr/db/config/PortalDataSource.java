package com.igt.psr.db.config;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "psrportalEntityManagerFactory", transactionManagerRef = "psrportalTransactionManager", basePackages = {
		"com.igt.psr.portal.repo" })
public class PortalDataSource {

	@Bean(name = "psrportalDataSource")
	@ConfigurationProperties(prefix = "psrportal.datasource")
	public DataSource dataSource() {
		return DataSourceBuilder.create().build();
	}

	@Bean(name = "psrportalEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean psrportalEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		return builder.dataSource(dataSource()).packages("com.igt.psr.portal.model").persistenceUnit("psrportal")
				.build();
	}

	@Bean(name = "psrportalTransactionManager")
	public PlatformTransactionManager psrportalTransactionManager(
			@Qualifier("psrportalEntityManagerFactory") EntityManagerFactory psrportalEntityManagerFactory) {
		return new JpaTransactionManager(psrportalEntityManagerFactory);
	}

}
