package com.igt.psr.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.igt.psr.db.model.PsrMatchCriteria;

public interface PsrMatchCriteriaRepository extends JpaRepository<PsrMatchCriteria, Long> {

	public void deleteByModelName(String modelName);

}
