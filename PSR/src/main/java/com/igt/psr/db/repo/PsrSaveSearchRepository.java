package com.igt.psr.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import com.igt.psr.db.model.PsrSaveSearch;


public interface PsrSaveSearchRepository extends JpaRepository<PsrSaveSearch, Long>{

	public PsrSaveSearch findByLoginUserId(String loginUserId);

}
