package com.igt.psr.db.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Table(name = "PSR_SAVE_SEARCH")
@Data
public class PsrSaveSearch implements Serializable{
	/**
	 * @author rgoswami
	 */
	private static final long serialVersionUID = 736510651245130342L;
	@Id
	@Column(name = "LOGIN_USER_ID", nullable = false)
	private String loginUserId;
	@Column(name = "VPO")
	private String vpo;
	@Column(name = "PRODUCTION_OFFICE")
	private String productionOffice;
	@Column(name = "VENDOR_ID")
	private String vendorId;
	@Column(name = "OC_COMMIT")
	private String ocCommit;
	@Column(name = "FACTORY_ID")
	private String factoryId;
	@Column(name = "FACTORY_NAME")
	private String factoryName;
	@Column(name = "COUNTRY_ORIGIN")
	private String countryOrigin;
	@Column(name = "BRAND")
	private String brand;
	@Column(name = "STYLE")
	private String style;
	@Column(name = "STYLE_DESCRIPTION")
	private String styleDescription;
	@Column(name = "CPO")
	private String cpo;
	@Column(name = "DEPT")
	private String dept;
	@Column(name = "SEASON")
	private String season;
	@Column(name = "COLOR_WASHNAME")
	private String colorWashName;
	@Column(name = "NDC_DATE")
	private String ndcdate;
	@Column(name = "VPO_NLT_GAC_DATE")
	private String vpoNltGacDate;
	@Column(name = "VPO_FTY_GAC_DATE")
	private String vpoFtyGacDate;
	@Column(name = "NDC_WK")
	private String ndcWk;
	@Column(name = "VPO_STATUS")
	private String vpoStatus;
	@Column(name = "VPO_LINE_STATUS")
	private String vpoLineStatus;
	@Column(name = "CATEGORY")
	private String category;
	@Column(name = "CPO_COLOR_DESC")
	private String cpoColorDesc;
	@Column(name = "CPO_DEPT")
	private String cpoDept;
	@Column(name = "INIT_FLOW")
	private String initFlow;
	@Column(name = "VPO_SELECT")
	private String vpoSelect;
	@Column(name = "VENDOR_ID_SELECT")
	private String vendorIdSelect;
	@Column(name = "OC_COMMIT_SELECT")
	private String ocCommitSelect;
	@Column(name = "FACTORY_ID_SELECT")
	private String factoryIdSelect;
	@Column(name = "FACTORY_NAME_SELECT")
	private String factoryNameSelect;
	@Column(name = "BRAND_SELECT")
	private String brandSelect;
	@Column(name = "STYLE_SELECT")
	private String styleSelect;
	@Column(name = "STYLE_DESC_SELECT")
	private String styleDescSelect;
	@Column(name = "PROD_OFFICE_SELECT")
	private String prodOfficeSelect;
	@Column(name = "COUNTRY_ORIGIN_SELECT")
	private String countryOriginSelect;
	@Column(name = "MILESTONE_SELECT")
	private String milestoneselect;
	@Column(name = "CPO_SELECT")
	private String cpoSelect;
	@Column(name = "DEPT_SELECT")
	private String deptSelect;
	@Column(name = "SEASON_SELECT")
	private String seasonSelect;
	@Column(name = "COLOR_WASH_NAME_SELECT")
	private String colorWashNameSelect;
	@Column(name = "NDCDATE_SELECT")
	private String ndcdateSelect;
	@Column(name = "VPO_NLT_GAC_DATE_SELECT")
	private String vpoNltGacDateSelect;
	@Column(name = "VPO_FTY_GAC_DATE_SELECT")
	private String vpoFtyGacDateSelect;
	@Column(name = "NDC_WK_SELECT")
	private String ndcWkSelect;
	@Column(name = "VPO_STATUS_SELECT")
	private String vpoStatusSelect;
	@Column(name = "VPO_LINE_STATUS_SELECT")
	private String vpoLineStatusSelect;
	@Column(name = "CATEGORY_SELECT")
	private String categorySelect;
	@Column(name = "CPO_COLOR_DESC_SELECT")
	private String cpoColorDescSelect;
	@Column(name = "CPO_DEPT_SELECT")
	private String cpoDeptSelect;
	@Column(name = "INIT_FLOW_SELECT")
	private String initFlowSelect;
	 
}
