package com.igt.psr.db.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;
import lombok.experimental.Accessors;

@Embeddable
@Data
@Accessors(chain = true)
public class PsrEventModelIdentity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Column(name = "MODEL_ID")
	private int modelId;

	@Column(name = "EVENT_CODE", nullable = true)
	private String eventCode;

}
