package com.igt.psr.tsam.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @author Thippeswamy
 *
 */
@Entity
@Table(name = "Vorderd")
@NoArgsConstructor
@Data
@Accessors(chain = true)

public class VOrderD implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "owner")
	private String owner;
}