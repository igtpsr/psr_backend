package com.igt.psr.tsam.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.igt.psr.common.dao.QueryGenerator;
import com.igt.psr.common.util.UIViewHelper;
import com.igt.psr.ui.view.PostXmlInputParams;

@Service
public class TsamDao {

	private static final Logger log = LoggerFactory.getLogger(TsamDao.class);

	@PersistenceUnit(unitName = "tsam")
	private EntityManagerFactory emf;

	public List<String> getPsrHeaders() {
		EntityManager em = emf.createEntityManager();
		Query q = em.createNativeQuery(" select distinct order_no  from vorder_d ");
		List<String> psrEventDescList = q.getResultList();
		log.info(String.format(" Size of PSR Headers Object : %s", psrEventDescList.size()));
		return psrEventDescList;
	}

	public List<Object[]> getBambooroseData() {
		EntityManager em = emf.createEntityManager();
		Query q = em.createNativeQuery(QueryGenerator.getBambooroseDataqry());
		List<Object[]> bambooroseData = q.getResultList();
		log.info(String.format(" Size of PSR Headers Object : %s", bambooroseData.size()));
		return bambooroseData;
	}

	public Map<String, List<String>> getRowNoFromBrose(List<PostXmlInputParams> postXmlInputParams) throws Exception {
		log.info(" enetered into the method  getRowNoFromBrose ");
		EntityManager em = emf.createEntityManager();
		Map<String, List<String>> rowNoMap = new HashMap<>();
		try {
			// Map<String,String> map =
			// PSRHelper.getListToCommaSeparatedStr(postXmlInputParams);
			for (PostXmlInputParams postXmlInputParam : postXmlInputParams) {
				Query q = em.createNativeQuery(
						" select profoma_po ,row_no from vorder_d where deliver_to in('" + postXmlInputParam.getShipTo()
								+ "') " + "and memo8 in('" + postXmlInputParam.getColorWashName()
								+ "') and profoma_po in('" + postXmlInputParam.getProfomaPo() + "') and plan_id in('"
								+ postXmlInputParam.getPlanId() + "')");
				List<Object[]> list = q.getResultList();
				for (Object[] objects : list) {
					if (rowNoMap.containsKey(String.valueOf(objects[0]))) {
						List<String> rowNums = rowNoMap.get(objects[0]);
						rowNums.add(objects[1] + ":" + UIViewHelper.getDate(postXmlInputParam.getRevisedDateStr()));
						rowNoMap.put(String.valueOf(objects[0]), rowNums);
					} else {
						List<String> rowNums = new ArrayList<>();
						rowNums.add(objects[1] + ":" + UIViewHelper.getDate(postXmlInputParam.getRevisedDateStr()));
						rowNoMap.put(String.valueOf(objects[0]), rowNums);
					}
				}
			}
		} catch (Exception e) {
			log.error(String.format(" An error is occured : %s", e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
		log.info(String.format(" rowNoMap %s", rowNoMap.toString()));
		return rowNoMap;
	}
	
	public Map<String, String> getSmartTagRecords(String requestNo,  String style, String colorWashName, String profomaPo) throws Exception {
		
		EntityManager em = emf.createEntityManager();
//		Query q = em.createNativeQuery("SELECT ITEM_NO STYLE_NO,FABRICATION_CATEGORY FABRICATION_YARN "
//				+ "FROM QUOTE QUOTE INNER JOIN QUOTE_EXT QUOTE_EXT ON QUOTE.REQUEST_NO = QUOTE_EXT.REQUEST_NO "
//				+ "AND QUOTE.OWNER = QUOTE_EXT.OWNER AND QUOTE.REQUEST_NO='" + requestNo + "'");
		
		Query q = em.createNativeQuery("SELECT QWE.CUSTOMER_ID AS CUSTOMER, QWE.CUSTOMER_BRAND,QWE.CUSTOMER_DEPT,QUOTE.COMMODITY,QUOTE.BUSINESS AS CATEGORY,QUOTE.CLASS,QUOTE.SUBCLASS,"
				+ "ORDER_D.STYLE_NO AS INTERNAL_STYLE_NUMBER,SIZE_RANGE_H.DESCRIPTION AS SIZE_RANGE,ORDER_D_ANC.MEMO8 AS VPO_COLOR_WAY, FABRICATION_CATEGORY AS FABRICATION_YARN"
				+ " FROM QUOTE_WHS_EXT QWE"
				+ " INNER JOIN QUOTE ON QWE.REQUEST_NO = QUOTE.REQUEST_NO"
				+ " INNER JOIN ORDER_D ORDER_D ON ORDER_D.REQUEST_NO = QUOTE.REQUEST_NO"
				+ " INNER JOIN SIZE_RANGE_H ON SIZE_RANGE_H.SIZE_RANGE = ORDER_D.SIZE_RANGE"
				+ " INNER JOIN ORDER_D_ANC ON ORDER_D_ANC.PROFOMA_PO = ORDER_D.PROFOMA_PO AND ORDER_D_ANC.ROW_NO = ORDER_D.ROW_NO"
				+ " INNER JOIN QUOTE_EXT ON QUOTE.REQUEST_NO = QUOTE_EXT.REQUEST_NO"
				+ " WHERE QUOTE.REQUEST_NO = '" + requestNo + "' AND ORDER_D.PROFOMA_PO = '" + profomaPo + "' AND ORDER_D_ANC.MEMO8 ='" + colorWashName + "'"
				+ " AND ORDER_D.ITEM_NO = '" + style + "'");
		List<Object[]> psrEventList = q.getResultList();
		Map<String, String> psrEvent = new LinkedHashMap<>();
		for (Object[] object : psrEventList) {
			psrEvent.put("Customer", (String) object[0]);
			psrEvent.put("Customer Brand", (String) object[1]);
			psrEvent.put("Customer Dept", (String) object[2]);
			psrEvent.put("Commodity", (String) object[3]);
			psrEvent.put("Category", (String) object[4]);
			psrEvent.put("Class", (String) object[5]);
			psrEvent.put("Subclass", (String) object[6]);
			psrEvent.put("Internal Style Number", (String) object[7]);
			psrEvent.put("Size Range", (String) object[8]);
			psrEvent.put("Vpo Color Way", (String) object[9]);
			psrEvent.put("Fabrication Yarn", (String) object[10]);
			//psrEvent.put((String) object[0], (String) object[1], (String) object[2], (String) object[3]);
		}
		return psrEvent;
	}
	
}
