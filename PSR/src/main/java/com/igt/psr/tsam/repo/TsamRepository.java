package com.igt.psr.tsam.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.igt.psr.tsam.model.VOrderD;

public interface TsamRepository extends JpaRepository<VOrderD, String> {

}
