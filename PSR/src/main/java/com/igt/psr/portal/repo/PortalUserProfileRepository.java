package com.igt.psr.portal.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.igt.psr.portal.model.PtlUserProfile;

public interface PortalUserProfileRepository extends JpaRepository<PtlUserProfile, Long> {

}
