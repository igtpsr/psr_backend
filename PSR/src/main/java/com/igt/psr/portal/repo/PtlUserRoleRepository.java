package com.igt.psr.portal.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.igt.psr.portal.model.PtlUserRole;

public interface PtlUserRoleRepository extends JpaRepository<PtlUserRole, Long> {

}
