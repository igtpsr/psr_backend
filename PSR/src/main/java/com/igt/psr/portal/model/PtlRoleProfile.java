package com.igt.psr.portal.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "PTL_ROLE_PROFILE")
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class PtlRoleProfile implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ROLE_ID")
	private int roleId;
	@Column(name = "ROLE_NAME")
	private String roleName;
	@Column(name = "MODIFY_TS")
	private Date modifyTs;
	@Column(name = "MODIFY_USER")
	private String modifyUser;
	@Column(name = "CREATE_DATE")
	private Date createDate;
	@Column(name = "DESCRIPTION")
	private String description;
	@Column(name = "APP_NAME")
	private String appName;
	@Column(name = "DISABLE_ROLE")
	private String disableRole;
	@Column(name = "MANDATORY_BR_ID")
	private Boolean brUserIdStatus;

}