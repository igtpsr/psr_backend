package com.igt.psr.portal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "PTL_USER_PROFILE")
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class PtlUserProfileTemp implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "USER_ID")
	private String userId;
	@Column(name = "BR_USER_ID")
	private String brUserId;
	@Column(name = "USER_NAME")
	private String userName;
	@Column(name = "PSWD")
	private String pswd;
	@Column(name = "EMAIL")
	private String email;
	@Column(name = "TITLE")
	private String title;
	@Column(name = "ROLE_ID")
	private int roleId;
	@Column(name = "COUNTRY")
	private String country;
	@Column(name = "PSWD_EFFECTIVE_DATE")
	private Date pswEffectiveDate;
	@Column(name = "PSWD_EFFECTIVE_DAYS")
	private int pswdEffectiveDays;
	@Column(name = "PSWD_NOTIFY_DAYS")
	private int pswdNotifyDays;
	@Column(name = "PSWD_EXPIRE_DATE")
	private Date pswdExpireDate;
	@Column(name = "PSWD_NEVER_EXPIRES")
	private String pswdNeverExpires;
	@Column(name = "DISABLE_USER")
	private String disableUser;
	@Column(name = "PSWD_CHANGE_DATE")
	private Date pswdChangeDate;
	@Column(name = "PSWD_CHG_NXT_LOGIN")
	private String pswdChgNxtLogin;
	@Column(name = "PSWD_RESET_DIGEST")
	private String pswdResetDigest;
	@Column(name = "LOGON_ATTEMPTS")
	private int logonAttempts;
	@Column(name = "LAST_LOGIN_TS")
	private Timestamp lastLoginTs;
	@Column(name = "MODIFY_TS")
	private Timestamp modifyTs;
	@Column(name = "MODIFY_USER")
	private String modifyUser;
	@Column(name = "CREATE_DATE")
	private Timestamp createDate;
	@Column(name = "QUERY_ID")
	private Integer queryId;

}