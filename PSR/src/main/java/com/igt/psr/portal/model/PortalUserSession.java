package com.igt.psr.portal.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "PORTAL_USER_SESSION")
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class PortalUserSession implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "LOGIN_USER_ID")
	private String loginUserId;
	@Column(name = "USER_TOKEN_ID", nullable = false)
	private String userTokenId;
	@Column(name = "LAST_INACTIVE_INTERVAL_TIME", nullable = false)
	private Date lastInactiveIntervalTime;
}