package com.igt.psr.portal.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.igt.psr.portal.model.PtlRoleProfile;

public interface PtlRoleProfileRepository extends JpaRepository<PtlRoleProfile, Long> {

}
