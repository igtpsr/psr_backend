package com.igt.psr.portal.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "BR_USER_PROFILE")
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class BrUserProfile implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name = "BR_USER_ID")
	private String brUserId;
	@Column(name = "BR_USER_NAME", nullable = false)
	private String brUserName;
	@Column(name = "BR_ROLE_ID", nullable = false)
	private Integer brRoleId;
	@Column(name = "BR_ROLE_NAME", nullable = false)
	private String brRoleName;
	@Column(name = "BR_ATTR_NAME", nullable = false)
	private String brAttrName;
	@Column(name = "BR_ATTR_VALUE", nullable = false)
	private String brAttrValue;
}