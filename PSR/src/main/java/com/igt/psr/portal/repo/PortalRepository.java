package com.igt.psr.portal.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.igt.psr.portal.model.PortalUserSession;

public interface PortalRepository extends JpaRepository<PortalUserSession, Long> {

}
