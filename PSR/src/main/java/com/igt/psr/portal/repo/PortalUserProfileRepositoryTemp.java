package com.igt.psr.portal.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.igt.psr.portal.model.PtlUserProfileTemp;

public interface PortalUserProfileRepositoryTemp extends JpaRepository<PtlUserProfileTemp, Long> {

}
