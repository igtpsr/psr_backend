package com.igt.psr.portal.model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Entity
@Table(name = "PTL_USER_ROLE")
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class PtlUserRole implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private PtlUserRoleId id;

}