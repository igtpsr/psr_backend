package com.igt.psr.portal.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;
import lombok.experimental.Accessors;

@Embeddable
@Data
@Accessors(chain = true)
public class PtlUserRoleId implements Serializable {

	@Column(name = "ROLE_ID")
	private int roleId;
	@Column(name = "USER_ID")
	private String userId;
}
