package com.igt.psr.portal.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import org.hibernate.exception.JDBCConnectionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.igt.psr.common.constants.ApplicationConstants;
import com.igt.psr.common.dao.QueryGenerator;
import com.igt.psr.common.util.DAOHelper;
import com.igt.psr.portal.model.BrUserProfile;
import com.igt.psr.portal.model.PortalUserSession;
import com.igt.psr.portal.model.PtlRoleProfile;
import com.igt.psr.portal.model.PtlUserProfile;
import com.igt.psr.portal.model.PtlUserProfileTemp;
import com.igt.psr.portal.model.PtlUserRole;
import com.igt.psr.portal.repo.PortalRepository;
import com.igt.psr.portal.repo.PortalUserProfileRepository;
import com.igt.psr.portal.repo.PortalUserProfileRepositoryTemp;
import com.igt.psr.portal.repo.PtlRoleProfileRepository;
import com.igt.psr.portal.repo.PtlUserRoleRepository;
import com.igt.psr.rest.common.view.UserSessionDetails;
import com.igt.psr.ui.view.PtlUserProfileForm;

@Service
public class PortalDao {

	private static final Logger log = LoggerFactory.getLogger(PortalDao.class);

	@Autowired
	PortalRepository portalRepository;

	@Autowired
	PtlRoleProfileRepository ptlRoleProfileRepository;

	@Autowired
	PortalUserProfileRepository portalUserProfileRepository;

	@Autowired
	PortalUserProfileRepositoryTemp portalUserProfileRepositoryTemp;

	@Autowired
	PtlUserRoleRepository ptlUserRoleRepository;

	@PersistenceUnit(unitName = "psrportal")
	private EntityManagerFactory emf;

	public List<BrUserProfile> getUserDetails(String username) throws Exception {
		log.info(String.format(" entered into the method getUserDetails with username %s", username));
		EntityManager em = emf.createEntityManager();
		List<BrUserProfile> userProfileInfo = new ArrayList<>();
		try {
			List<Object[]> userDetails = em
					.createNativeQuery("select * from BR_USER_PROFILE where BR_USER_ID in( '" + username + "')")
					.getResultList();
			for (Object[] objects : userDetails) {
				userProfileInfo
						.add(new BrUserProfile().setBrUserId((String) objects[0]).setBrUserName((String) objects[1])
								.setBrRoleId((Integer) objects[2]).setBrRoleName((String) objects[3])
								.setBrAttrName((String) objects[4]).setBrAttrValue((String) objects[5]));
			}
		} catch (Exception e) {
			log.error(String.format(ApplicationConstants.ERROR_OCCURED, e.getMessage()));
		} finally {
			em.close();
		}
		return userProfileInfo;
	}

	public UserSessionDetails getUserIdFromSession(String authKey, Integer timeout) throws Exception {
		log.debug(" entered into the method getUserIdFromSession ");
		UserSessionDetails userSession = null;
		if (authKey != null && !authKey.trim().isEmpty()) {
			EntityManager em = emf.createEntityManager();
			userSession = new UserSessionDetails();
			try {
				List<Object[]> objects = em.createNativeQuery(QueryGenerator.getSessionDetailsQry(authKey, timeout))
						.getResultList();
				if (objects != null && !objects.isEmpty()) {
					for (Object[] object : objects) {
						userSession = new UserSessionDetails().setLoginUserId((String) object[0])
								.setUserTokenId((String) object[1]).setLastInactiveIntervalTime((Date) object[2])
								.setIsInactive((Integer) object[3]).setInActiveTime((Integer) object[4]);
					}
				}
			} catch (Exception e) {
				if (e.getMessage() != null && e.getMessage().contains("Unable to acquire JDBC Connection"))
					throw new Exception(e);
				log.error(String.format(ApplicationConstants.ERROR_OCCURED, e.getMessage()));
				e.printStackTrace();
			} finally {
				em.close();
			}
		}
		return userSession;
	}

	public void updateInactiveIntervalTime(UserSessionDetails userSession) throws Exception {
		log.debug(" entered into the method updateInactiveIntervalTime ");
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			Query addQuery = em.createNativeQuery(
					" Update [PORTAL_USER_SESSION] set LAST_INACTIVE_INTERVAL_TIME = CURRENT_TIMESTAMP where USER_TOKEN_ID='"
							+ userSession.getUserTokenId() + "' ");
			int addQueryCount = addQuery.executeUpdate();
			em.getTransaction().commit();
			if (addQueryCount > 0) {
				log.info(" updated user session ");
			}
		} catch (Exception e) {
			em.getTransaction().rollback();
			log.error(String.format(ApplicationConstants.ERROR_STRING, e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
	}

	public void deleteUserSession(UserSessionDetails userSessionInfo) throws Exception {
		log.info(" entered into the method deleteUserSession ");
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			Query deleteQuery = em.createNativeQuery(
					" delete from PORTAL_USER_SESSION where USER_TOKEN_ID ='" + userSessionInfo.getUserTokenId() + "'");
			int deleteQueryCount = deleteQuery.executeUpdate();
			em.getTransaction().commit();
			if (deleteQueryCount > 0) {
				log.info(" deleted the user session ");
			}
		} catch (Exception e) {
			em.getTransaction().rollback();
			log.error(String.format(ApplicationConstants.ERROR_STRING, e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
	}

	public Map<String, String> getRolesBasedOnUser(String validUser) throws Exception {
		log.info(" entered into the method getRolesBasedOnUser ");
		if (validUser != null) {
			EntityManager em = emf.createEntityManager();
			Map<String, String> roleMap = new HashMap<>();
			try {
				List<Object[]> objects = em.createNativeQuery(
						"select uProfile.user_id, uRole.ROLE_NAME  from PTL_USER_PROFILE uProfile ,PTL_ROLE_PROFILE uRole "
								+ " where uProfile.ROLE_ID = uRole.ROLE_ID and uProfile.user_id ='" + validUser + "'")
						.getResultList();
				for (Object[] object : objects) {
					roleMap.put((String) object[0], object[1] + "");
				}
			} catch (Exception e) {
				log.error(String.format(ApplicationConstants.ERROR_OCCURED, e.getMessage()));
			} finally {
				em.close();
			}
			return roleMap;
		}
		return null;
	}

	public String isValidLoginUser(String username, String password) throws Exception {
		log.info(" entered into the method isValidLoginUser ");
		String validUser = null;
		EntityManager em = emf.createEntityManager();
		try {
			validUser = (String) em.createNativeQuery(" SELECT USER_ID FROM PTL_USER_PROFILE where USER_ID ='"
					+ username + "' and PSWD = '" + password + "'").getSingleResult();

		} catch (Exception e) {
			log.error(String.format(ApplicationConstants.ERROR_OCCURED, e.getMessage()));
		} finally {
			em.close();
		}
		return validUser;
	}

	public PtlUserProfile getUserDetailsInfo(String username, String password, boolean isLdapPassed) throws Exception {
		log.info(" entered into the method getUserDetailsInfo ");
		PtlUserProfile ptlUserProfile = null;
		EntityManager em = emf.createEntityManager();
		try {
			Object[] object = (Object[]) em
					.createNativeQuery(QueryGenerator.getUserValidationQry(username, password, isLdapPassed))
					.getSingleResult();
			ptlUserProfile = new PtlUserProfile().setUserId((String) object[0]).setBrUserId((String) object[1])
					.setUserName((String) object[2]).setPswd((String) object[3]).setEmail((String) object[4])
					.setTitle((String) object[5]).setRoleId((object[6] != null ? (int) object[6] : 0))
					.setCountry((String) object[7]).setPswEffectiveDate((object[8] != null ? ((Date) object[8]) : null))
					.setPswdEffectiveDays((object[9] != null ? (int) object[9] : 0))
					.setPswdNotifyDays((object[10] != null ? (int) object[10] : 0))
					.setPswdExpireDate((object[11] != null ? ((Date) object[11]) : null))
					.setPswdNeverExpires((String) object[12]).setDisableUser((String) object[13])
					.setPswdChangeDate((object[14] != null ? ((Date) object[14]) : null))
					.setPswdChgNxtLogin((String) object[15]).setPswdResetDigest((String) object[16])
					.setLogonAttempts((object[17] != null ? (int) object[17] : 0))
					.setLastLoginTs((object[18] != null ? (Timestamp) object[18] : null))
					.setModifyTs((object[19] != null ? (Timestamp) object[19] : null))
					.setModifyUser((String) object[20])
					.setCreateDate((object[21] != null ? (Timestamp) object[21] : null))
					.setQueryId((object[22] != null ? (Integer) object[22] : 0));
			// uncomment below code for check encrypted password
			// if (Encryption.checkPassword(password, ptlUserProfile.getPswd())) {
			// return ptlUserProfile;
			// } else {
			// ptlUserProfile = null;
			// throw new Exception("Invalid User or Password for the User Id " + username);
			// }
			if (ptlUserProfile != null) {
				ptlUserProfile.setLastLoginTs(new Timestamp(new Date().getTime()));
				updateLastLoginTimeStamp(ptlUserProfile);
			}
		} catch (Exception e) {
			log.error(String.format(ApplicationConstants.ERROR_OCCURED, e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
		return ptlUserProfile;
	}

	private void updateLastLoginTimeStamp(PtlUserProfile ptlUserProfile) {
		log.info(" entered into the method updateLastLoginTimeStamp ");
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			Query updateLastLoginTS = em
					.createNativeQuery(" UPDATE PTL_USER_PROFILE SET LAST_LOGIN_TS =GETDATE() where user_id = '"
							+ ptlUserProfile.getUserId() + "'");
			int updateLastLoginCount = updateLastLoginTS.executeUpdate();
			em.getTransaction().commit();
			if (updateLastLoginCount > 0) {
				log.info(" Updated the Last Login Time Stamp into the System ");
			}
		} catch (Exception e) {
			em.getTransaction().rollback();
			log.error(String.format(" Error : %s ", e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
	}

	public void saveUserSession(PortalUserSession userSessionInfo) {
		log.info(" entered into the method saveUserSession ");
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			Query addQuery = em.createNativeQuery(
					" INSERT INTO [dbo].[PORTAL_USER_SESSION]([LOGIN_USER_ID],[USER_TOKEN_ID],[LAST_INACTIVE_INTERVAL_TIME]) VALUES ('"
							+ userSessionInfo.getLoginUserId() + "','" + userSessionInfo.getUserTokenId()
							+ "',CURRENT_TIMESTAMP) ");
			int addQueryCount = addQuery.executeUpdate();
			em.getTransaction().commit();
			if (addQueryCount > 0) {
				log.info(" saved the user session ");
			}
		} catch (Exception e) {
			em.getTransaction().rollback();
			log.error(String.format(ApplicationConstants.ERROR_STRING, e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
	}

	public void cleanUserTokens(int cleanUpInterval) {
		log.info(" entered into the method cleanUserTokens ");
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			Query cleanUpQuery = em.createNativeQuery(
					" delete FROM  Portal_user_session where ABS(DATEDIFF(SECOND, last_inactive_interval_time,CURRENT_TIMESTAMP)) > '"
							+ cleanUpInterval + "'");
			int deleteQueryCount = cleanUpQuery.executeUpdate();
			em.getTransaction().commit();
			if (deleteQueryCount > 0) {
				log.info(String.format(" cleaned the user session tokens ---->deleted count %d", deleteQueryCount));
			}
		} catch (Exception e) {
			em.getTransaction().rollback();
			log.error(String.format(ApplicationConstants.ERROR_STRING, e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
	}

	public void updateDefaultView(Integer queryId, Integer existingQueryId, String loginUserId) {
		log.info(" entered into the method updateDefaultView ");
		EntityManager em = emf.createEntityManager();
		String existingQueryStr = "";
		try {
			if (existingQueryId != null) {
				existingQueryStr = " AND QUERY_ID = '" + existingQueryId + "'";
			}
			em.getTransaction().begin();
			Query updateDefaultView = em.createNativeQuery(" UPDATE [PTL_USER_PROFILE] SET QUERY_ID='" + queryId
					+ "' WHERE USER_ID = '" + loginUserId + "' " + existingQueryStr);
			int updateDefaultViewCount = updateDefaultView.executeUpdate();
			em.getTransaction().commit();
			if (updateDefaultViewCount > 0) {
				log.info(String.format(" updated the default view --> %d", updateDefaultViewCount));
			}
		} catch (Exception e) {
			em.getTransaction().rollback();
			log.error(String.format(ApplicationConstants.ERROR_STRING, e.getMessage()));
			throw e;
		} finally {
			em.close();
		}

	}

	public List<PtlUserProfileForm> getUserProfile(String userId, String disable, String searchOperator,
			String roleInput, String roleOperator) {
		log.info(" entered into the method getUserProfile Portal DAO service ");
		List<PtlUserProfileForm> psrUserProfileForms = new ArrayList<>();
		List<Object[]> userProfile = emf.createEntityManager()
				.createNativeQuery(
						QueryGenerator.getUserProfileQuery(userId, disable, searchOperator, roleInput, roleOperator))
				.getResultList();
		for (Object[] objects : userProfile) {
			psrUserProfileForms
					.add(new PtlUserProfileForm().setUserId((String) objects[0]).setRoleName((String) objects[1]));
		}
		return psrUserProfileForms;
	}

	public List<PtlUserProfileForm> getUserProfileDetails(String userId) {
		log.info(" entered into the method getUserProfileDetails Portal DAO service ");
		EntityManager em = emf.createEntityManager();
		Query q = em.createNativeQuery(QueryGenerator.getUserProfiledetailsQuery());
		q.setParameter(1, userId);
		List<Object[]> userProfileListDetails = q.getResultList();
		return DAOHelper.constructUserProfileDetails(userProfileListDetails);
	}

	public void deleteUserProfile(String userId) {
		log.info(" entered into the method deleteUserProfile Portal DAO service ");
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			int userProfileDeleteQuery = em.createNativeQuery(" DELETE FROM PTL_USER_ROLE WHERE USER_ID ='" + userId
					+ "' DELETE FROM PTL_USER_PROFILE WHERE USER_ID ='" + userId + "'  ").executeUpdate();
			em.getTransaction().commit();
			if (userProfileDeleteQuery > 0) {
				log.info(String.format(" User Profile is deleted successfully for User Id %s", userId));
			} else {
				log.error(String.format(" Database operation is failed while deleting the User Profile with User Id %s",
						userId));
			}
		} catch (Exception e) {
			em.getTransaction().rollback();
			log.error(String.format(" Error occured in deleteUserProfile : %s", e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
	}

	public List<PtlRoleProfile> getRoleProfile(String roleName, String disable, String searchOperator) {
		log.info(" entered into the method getRoleProfile Portal DAO service ");
		List<PtlRoleProfile> psrRoleProfileForms = new ArrayList<>();
		try {
			List<Object[]> roleProfile = emf.createEntityManager()
					.createNativeQuery(QueryGenerator.getRoleProfileQuery(roleName, disable, searchOperator))
					.getResultList();
			for (Object[] object : roleProfile) {
				psrRoleProfileForms.add(new PtlRoleProfile().setRoleId((int) object[0]).setRoleName((String) object[1])
						.setDescription((String) object[2]).setAppName((String) object[3]).setBrUserIdStatus((boolean) object[4]));
			}
		} catch (Exception e) {
			log.error(String.format(" Error occured in getRoleProfile : %s", e.getMessage()));
		}
		return psrRoleProfileForms;
	}

	public PtlRoleProfile saveRoleProfile(PtlRoleProfile roleProfile) {
		log.info(" entered into the method saveRoleProfile Portal DAO service ");
		return ptlRoleProfileRepository.save(roleProfile);
	}

	public Map<Integer, String> getRoles() {
		log.info(" entered into the method getRoles Portal DAO service ");
		Map<Integer, String> roleIdAndNames = new LinkedHashMap<>();
		try {
			EntityManager em = emf.createEntityManager();
			List<Object[]> queryStatus = em.createNativeQuery(QueryGenerator.getRoleQuery()).getResultList();
			for (Object[] object : queryStatus) {
				roleIdAndNames.put((Integer) object[0], (String) object[1]);
			}
		} catch (Exception e) {
			log.error(String.format(" Error occured in getRoles : %s", e.getMessage()));
		}
		return roleIdAndNames;
	}

	public int getNewRoleId() {
		log.info(" entered into the method getNewRoleId Portal DAO service ");
		EntityManager em = emf.createEntityManager();
		try {
			return (Integer) em.createNativeQuery(" SELECT MAX(ROLE_ID)+1 from PTL_ROLE_PROFILE ").getSingleResult();
		} catch (Exception e) {
			em.getTransaction().rollback();
			log.error(String.format(" Error occured in updateRoleProfile : %s", e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
	}

	public void updateRoleProfile(PtlRoleProfile roleUpdate, String roleId, String loginUserId) {
		log.info(" entered into the method updateRoleProfile Portal DAO service ");
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			int userProfileDeleteQuery = em
					.createNativeQuery(QueryGenerator.updateRoleQuery(roleUpdate, roleId, loginUserId)).executeUpdate();
			em.getTransaction().commit();
			if (userProfileDeleteQuery > 0) {
				log.info(String.format(" Role Profile is updated successfully for Role Id %s", loginUserId));
			} else {
				log.error(String.format(" Database operation is failed while updating the Role Profile with Role Id %s",
						loginUserId));
			}
		} catch (Exception e) {
			em.getTransaction().rollback();
			log.error(String.format(" Error occured in updateRoleProfile : %s", e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
	}

	public List<PtlRoleProfile> getRoleProfileDetails(String roleId) {
		log.info(" entered into the method getRoleProfile Portal DAO service ");
		List<PtlRoleProfile> psrRoleProfileForms = new ArrayList<>();
		try {
			List<Object[]> roleProfile = emf.createEntityManager().createNativeQuery(
					" SELECT ROLE_ID,ROLE_NAME,DESCRIPTION,APP_NAME,DISABLE_ROLE, MANDATORY_BR_ID from PTL_ROLE_PROFILE where  ROLE_ID = '"
							+ roleId + "' ")
					.getResultList();
			for (Object[] object : roleProfile) {
				psrRoleProfileForms.add(new PtlRoleProfile().setRoleId((int) object[0]).setRoleName((String) object[1])
						.setDescription((String) object[2]).setAppName((String) object[3])
						.setDisableRole((String) object[4]).setBrUserIdStatus((Boolean) object[5]));
			}
		} catch (Exception e) {
			log.error(String.format(" Error occured in getRoleProfile : %s", e.getMessage()));
		}
		return psrRoleProfileForms;
	}

	public void deleteRoleProfile(String roleId) {
		log.info(" entered into the method deleteRoleProfile Portal DAO service ");
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			int roleProfileDeleteQuery = em
					.createNativeQuery(" UPDATE PTL_USER_PROFILE SET ROLE_ID = '0' WHERE ROLE_ID ='" + roleId
							+ "' DELETE FROM PTL_USER_ROLE WHERE ROLE_ID = '" + roleId
							+ "' DELETE FROM PTL_ROLE_PROFILE WHERE ROLE_ID ='" + roleId + "'  ")
					.executeUpdate();
			em.getTransaction().commit();
			if (roleProfileDeleteQuery > 0) {
				log.info(String.format(" Role is deleted successfully for Role Id %s", roleId));
			} else {
				log.error(
						String.format(" Database operation is failed while deleting the Role with Role Id %s", roleId));
			}
		} catch (Exception e) {
			em.getTransaction().rollback();
			log.error(String.format(" Error occured in deleteRoleProfile : %s", e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
	}

	public void resetDefaultView(String queryId) {
		log.info(" entered into the method resetDefaultView ");
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			int updateDefaultViewCount = em
					.createNativeQuery(
							"UPDATE [PTL_USER_PROFILE] set QUERY_ID = null where QUERY_ID ='" + queryId + "'")
					.executeUpdate();
			em.getTransaction().commit();
			if (updateDefaultViewCount > 0) {
				log.info(String.format(" Default view has been updated successfully for Query Id %s", queryId));
			} else {
				log.error(String.format(
						" Database operation is failed while updating the User Profile with Query Id %s", queryId));
			}
		} catch (Exception e) {
			em.getTransaction().rollback();
			log.error(String.format(" Error occured in resetDefaultView : %s", e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
	}

	/**
	 * Date 13/02/2019 Author: Randhir Update user profile
	 * 
	 * @param ptlUserProfileForm
	 * @param UserId
	 * @param loginUserId
	 */
	public void updateUserProfile(PtlUserProfileForm ptlUserProfileForm, String UserId, String loginUserId) {
		log.info(String.format(" entered into the method updateUserProfile Portal DAO service UserId %s", UserId));
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			int userProfileUpdateStatus = em
					.createNativeQuery(QueryGenerator.updateUserQuery(ptlUserProfileForm, UserId, loginUserId))
					.executeUpdate();
			em.getTransaction().commit();
			if (userProfileUpdateStatus > 0) {
				log.info(String.format(" User Profile is updated successfully for UserId %s", UserId));
			} else {
				log.error(String.format(" Database operation is failed while updating the User Profile with UserId %s",
						UserId));
			}
		} catch (Exception e) {
			em.getTransaction().rollback();
			log.error(String.format(" Error occured in updateUserProfile : %s", e.getMessage()));
			throw e;
		} finally {
			em.close();
		}
	}

	/**
	 * Date: 13/02/2019 Author: Randhir save the data using in PtlUserProfileTemp
	 * table
	 * 
	 * @param userProfile
	 * @return Object of PtlUserProfileTemp
	 */
	public PtlUserProfileTemp saveUserProfile(PtlUserProfileTemp userProfile) {
		log.info(" entered into the method saveUserProfile Portal DAO service ");
		// uncomment below code for save encrypted password
		// userProfile.setPswd(Encryption.getEncryptedPassword(userProfile.getPswd()));
		return portalUserProfileRepositoryTemp.save(userProfile);
	}

	/**
	 * Date: 13/02/2019 Author: Randhir Save the data in PtlUserRole table
	 * 
	 * @param userRole
	 * @return Object of PtlUserRole
	 */
	public PtlUserRole saveUserRole(PtlUserRole userRole) {
		log.info(" entered into the method saveUserRole Portal DAO service ");
		return ptlUserRoleRepository.save(userRole);
	}

	/**
	 * Date: 13/02/2019 Author: Randhir To check User Id is already present or not
	 * 
	 * @param userId
	 * @return value of count
	 * @throws Exception
	 */
	public Integer isUserIdAlreadyExists(String userId) throws Exception {
		EntityManager em = emf.createEntityManager();
		Integer count = (Integer) em
				.createNativeQuery("select Count(USER_ID) from PTL_USER_PROFILE where USER_ID ='" + userId + "' ")
				.getSingleResult();
		return count;
	}

	public Integer isRoleNameAlreadyExists(String roleName) throws Exception {
		EntityManager em = emf.createEntityManager();
		Integer count = (Integer) em
				.createNativeQuery("select Count(ROLE_NAME) from PTL_ROLE_PROFILE where ROLE_NAME ='" + roleName + "' ")
				.getSingleResult();
		return count;
	}

	public Map<String, String> getCountryNames() {
		log.info(" entered into the method getCountryNames Portal DAO service ");
		Map<String, String> allCntryNames = new LinkedHashMap<>();
		try {
			EntityManager em = emf.createEntityManager();
			List<Object[]> cntryStatus = em.createNativeQuery(QueryGenerator.getCntryQuery()).getResultList();
			for (Object[] object : cntryStatus) {
				allCntryNames.put((String) object[0], (String) object[1]);
			}
		} catch (Exception e) {
			log.error(String.format(" Error occured in getQueryNames : %s", e.getMessage()));
		}
		return allCntryNames;
	}
	
	public Boolean isTokenIdExists(String authentication) {
		log.info(" getTokenId Psrdao... ");
		int result = 0;
		try {
			EntityManager em = emf.createEntityManager();
			result = (int) em
					.createNativeQuery(
							"select count(*) from portal_user_session where user_token_id='" + authentication + "'")
					.getSingleResult();
		} catch (Exception e) {
			log.info("Exception occured in getTokenId; " + e.getMessage());
		}
		return (result > 0 ? true : false);
	}

}
