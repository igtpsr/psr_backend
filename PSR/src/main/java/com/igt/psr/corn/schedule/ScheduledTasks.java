package com.igt.psr.corn.schedule;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.igt.psr.common.constants.ApplicationConstants;
import com.igt.psr.rest.service.PSRPortalService;

@Component
public class ScheduledTasks {

	private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

	@Value("${login.token.cleanup.time}")
	public int cleanUpInterval;

	@Autowired
	private PSRPortalService portalService;

	public static int noOfTimesSchedulerExected;

	@Scheduled(fixedRate = ApplicationConstants.SCHEDULE_TIME)
	public void schedularToCleanTheTokens() throws JsonProcessingException {
		log.info(" Enter into the schedularToCleanTheTokens method "+noOfTimesSchedulerExected++);
		try {
			portalService.cleanUserTokens(cleanUpInterval);
		} catch (Exception e) {
			log.error(" an error has been occured in schedularToCleanTheTokens "+e.getMessage());
		}
	}
}
