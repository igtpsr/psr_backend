package com.igt.psr.rest.service;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.igt.psr.db.dto.PsrSaveSearchDto;
import com.igt.psr.db.model.PsrOrder;
import com.igt.psr.ui.view.ChangeTrack;
import com.igt.psr.ui.view.FileTrack;
import com.igt.psr.ui.view.PostXmlInputParams;
import com.igt.psr.ui.view.PsrOrderView;
import com.igt.psr.ui.view.PsrUpdateView;
import com.igt.psr.ui.view.SearchForm;

public interface PSRService {

	public List<PsrOrder> getTestData();

	public List<PsrOrderView> getAllPsrData();

	public PsrOrder save(PsrOrder psr) throws Exception;

	public List<Map> getSearchInfo(SearchForm searchForm) throws Exception;

	public List getCustomisedViews(String loginUserId) throws Exception;

	public List<Map> getPsrDynamicGridData(SearchForm searchForm) throws Exception;

	public List<String> getEventDescriptions(String brUserId, String loginUserId, String roleName) throws Exception;

	public Map<String, String> getEventMileStones(String brUserId, String loginUserId, String roleName)
			throws Exception;

	public List<String> getSelectedColumnView(String selectedColumn, String loginUserId) throws Exception;
	
	public List<String> getReOrderColumn(String selectedColumn, String loginUserId) throws Exception;

	public List<PsrOrder> doSearch() throws Exception;

	public String updateOrders(List<PsrUpdateView> updateOrders, String imageUrl, String loginUserId, String roleName,
			String searchTs) throws Exception;

	public Integer getPsrDynamicGridDataCount(SearchForm searchForm) throws Exception;

	public void doReCalculate(String loginUserId, String commaSepratedOrderIds) throws Exception;

	public String deleteImage(String psrOrderId, String loginUserId, String roleName, String searchTs) throws Exception;

	public Map<String, LinkedHashSet<ChangeTrack>> getChangeTrackDetails(String psrOrderIds, String from,
			String searchKeyword, String fromDate, String toDate) throws Exception;

	public int updateTheColorChangeFlag(List<PostXmlInputParams> postXmlInputParams) throws Exception;

	public List<Map> getPsrExportAllData(SearchForm searchForm, String queryId, String milstoneList, String checkedOrderId) throws Exception;

	public void deleteAvailableView(String queryId) throws Exception;

	public String updateAvailableView(String queryId, String selectedColumns, String checkBoxValue,String columnReorder, String loginUserId)
			throws Exception;

	public void updateAcknowledge(String psrOrderIds) throws Exception;

	public String saveExcelInfo(String xmlStr, String loginUserId, String roleName, String currentDateWithOutTimeDelay,
			String fileName) throws Exception;

	public void saveExportDataForExcel(FileTrack fileTrack);

	public void saveImportDataForExcel(FileTrack fileTrack);

	public int checkFileName(String loginUserId, String excelFileName);

	public Map<String, Set<FileTrack>> getFileTrackInfo(String loginUserId, String roleName, String from, String fromDate, String toDate) throws Exception;

	public Date getESTDateFromDB();

	public String saveSearch(PsrSaveSearchDto psrSaveSearchDto, String loginUserId) throws Exception;

	public PsrSaveSearchDto getSavedSearchById(String loginUserId) throws Exception;
	
	public String updateOrdersByImportExcel(List<PsrUpdateView> updateOrders, String loginUserId, String roleName, String[] availableOrder) throws Exception;
	
	public void recalcModel(String loginUserId,String modelName) throws Exception;

}
