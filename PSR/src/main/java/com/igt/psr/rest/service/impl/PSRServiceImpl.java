package com.igt.psr.rest.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.igt.psr.common.util.DAOHelper;
import com.igt.psr.common.util.PSRHelper;
import com.igt.psr.dao.PsrDao;
import com.igt.psr.db.dto.PsrSaveSearchDto;
import com.igt.psr.db.model.PsrOrder;
import com.igt.psr.db.model.PsrOrderPK;
import com.igt.psr.portal.dao.PortalDao;
import com.igt.psr.rest.service.PSRService;
import com.igt.psr.ui.view.ChangeTrack;
import com.igt.psr.ui.view.FileTrack;
import com.igt.psr.ui.view.PostXmlInputParams;
import com.igt.psr.ui.view.PsrOrderView;
import com.igt.psr.ui.view.PsrUpdateView;
import com.igt.psr.ui.view.SearchForm;

@Service
public class PSRServiceImpl implements PSRService {

	private static final Logger log = LoggerFactory.getLogger(PSRServiceImpl.class);

	@Autowired
	private PsrDao psrDao;
	@Autowired
	private PortalDao portalDao;

	@Override
	public List<PsrOrder> getTestData() {
		log.info("enter into the method getAll ");
		return psrDao.findAll();
	}

	@Override
	public List getCustomisedViews(String logInUserId) throws Exception {
		log.info("enter into the method getAll ");
		return psrDao.getCustomisedViewNames(logInUserId);
	}

	@Override
	public List<PsrOrderView> getAllPsrData() {
		log.info("enter into the method getAll psr service ");
		List<PsrOrder> list = psrDao.findAll();
		List<PsrOrderView> orderViewList = new ArrayList<>();

		List<PsrOrderView> eventNameList = psrDao.getMatchingCriteriaModelNames();

		PsrOrderView orderView = null;
		if (null != list && !list.isEmpty()) {
			for (PsrOrder psrModel : list) {
				orderView = DAOHelper.getPsrOrderView(psrModel, eventNameList);
				orderViewList.add(orderView);
			}
		}
		return orderViewList;
	}

	@Override
	public List<PsrOrder> doSearch() {
		log.info("enter into the method doSearch ");
		return psrDao.findAll();

	}

	@Override
	public PsrOrder save(PsrOrder psr) {
		log.info(" entered into method save ");
		return psrDao.save(psr);
	}

	@Override
	public List<Map> getSearchInfo(SearchForm searchForm) throws JsonProcessingException {
		log.info(" entered into method search ");
		return psrDao.getSearchInfo(searchForm);
	}

	public ResponseEntity<PsrOrder> updatePsrData(Long vpo, PsrOrder viewobject) {
		log.info(" entered into method updatePsrData ");
		PsrOrder psr = psrDao.findOne(new PsrOrderPK().setVpo(vpo + ""));
		if (psr == null) {
			return ResponseEntity.notFound().build();
		}
		psr = DAOHelper.getPsrModelObject(viewobject);
		PsrOrder updatepsr = psrDao.save(psr);
		return ResponseEntity.ok().body(updatepsr);
	}

	@Override
	public List<Map> getPsrDynamicGridData(SearchForm searchForm) throws Exception {
		log.info(" entered into method getPsrDynamicGridData ");
		return psrDao.getPsrDynamicGridData(searchForm);
	}

	@Override
	public List<String> getEventDescriptions(String bruserId, String loginUserId, String roleName) throws Exception {
		return psrDao.getEventDescriptions(bruserId, loginUserId, roleName);
	}

	@Override
	public Map<String, String> getEventMileStones(String brUserId, String loginUserId, String roleName)
			throws Exception {
		return psrDao.getEventMileStones(brUserId, loginUserId, roleName);
	}

	@Override
	public List<String> getSelectedColumnView(String queryName, String loginUserId) throws Exception {
		return psrDao.getSelectedColumnView(queryName, loginUserId);
	}
	
	
	
	@Override
	public List<String> getReOrderColumn(String queryName, String loginUserId) throws Exception {
		return psrDao.getReOrderColumn(queryName, loginUserId);
	}

	@Override
	public String updateOrders(List<PsrUpdateView> updateOrders, String imageUrl, String loginUserId, String roleName,
			String searchTs) throws Exception {
		return psrDao.updateOrders(updateOrders, imageUrl, loginUserId, roleName, searchTs);
	}

	@Override
	public Integer getPsrDynamicGridDataCount(SearchForm searchForm) throws Exception {
		return psrDao.getPsrDataTotalCount(searchForm);
	}

	@Override
	public void doReCalculate(String loginUserId, String commaSepratedOrderIds) throws Exception {
		psrDao.doReCalculate(loginUserId, commaSepratedOrderIds);
	}

	@Override
	public String deleteImage(String psrOrderId, String loginUserId, String roleName, String searchTs)
			throws Exception {
		return psrDao.deleteImage(psrOrderId, loginUserId, roleName, searchTs);
	}

	@Override
	public Map<String, LinkedHashSet<ChangeTrack>> getChangeTrackDetails(String psrOrderIds, String from,
			String searchKeyword, String fromDate, String toDate) throws Exception {
		return psrDao.getChangeTrackDetails(psrOrderIds, from, searchKeyword, fromDate, toDate);
	}

	@Override
	public int updateTheColorChangeFlag(List<PostXmlInputParams> postXmlInputParams) throws Exception {
		return psrDao.updateTheColorChangeFlag(postXmlInputParams);

	}

	@Override
	public List<Map> getPsrExportAllData(SearchForm searchForm, String queryId, String milstoneList, String checkedOrderId) throws Exception {
		log.info(" entered into method getPsrExportAllData ");
		try {
			String selectedColumns = "";
			if (PSRHelper.checkNotEmpty(queryId)) {
				selectedColumns = psrDao.getSelectedColumnsWRTQueryId(queryId);
			}
			if (PSRHelper.checkNotEmpty(selectedColumns)) {
				selectedColumns = selectedColumns.trim().replaceAll("#", "").replaceAll(" ", "").toLowerCase();
			}
			//List<String> eventDescList = psrDao.getEventDescriptions(searchForm.getBrUserId(),
					//searchForm.getLoginUserId(), searchForm.getRoleName());
			List<String> eventDescList = Arrays.asList(milstoneList.split(","));
			return psrDao.getPsrExportAllData(searchForm, eventDescList, selectedColumns, checkedOrderId);
		} catch (Exception e) {
			log.error(String.format(" Error occured in getPsrExportAllData : %s", e.getMessage()));
			throw e;
		}
	}

	@Override
	public void deleteAvailableView(String queryId) throws Exception {
		portalDao.resetDefaultView(queryId);
		psrDao.deleteAvailableView(queryId);
	}

	@Override
	public String updateAvailableView(String queryId, String selectedColumns, String checkBoxValue, String columnReorder, String loginUserId)
			throws Exception {
		return psrDao.updateAvailableView(queryId, selectedColumns, checkBoxValue,columnReorder, loginUserId);
	}

	@Override
	public void updateAcknowledge(String psrOrderIds) throws Exception {
		psrDao.updateAcknowledge(psrOrderIds);
	}

	@Override
	public String saveExcelInfo(String xmlStr, String loginUserId, String roleName, String currentDateWithOutTimeDelay,
			String fileName) throws Exception {
		String message = psrDao.callStoredProcedureToSavePsrData(xmlStr, loginUserId, roleName,
				currentDateWithOutTimeDelay);
		return message;
	}

	@Override
	public void saveExportDataForExcel(FileTrack fileTrack) {
		psrDao.saveExportDataForExcel(fileTrack);
	}

	@Override
	public void saveImportDataForExcel(FileTrack fileTrack) {
		psrDao.saveImportDataForExcel(fileTrack);
	}

	@Override
	public Map<String, Set<FileTrack>> getFileTrackInfo(String loginUserId, String roleName, String from, String fromDate, String toDate) throws Exception {
		return psrDao.getFileTrackInfo(loginUserId, roleName, from, fromDate, toDate);
	}

	@Override
	public int checkFileName(String loginUserId, String excelFileName) {
		return psrDao.checkFileName(loginUserId, excelFileName);
	}

	@Override
	public Date getESTDateFromDB() {
		return psrDao.getESTDateFromDB();
	}

	@Override
	public String saveSearch(PsrSaveSearchDto psrSaveSearchDto, String loginUserId) throws Exception {
		return psrDao.saveSearchForReferance(psrSaveSearchDto, loginUserId);
	}

	@Override
	public PsrSaveSearchDto getSavedSearchById(String loginUserId) throws Exception {
		return psrDao.getSavedSearchData(loginUserId);
	}
	
	@Override
	public String updateOrdersByImportExcel(List<PsrUpdateView> updateOrders, String loginUserId, String roleName, String[] availableOrder) throws Exception {
		return psrDao.updateOrdersByImportExcel(updateOrders, loginUserId, roleName,availableOrder);
	}
	
	@Override
	public void recalcModel(String loginUserId,String modelName) throws Exception {
		psrDao.recalcModel(loginUserId, modelName);
	}

}
