package com.igt.psr.rest.service;

import java.util.List;
import java.util.Map;

import com.igt.psr.ui.view.PostXmlInputParams;

public interface TsamService {

	public Map<String, List<String>> getRowNoFromBrose(List<PostXmlInputParams> postXmlInputParams) throws Exception;
	
	public Map<String, String> getSmartTagRecords(String requestNo,  String style, String colorWashName, String profomaPo) throws Exception;


}
