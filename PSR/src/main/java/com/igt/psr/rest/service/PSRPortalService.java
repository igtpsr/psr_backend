package com.igt.psr.rest.service;

import java.util.List;
import java.util.Map;

import com.igt.psr.portal.model.BrUserProfile;
import com.igt.psr.portal.model.PortalUserSession;
import com.igt.psr.portal.model.PtlRoleProfile;
import com.igt.psr.portal.model.PtlUserProfile;
import com.igt.psr.rest.common.view.UserSessionDetails;
import com.igt.psr.ui.view.BrUserProfileForm;
import com.igt.psr.ui.view.PtlRoleProfileForm;
import com.igt.psr.ui.view.PtlUserProfileForm;
import com.igt.psr.ui.view.UserResponse;

public interface PSRPortalService {

	public List<BrUserProfile> getUserDetails(String username) throws Exception;

	public UserSessionDetails getUserIdFromSession(String authKey,Integer timeout) throws Exception;

	public void updateInactiveIntervalTime(UserSessionDetails userSession) throws Exception;

	public void deleteUserSession(UserSessionDetails userSessionInfo) throws Exception;

	public Map<String, String> getRolesBasedOnUser(String validUser) throws Exception;

	public String isValidLoginUser(String username, String password) throws Exception;

	public void saveSession(PortalUserSession userSessionInfo) throws Exception;

	public PtlUserProfile getUserDetailsInfo(String username, String password, boolean isLdapPassed) throws Exception;

	public void updateDefaultView(String loginUserId, String queryName, String existingQryNm) throws Exception;

	public List<PtlUserProfileForm> getUserProfile(String userId, String disable, String roleInput,
			String searchOperator, String roleOperator) throws Exception;

	public UserResponse getUserProfileDetails(String userId) throws Exception;

	public void deleteUserProfile(String userId) throws Exception;

	public List<PtlRoleProfileForm> getRoleProfile(String roleName, String disable, String searchOperator)
			throws Exception;

	public void saveRoleProfile(PtlRoleProfileForm roleProfile, String loginUserId) throws Exception;

	public void updateRoleProfile(PtlRoleProfile roleUpdate, String roleId, String loginUserId) throws Exception;

	public List<PtlRoleProfileForm> getRoleProfileDetails(String roleId) throws Exception;

	public void deleteRoleProfile(String roleId) throws Exception;

	public void updateUserProfile(PtlUserProfileForm userUpdate, String UserId, String loginUserId) throws Exception;

	public void saveUserProfile(PtlUserProfileForm userProfile, String loginUserId) throws Exception;

	public List<BrUserProfileForm> getBrUserProfile(String brUserId) throws Exception;
	
	public Boolean isTokenIdExists(String authentication) throws Exception;

	public void cleanUserTokens(int cleanUpInterval) throws Exception;

}
