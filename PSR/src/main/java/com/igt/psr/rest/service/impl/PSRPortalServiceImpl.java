package com.igt.psr.rest.service.impl;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.igt.psr.common.util.DAOHelper;
import com.igt.psr.common.util.PSRHelper;
import com.igt.psr.common.util.UIViewHelper;
import com.igt.psr.dao.PsrDao;
import com.igt.psr.portal.dao.PortalDao;
import com.igt.psr.portal.model.BrUserProfile;
import com.igt.psr.portal.model.PortalUserSession;
import com.igt.psr.portal.model.PtlRoleProfile;
import com.igt.psr.portal.model.PtlUserProfile;
import com.igt.psr.rest.common.view.UserSessionDetails;
import com.igt.psr.rest.service.PSRPortalService;
import com.igt.psr.ui.view.BrUserProfileForm;
import com.igt.psr.ui.view.PtlRoleProfileForm;
import com.igt.psr.ui.view.PtlUserProfileForm;
import com.igt.psr.ui.view.UserResponse;

@Service
public class PSRPortalServiceImpl implements PSRPortalService {

	private static final Logger log = LoggerFactory.getLogger(PSRPortalServiceImpl.class);

	@Autowired
	PortalDao portalDao;
	@Autowired
	PsrDao psrDao;
	@Autowired
	PSRPortalService psrPortalService;

	@Override
	public List<BrUserProfile> getUserDetails(String username) throws Exception {
		return portalDao.getUserDetails(username);
	}

	@Override
	public UserSessionDetails getUserIdFromSession(String authKey,Integer timeout) throws Exception {
		return portalDao.getUserIdFromSession(authKey,timeout);
	}

	@Override
	public void updateInactiveIntervalTime(UserSessionDetails usersession) throws Exception {
		portalDao.updateInactiveIntervalTime(usersession);
	}

	@Override
	public void deleteUserSession(UserSessionDetails userSessionInfo) throws Exception {
		portalDao.deleteUserSession(userSessionInfo);
	}

	@Override
	public Map<String, String> getRolesBasedOnUser(String validUser) throws Exception {
		return portalDao.getRolesBasedOnUser(validUser);
	}

	@Override
	public String isValidLoginUser(String username, String password) throws Exception {
		return portalDao.isValidLoginUser(username, password);
	}

	@Override
	public void saveSession(PortalUserSession userSessionInfo) throws Exception {
		portalDao.saveUserSession(userSessionInfo);
	}

	@Override
	public PtlUserProfile getUserDetailsInfo(String username, String password, boolean isLdapPassed) throws Exception {
		PtlUserProfile ptlUserProfile = portalDao.getUserDetailsInfo(username, password, isLdapPassed);
		return ptlUserProfile.setQueryName(
				ptlUserProfile != null ? (ptlUserProfile.getQueryId() != null || ptlUserProfile.getQueryId() == 0)
						? psrDao.getQueryName(ptlUserProfile.getQueryId())
						: null : null);
	}

	@Override
	public void updateDefaultView(String loginUserId, String queryName, String existingQryNm) throws Exception {
		Integer queryId = null;
		Integer existingQueryId = null;
		if (queryName != null && !queryName.trim().equalsIgnoreCase("null") && !queryName.isEmpty()) {
			if (queryName.contains(" (Public)")) {
				queryName = queryName.substring(0, queryName.lastIndexOf(" (Public)")).trim();
			}
			queryId = psrDao.getQueryId(loginUserId, queryName);
		}
		if (existingQryNm != null && !existingQryNm.trim().equalsIgnoreCase("null") && !existingQryNm.isEmpty()) {
			if (existingQryNm.contains(" (Public)")) {
				existingQryNm = existingQryNm.substring(0, existingQryNm.lastIndexOf(" (Public)")).trim();
			}
			existingQueryId = psrDao.getQueryId(loginUserId, existingQryNm);
		}
		if (queryId != null) {
			portalDao.updateDefaultView(queryId, existingQueryId, loginUserId);
		}

	}

	@Override
	public List<PtlUserProfileForm> getUserProfile(String userId, String disable, String searchOperator,
			String roleInput, String roleOperator) throws Exception {
		log.info("enter into the method getUserProfile userProfile service ");
		return UIViewHelper
				.getUserProfileForm(portalDao.getUserProfile(userId, disable, searchOperator, roleInput, roleOperator));
	}

	@Override
	public UserResponse getUserProfileDetails(String userId) throws Exception {
		log.info(" enter into the method getUserProfileDetails userId details service ");
		UserResponse userDetails = new UserResponse();
		List<PtlUserProfileForm> ptlUserProfile = portalDao.getUserProfileDetails(userId);
		// List<String> brUserIds = psrDao.getBrUserId();
		Map<Integer, String> allQueryIds = psrDao.getQueryNames();
		Map<String, String> allCntryNames = portalDao.getCountryNames();
		Map<Integer, String> ptlRoleProfileDetails = portalDao.getRoles();
		userDetails.setPtlUserProfileDetails(ptlUserProfile);
		// userDetails.setAllBrUserIds(brUserIds);
		userDetails.setAllQueryIds(allQueryIds);
		userDetails.setAllCntryNames(allCntryNames);
		userDetails.setPtlRoleProfileDetails(ptlRoleProfileDetails);
		return userDetails;
	}

	@Override
	public void deleteUserProfile(String userId) throws Exception {
		log.info("enter into the method deleteUserProfile userId delete service ");
		portalDao.deleteUserProfile(userId);
	}

	@Override
	public List<PtlRoleProfileForm> getRoleProfile(String roleName, String disable, String searchOperator)
			throws Exception {
		log.info("enter into the method getRoleProfile roleProfile service ");
		return UIViewHelper.getRoleProfileForm(portalDao.getRoleProfile(roleName, disable, searchOperator));
	}

	@Override
	public void saveRoleProfile(PtlRoleProfileForm roleProfile, String loginUserId) throws Exception {
		log.info(" enter into the method saveRoleProfile Save Role Profile service ");
		Integer isRoleNameExists = isRoleNameAlreadyExists(roleProfile.getRoleName());
		if (isRoleNameExists != 0) {
			log.warn(String.format(" Role Name already present with roleName %s", roleProfile.getRoleName()));
			throw new SQLIntegrityConstraintViolationException(
					" Role Name already present with Role Name " + roleProfile.getRoleName());
		} else {
			roleProfile.setRoleId(portalDao.getNewRoleId());
			portalDao.saveRoleProfile(DAOHelper.saveRoleProfileDetails(roleProfile, loginUserId));
		}
	}

	@Override
	public void updateRoleProfile(PtlRoleProfile roleUpdate, String roleId, String loginUserId) throws Exception {
		log.info(" enter into the method updateRoleProfile Role update service ");
		PtlRoleProfile portalRoleProfile = DAOHelper.getRoleProfile(roleUpdate);
		portalDao.updateRoleProfile(portalRoleProfile, roleId, loginUserId);
	}

	@Override
	public List<PtlRoleProfileForm> getRoleProfileDetails(String roleId) throws Exception {
		log.info("enter into the method getRoleProfileDetails service ");
		return UIViewHelper.getRoleProfileDetails(portalDao.getRoleProfileDetails(roleId));
	}

	@Override
	public List<BrUserProfileForm> getBrUserProfile(String brUserId) throws Exception {
		log.info("enter into the method getBrUserProfile service ");
		return UIViewHelper.getBrUserProfile(psrDao.getBrUserProfile(brUserId));
	}

	@Override
	public void deleteRoleProfile(String roleId) throws Exception {
		log.info("enter into the method deleteRoleProfile service ");
		portalDao.deleteRoleProfile(roleId);
	}

	@Override
	public void updateUserProfile(PtlUserProfileForm userUpdate, String UserId, String loginUserId) throws Exception {
		log.info(" enter into the method updateUserProfile User update service ");
		portalDao.updateUserProfile(userUpdate, UserId, loginUserId);
	}

	@Override
	public void saveUserProfile(PtlUserProfileForm userProfile, String loginUserId) throws Exception {
		log.info(" enter into the method saveUserProfile Save User Profile service ");
		Integer isUserIdExists = isUserIdAlreadyExists(userProfile.getUserId());
		if (isUserIdExists != 0) {
			log.warn(String.format(" User ID already present with userId %s", userProfile.getUserId()));
			throw new SQLIntegrityConstraintViolationException(
					" User ID already present with userId " + userProfile.getUserId());
		} else {
			if (userProfile.getPswdNeverExpires() != null && !userProfile.getPswdNeverExpires().isEmpty()
					&& userProfile.getPswdNeverExpires().trim().equalsIgnoreCase("Y")) {
				userProfile.setPswEffectiveDate(new Date());
				userProfile.setPswdExpireDate(PSRHelper.getDateWithAdditionOfYear());
			}
			portalDao.saveUserProfile(DAOHelper.saveUserProfileDetails(userProfile, loginUserId));
			portalDao.saveUserRole(DAOHelper.saveUserRoleDetails(userProfile, loginUserId));
		}
	}

	private Integer isUserIdAlreadyExists(String userId) throws Exception {
		return portalDao.isUserIdAlreadyExists(userId);
	}

	private Integer isRoleNameAlreadyExists(String roleName) throws Exception {
		return portalDao.isRoleNameAlreadyExists(roleName);
	}
	
	@Override
	public Boolean isTokenIdExists(String authentication) throws Exception {
		return portalDao.isTokenIdExists(authentication);
	}

	@Override
	public void cleanUserTokens(int cleanUpInterval) throws Exception {
		portalDao.cleanUserTokens(cleanUpInterval);
	}
	
}
