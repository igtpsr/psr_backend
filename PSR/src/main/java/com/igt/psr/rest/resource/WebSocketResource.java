package com.igt.psr.rest.resource;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.igt.psr.common.util.PSRHelper;
import com.igt.psr.common.util.PSRResponse;
import com.igt.psr.rest.common.view.UserSessionDetails;
import com.igt.psr.rest.service.PSRPortalService;
import com.igt.psr.ui.view.PsrAuth;

@RestController
@RequestMapping("/rest")
public class WebSocketResource {
	private static final Logger log = LoggerFactory.getLogger(WebSocketResource.class);

	@Autowired
	private SimpMessagingTemplate template;

	@Value("${mgf.ldap.url}")
	public String ldapUrl;
	@Value("${mgf.ldap.domain}")
	public String domain;

	@Value("${login.session.timeout}")
	public int timeout;

	PSRResponse psrResponse = new PSRResponse();

	@Autowired
	PSRPortalService psrPortalService;

	// @Scheduled(fixedRate = 10000)
	// public void send() throws Exception {
	// System.out.println("Coming to websocket");
	// template.convertAndSend("/queue/test", "sample");
	// }

	@ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
	@MessageMapping(value = "/updateSession")
	public void updateSessionTime(PsrAuth psrauth) throws Exception {
		log.debug(String.format("Enetered into the WebSocketResource and updateSessionTime, path=/updateSession , method %s",				psrauth.getTokenId()+ " and uniqueId is: "+psrauth.getUniqueId()));
		String authKey = psrauth.getTokenId();
		String msg = "logout";
		String path = "/queue/logout/";
		String nPath = "/queue/network/";
		try {
			if (PSRHelper.checkEmptyOrNull(authKey)) {
				UserSessionDetails userSessionInfo = psrPortalService.getUserIdFromSession(authKey, timeout);
				log.debug(String.format("UserSessionDetails %s", userSessionInfo.toString()));
				if (userSessionInfo.getIsInactive() == 0) {
					psrPortalService.deleteUserSession(userSessionInfo);
					template.convertAndSend(path+psrauth.getUniqueId(), msg);// redirect to logout
				} else {
					if (userSessionInfo.getInActiveTime() > (timeout - 5)) {
						psrPortalService.updateInactiveIntervalTime(userSessionInfo);
					}
				}
			} else {
				// redirect to logout
				log.debug(String.format("Authentication key is null/missed in the request."));
				template.convertAndSend(path+psrauth.getUniqueId(), msg);
			}
		} catch (Exception e) {
			if (e.getMessage() != null && e.getMessage().contains("Unable to acquire JDBC Connection")) {
				log.error(String.format(" Network Problem happened for session = " + psrauth.getTokenId() + " : %s",
						e.getMessage()));
				template.convertAndSend(nPath+psrauth.getUniqueId(), msg);// redirect to logout
			} else {
				template.convertAndSend(path+psrauth.getUniqueId(), msg);
				log.error(String.format(" Error in user session session = " + psrauth.getTokenId() + " : %s",
						e.getMessage()));
				// e.printStackTrace();
			}
		}
	}

	@MessageExceptionHandler
	public String handleException(Throwable exception) {
		log.info(String.format("Exception Occured On Websocket Connection : %s", exception));
		return exception.getMessage();
	}
}
