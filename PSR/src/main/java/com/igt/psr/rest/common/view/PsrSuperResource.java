package com.igt.psr.rest.common.view;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

public class PsrSuperResource {
	@Autowired
	private HttpSession httpSession;

	public void setUserInSession(String loginUser) {
		if (loginUser != null && !loginUser.trim().isEmpty()) {
			httpSession.setAttribute("loginUserId", loginUser);
		}
	}

	public String getUserFromSession() {
		return (String) httpSession.getAttribute("loginUserId");
	}
}
