package com.igt.psr.rest.resource;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.igt.psr.common.constants.ApplicationConstants;
import com.igt.psr.common.util.PSRResponse;
import com.igt.psr.portal.model.PtlRoleProfile;
import com.igt.psr.rest.service.PSRPortalService;
import com.igt.psr.ui.view.BrUserProfileForm;
import com.igt.psr.ui.view.PsrAuth;
import com.igt.psr.ui.view.PtlRoleProfileForm;
import com.igt.psr.ui.view.PtlUserProfileForm;
import com.igt.psr.ui.view.UserResponse;

@RestController
@RequestMapping("/rest/")
public class PortalResource {

	private static final Logger log = LoggerFactory.getLogger(PortalResource.class);

	@Autowired
	PSRPortalService psrPortalService;
	PSRResponse psrResponse = new PSRResponse();

	@PutMapping("/updateDefaultView")
	public PSRResponse updateDefaultView(@RequestParam String loginUserId, @RequestParam String queryName,
			@RequestParam String existingQryNm) throws Exception {
		log.info(" entered into method updateDefaultView ,path=/updateDefaultView type=put ");
		try {
			psrPortalService.updateDefaultView(loginUserId, queryName, existingQryNm);
			psrResponse.setType("success");
			psrResponse.setMessage("Updated the query name Successfully");
			return psrResponse;
		} catch (Exception e) {
			psrResponse.setType("error");
			psrResponse.setMessage("An error has been occured while updated the Query Name");
			log.error(String.format(" Error occured e : %s", e.getMessage()));
			return psrResponse;
		}
	}

	@GetMapping("/getUserProfile")
	public List<PtlUserProfileForm> getUserProfile(@RequestParam String userId, @RequestParam String disable,
			@RequestParam String roleInput, String searchOperator, String roleOperator) throws Exception {
		log.info("enter into the method getUserProfile , path=/ type=get with userId " + userId + "disable :" + disable
				+ "searchOperator->" + searchOperator);
		return psrPortalService.getUserProfile(userId, disable, searchOperator, roleInput, roleOperator);
	}

	@GetMapping("/getUserDetails")
	public UserResponse getUserProfileDetails(@RequestParam String userId) throws Exception {
		log.info(String.format("enter into the method getUserProfileDetails, path=/ type=get with userId %s", userId));
		return psrPortalService.getUserProfileDetails(userId);
	}

	@DeleteMapping("/deleteUserProfile")
	public PSRResponse deleteUserProfile(@RequestParam String userId) throws Exception {
		log.info("enter into the method deleteUserProfile , path=/deleteUserProfile, type=delete with userId  "
				+ userId);
		try {
			psrPortalService.deleteUserProfile(userId);
			psrResponse.setType("success");
			psrResponse.setMessage("User Deleted Successfully");
			return psrResponse;
		} catch (Exception e) {
			psrResponse.setType("error");
			psrResponse.setMessage("User profile not deleted Successfully");
			log.error(String.format(" Error occured in deleteUserProfile : %s", e.getMessage()));
			return psrResponse;
		}
	}

	@GetMapping("/getRoleProfile")
	public List<PtlRoleProfileForm> getRoleProfile(@RequestParam String roleName, @RequestParam String disable,
			String searchOperator) throws Exception {
		log.info("enter into the method getRoleProfile , path=/ type=get with roleName " + roleName + "disable"
				+ disable + "searchOperator->" + searchOperator);
		return psrPortalService.getRoleProfile(roleName, disable, searchOperator);
	}

	@PostMapping("/saveRoleProfile")
	public PSRResponse saveRoleProfile(@RequestBody PtlRoleProfileForm roleProfile, @RequestParam String loginUserId)
			throws Exception {
		log.info(" entered into method saveRoleProfile ,path=/ type=post with roleProfile " + roleProfile.toString()
				+ "loginUserId ->" + loginUserId);
		try {
			psrPortalService.saveRoleProfile(roleProfile, loginUserId);
			psrResponse.setType(ApplicationConstants.SUCCESS);
			psrResponse.setMessage(ApplicationConstants.SAVE_SUCCESS);
		} catch (SQLIntegrityConstraintViolationException e) {
			psrResponse.setType(ApplicationConstants.ERROR);
			psrResponse.setMessage(" Duplicate Role Name. Please change the Role Name");
			log.error(String.format(ApplicationConstants.ERROR_OCCURED_IN_SAVE_USER_PROFILE, e.getMessage()));
		} catch (Exception e) {
			psrResponse.setType(ApplicationConstants.ERROR);
			psrResponse.setMessage("Error in saving role Profile");
			log.error(String.format(" Error has been occured : %s", e.getMessage()));
		}
		return psrResponse;
	}

	@PutMapping("/updateRoleProfile")
	public PSRResponse updateRoleProfile(@RequestBody PtlRoleProfile roleUpdate, @RequestParam String roleId,
			@RequestParam String loginUserId) throws Exception {
		log.info(" enter into the method updateRoleProfile , path=/updateRoleProfile, type=Put with roleId =" + roleId
				+ "roleUpdate->" + roleUpdate.toString() + "loginUserId->" + loginUserId);
		try {
			psrPortalService.updateRoleProfile(roleUpdate, roleId, loginUserId);
			psrResponse.setType(ApplicationConstants.SUCCESS);
			psrResponse.setMessage("Role Updated Successfully");
			return psrResponse;
		} catch (Exception e) {
			psrResponse.setType(ApplicationConstants.ERROR);
			psrResponse.setMessage("Role profile not Updated Successfully");
			log.error(String.format(" Error occured in updateRoleProfile : %s", e.getMessage()));
			return psrResponse;
		}
	}

	@GetMapping("/getRoleProfileDetails")
	public List<PtlRoleProfileForm> getRoleProfileDetails(@RequestParam String roleId) throws Exception {
		log.info(String.format("enter into the method getRoleProfileDetails , path=/ type=get %s", roleId));
		return psrPortalService.getRoleProfileDetails(roleId);
	}

	@GetMapping("/getBrUserProfile")
	public List<BrUserProfileForm> getBrUserProfile(@RequestParam String brUserId) throws Exception {
		log.info(String.format("enter into the method getBrUserProfile , path=/ type=get %s", brUserId));
		return psrPortalService.getBrUserProfile(brUserId);
	}

	@PutMapping("/deleteRoleProfile")
	public PSRResponse deleteRoleProfile(@RequestParam String roleId) throws Exception {
		log.info(String.format(
				"enter into the method deleteRoleProfile, path=/deleteRoleProfile, type=Put and roleId : %s", roleId));
		try {
			psrPortalService.deleteRoleProfile(roleId);
			psrResponse.setType(ApplicationConstants.SUCCESS);
			psrResponse.setMessage(ApplicationConstants.DELETE_SUCCESS);
			return psrResponse;
		} catch (Exception e) {
			psrResponse.setType(ApplicationConstants.ERROR);
			psrResponse.setMessage("Role not deleted Successfully");
			log.error(String.format(" Error occured in deleteRoleProfile : %s", e.getMessage()));
			return psrResponse;
		}
	}

	@PutMapping("/updateUserProfile")
	public PSRResponse updateUserProfile(@RequestBody PtlUserProfileForm userUpdate, @RequestParam String UserId,
			@RequestParam String loginUserId) throws Exception {
		log.info(" enter into the method updateUserProfile , path=/updateUserProfile, type=Put with userUpdate->"
				+ userUpdate.toString() + "loginUserId->" + loginUserId);
		try {
			psrPortalService.updateUserProfile(userUpdate, UserId, loginUserId);
			psrResponse.setType(ApplicationConstants.SUCCESS);
			psrResponse.setMessage("User Updated Successfully");
			return psrResponse;
		} catch (Exception e) {
			psrResponse.setType(ApplicationConstants.ERROR);
			psrResponse.setMessage("User not Updated Successfully");
			log.error(String.format(" Error occured in updateUserProfile : %s", e.getMessage()));
			return psrResponse;
		}
	}

	@PostMapping("/saveUserPrfile")
	public PSRResponse saveUserProfile(@RequestBody PtlUserProfileForm userProfile, @RequestParam String loginUserId)
			throws Exception {
		log.info(" entered into method saveUserProfile, path=/saveUserPrfile, method-type=post with userProfile :: "
				+ userProfile.toString() + "loginUserId :: " + loginUserId);
		try {
			psrPortalService.saveUserProfile(userProfile, loginUserId);
			psrResponse.setType(ApplicationConstants.SUCCESS);
			psrResponse.setMessage(ApplicationConstants.SAVE_SUCCESS);
		} catch (SQLIntegrityConstraintViolationException e) {
			psrResponse.setType(ApplicationConstants.ERROR);
			psrResponse.setMessage(" Duplicate User Id. Please change the User Id");
			log.error(String.format(ApplicationConstants.ERROR_OCCURED_IN_SAVE_USER_PROFILE, e.getMessage()));
		} catch (Exception e) {
			psrResponse.setType(ApplicationConstants.ERROR);
			psrResponse.setMessage(" Error occured while saving the data , please contact the Administrator . ");
			log.error(String.format(ApplicationConstants.ERROR_OCCURED_IN_SAVE_USER_PROFILE, e.getMessage()));
		}
		return psrResponse;
	}

	@PostMapping("/browserClosed")
	public void browserClosed(@RequestBody String authentication) {
		log.info("Browser closed/refreshed without logout for tokenId " + authentication);
	}

	@PostMapping("/isTokenAvailable")
	public Boolean isTokenAvailable(@RequestBody PsrAuth psrAuth) {
		log.info("Entered into the method isTokenAvailable: " + psrAuth.getTokenId()
				+ "  , path=/isTokenAvailable , method-type=get ");
		try {
			return psrPortalService.isTokenIdExists(psrAuth.getTokenId());
		} catch (Exception e) {
			log.error("An error has been occured --> " + e.getMessage());
			return false;
		}
	}

}
