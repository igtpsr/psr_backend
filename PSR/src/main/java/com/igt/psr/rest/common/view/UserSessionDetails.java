package com.igt.psr.rest.common.view;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserSessionDetails implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String loginUserId;
	private String userTokenId;
	private Date lastInactiveIntervalTime;
	private Integer isInactive;
	private Integer inActiveTime;
}