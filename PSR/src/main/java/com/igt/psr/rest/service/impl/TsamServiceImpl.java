package com.igt.psr.rest.service.impl;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.igt.psr.common.util.PSRHelper;
import com.igt.psr.dao.PsrDao;
import com.igt.psr.rest.service.TsamService;
import com.igt.psr.tsam.dao.TsamDao;
import com.igt.psr.ui.view.PostXmlInputParams;

@Service
public class TsamServiceImpl implements TsamService {

	private static final Logger log = LoggerFactory.getLogger(TsamServiceImpl.class);

	@Autowired
	private TsamDao tsamDao;

	@Autowired
	private PsrDao psrDao;

	@Override
	public Map<String, List<String>> getRowNoFromBrose(List<PostXmlInputParams> postXmlInputParams) throws Exception {
		log.info(" entered into the class TsamServiceImpl method getRowNoFromBrose ");
		return tsamDao.getRowNoFromBrose(postXmlInputParams);
	}

	private boolean validateRevisedDate(List<PostXmlInputParams> postXmlInputParams) {
		boolean isValidRevisedDate = false;
		if (postXmlInputParams != null && postXmlInputParams.size() > 0) {
			for (PostXmlInputParams postXmlInputParam : postXmlInputParams) {
				String existedResvisedDate = psrDao.getRevisedDateForGacByVendor(postXmlInputParam.getPsrOrderId());
				if (existedResvisedDate == null) {
					isValidRevisedDate = false;
					break;
				} else {
					isValidRevisedDate = existedResvisedDate
							.equalsIgnoreCase(PSRHelper.getDateStrForSP(postXmlInputParam.getRevisedDate()));
					if (isValidRevisedDate == false)
						break;
				}
			}
		}
		return isValidRevisedDate;
	}
	
	@Override
	public Map<String, String> getSmartTagRecords(String requestNo,  String style, String colorWashName, String profomaPo) throws Exception {
		log.info(" entered into the class TsamServiceImpl method getRowNoFromBrose ");
		return tsamDao.getSmartTagRecords(requestNo, style, colorWashName, profomaPo);
	}

}
