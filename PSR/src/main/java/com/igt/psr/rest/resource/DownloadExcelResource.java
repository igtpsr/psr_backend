package com.igt.psr.rest.resource;

import static com.igt.psr.common.constants.ApplicationConstants.FAILURE;
import static com.igt.psr.common.constants.ApplicationConstants.SAVE_SUCCESS;
import static com.igt.psr.common.constants.ApplicationConstants.SUCCESS;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.igt.psr.common.constants.ApplicationConstants;
import com.igt.psr.common.exception.ImageException;
import com.igt.psr.common.util.AvailableViewRes;
import com.igt.psr.common.util.PSRHelper;
import com.igt.psr.common.util.PSRResponse;
import com.igt.psr.common.util.ReadXLSM;
import com.igt.psr.dao.PsrDao;
import com.igt.psr.rest.common.view.PsrSuperResource;
import com.igt.psr.rest.service.PSRService;
import com.igt.psr.ui.view.FileTrack;
import com.igt.psr.ui.view.PsrUpdateView;
import com.igt.psr.ui.view.SearchForm;

@ConfigurationProperties
@RestController
@RequestMapping("/rest")
public class DownloadExcelResource extends PsrSuperResource {
	
	@Autowired
	private PsrDao psrDao;

	private static final Logger log = LoggerFactory.getLogger(DownloadExcelResource.class);

	@Value("${image.url}")
	public String imageurl;
	@Value("${mgf.owner}")
	public String owner;

	@Value("${mgf.bamboorose.wbServiceURL}")
	public String wbServiceURL;
	@Value("${mgf.bamboorose.username}")
	public String bambooroseUser;
	@Value("${mgf.bamboorose.password}")
	public String bamboorosePwd;

	@Value("${mgf.excel.code}")
	String secretCode;

	@Value("${mgf.excel.enableContent}")
	String enableContent;

	@Value("${mgf.grid.rows}")
	Integer rowsLimit;

	@Autowired
	private PSRService psrService;

	ReadXLSM readFile = new ReadXLSM();

	PSRResponse psrResponse = new PSRResponse();
	AvailableViewRes availableViewRes;

	@GetMapping("/downloadExcel")
	public HttpEntity<byte[]> downloadExcel(SearchForm searchForm, String queryId, String milstoneList,
			@RequestParam String loginUserId, HttpServletResponse response, HttpServletRequest request, String checkedOrderId)
			throws Exception {
		log.info("enter into the method downloadExcel , path=/downloadExcel , type=get ");
		XSSFWorkbook workbook = null;
		FileInputStream file = null;
		FileOutputStream fileOutputStream = null;
		File excelTemplate = null;
		byte[] fileContent = null;

		try {
			Date startTime = psrService.getESTDateFromDB();
			Calendar cal = Calendar.getInstance();
			cal.setTime(startTime);
			String tempDate = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-"
					+ (cal.get(Calendar.DATE)) + "_" + cal.get(Calendar.HOUR_OF_DAY) + "_" + cal.get(Calendar.MINUTE)
					+ "_" + cal.get(Calendar.SECOND) + "_" + cal.get(Calendar.MILLISECOND);
			String fileName = "PSR_" + tempDate + "_" + UUID.randomUUID().toString();

			String tempFileName = "PSR_" + UUID.randomUUID().toString() + ".xlsm";
			excelTemplate = PSRHelper.getTemplateFile("classpath:config/TemplateExcelWithMacros.xlsm", tempFileName);
			file = new FileInputStream(excelTemplate);

			// Create Workbook instance holding reference to .xlsx file
			workbook = new XSSFWorkbook(file);
			XSSFSheet sheet = workbook.getSheet("hiddendata");
			//XSSFSheet headerSheet = workbook.getSheet("headers"); // to validate headers while importing excel
			
			String holdReorderColumn = psrDao.getReOrderColumnsWRTQueryId(queryId);
			
			// To set the value of fileName into validation sheet
						XSSFSheet validationSheet1 = workbook.getSheet("validations");
						Cell fileNameCell1 = validationSheet1.getRow(3).getCell(16, Row.CREATE_NULL_AS_BLANK);
						fileNameCell1.setCellValue(holdReorderColumn);
			
			
			// XSSFSheet headerSheet = workbook.getSheet("headers"); // to validate headers
			// while importing excel

			// To set the value of fileName into validation sheet
			XSSFSheet validationSheet = workbook.getSheet("validations");
			Cell fileNameCell = validationSheet.getRow(3).getCell(6, Row.CREATE_NULL_AS_BLANK);
			fileNameCell.setCellValue(fileName);

			List<Map> exportData = psrService.getPsrExportAllData(searchForm, queryId, milstoneList, checkedOrderId);

			// List<Map> exportData =
			// PSRHelper.reverseList(psrService.getPsrExportAllData(searchForm, queryId));

			if (exportData.size() > rowsLimit) {
				throw new Exception("Row data limit exceeded");
			}

			if (exportData != null && exportData.size() > 0) {
				Row hrow = sheet.createRow(0);
				// Row headerRow = headerSheet.createRow(0);
				int columnCount = 0;
				// int headerColumnCount = 0;
				for (Object headerName : exportData.get(0).keySet()) {
					Cell cell = hrow.createCell(columnCount++);
					// Cell headerCell = headerRow.createCell(headerColumnCount++);
					if (hrow.getRowNum() == 0) {
						cell.setCellValue(headerName.toString());
						// headerCell.setCellValue(headerName.toString());
					}
				}

				int rownumforvalues = 1;
				for (Map map : exportData) {
					Row row = sheet.createRow(rownumforvalues++);
					columnCount = 0;
					for (Object name : map.keySet()) {
						Cell cell = row.createCell(columnCount++);
						String val = (map.get(name) != null ? map.get(name).toString() : "");
						cell.setCellValue(val);
					}
				}

				File outputStreamFile = ResourceUtils.getFile(excelTemplate.getAbsolutePath());
				fileOutputStream = new FileOutputStream(outputStreamFile);
				// Write workbook into the excel
				workbook.write(fileOutputStream);

				fileContent = FileUtils.readFileToByteArray(excelTemplate.getAbsoluteFile());

				HttpHeaders header = new HttpHeaders();
				header.setContentType(
						new MediaType("application", "vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
				header.set(HttpHeaders.CONTENT_DISPOSITION,
						"attachment; filename=" + fileName.substring(0, fileName.lastIndexOf("_")));
				header.setContentLength(fileContent.length);
				header.set(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.CONTENT_DISPOSITION);

				Date endTime = psrService.getESTDateFromDB();

				FileTrack fileTrack = new FileTrack();
				fileTrack.setFileName(fileName);
				fileTrack.setDesc("Downloaded");
				// fileTrack.setUploadStatus(false);
				fileTrack.setUserId(loginUserId);
				fileTrack.setStartTime(startTime);
				fileTrack.setEndTime(endTime);

				log.info("fileTrack object " + fileTrack.toString());
				psrService.saveExportDataForExcel(fileTrack);
				return new HttpEntity<byte[]>(fileContent, header);
			} else {
				return null;
			}
		} catch (Exception e) {
			log.error("Error occured in downloadExcel " + e.getMessage());
			if (e.getMessage().contains("Row data limit exceeded")) {
				return ResponseEntity.status(417).body(null);
			} else if (e.getMessage()
					.contains("The process cannot access the file because it is being used by another process")) {
				return ResponseEntity.status(103).body(null);
			} else {
				return ResponseEntity.status(100).body(null);
			}
			// return (ResponseEntity<Resource>)
			// ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR);
		} finally {
			if (fileOutputStream != null) {
				fileOutputStream.close();
			}

			if (file != null) {
				file.close();
			}

			if (excelTemplate != null) {
				excelTemplate.delete();
			}

		}

	}

	@PostMapping("/importExcel")
	public PSRResponse importExcel(@RequestParam("file") MultipartFile fis, @RequestParam String loginUserId,
			@RequestParam String roleName) throws Exception {

		String fileName = null;
		log.info("importExcel file data File type : " + fis.getContentType() + " Name : " + fis.getOriginalFilename()
				+ " with loginUserId " + loginUserId);
		try {
			Date startTime = psrService.getESTDateFromDB();
			// int fileStatus = psrService.checkFileName(loginUserId,
			// fis.getOriginalFilename());
			// if (fileStatus > 0) {
			FileInputStream file = (FileInputStream) fis.getInputStream();

			Map<Integer, String> readData = readFile.readXLSM(file, secretCode, enableContent);

			if (readData.get(0) != null && !readData.get(0).equalsIgnoreCase("")) {
				fileName = readData.get(0);
			} else {
				throw new Exception("Filename is missing.");
			}

			if (readData.get(1).equalsIgnoreCase("headersMismatched")) {
				throw new Exception("Headers are mismatched in the file.");
			}
			if (readData.get(1).equalsIgnoreCase("enableContentFalse")) {
				throw new Exception("Please edit the file with Enable Content.");
			}
			if (!readData.get(1).equalsIgnoreCase("secretCodeMissed")) {
				log.info(" xml String \n" + readData.get(1));
				String message = null;
				for (Entry<Integer, String> entry : readData.entrySet()) {
					if (entry.getKey() != null && entry.getKey() != 0) {
						message = psrService.saveExcelInfo(entry.getValue(), loginUserId, roleName,
								ApplicationConstants.getCurrentDateWithOutTimeDelay(), fis.getOriginalFilename());
					}
				}
				if (message != null && !message.isEmpty() && message.equalsIgnoreCase(SUCCESS)) {
					psrResponse.setType(ApplicationConstants.SUCCESS_SMALL_LETTERS);
					psrResponse.setMessage(ApplicationConstants.UPLOAD_SUCCESS);
				} else {
					throw new Exception("Upload Failed.");
				}
			} else {
				throw new Exception("File is mismatched. Please upload the correct file.");
			}
			/*
			 * } else { throw new
			 * Exception("File is mismatched. Please upload the correct file."); }
			 */
			// log.info(" Time consumed for upload is " + (startTime - new
			// Date().getSeconds()) + " secs");
			Date endTime = psrService.getESTDateFromDB();
			FileTrack fileTrack = new FileTrack();
			fileTrack.setLoginUserId(loginUserId);
			fileTrack.setFileName(fileName);
			fileTrack.setDesc("Uploaded");
			// fileTrack.setUploadStatus(true);
			fileTrack.setUserId(loginUserId);
			fileTrack.setStartTime(startTime);
			fileTrack.setEndTime(endTime);

			log.info("fileTrack object " + fileTrack.toString());

			psrService.saveImportDataForExcel(fileTrack);

		} catch (Exception e) {
			log.error(String.format(ApplicationConstants.ERROR_OCCURED, e.getMessage()));
			psrResponse.setType(ApplicationConstants.ERROR_SMALL_LETTERS);
			if (e.getMessage() != null && e.getMessage().contains("This is failed due to deadlock situation")) {
				psrResponse.setMessage("This is failed due to deadlock situation, please try after sometime");
			} else {
				psrResponse.setMessage(ApplicationConstants.ERROR_OCCURED_WHILE_SAVE_DATA);
			}
		}
		return psrResponse;
	}

	@PostMapping("/updateOrdersByImportExcel")
	public PSRResponse updateOrdersByImportExcel(@RequestParam String loginUserId, @RequestParam String roleName,
			@RequestParam String sheetName, @RequestParam String orderAvailableFields,
			@RequestBody List<PsrUpdateView> updateOrders) {
		log.info(String.format(" enter into the method update , path=/updateOrdersByImportExcel , type=post"));
		String[] availableOrder = orderAvailableFields.split(",");

		try {

			String message = psrService.updateOrdersByImportExcel(updateOrders, loginUserId, roleName, availableOrder);
			if (message != null && !message.isEmpty() && message.equalsIgnoreCase(SUCCESS)) {
				psrResponse.setType(ApplicationConstants.SUCCESS_SMALL_LETTERS);
				psrResponse.setMessage(ApplicationConstants.UPLOAD_SUCCESS);

			} else {
				throw new Exception("Upload Failed.");
			}

			/* file change tracking logic */
			Date startTime = psrService.getESTDateFromDB();
			Date endTime = psrService.getESTDateFromDB();
			FileTrack fileTrack = new FileTrack();
			fileTrack.setLoginUserId(loginUserId);
			fileTrack.setFileName(sheetName);
			fileTrack.setDesc("Uploaded");
			fileTrack.setUserId(loginUserId);
			fileTrack.setStartTime(startTime);
			fileTrack.setEndTime(endTime);

			log.info("fileTrack object " + fileTrack.toString());

			psrService.saveImportDataForExcel(fileTrack);

		} catch (Exception e) {
			psrResponse.setType(ApplicationConstants.ERROR_SMALL_LETTERS);
			if (e.getMessage() != null && e.getMessage().contains("This is failed due to deadlock situation")) {
				psrResponse.setMessage("This is failed due to deadlock situation, please try after sometime");
			} else {
				psrResponse.setMessage(ApplicationConstants.ERROR_OCCURED_WHILE_SAVE_DATA);
			}
			log.error(String.format(ApplicationConstants.ERROR_OCCURED, e.getMessage()));

		}
		return psrResponse;
	}

	@GetMapping("/fileTrack")
	public Map<String, Set<FileTrack>> getFileTrackInfo(@RequestParam String loginUserId, @RequestParam String roleName,
			@RequestParam String from, @RequestParam String fromDate, @RequestParam String toDate) throws Exception {
		log.info("Entered into the getFileTrackInfo ");
		try {
			return psrService.getFileTrackInfo(loginUserId, roleName, from, fromDate, toDate);
		} catch (Exception e) {
			log.error(" An error has occured in getFileTrackInfo --> " + e.getMessage());
			return null;
		}
	}

}
