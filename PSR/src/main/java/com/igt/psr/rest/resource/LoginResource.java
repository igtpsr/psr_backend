package com.igt.psr.rest.resource;

import static com.igt.psr.common.constants.ApplicationConstants.ERROR_SMALL_LETTERS;
import static com.igt.psr.common.constants.ApplicationConstants.NO_ROLE_MAPPED;
import static com.igt.psr.common.constants.ApplicationConstants.YES;

import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.igt.psr.common.constants.ApplicationConstants;
import com.igt.psr.common.exception.LoginValidateException;
import com.igt.psr.common.util.LdapAuth;
import com.igt.psr.common.util.PSRHelper;
import com.igt.psr.common.util.PSRResponse;
import com.igt.psr.portal.model.PortalUserSession;
import com.igt.psr.portal.model.PtlUserProfile;
import com.igt.psr.rest.common.view.PsrSuperResource;
import com.igt.psr.rest.common.view.UserSessionDetails;
import com.igt.psr.rest.service.PSRPortalService;
import com.igt.psr.rest.service.PSRService;
import com.igt.psr.ui.view.AuthResponse;
import com.igt.psr.ui.view.Login;
import com.sun.mail.util.BASE64DecoderStream;
import com.sun.mail.util.BASE64EncoderStream;

@RestController
@RequestMapping("/rest")
public class LoginResource extends PsrSuperResource {

	private static final Logger log = LoggerFactory.getLogger(LoginResource.class);
	private static Cipher ecipher;
	private static Cipher dcipher;

	private static SecretKey key;
	private static int counter;

	@Autowired
	PSRPortalService psrPortalService;

	@Autowired
	private PSRService psrService;

	LdapAuth ldapAuth;

	@Value("${mgf.ldap.url}")
	public String ldapUrl;
	@Value("${mgf.ldap.domain}")
	public String domain;
	@Value("${mgf.grid.rows}")
	public Integer gridRowsLimit;

	PSRResponse psrResponse = new PSRResponse();

	@Autowired
	HttpSession session;

	@GetMapping(value = "/isAlive")
	public String isAlive() {
		log.info(" Entered into the method isAlive path=/isAlive type=get");
		return "success";
	}

	@PostMapping(value = "/login")
	public AuthResponse getAuth(@Valid @RequestBody Login credential) {
		log.info("enetered into the method getAuth type=post and path=/login");
		AuthResponse authRes = null;
		String validUser = null;
		PtlUserProfile ptlUserProfile = null;
		try {
			ldapAuth = new LdapAuth();
			ldapAuth.setDomain(domain);
			ldapAuth.setLdapUrl(ldapUrl);
			Login login = null;
			if (credential != null && PSRHelper.checkEmptyOrNull(credential.getUsername())) {
				login = credential;
			} else {
				login = this.authenticate(credential.getCredentials());
			}
			if (login != null) {
				// Ldap Authentication
				validUser = ldapAuth.authenticate(login.getUsername(), login.getPassword());
				if (validUser != null) {
					authRes = this.setUserSessionAttributes(credential, validUser);
					if (authRes != null && PSRHelper.checkEmptyOrNull(authRes.getRoleName())) {
						ptlUserProfile = psrPortalService.getUserDetailsInfo(login.getUsername(), login.getPassword(),
								false);
						this.setDefaultView(authRes, ptlUserProfile);
						List<String> milestone = psrService.getEventDescriptions(authRes.getBrUserId(), authRes.getLoginUserId(),
								authRes.getRoleName());
						if (milestone != null) {
							authRes.setMilestone(milestone);
						}
						authRes.setType("success");
						log.info(String.format("LDAP Authentication is passed for --> %s", credential.getUsername()));
					} else {
						authRes.setType(NO_ROLE_MAPPED);
						authRes.setMessage(LoginValidateException.noRoleMapped(credential.getUsername()));
						log.error(LoginValidateException.noRoleMapped(credential.getUsername()));
					}
				} else {
					// do authentication against database for the login user
					ptlUserProfile = psrPortalService.getUserDetailsInfo(login.getUsername(), login.getPassword(),
							true);
					validUser = ptlUserProfile != null ? ptlUserProfile.getUserId() : null;
					if (validUser != null) {
						authRes = this.setUserSessionAttributes(credential, validUser);
						if (authRes != null && PSRHelper.checkEmptyOrNull(authRes.getRoleName())) {
							this.setDefaultView(authRes, ptlUserProfile);
							List<String> milestone = psrService.getEventDescriptions(authRes.getBrUserId(), authRes.getLoginUserId(),
									authRes.getRoleName());
							if (milestone != null) {
								authRes.setMilestone(milestone);
							}
							authRes.setType("success");
							log.info(
									"LDAP Authentication is failed but passed against Data Base as a non ldap user --> "
											+ credential.getUsername());
						} else {
							authRes.setType(NO_ROLE_MAPPED);
							authRes.setMessage(LoginValidateException.noRoleMapped(credential.getUsername()));
							log.error(LoginValidateException.noRoleMapped(credential.getUsername()));
						}
					}
				}
				doLoginValidations(ptlUserProfile);
			} else {
				authRes = new AuthResponse();
				authRes.setType(ERROR_SMALL_LETTERS);
				log.error(String.format(ApplicationConstants.AUTHENTICATION_FAILED, credential.getUsername()));
				throw new Exception("Authentication is failed for user " + credential.getUsername());
			}
		} catch (LoginValidateException e) {
			authRes = new AuthResponse();
			authRes.setType(ERROR_SMALL_LETTERS);
			authRes.setMessage(e.getMessage());
			log.error(String.format(ApplicationConstants.AUTHENTICATION_FAILED,
					credential.getUsername() + " Reason --> " + e.getMessage()));
		} catch (Exception e) {
			authRes = new AuthResponse();
			authRes.setType(ERROR_SMALL_LETTERS);
			authRes.setMessage("Authentication is failed for user " + credential.getUsername());
			log.error(String.format(ApplicationConstants.AUTHENTICATION_FAILED, credential.getUsername()));
		}
		return authRes;
	}

	private void doLoginValidations(PtlUserProfile ptlUserProfile) throws LoginValidateException {
		if (ptlUserProfile.getDisableUser() == null || ptlUserProfile.getDisableUser().trim().isEmpty()
				|| ptlUserProfile.getDisableUser().trim().equalsIgnoreCase(YES)) {
			throw new LoginValidateException(LoginValidateException.disableUser(ptlUserProfile.getUserId()));
		} else if (ptlUserProfile.getPswEffectiveDate() == null
				|| ptlUserProfile.getPswEffectiveDate().after(new Date())) {
			throw new LoginValidateException(LoginValidateException.inActive(ptlUserProfile.getUserId()));
		} else if (ptlUserProfile.getPswdExpireDate() == null
				|| ptlUserProfile.getPswdExpireDate().before(new Date())) {
			throw new LoginValidateException(LoginValidateException.expiredUser(ptlUserProfile.getUserId()));
		}
	}

	private void setDefaultView(AuthResponse authRes, PtlUserProfile ptlUserProfile) {
		if (ptlUserProfile.getBrUserId() != null && !ptlUserProfile.getBrUserId().trim().isEmpty()) {
			authRes.setBrUserId(ptlUserProfile.getBrUserId());
		}
		if (ptlUserProfile.getQueryId() != null || ptlUserProfile.getQueryName() != null) {
			authRes.setQueryName(ptlUserProfile.getQueryName());
			authRes.setQueryId(ptlUserProfile.getQueryId());
		}
	}

	private AuthResponse setUserSessionAttributes(Login credential, String validUser) throws Exception {
		log.info("enetered into the method setUserSessionAttributes ");
		AuthResponse authRes = new AuthResponse();
		// generateEncriptionKey();
		String encryptedKey = credential.getUsername() + (new Date()).getTime();
		// encrypt(credential.getUsername() +// new Date());
		PortalUserSession userSessionInfo = new PortalUserSession().setLoginUserId(validUser)
				.setUserTokenId(encryptedKey).setLastInactiveIntervalTime(new Date());
		Map<String, String> roleMap = psrPortalService.getRolesBasedOnUser(validUser);
		authRes.setTokenId(userSessionInfo.getUserTokenId());
		authRes.setLoginUserId(validUser);
		authRes.setGridRowsLimit(gridRowsLimit);
		if (roleMap != null && roleMap.size() > 0) {
			for (Map.Entry<String, String> entry : roleMap.entrySet()) {
				authRes.setRoleName(entry.getValue());
			}
			authRes.setRoleMap(roleMap);
			// now we are getting the first value as a role since we configured only 1 role
			psrPortalService.saveSession(userSessionInfo);
		}
		return authRes;
	}

	private void generateEncriptionKey() throws GeneralSecurityException {
		key = KeyGenerator.getInstance("DES").generateKey();
		ecipher = Cipher.getInstance("DES");
		ecipher.init(Cipher.ENCRYPT_MODE, key);
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	@ResponseStatus
	public ResponseEntity<PSRResponse> logout(@RequestParam String authKey, @RequestParam String validSession)
			throws Exception {
		log.info("enetered into the method logout with path=/logout type=GET ");
		if (PSRHelper.checkEmptyOrNull(authKey)) {
			session.invalidate();
			UserSessionDetails userSessionInfo = psrPortalService.getUserIdFromSession(authKey, null);
			if (userSessionInfo != null && !userSessionInfo.getLoginUserId().trim().isEmpty()) {
				psrPortalService.deleteUserSession(userSessionInfo);
			}
			if (validSession == null || validSession.equalsIgnoreCase("expired")) {
				log.info("your session has been expired , Please login ....!");
				psrResponse.setMessage("your session has been expired , Please login ....!");
				psrResponse.setType("sessionexpired");
				return new ResponseEntity<PSRResponse>(psrResponse, HttpStatus.GATEWAY_TIMEOUT);
			} else if (validSession != null && validSession.equalsIgnoreCase("logout")) {
				log.info("you have sucessfully logout, Please login to continue....!");
				psrResponse.setMessage("you have sucessfully logout, Please login to continue....!");
				psrResponse.setType("logout");
				return new ResponseEntity<PSRResponse>(psrResponse, HttpStatus.OK);
			} else if (validSession != null && validSession.equalsIgnoreCase(ERROR_SMALL_LETTERS)) {
				log.error("an error has been occured while login , Please contact administrator.....!");
				psrResponse.setMessage("an error has been occured while login , Please contact administrator.....!");
				psrResponse.setType(ERROR_SMALL_LETTERS);
				return new ResponseEntity<>(psrResponse, HttpStatus.BAD_REQUEST);
			} else {
				log.error(userSessionInfo.getLoginUserId() + ", You have sucessfully logout.");
				psrResponse.setMessage("you have sucessfully logout.");
				psrResponse.setType("logout");
				return new ResponseEntity<PSRResponse>(psrResponse, HttpStatus.OK);
			}
		}
		else {
			log.error("Token Id is missed/null in the request.");
			psrResponse.setMessage("you have sucessfully logout.");
			psrResponse.setType("logout");
			return new ResponseEntity<PSRResponse>(psrResponse, HttpStatus.OK);
		}
	}


	// decoding the user credentials
	public Login authenticate(String credential) throws Exception {
		log.info("enetered into the method authenticate ");
		Login login = null;
		if (null == credential) {
			return login;
		}
		// header value format will be "Basic encodedstring" for Basic
		// authentication. Example "Basic YWRtaW46YWRtaW4="
		// Decoder decoder = Base64.getDecoder();
		// byte[] decodedByte = decoder.decode(credential);
		// String usernameAndPassword = new String(decodedByte);
		// final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword,
		// ":");
		final StringTokenizer tokenizer = new StringTokenizer(credential, ":");
		// tokenizer.nextToken();
		final String username = tokenizer.nextToken();
		final String password = tokenizer.nextToken();
		// call some UserService/LDAP here
		return new Login().setUsername(username).setPassword(password);
	}

	public static String encrypt(String str) {
		try {
			// encode the string into a sequence of bytes using the named charset
			// storing the result into a new byte array.
			byte[] utf8 = str.getBytes(StandardCharsets.UTF_8);
			byte[] enc = ecipher.doFinal(utf8);
			// encode to base64
			enc = BASE64EncoderStream.encode(enc);
			return new String(enc);
		} catch (Exception e) {
			log.error(String.format("Error occured: %s", e.getMessage()));
		}
		return null;
	}

	public static String decrypt(String str) {
		try {
			// decode with base64 to get bytes
			byte[] dec = BASE64DecoderStream.decode(str.getBytes());
			byte[] utf8 = dcipher.doFinal(dec);
			// create new string based on the specified charset
			return new String(utf8, StandardCharsets.UTF_8);
		} catch (Exception e) {
			log.error(String.format("Error occured: %s", e.getMessage()));
		}
		return null;
	}
	
}
