package com.igt.psr.rest.resource;

import static com.igt.psr.common.constants.ApplicationConstants.SAVE_SUCCESS;
import static com.igt.psr.common.constants.ApplicationConstants.SUCCESS;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.igt.psr.common.constants.ApplicationConstants;
import com.igt.psr.common.constants.ApplicationErrorConstants;
import com.igt.psr.common.util.PSRResponse;
import com.igt.psr.db.model.PsrMatchCriteria;
import com.igt.psr.rest.common.view.PsrSuperResource;
import com.igt.psr.rest.service.EventService;
import com.igt.psr.ui.view.EventModel;
import com.igt.psr.ui.view.EventModelName;
import com.igt.psr.ui.view.PsrEventForm;
import com.igt.psr.ui.view.PsrEventModelView;
import com.igt.psr.ui.view.UserCustimozedView;

@RestController
@RequestMapping("/rest")
public class EventResource extends PsrSuperResource {
	private static final Logger log = LoggerFactory.getLogger(EventResource.class);

	@Autowired
	private EventService eventService;
	PSRResponse psrResponse = new PSRResponse();

	@PostMapping("/addeventmaster")
	public PSRResponse addEventMaster(@RequestBody PsrEventForm psrEvent) throws JSONException {
		log.info(String.format(" entered into method addEventMaster ,path=/ type=post %s", psrEvent.toString()));
		try {
			eventService.addEventMaster(psrEvent);
			psrResponse.setType(ApplicationConstants.SUCCESS_SMALL_LETTERS);
			psrResponse.setMessage(ApplicationConstants.SAVE_SUCCESS);

		} catch (Exception e) {
			psrResponse.setType(ApplicationConstants.ERROR_SMALL_LETTERS);
			psrResponse.setMessage("Error occurred while saving in Event code");
			log.error(String.format(ApplicationConstants.ERROR_OCCURED, e.getMessage()));
		}
		return psrResponse;
	}

	// Jagadish
	@PostMapping("/addeventmodel")
	public PSRResponse addEventModel(@RequestParam String modelName,@RequestParam String loginUserId,
			@RequestBody List<PsrEventModelView> psrEventModelDetails) throws JSONException {
		log.info(String.format(" entered into method addEventModel ,path=/ type=post %s",
				psrEventModelDetails.toString()));
		try {
			eventService.addEventModelDetails(modelName, psrEventModelDetails,loginUserId);
			psrResponse.setType(ApplicationConstants.SUCCESS_SMALL_LETTERS);
			psrResponse.setMessage(ApplicationConstants.SAVE_SUCCESS);
		} catch (Exception e) {
			psrResponse.setType(ApplicationConstants.ERROR_SMALL_LETTERS);
			psrResponse.setMessage("Error occured while saving the data , please contact the Administrator . ");
			log.error(String.format(ApplicationConstants.ERROR_OCCURED, e.getMessage()));

		}
		return psrResponse;
	}

	@DeleteMapping("/deleteEventMaster")
	public PSRResponse deleteEventMaster(@RequestParam String eventcode) throws JSONException {
		log.info(String.format(" entered into method deleteEventMaster ,path=/ type=delete %s ", eventcode));
		try {
			eventService.deleteEventMaster(eventcode);
			psrResponse.setType(ApplicationConstants.SUCCESS_SMALL_LETTERS);
			psrResponse.setMessage("Deleted Successfully");
		} catch (Exception e) {
			psrResponse.setType(ApplicationConstants.ERROR_SMALL_LETTERS);
			psrResponse.setMessage("Error occured while deleting the data , please contact the Administrator . ");
			log.error(String.format(ApplicationConstants.ERROR_OCCURED, e.getMessage()));

		}
		return psrResponse;
	}

	// Jagadish
	@PostMapping("/addEventModelsToCriteria/{modelName}")
	public PSRResponse addEventModelsToCriteria(@PathVariable(value = "modelName") String modelName,
			@RequestBody List<PsrMatchCriteria> matchList) throws JSONException {
		log.info(String.format(
				"enter into the method addEventModelsToCriteria , path=/addEventModelsToCriteria , type=post %s",
				modelName));
		try {
			eventService.addEventModelsToCriteria(modelName, matchList);
			psrResponse.setType(ApplicationConstants.SUCCESS_SMALL_LETTERS);
			psrResponse.setMessage(ApplicationConstants.SAVE_SUCCESS);
		} catch (Exception e) {
			psrResponse.setType(ApplicationConstants.ERROR_SMALL_LETTERS);
			psrResponse.setMessage("Duplicate model name.");
			log.error(String.format(ApplicationConstants.ERROR_OCCURED, e.getMessage()));

		}
		return psrResponse;
	}

	@PostMapping("/addEventModel/{modelName}")
	public String addSingleEventModelToCriteria(@PathVariable(value = "modelName") String modelName,
			@RequestBody PsrMatchCriteria eventModel) throws Exception {
		log.info(String.format(
				"enter into the method addSingleEventModelToCriteria , path=/addEventModel , type=post %s", modelName));
		try {
			eventService.addSingleEventModelsToCriteria(modelName, eventModel);
			return ApplicationConstants.SUCCESS_SMALL_LETTERS;
		} catch (Exception e) {

			log.error(String.format(ApplicationConstants.ERROR_OCCURED, e.getMessage()));
			return ApplicationConstants.ERROR_SMALL_LETTERS;
		}
	}

	@PostMapping("/addCustomizedView")
	public PSRResponse addCustomizedView(@RequestParam String loginUserId, @RequestParam String queryName,
			@RequestParam String checkboxValue, @RequestParam String selectedCheckbox, @RequestBody String columnReorder) throws JSONException {
		log.info("enter into the method addCustomizedView , path=/addCustomizedView , type=post "
				+ selectedCheckbox.toString() + "QueryName ::" + queryName + " CheckBoxValue =" + checkboxValue);
		try {
			//String commaSeparatedVals = String.join(",", selectedCheckbox);
			UserCustimozedView customizedView = new UserCustimozedView().setQueryName(queryName)
					.setSelectedColumns(selectedCheckbox).setLoginUserId(loginUserId).setPublicView(checkboxValue).setColumnReorder(columnReorder);
			int viewExists = eventService.addCustomizedView(customizedView);
			if (viewExists < 0) {
				psrResponse.setType(ApplicationConstants.ERROR_SMALL_LETTERS);
				psrResponse.setMessage(ApplicationErrorConstants.QUERY_NAME);
			} else {
				psrResponse.setType(ApplicationConstants.SUCCESS_SMALL_LETTERS);
				psrResponse.setMessage(ApplicationErrorConstants.QUERY_SAVE_SUCCESS);
			}
		} catch (Exception e) {
			psrResponse.setType(ApplicationConstants.ERROR_SMALL_LETTERS);
			log.error(String.format(ApplicationConstants.ERROR_OCCURED, e.getMessage()));

		}
		return psrResponse;
	}

	@DeleteMapping("/deleteEventModel")
	public PSRResponse deleteEventModel(@RequestParam String modelName) {
		log.info(String.format(" entered into method deleteEventModel ,path=/ type=post %s", modelName));
		try {
			int count = eventService.deleteEventModel(modelName);
			if (count == 0) {
				psrResponse.setType("auto-attach-error");
				psrResponse.setMessage("Model is auto-attached. Hence cannot be deleted");
			} else {
				psrResponse.setType(ApplicationConstants.SUCCESS_SMALL_LETTERS);
				psrResponse.setMessage("Deleted Successfully");
			}
		} catch (Exception e) {
			psrResponse.setType(ApplicationConstants.ERROR_SMALL_LETTERS);
			psrResponse.setMessage("Error occured while deleting the data , please contact the Administrator . ");

			log.error(String.format(ApplicationConstants.ERROR_OCCURED, e.getMessage()));
		}
		return psrResponse;
	}

	@GetMapping("/getModelNames")
	public List<EventModelName> getModelNames(@RequestParam String modelName, String eventModelSearch)
			throws Exception {
		log.info(String.format("enter into the method getModelNames , path=/ type=get %s",
				modelName + eventModelSearch));
		return eventService.getModelNames(modelName, eventModelSearch);
	}

	@GetMapping("/eventMaster")
	public List<PsrEventForm> getEventMaster(@RequestParam String eventcode, String eventcodeselect) throws Exception {
		log.info(String.format("enter into the method getEventMaster , path=/ type=get %s",
				eventcode + eventcodeselect));
		return eventService.getEventMaster(eventcode, eventcodeselect);
	}

	// Jagadish -- get modelName -- from match criteria to Event model screen
	@GetMapping("/eventModelDetails")
	public List<PsrEventModelView> getEventModelDetails(@RequestParam String modelName) throws Exception {
		log.info("enter into the method getModelDetails, path=/ type=get ");
		return eventService.getModelDetails(modelName);
	}

	@GetMapping("/eventCodeDetails")
	public List<PsrEventForm> getEventCodeDetails(@RequestParam String eventcode) throws Exception {
		log.info("enter into the method eventCodeDetails, path=/ type=get ");
		return eventService.getEventCodeDetails(eventcode);
	}

	/* get FieldValue and MatchCase */
	@GetMapping("/eventMatchCriteria")
	public List<EventModel> getEventMatchCriteria(@RequestParam String modelName) throws Exception {
		log.info("enter into the method eventMatchCriteria, path=/ type=get ");
		return eventService.getEventMatchCriteria(modelName);
	}
	
	@PostMapping("/disableModel")
	public PSRResponse disableModel(@RequestParam String loginUserId, @RequestParam String modelName,  @RequestParam Boolean status) {
		log.info(" enter into the method disableModel , path=/recalculate , type=post ");
		try {
			log.info(String.format(" modelName --> %s ", modelName));
			log.info(String.format(" status --> %s ", status));
			log.info(String.format(" loginUser --> %s ", loginUserId));
			
			eventService.disableModel(status,modelName);
			psrResponse.setType(SUCCESS);
			psrResponse.setMessage(SAVE_SUCCESS);
		} catch (Exception e) {
			psrResponse.setType(ApplicationConstants.ERROR_SMALL_LETTERS);
			psrResponse.setMessage("Error occured while disabling the model changes, please contact the Administrator . ");
			log.error(String.format(ApplicationConstants.ERROR_OCCURED, e.getMessage()));
		}
		return psrResponse;
	}

}
