package com.igt.psr.rest.resource;

import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/")
public class CommonResource {

	private static final Logger log = LoggerFactory.getLogger(CommonResource.class);

	@Value("${mgf.report.url1}")
	public String reportURL1;
	@Value("${mgf.report.url2}")
	public String reportURL2;
	@Value("${mgf.report.url3}")
	public String reportURL3;
	@Value("${mgf.report.url4}")
	public String reportURL4;
	@Value("${mgf.report.url5}")
	public String reportURL5;
	@Value("${mgf.report.url6}")
	public String reportURL6;
	@Value("${mgf.report.url7}")
	public String reportURL7;
	@Value("${mgf.report.url8}")
	public String reportURL8;
	@Value("${mgf.report.url9}")
	public String reportURL9;
	@Value("${mgf.report.url10}")
	public String reportURL10;
	@Value("${mgf.report.url11}")
	public String reportURL11;
	@Value("${mgf.report.url12}")
	public String reportURL12;
	@Value("${mgf.report.url13}")
	public String reportURL13;
	
	@GetMapping("getReportUrls")
	public Map<String, String> getReportUrls() {
		log.info(" Enetered to getReportUrl");
		Map<String, String> reportURLs = new LinkedHashMap<>();
		reportURLs.put("report1", reportURL1);
		reportURLs.put("report2", reportURL2);
		reportURLs.put("report3", reportURL3);
		reportURLs.put("report4", reportURL4);
		reportURLs.put("report5", reportURL5);
		reportURLs.put("report6", reportURL6);
		reportURLs.put("report7", reportURL7);
		reportURLs.put("report8", reportURL8);
		reportURLs.put("report9", reportURL9);
		reportURLs.put("report10", reportURL10);
		reportURLs.put("report11", reportURL11);
		reportURLs.put("report12", reportURL12);
		reportURLs.put("report13", reportURL13);
		return reportURLs;
	}
}
