package com.igt.psr.rest.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.igt.psr.common.constants.ApplicationConstants;
import com.igt.psr.common.util.DAOHelper;
import com.igt.psr.common.util.UIViewHelper;
import com.igt.psr.dao.EventDao;
import com.igt.psr.dao.PsrDao;
import com.igt.psr.db.model.PsrEventModel;
import com.igt.psr.db.model.PsrMatchCriteria;
import com.igt.psr.db.model.PsrModelMaster;
import com.igt.psr.rest.service.EventService;
import com.igt.psr.ui.view.EventModel;
import com.igt.psr.ui.view.EventModelName;
import com.igt.psr.ui.view.PsrEventForm;
import com.igt.psr.ui.view.PsrEventModelView;
import com.igt.psr.ui.view.UserCustimozedView;

@Service
public class EventServiceImpl implements EventService {

	private static final Logger log = LoggerFactory.getLogger(EventServiceImpl.class);

	@Autowired
	private EventDao eventDao;
	@Autowired
	private PsrDao psrDao;

	@Override
	public void addEventMaster(PsrEventForm psrEvent) throws Exception {
		Integer isEventCodeExists = isEventCodeAlreadyExists(psrEvent.getEventcode());
		if (isEventCodeExists != 0) {
			eventDao.addPsrEventModel(psrEvent);
			eventDao.addEventMaster(DAOHelper.getPsrEventMasterUpdate(psrEvent));
		} else {
			eventDao.addEventMaster(DAOHelper.getPsrEventMasterSave(psrEvent));
		}
	}

	private Integer isEventCodeAlreadyExists(String eventCode) throws Exception {
		return eventDao.isEventCodeAlreadyExists(eventCode);
	}

	// Jagadish EVENT MODEL SAVE
	@Override
	public void addEventModelDetails(String modelName, List<PsrEventModelView> psrEventDetails,String loginUserId) throws Exception {
		log.info(String.format("  psrEventDetails : %s",
				psrEventDetails.toString() + "   size : " + psrEventDetails.toString()));
		// To do if possible we will bring model id from the UI Grid so that we can
		// avoid the db call to get the model id
		int modelId = eventDao.getModelId(modelName);
		for (PsrEventModelView psrEventDetail : psrEventDetails) {			
			PsrEventModel pem = DAOHelper.getPsrEventModelDetails(psrEventDetail, modelId,loginUserId);
			eventDao.addEventModel(pem);
		}

	}

	// Jagadish Method to handle match criteria(match_case and filed_value)
	@Override
	public void addEventModelsToCriteria(String modelName, List<PsrMatchCriteria> matchCriteriaList) throws Exception {
		log.info(String.format("  eventModels : %s",
				matchCriteriaList.toString() + "   size : " + matchCriteriaList.size()));
		List<PsrMatchCriteria> pmcList = new ArrayList<>();
		PsrModelMaster pmm = null;
		boolean isModelNameExists = isModelNameAlreadyExists(modelName);
		if (isModelNameExists) {
			eventDao.deleteModelFromMatchCriteria(modelName);
		}
		if (matchCriteriaList != null && !matchCriteriaList.isEmpty()) {
			pmm = DAOHelper.getPsrModelMaster(modelName);
			for (PsrMatchCriteria matchList : matchCriteriaList) {
				pmcList.add(DAOHelper.getPsrMatchCriteria(modelName, matchList));
			}
			eventDao.addEventModelsToCriteria(pmcList, pmm);
		}
	}

	private boolean isModelNameAlreadyExists(String modelName) throws Exception {
		return eventDao.isModelNameAlreadyExists(modelName);
	}

	@Override
	public void deleteEventMaster(String eventCode) throws Exception {
		eventDao.deleteEventMaster(eventCode);
	}

	@Override
	public void addSingleEventModelsToCriteria(String modelName, PsrMatchCriteria matchList) throws Exception {
		PsrModelMaster pmm = DAOHelper.getPsrModelMaster(modelName);
		PsrMatchCriteria pmc = DAOHelper.getPsrMatchCriteria(modelName, matchList);
		eventDao.addSingleEventModelsToCriteria(pmm, pmc);
	}

	@Override
	public int deleteEventModel(String modelName) throws Exception {
		return eventDao.deleteEventModel(modelName);
	}

	@Override
	public int addCustomizedView(UserCustimozedView customizedView) throws Exception {
		if (customizedView.getQueryName() != null && customizedView.getQueryName().contains(" (Public)")) {
			customizedView.setQueryName(customizedView.getQueryName()
					.substring(0, customizedView.getQueryName().lastIndexOf(" (Public)")).trim());
		}
		Integer queryId = psrDao.getQueryId(customizedView.getLoginUserId(), customizedView.getQueryName());
		if (queryId == null) {
			return eventDao.addCustomizedView(DAOHelper.getPsrUserCustimozedView(customizedView));
		}
		return ApplicationConstants.MINUS_ONE;
	}

	@Override
	public List<EventModelName> getModelNames(String modelName, String eventModelSearch) throws Exception {
		log.info("enter into the method getModelNames event service ");
		return eventDao.getModelNames(modelName, eventModelSearch);

	}

	@Override
	public List<PsrEventForm> getEventMaster(String eventCode, String eventcodeselect) throws Exception {
		log.info("enter into the method getEventMaster event service ");
		return UIViewHelper.getEventMasterForm(eventDao.getEventMaster(eventCode, eventcodeselect));
	}

	// Jagadish -- get modelName -- from match criteria to Event model screen
	@Override
	public List<PsrEventModelView> getModelDetails(String modelDetails) throws Exception {
		log.info("enter into the method getModelDetails model details service ");
		return DAOHelper.getModeldetailsForm(eventDao.getModelDetails(modelDetails));
	}

	@Override
	public List<PsrEventForm> getEventCodeDetails(String eventDetails) throws Exception {
		log.info("enter into the method getEventCodeDetails eventCode details service ");
		return DAOHelper.getEventCodeDetailsForm(eventDao.getEventCodeDetails(eventDetails));
	}

	@Override
	public List<EventModel> getEventMatchCriteria(String modelName) throws Exception {
		log.info("enter into the method getEventMatchCriteria Service");
		return DAOHelper.getMatchCriteriaForm(eventDao.getEventMatchCriteria(modelName));
	}
	
	@Override
	public void disableModel(Boolean status, String modelName) throws Exception {
		log.info("enter into the method disableModel eventCode details service ");
		eventDao.updateDisableModel(status, modelName);

	}

}
