package com.igt.psr.rest.resource;

import static com.igt.psr.common.constants.ApplicationConstants.ERROR;
import static com.igt.psr.common.constants.ApplicationConstants.FAILURE;
import static com.igt.psr.common.constants.ApplicationConstants.SAVE_SUCCESS;
import static com.igt.psr.common.constants.ApplicationConstants.SUCCESS;
import static com.igt.psr.common.constants.ApplicationConstants.getCurrentDateWithTime;
import com.igt.psr.db.dto.PsrSaveSearchDto;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.URLDecoder;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.igt.psr.common.constants.ApplicationConstants;
import com.igt.psr.common.exception.ImageException;
import com.igt.psr.common.util.AvailableViewRes;
import com.igt.psr.common.util.PSRHelper;
import com.igt.psr.common.util.PSRResponse;
import com.igt.psr.common.util.UIViewHelper;
import com.igt.psr.jaxb.model.Document;
import com.igt.psr.rest.common.view.PsrSuperResource;
import com.igt.psr.rest.service.PSRService;
import com.igt.psr.rest.service.TsamService;
import com.igt.psr.ui.view.ChangeTrack;
import com.igt.psr.ui.view.MileStoneWithCode;
import com.igt.psr.ui.view.PostXmlInputParams;
import com.igt.psr.ui.view.PsrDynamicData;
import com.igt.psr.ui.view.PsrUpdateView;
import com.igt.psr.ui.view.SearchForm;
import com.igt.psr.ui.view.SmartTagDetails;
import com.igt.psr.db.repo.PsrSaveSearchRepository;

@ConfigurationProperties
@RestController
@RequestMapping("/rest")
public class PsrResource extends PsrSuperResource {

	private static final Logger log = LoggerFactory.getLogger(PsrResource.class);

	@Value("${image.url}")
	public String imageurl;
	@Value("${mgf.owner}")
	public String owner;

	@Value("${mgf.bamboorose.wbServiceURL}")
	public String wbServiceURL;
	@Value("${mgf.bamboorose.username}")
	public String bambooroseUser;
	@Value("${mgf.bamboorose.password}")
	public String bamboorosePwd;

	@Autowired
	private PSRService psrService;
	PSRResponse psrResponse = new PSRResponse();
	AvailableViewRes availableViewRes;
	@Autowired
	private TsamService tsamService;

	@Autowired
	PsrSaveSearchRepository psrSaveSearchRepository;

	@GetMapping("/customisedViews")
	public List getCustomisedViews(@RequestParam String loginUserId) throws Exception {
		log.info("enter into the method getCustomisedViews , path=/getCustomisedViews , type=get ");
		return psrService.getCustomisedViews(loginUserId);
	}

	@PostMapping("/updateOrders")
	public PSRResponse update(@RequestParam String loginUserId, @RequestParam String roleName,
			@RequestParam String searchTs, @RequestBody List<PsrUpdateView> updateOrders) {
		log.info(String.format(" enter into the method update , path=/updateOrders , type=post  searchTs %s ",
				searchTs));
		try {
			String message = psrService.updateOrders(updateOrders, imageurl, loginUserId, roleName, searchTs);
			if (message != null && !message.isEmpty() && message.equalsIgnoreCase(SUCCESS)) {
				psrResponse.setType(ApplicationConstants.SUCCESS_SMALL_LETTERS);
				psrResponse.setMessage(SAVE_SUCCESS);
			} else {
				psrResponse.setType(ApplicationConstants.ERROR_SMALL_LETTERS);
				psrResponse.setMessage(FAILURE);
			}
		} catch (ImageException e) {
			psrResponse.setType(ApplicationConstants.ERROR_SMALL_LETTERS);
			psrResponse.setMessage("Unable to read the image, It looks like corrupted.");
			log.error(String.format(ApplicationConstants.ERROR_OCCURED, e.getMessage()));
		} catch (Exception e) {
			psrResponse.setType(ApplicationConstants.ERROR_SMALL_LETTERS);
			if (e.getMessage() != null && e.getMessage().contains("This is failed due to deadlock situation")) {
				psrResponse.setMessage("This is failed due to deadlock situation, please try after sometime");
			} else {
				psrResponse.setMessage(ApplicationConstants.ERROR_OCCURED_WHILE_SAVE_DATA);
			}
			log.error(String.format(ApplicationConstants.ERROR_OCCURED, e.getMessage()));

		}
		return psrResponse;
	}

	@PostMapping("/recalculate")
	public PSRResponse doReCalculate(@RequestParam String loginUserId, @RequestBody List<String> orderIds) {
		log.info(" enter into the method doReCalculate , path=/recalculate , type=post ");
		try {
			String commaSepratedOrderIds = String.join(",", orderIds);
			log.info(String.format(" commaSepratedOrderIds --> %s ", commaSepratedOrderIds));
			psrService.doReCalculate(loginUserId, commaSepratedOrderIds);
			psrResponse.setType(SUCCESS);
			psrResponse.setMessage(SAVE_SUCCESS);
		} catch (Exception e) {
			psrResponse.setType(ApplicationConstants.ERROR_SMALL_LETTERS);
			if (e.getMessage() != null && e.getMessage().contains("This is failed due to deadlock situation")) {
				psrResponse.setMessage("This is failed due to deadlock situation, please try after sometime");
			} else {
				psrResponse.setMessage(ApplicationConstants.ERROR_OCCURED_WHILE_SAVE_DATA);
			}
			log.error(String.format(ApplicationConstants.ERROR_OCCURED, e.getMessage()));
		}
		return psrResponse;
	}

	@PostMapping("/posttobamboorose")
	public PSRResponse postDataToBamboorose(@RequestBody List<PostXmlInputParams> postXmlInputParams)
			throws JAXBException, DatatypeConfigurationException, ParseException {
		log.info(String.format(" entered into method postDataToBamboorose ,path=/posttobamboorose type=post %s",
				postXmlInputParams));
		try {
			Map<String, List<String>> rowNos = tsamService.getRowNoFromBrose(postXmlInputParams);
			if (rowNos != null && rowNos.size() > 0) {
				JAXBContext jaxbContext = JAXBContext.newInstance(Document.class);
				Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
				Document document = UIViewHelper.getDocumentObject(owner, rowNos);
				/* set this flag to true to format the output */
				jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				StringWriter w = new StringWriter();
				jaxbMarshaller.marshal(document, w);
				boolean isChangeColorFlag = this.getBrosePostOrderResponseXML(wbServiceURL,
						w.toString().replaceAll("\"", "\'").replaceAll("\n", ""), bambooroseUser, bamboorosePwd);
				log.info(String.format(" XML is updated to Bamboorose sucessfully or not -->  %s", isChangeColorFlag));
				if (isChangeColorFlag == true) {
					int count = psrService.updateTheColorChangeFlag(postXmlInputParams);
					log.info(String.format(" No of records update is --> %s", count));
					psrResponse.setType(SUCCESS);
					psrResponse.setMessage("Posted Successfully");
				} else {
					psrResponse.setType(ApplicationConstants.ERROR_SMALL_LETTERS);
					psrResponse.setMessage(" Failed to post data on bamboorose, please contact the Administrator.");
				}
			} else {
				psrResponse.setType(ApplicationConstants.ERROR_SMALL_LETTERS);
				psrResponse.setMessage(" These is no row Numbers available in Bamboorose to post the data. ");
			}
		} catch (Exception e) {
			psrResponse.setType(ApplicationConstants.ERROR_SMALL_LETTERS);
			psrResponse.setMessage(ApplicationConstants.ERROR_OCCURED_WHILE_SAVE_DATA);
			log.error(String.format(ApplicationConstants.ERROR_OCCURED, e.getMessage()));

		}
		return psrResponse;
	}

	private boolean getBrosePostOrderResponseXML(String wbServiceURL2, String xml, String bambooroseUser,
			String bamboorosePwd) throws Exception {
		log.info(" entered into the method testCommandPromt ");
		String path = this.getClass().getClassLoader().getResource("").getPath();
		String fullPath = URLDecoder.decode(path, "UTF-8");
		String pathArr = fullPath.replace("classes", "lib").replaceFirst("/", "");
		boolean isChangeColorFlag = false;
		log.info(String.format(" wsclient library path --> %s", pathArr));
		String command = "java -jar " + pathArr + "wsclient-1.0.jar " + wbServiceURL2 + " " + bambooroseUser + " "
				+ bamboorosePwd + " \"" + xml + "\"";
		log.info(String.format(" Java Command to call Bamboorose webservice ---> %s", command));
		StringBuilder output = new StringBuilder();
		try {
			Process process = Runtime.getRuntime().exec(command);
			BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				if (line.contains(SUCCESS)) {
					isChangeColorFlag = true;
				}
				output.append("\n " + line);
				log.info(line);
			}
			reader.close();
		} catch (Exception e) {
			log.error(String.format(" An error occured whie calling Bamboorose Web Service %s", e.getMessage()));

		}
		return isChangeColorFlag;
	}

	@GetMapping("/search")
	public List<Map> search(@Valid SearchForm searchForm) throws Exception {
		log.info(String.format(" entered into method create ,path=/search type=get %s", searchForm.toString()));
		try {
			return psrService.getSearchInfo(searchForm);
		} catch (Exception e) {
			log.error(String.format("Error occured in search: %s ", e.getMessage()));
			throw e;
		}
	}

	@GetMapping("/psrDynamicData")
	public PsrDynamicData getPsrDynamicGridData(@Valid SearchForm searchForm) throws Exception {
		log.info("enter into the method getPsrDynamicGridData , path=/getPsrDynamicGridData , type=get ");
		List<Map> map = psrService.getPsrDynamicGridData(searchForm);
		Integer count = getPsrDynamicGridDataCount(searchForm);
		return new PsrDynamicData().setTotalRows(count).setMap(map).setSearchTs(getCurrentDateWithTime());
	}

	@GetMapping("/psrDataCount")
	public Integer getPsrDynamicGridDataCount(@Valid SearchForm searchForm) throws Exception {
		log.info("enter into the method getPsrDynamicGridDataCount , path=/psrDataCount , type=get ");
		try {
			return psrService.getPsrDynamicGridDataCount(searchForm);
		} catch (Exception e) {
			log.error(String.format(" Error occured e : %s", e.getMessage()));
			throw e;
		}
	}

	@GetMapping("/eventDescriptions")
	public List<String> getEventDescriptions(@RequestParam String brUserId, @RequestParam String loginUserId,
			@RequestParam String roleName) throws Exception {
		log.info("enter into the method getEventDescriptions , path=/eventDescriptions , type=get ");
		return psrService.getEventDescriptions(brUserId, loginUserId, roleName);
	}

	@GetMapping("/eventMileStone")
	public List<MileStoneWithCode> getEventMileStones(@RequestParam String brUserId, @RequestParam String loginUserId,
			@RequestParam String roleName) throws Exception {
		log.info("enter into the method getEventMileStones , path=/getEventMileStones , type=get ");
		List<MileStoneWithCode> milestoneWithCodes = null;
		Map<String, String> milestones = psrService.getEventMileStones(brUserId, loginUserId, roleName);
		if (milestones != null && milestones.size() > 0) {
			milestoneWithCodes = new ArrayList<>();
			for (Map.Entry<String, String> entry : milestones.entrySet()) {
				milestoneWithCodes
						.add(new MileStoneWithCode().setEventcode(entry.getValue()).setEventDesc(entry.getKey()));
			}
		}
		return milestoneWithCodes;

	}

	@GetMapping("/selectedColumnView")
	public List<String> getSelectedColumnView(@RequestParam String queryName, @RequestParam String loginUserId)
			throws Exception {
		log.info("enter into the method selectedColumnView , path=/selectedColumnView , type=get ");
		return psrService.getSelectedColumnView(queryName, loginUserId);
	}
	
	@GetMapping("/reOrderColumn")
	public List<String> getReOrderColumn(@RequestParam String queryName, @RequestParam String loginUserId)
			throws Exception {
		log.info("enter into the method selectedColumnView , path=/selectedColumnView , type=get ");
		return psrService.getReOrderColumn(queryName, loginUserId);
	}

	@DeleteMapping("/removeImage")
	public PSRResponse removeImage(@RequestParam String imageName, @RequestParam String loginUserId,
			@RequestParam String roleName, @RequestParam String searchTs) throws Exception {
		log.info(" entered into method removeImage ,path=/ type=delete ");
		try {
			String psrOrderId = imageName.substring(imageName.lastIndexOf('_') + 1, imageName.lastIndexOf('.'));
			psrService.deleteImage(psrOrderId, loginUserId, roleName, searchTs);
			PSRHelper.deleteImage(imageurl + imageName);
			psrResponse.setType(SUCCESS);
			psrResponse.setMessage("Image removed Successfully");
			return psrResponse;
		} catch (Exception e) {
			psrResponse.setType(ApplicationConstants.ERROR_SMALL_LETTERS);
			psrResponse.setMessage("Image not removed Successfully");
			log.error(String.format(" Error occured e : %s", e.getMessage()));
			return psrResponse;
		}
	}

	@GetMapping("/changeTrackDetails")
	public Map<String, LinkedHashSet<ChangeTrack>> getChangeTrackDetails(@RequestParam String psrOrderIds,
			@RequestParam String from, @RequestParam String searchKeyword, @RequestParam String fromDate,
			@RequestParam String toDate) throws Exception {
		log.info("enter into the method getChangeTrackDetails , path=/changeTrackDetails , type=get ");
		return psrService.getChangeTrackDetails(psrOrderIds, from, searchKeyword, fromDate, toDate);
	}

//	@GetMapping("/export")
//	public List<Map> getExportData(@Valid SearchForm searchForm, @RequestParam String loginUserId,
//			@RequestParam String queryId) throws Exception {
//		log.info("enter into the method getExportData , path=/export , type=get ");
//		return psrService.getPsrExportAllData(searchForm, queryId);
//	}

	@DeleteMapping("/deleteAvailableView")
	public PSRResponse deleteAvailableView(@RequestParam String queryId) throws Exception {
		log.info("enter into the method deleteAvailableView , path=/deleteAvailableView, type=delete ");
		try {
			psrService.deleteAvailableView(queryId);
			psrResponse.setType(SUCCESS);
			psrResponse.setMessage("Deleted Successfully");
			return psrResponse;
		} catch (Exception e) {
			psrResponse.setType(ApplicationConstants.ERROR_SMALL_LETTERS);
			psrResponse.setMessage("Availble view not Deleted Successfully");
			log.error(String.format(" Error occured in deleteAvailableView : %s", e.getMessage()));
			return psrResponse;
		}
	}

	@PostMapping("/updateAvailableView")
	public PSRResponse updateAvailableView(@RequestParam String queryId, @RequestParam String checkBoxValue,
			@RequestBody String columnReorder, @RequestParam String loginUserId, @RequestParam String selectedcheckbox) throws Exception {
		log.info("enter into the method updateAvailableView , path=/updateAvailableView, type=update ");
		try {
			String commaSeparatedVals = columnReorder;
			availableViewRes = new AvailableViewRes();
			availableViewRes.setQueryName(
					psrService.updateAvailableView(queryId,selectedcheckbox, checkBoxValue,commaSeparatedVals, loginUserId));
			availableViewRes.setType(SUCCESS);
			availableViewRes.setMessage("updated Successfully");
			return availableViewRes;
		} catch (Exception e) {
			psrResponse.setType(ERROR);
			psrResponse.setMessage("Availble view not updated Successfully");
			log.error(String.format(" Error occured in updateAvailableView : %s", e.getMessage()));
			return psrResponse;
		}
	}

	@PutMapping("/updateAcknowledge")
	public PSRResponse updateAcknowledge(@RequestBody String psrOrderIds) throws Exception {
		log.info(" enter into the method updateAcknowledge , path=/updateAcknowledge, type=Put with updateAcknowledge");
		try {
			psrService.updateAcknowledge(psrOrderIds);
			psrResponse.setType(ApplicationConstants.SUCCESS);
			psrResponse.setMessage("Acknowledgement Success.");
			return psrResponse;
		} catch (Exception e) {
			psrResponse.setType(ApplicationConstants.ERROR);
			psrResponse.setMessage("Error occurred while updating Acknowledgement");
			log.error(String.format(" Error occured in updateAcknowledge : %s", e.getMessage()));
			return psrResponse;
		}
	}

	@PostMapping("/saveSearch")
	public PSRResponse saveSearch(@RequestParam String loginUserId, @RequestBody PsrSaveSearchDto psrSaveSearchDto)
			throws Exception {
		log.info("enter into the method saveSearch , path=/saveSearch, type=post");
		try {
			psrService.saveSearch(psrSaveSearchDto, loginUserId);
			psrResponse.setType(ApplicationConstants.SUCCESS);
			psrResponse.setMessage("saveSearch Success.");
			return psrResponse;
		} catch (Exception e) {
			psrResponse.setType(ApplicationConstants.ERROR);
			psrResponse.setMessage("Error occurred while save Search");
			log.error(String.format(" Error occured in saveSearch : %s", e.getMessage()));
			return psrResponse;
		}
	}

	@GetMapping("/saveSearch")
	public PsrSaveSearchDto getSavedSearchDataById(@RequestParam String loginUserId) throws Exception {
		log.info("enter into the method SavedSearchData , path=/saveSearch , type=get ");
		PsrSaveSearchDto view = psrService.getSavedSearchById(loginUserId);
		if (view.equals(null)) {
			throw new Exception();
		}
		return view;
	}
	
	@PostMapping("/recalcModel")
	public PSRResponse recalcModel(@RequestParam String loginUserId, @RequestParam String modelName) {
		log.info(" enter into the method doReCalculate , path=/recalculate , type=post ");
		try {
			log.info(String.format(" modelName --> %s ", modelName));
			psrService.recalcModel(loginUserId,modelName);
			psrResponse.setType(SUCCESS);
			psrResponse.setMessage(SAVE_SUCCESS);
		} catch (Exception e) {
			psrResponse.setType(ApplicationConstants.ERROR_SMALL_LETTERS);
			if (e.getMessage() != null && e.getMessage().contains("This is failed due to deadlock situation")) {
				psrResponse.setMessage("This is failed due to deadlock situation, please try after sometime");
			} else {
				psrResponse.setMessage(ApplicationConstants.ERROR_OCCURED_WHILE_SAVE_DATA);
			}
			log.error(String.format(ApplicationConstants.ERROR_OCCURED, e.getMessage()));
		}
		return psrResponse;
	}
	
	@GetMapping("/getSmartTagRecords")
	public Map<String, String>getSmartTagRecords(@RequestParam String requestNo, @RequestParam String style, @RequestParam String colorWashName, @RequestParam String profomaPo) throws Exception {
		log.info("enter into the method getSmartTagRecords , path=/getSmartTagRecords , type=get ");
		List<SmartTagDetails> milestoneWithCodes = null;
		Map<String, String> milestones = tsamService.getSmartTagRecords(requestNo, style, colorWashName, profomaPo);
		if (milestones != null && milestones.size() > 0) {
			milestoneWithCodes = new ArrayList<>();
			for (Map.Entry<String, String> entry : milestones.entrySet()) {
				milestoneWithCodes
						.add(new SmartTagDetails().setFabricationCategory(entry.getValue()).setItemNumber(entry.getKey()));
			}
		}
		return milestones;
		
	}

}
