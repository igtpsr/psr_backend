package com.igt.psr.rest.service;

import java.util.List;

import com.igt.psr.db.model.PsrMatchCriteria;
import com.igt.psr.ui.view.EventModel;
import com.igt.psr.ui.view.EventModelName;
import com.igt.psr.ui.view.PsrEventForm;
import com.igt.psr.ui.view.PsrEventModelView;
import com.igt.psr.ui.view.UserCustimozedView;

public interface EventService {

	public void addEventMaster(PsrEventForm psrEvent) throws Exception;

	public void deleteEventMaster(String eventCode) throws Exception;

	public int deleteEventModel(String modelName) throws Exception;
	
//	public void deleteEventModelDetails(String eventCode) throws Exception;
	
	public int addCustomizedView(UserCustimozedView customizedView) throws Exception;

	public List<EventModelName> getModelNames(String modelName, String eventModelSearch) throws Exception;

	public List<PsrEventForm> getEventMaster(String eventCode, String eventcodeselect) throws Exception;

	public List<PsrEventModelView> getModelDetails(String modelName) throws Exception;

	public List<EventModel> getEventMatchCriteria(String modelName) throws Exception;

	public void addEventModelDetails(String modelName, List<PsrEventModelView> psrEventDetails, String loginUserId) throws Exception;

	public void addEventModelsToCriteria(String modelName, List<PsrMatchCriteria> matchCriteriaList) throws Exception;

	public void addSingleEventModelsToCriteria(String modelName, PsrMatchCriteria matchList) throws Exception;

	public List<PsrEventForm> getEventCodeDetails(String eventDetails) throws Exception;
	
	public void disableModel(Boolean status, String modelName) throws Exception;

}
