package com.igt.psr.config;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.igt.psr.common.exception.LoginValidateException;
import com.igt.psr.common.util.PSRHelper;
import com.igt.psr.common.util.PSRResponse;
import com.igt.psr.rest.common.view.PsrSuperResource;
import com.igt.psr.rest.common.view.UserSessionDetails;
import com.igt.psr.rest.service.PSRPortalService;

@Component
public class PsrLoginInterceptor extends PsrSuperResource implements HandlerInterceptor {

	private static final Logger log = LoggerFactory.getLogger(PsrLoginInterceptor.class);

	@Value("${mgf.ldap.url}")
	public String ldapUrl;
	@Value("${mgf.ldap.domain}")
	public String domain;

	@Value("${login.session.timeout}")
	public int timeout;

	PSRResponse psrResponse = new PSRResponse();

	@Autowired
	PSRPortalService psrPortalService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		log.info("Entered into the classPsrLoginInterceptor and  preHandle method ");
		String authKey = null;
		String path = null;
		try {
			path = request.getServletPath();
			authKey = request.getHeader("authentication");
			if (authKey == null) {
				authKey = request.getParameter("authentication");
			}
			log.info(" authentication/session id --> "+authKey+", path = "+path);
			if (path != null && (path.contains("resetPassword") || path.contains("login") || path.contains("logout")
					|| path.contains("isAlive") || path.contains("isTokenAvailable") || path.contains("browserClosed"))) {
				return true;
			}
			if(PSRHelper.checkEmptyOrNull(authKey)) {
				UserSessionDetails userSessionInfo = psrPortalService.getUserIdFromSession(authKey,timeout);
				log.info(String.format("UserSessionDetails %s", userSessionInfo.toString()));
				if (userSessionInfo.getIsInactive() == 0) {
					psrPortalService.deleteUserSession(userSessionInfo);
					throw new LoginValidateException(LoginValidateException.sessionExpired());
					//response.sendRedirect("/rest/logout?authentication=" + authKey + "&validSession=expired");
				} else {
					if (userSessionInfo.getInActiveTime() > (timeout - 5)) {
						psrPortalService.updateInactiveIntervalTime(userSessionInfo);
					}
				}
				return true;
			} else {
				throw new LoginValidateException(LoginValidateException.tokenMissed());
			}
		} catch (Exception e) {
			response.sendError(response.SC_EXPECTATION_FAILED, e.getMessage());
			log.error(String.format(" Error in login interceptor : %s", e.getMessage()));
			//response.sendRedirect("/rest/logout?validSession=error");
		}
		return false;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {

	}
}
