package com.igt.psr.config.role;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RoleHandler implements Serializable {
	private static final Logger log = LoggerFactory.getLogger(RoleHandler.class);

	private static List<String> urlList = new ArrayList<>();
	static {
		urlList.add("eventcode");

	}

	public static boolean isValidRole(String path, String roleName) {
		log.info("entered into the method isValidRole ");
		if (roleName != null && roleName.equalsIgnoreCase("PSR_ADMIN")) {
			if (path != null && !path.trim().isEmpty()) {
				return path.contains("eventcode");
			}
		} else if (roleName != null && roleName.equalsIgnoreCase("PTL_ADMIN")) {
			if (path != null && !path.trim().isEmpty()) {
				return path.contains("eventcode");
			}
		}
		return false;
	}

}
