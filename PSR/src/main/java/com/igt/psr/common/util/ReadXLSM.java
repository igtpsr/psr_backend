package com.igt.psr.common.util;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.igt.psr.db.model.PsrOrder;
import com.igt.psr.ui.view.PsrAuth;
import com.igt.psr.ui.view.PsrEventsView;

public class ReadXLSM {

	DataFormatter formatter = new DataFormatter();

	private final Logger log = LoggerFactory.getLogger(ReadXLSM.class);

	public Map<Integer, String> readXLSM(FileInputStream file, String secretCode, String enableContent)
			throws Exception {
		log.info(" entered into the method readXLSM ");
		String xmlStr = null;
		List<PsrEventsView> eventsList = new ArrayList<>();
		List<PsrOrder> orders = new LinkedList<>();
		Map<Integer, String> data = new HashMap<>();
		try {
			XSSFWorkbook workbook = new XSSFWorkbook(file);

			if (workbook.getSheet("validations") == null) {
				throw new Exception("File is mismatched. Please upload the correct file.");
			}
			String fileName = formatter
					.formatCellValue(workbook.getSheet("validations").getRow(3).getCell(6, Row.CREATE_NULL_AS_BLANK));
			data.put(0, fileName);

			try {
				String code = formatter.formatCellValue(
						workbook.getSheet("validations").getRow(1).getCell(6, Row.CREATE_NULL_AS_BLANK));
				if (!code.equalsIgnoreCase(secretCode)) {
					file.close();
					data.put(1, "secretCodeMissed");
					return data;
				}
			} catch (Exception e) {
				data.put(1, "secretCodeMissed");
				return data;
			}
			try {
				String checkEnableContent = formatter.formatCellValue(
						workbook.getSheet("validations").getRow(2).getCell(6, Row.CREATE_NULL_AS_BLANK));
				if (!checkEnableContent.equalsIgnoreCase(enableContent)) {
					file.close();
					data.put(1, "enableContentFalse");
					return data;
				}
			} catch (Exception e) {
				data.put(1, "enableContentFalse");
				return data;
			}
			String fname = fileName.substring(0, fileName.lastIndexOf('_'));
			XSSFSheet sheet = workbook.getSheet(fname);

			// To check headers validation
			/*
			 * XSSFSheet headerSheet = workbook.getSheet("headers"); Row dataRow =
			 * sheet.getRow(0); Row headerRow = headerSheet.getRow(0); Iterator<Cell> ci =
			 * dataRow.cellIterator(); Iterator<Cell> ci2 = headerRow.cellIterator();
			 * 
			 * int dataRowCount = dataRow.getLastCellNum(); int headerRowCount =
			 * headerRow.getLastCellNum(); if (dataRowCount != headerRowCount) { data.put(1,
			 * "headersMismatched"); return data; }
			 * 
			 * while (ci.hasNext()) { while (ci2.hasNext()) { if
			 * (!ci.next().toString().equalsIgnoreCase(ci2.next().toString())) { data.put(1,
			 * "headersMismatched"); return data; } break; } }
			 */
			// ends here for headers validation

			Iterator<Row> rowIterator = sheet.iterator();
			Map<Integer, String> headers = new HashMap();
			PsrEventsView events = new PsrEventsView();
			Map<Integer, List<PsrEventsView>> eventsWRTOrder = new HashMap<>();
			PsrOrder order = null;
			XSSFCell cell;
			XSSFRow row;

			while (rowIterator.hasNext()) {
				order = new PsrOrder();
				row = (XSSFRow) rowIterator.next();
				for (int i = 0; i < row.getLastCellNum(); i++) {
					cell = row.getCell(i, Row.CREATE_NULL_AS_BLANK);
					String cellInfo = formatter.formatCellValue(cell);
					
					if (row.getRowNum() == 0) {
						headers.put(cell.getColumnIndex(), cellInfo);
					} else {
						if (headers != null && headers.size() > 0 && headers.containsValue("Psr Order Id")) {
							for (Entry<Integer, String> entry : headers.entrySet()) {
								if (cell.getColumnIndex() == entry.getKey()) {
									String cellValue = entry.getValue();
									if (cellValue.trim().equalsIgnoreCase("Psr Order Id")
											&& PSRHelper.checkNotEmpty(cellInfo)) {
										order.setPsrOrderId(Integer.valueOf(cellInfo));
									}
									if (cellValue.trim().equalsIgnoreCase("Ndc Wk")) {
										order.setNdcWk(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Cpo Pack Type")) {
										order.setCpoPackType(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Fabric Code")) {
										order.setFabricCode(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Fabric Est Date")) {
										order.setFabricEstDateStr(setFormatedDate(cellInfo));
									}
									if (cellValue.trim().equalsIgnoreCase("Fabric Mill")) {
										order.setFabricMill(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Fiber Content")) {
										order.setFiberContent(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Actual Poundage Available")) {
										order.setActualPoundageAvailable(PSRHelper.handleValue(cellInfo));
									}
									if (cellValue.trim().equalsIgnoreCase("Appr Fabric Yarn Lot")) {
										order.setApprFabricYarnLot(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Gauge")) {
										order.setGauge(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Knitting %")) {
										if (cellInfo != null) {
											if (cellInfo.trim().contains("%")) {
												order.setKnitting(
														PSRHelper.handleValue(cellInfo.trim().replace("%", "")));
											} else {
												
												order.setKnitting(
														PSRHelper.handleValue(cellInfo));
											}
										}

									}
									if (cellValue.trim().equalsIgnoreCase("# of Knitting Machines")) {
										order.setKnittingMachines(PSRHelper.handleValue(cellInfo));
									}
									if (cellValue.trim().equalsIgnoreCase("Knitting Pcs")) {
										order.setKnittingPcs(PSRHelper.handleValue(cellInfo));
									}
									if (cellValue.trim().equalsIgnoreCase("Linking %")) {
										if (cellInfo != null) {
											if (cellInfo.contains("%")) {
												order.setLinking(
														PSRHelper.handleValue(cellInfo.trim().replace("%", "")));
											} else {
												order.setLinking(
														PSRHelper.handleValue(cellInfo));
											}
										}
									}
									if (cellValue.trim().equalsIgnoreCase("# of Linking Machines")) {
										order.setLinkingMachines(PSRHelper.handleValue(cellInfo));
									}
									if (cellValue.trim().equalsIgnoreCase("Linking Pcs")) {
										order.setLinkingPcs(PSRHelper.handleValue(cellInfo));
									}
									if (cellValue.trim().equalsIgnoreCase("# of Sewing Lines")) {
										order.setSewingLines(PSRHelper.handleValue(cellInfo));
									}
									if (cellValue.trim().equalsIgnoreCase("Packaging Ready Date")) {
										order.setPackagingReadyDateStr(setFormatedDate(cellInfo));
									}
									if (cellValue.trim().equalsIgnoreCase("Care Label Code")) {
										order.setCareLabelCode(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Color Code Pattern")) {
										order.setColorCodePatrn(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Comment")) {
										order.setComment(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Cust Fty Id")) {
										order.setCustFtyId(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Cut Appr Letter")) {
										order.setCutApprLetter(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Daily Output Per Line")) {
										order.setDailyOutputPerLine(PSRHelper.handleValue(cellInfo));
									}
									if (cellValue.trim().equalsIgnoreCase("Extra Process")) {
										order.setExtraProcess(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Fab Trim Order")) {
										order.setFabTrimOrder(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Factory Ref")) {
										order.setFactoryRef(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Fabric Test Report#")) {
										order.setFabricTestReportNumeric(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Artwork name")) {
										order.setGraphicName(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Sell Channel")) {
										order.setOrderType(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Pack Type")) {
										order.setPackType(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Ticket Color Code")) {
										order.setTicketColorCode(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Ticket Style Number")) {
										order.setTicketStyleNumeric(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Garment Test Report#")) {
										order.setGarmentTestReportNo(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Design Card Number/PLM Number")) {
										order.setPlmNo(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Wash")) {
										order.setWashYn(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Wash Facility")) {
										order.setWashFacility(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Fabric Ship Mode")) {
										order.setFabricShipMode(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Tech Pack Received Date")) {
										order.setTechPackReceivedDateStr(setFormatedDate(cellInfo));
									}
									if (cellValue.trim().equalsIgnoreCase("Sample Material Status")) {
										order.setSampleMaterialStatus(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Retail Price")) {
										order.setRetailPrice(PSRHelper.handleValue(cellInfo));
									}
									if (cellValue.trim().equalsIgnoreCase("Fabric Pp Date")) {
										order.setFabricPpDateStr(setFormatedDate(cellInfo));
									}
									if (cellValue.trim().equalsIgnoreCase("Delivery Month")) {
										order.setDeliveryMonth(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Cpo Acc Date By Vendor")) {
										order.setCpoAccDateByVendorStr(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Dist Channel")) {
										order.setDistChannel(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Floor Set")) {
										order.setFloorSet(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Style At Risk")) {
										order.setStyleAtRisk(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Sample Merch Eta")) {
										order.setSampleMerchEtaStr(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Sample Floor Set Eta")) {
										order.setSampleFloorSetEtaStr(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Sample Dcom Eta")) {
										order.setSampleDcomEtaStr(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Sample Mailer")) {
										order.setSampleMailer(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Sample Mailer Eta")) {
										order.setSampleMailerEtaStr(cellInfo);
									}
									// added 15 new columns on 30-07-2019
									if (cellValue.trim().equalsIgnoreCase("TOP sample ETA Date")) {
										order.setTopSampleEtaDateStr(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("TOP sample Comment")) {
										order.setTopSampleComment(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Photo/Merchant Sample Send date")) {
										order.setPhotoMerchantSampleSendDateStr(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Photo/Merchant Sample ETA date")) {
										order.setPhotoMerchantSampleEtaDateStr(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Photo/Merchant Sample Comment")) {
										order.setPhotoMerchantSampleComment(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Marketing Sample Send date")) {
										order.setMarketingSampleSendDateStr(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Marketing Sample ETA date")) {
										order.setMarketingSampleEtaDateStr(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Marketing Sample Comment")) {
										order.setMarketingSampleComment(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Visual Sample Send date")) {
										order.setVisualSampleSendDateStr(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Visual Sample ETA date")) {
										order.setVisualSampleEtaDateStr(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Visual Sample Comment")) {
										order.setVisualSampleComment(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Copyright Sample Send date")) {
										order.setCopyrightSampleSendDateStr(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Copyright Sample ETA date")) {
										order.setCopyrightSampleEtaDateStr(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Copyright Sample Comment")) {
										order.setCopyrightSampleComment(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Reason For Late Gac")) {
										order.setReasonForLateGac(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Additional Bulk Lot Approve")) {
										order.setAdditionalBulkLotApproveStr(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Color Comment")) {
										order.setColorComment(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Fabric Comment")) {
										order.setFabricComment(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Fabric Test Comment")) {
										order.setFabricTestComment(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Garment Test Comment")) {
										order.setGarmentTestComment(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Trim Comment")) {
										order.setTrimComment(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Sample Comment")) {
										order.setSampleComment(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Fit Sample Comment")) {
										order.setFitSampleComment(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("PP Sample Comment")) {
										order.setPpSampleComment(cellInfo);
									}
									if (cellValue.trim().equalsIgnoreCase("Garment Treatment Pcs")) {
										order.setGarmentTreatmentPcs(PSRHelper.handleValue(cellInfo));
									}
									if (cellValue.trim().equalsIgnoreCase("Finished Garment Pcs")) {
										order.setFinishedGarmentPcs(PSRHelper.handleValue(cellInfo));
									}
									if (cellValue.trim().equalsIgnoreCase("Packed Units Pcs")) {
										order.setPackedUnitsPcs(PSRHelper.handleValue(cellInfo));
									}
									// end of orders
									if (events.getEventCode() != null && events.getRevisedDateStr() != null
											&& events.getActualDateStr() != null) {
										events = new PsrEventsView();
									}
									if (entry.getValue().trim().toLowerCase().contains("eventcode")) {
										events.setEventCode(cellInfo.trim());
									}
									if (entry.getValue().trim().toLowerCase().contains("-reviseddate")) {
										// eventsData.put(entry.getKey(), entry.getValue());
										events.setRevisedDateStr(setFormatedDate(cellInfo));
									}
									if (entry.getValue().trim().toLowerCase().contains("-actualdate")) {
										// eventsData.put(entry.getKey(), entry.getValue());
										events.setActualDateStr(setFormatedDate(cellInfo));
									}
									if (events.getEventCode() != null && events.getRevisedDateStr() != null
											&& events.getActualDateStr() != null) {
										if (order != null && order.getPsrOrderId() != null) {
											events.setPsrOrderId(order.getPsrOrderId());
											eventsList.add(events);
											Set<PsrEventsView> s = new HashSet<PsrEventsView>();
											s.addAll(eventsList);
											eventsList = new ArrayList<PsrEventsView>();
											eventsList.addAll(s);
										}
									}
								}

							} // end for
						} else {
							throw new Exception("PSR Order Id is missing");
						}
					}
				}
				if (row.getRowNum() > 0) {
					orders.add(order);
				}
			}
			eventsWRTOrder = getEventsWRTOrder(eventsWRTOrder, eventsList);
			if (orders != null && orders.size() > 0 && orders.size() <= 100) {
				xmlStr = DAOHelper.constructXmlStr(orders, eventsWRTOrder, null);
				data.put(1, xmlStr);
			} else {
				final int N = orders.size();
				for (int i = 0; i < N; i += 100) {
					List<PsrOrder> subList = orders.subList(i, Math.min(N, i + 100));
					xmlStr = DAOHelper.constructXmlStr(subList, eventsWRTOrder, null);
					data.put(i + 1, xmlStr);
				}
			}

		} catch (Exception e) {
			// e.printStackTrace();
			log.error(String.format("Error occured in readXLSM ", e.getMessage()));
			throw e;
		}
		return data;
	}

	private String setFormatedDate(String cellInfo) throws Exception {
		// current string is in mm/dd/yyyy & converted to yyyy-mm-dd
		StringBuffer sb = new StringBuffer();
		if (cellInfo != null && !cellInfo.equalsIgnoreCase("") && !cellInfo.equalsIgnoreCase("false")
				&& !cellInfo.equalsIgnoreCase("true") && !cellInfo.equalsIgnoreCase("yes")
				&& !cellInfo.equalsIgnoreCase("no")) {
			int count = StringUtils.countMatches(cellInfo, "/");
			if (count == 2) {
				if (PSRHelper.checkEmptyOrNull(cellInfo)) {
					String[] s = cellInfo.split("/");
					sb.append(s[2] + "-" + s[0] + "-" + s[1]);
					return sb.toString();
				}
			} else {
				throw new Exception("Invalid date format in the imported file. Please verify it.");
			}
		}
		return "";
	}

	private Map<Integer, List<PsrEventsView>> getEventsWRTOrder(Map<Integer, List<PsrEventsView>> eventsWRTOrder,
			List<PsrEventsView> list) {
		eventsWRTOrder = new HashMap<>();
		Integer psrOrderId = null;
		List<PsrEventsView> eventsList = null;
		for (int i = 0; i < list.size(); i++) {
			PsrEventsView event = list.get(i);
			psrOrderId = event.getPsrOrderId();
			if (!eventsWRTOrder.containsKey(psrOrderId)) {
				eventsList = new ArrayList<>();
				eventsList.add(event);
				eventsWRTOrder.put(psrOrderId, eventsList);
			} else {
				eventsList = eventsWRTOrder.get(psrOrderId);
				eventsList.add(event);
				eventsWRTOrder.put(psrOrderId, eventsList);
			}
		}
		// System.out.println(" eventsWRTOrder --> " + eventsWRTOrder.toString());
		return eventsWRTOrder;
	}

}
