package com.igt.psr.common.exception;

public class ImageException extends Exception {

	private static final long serialVersionUID = 754834018703128676L;
	public String id;

	public ImageException(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Unable to read the image, It looks like corrupted.";
	}
}
