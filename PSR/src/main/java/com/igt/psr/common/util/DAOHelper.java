package com.igt.psr.common.util;

import static com.igt.psr.common.constants.ApplicationConstants.FALSE;
import static com.igt.psr.common.constants.ApplicationConstants.NO;
import static com.igt.psr.common.constants.ApplicationConstants.ZERO;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.igt.psr.common.constants.ApplicationConstants;
import com.igt.psr.common.constants.GridConstants;
import com.igt.psr.db.model.PsrEventMaster;
import com.igt.psr.db.model.PsrEventModel;
import com.igt.psr.db.model.PsrEventModelIdentity;
import com.igt.psr.db.model.PsrEvents;
import com.igt.psr.db.model.PsrEventsPK;
import com.igt.psr.db.model.PsrMatchCriteria;
import com.igt.psr.db.model.PsrModelMaster;
import com.igt.psr.db.model.PsrOrder;
import com.igt.psr.db.model.PsrUserCustimozedView;
import com.igt.psr.portal.model.PtlRoleProfile;
import com.igt.psr.portal.model.PtlUserProfileTemp;
import com.igt.psr.portal.model.PtlUserRole;
import com.igt.psr.portal.model.PtlUserRoleId;
import com.igt.psr.ui.view.EventModel;
import com.igt.psr.ui.view.EventModelName;
import com.igt.psr.ui.view.PsrEventForm;
import com.igt.psr.ui.view.PsrEventModelView;
import com.igt.psr.ui.view.PsrEventsView;
import com.igt.psr.ui.view.PsrGridView;
import com.igt.psr.ui.view.PsrOrderView;
import com.igt.psr.ui.view.PtlRoleProfileForm;
import com.igt.psr.ui.view.PtlUserProfileForm;
import com.igt.psr.ui.view.SearchForm;
import com.igt.psr.ui.view.UserCustimozedView;

public class DAOHelper {

	private static final Logger log = LoggerFactory.getLogger(DAOHelper.class);

	public static PsrOrder getPsrModelObject(PsrOrder psr) {
		return new PsrOrder().setPsrOrderId(psr.getPsrOrderId())
				.setActualPoundageAvailable(psr.getActualPoundageAvailable())
				.setApprFabricYarnLot(psr.getApprFabricYarnLot()).setAsn(psr.getAsn())
				.setBalanceShip(psr.getBalanceShip()).setBrand(psr.getBrand()).setCareLabelCode(psr.getCareLabelCode())
				.setColorCodePatrn(psr.getColorCodePatrn()).setComment(psr.getComment())
				.setCountryOrigin(psr.getCountryOrigin()).setCpo(psr.getCpo()).setCreateDate(psr.getCreateDate())
				.setCustFtyId(psr.getCustFtyId()).setCutApprLetter(psr.getCutApprLetter())
				.setDailyOutputPerLine(psr.getDailyOutputPerLine()).setDept(psr.getDept())
				.setExtraProcess(psr.getExtraProcess()).setFabTrimOrder(psr.getFabTrimOrder())
				.setFabricCode(psr.getFabricCode()).setFabricEstDate(psr.getFabricEstDate())
				.setFabricMill(psr.getFabricMill()).setFabricTestReportNumeric(psr.getFabricTestReportNumeric())
				.setFactoryId(psr.getFactoryId()).setFactoryName(psr.getFactoryName())
				.setFactoryRef(psr.getFactoryRef()).setFiberContent(psr.getFiberContent()).setFob(psr.getFob())
				.setPlActualGac(psr.getPlActualGac()).setCpoGacDate(psr.getCpoGacDate())
				.setGarmentTestActual(psr.getGarmentTestActual()).setGauge(psr.getGauge()).setGender(psr.getGender())
				.setGraphicName(psr.getGraphicName()).setGrossSellPrice(psr.getGrossSellPrice())
				.setImage(psr.getImage()).setKeyItem(psr.getKeyItem()).setKnitting(psr.getKnitting())
				.setKnittingMachines(psr.getKnittingMachines()).setKnittingPcs(psr.getKnittingPcs())
				.setLinking(psr.getLinking()).setLinkingMachines(psr.getLinkingMachines())
				.setLinkingPcs(psr.getLinkingPcs()).setMargin(psr.getMargin()).setMerchant(psr.getMerchant())
				.setModifyTs(psr.getModifyTs()).setModifyUser(psr.getModifyUser()).setNdcDate(psr.getNdcDate())
				.setNdcWk(psr.getNdcWk()).setOcCommit(psr.getOcCommit()).setOrderQty(psr.getOrderQty())
				.setOrderType(psr.getOrderType()).setPackType(psr.getPackType())
				.setPackagingReadyDate(psr.getPackagingReadyDate()).setProdOffice(psr.getProdOffice())
				.setProductType(psr.getProductType()).setProgramName(psr.getProgramName())
				.setSalesAmount(psr.getSalesAmount()).setSeason(psr.getSeason()).setSewingLines(psr.getSewingLines())
				.setShipMode(psr.getShipMode()).setShippedQty(psr.getShippedQty()).setStyle(psr.getStyle())
				.setStyleDesc(psr.getStyleDesc()).setTicketColorCode(psr.getTicketColorCode())
				.setTicketStyleNumeric(psr.getTicketStyleNumeric()).setVendorId(psr.getVendorId())
				.setVendorName(psr.getVendorName()).setVpoFtyGacDate(psr.getVpoFtyGacDate())
				.setVpoNltGacDate(psr.getVpoNltGacDate()).setVpoStatus(psr.getVpoStatus()).setImage("");
	}

	public static PsrOrderView getPsrOrderView(PsrOrder psrModel, List<PsrOrderView> eventNameList) {
		PsrOrderView orderView = new PsrOrderView().setVpo(psrModel.getVpo()).setProdOffice(psrModel.getProdOffice())
				.setVendorId(psrModel.getVendorId()).setVendorName(psrModel.getVendorName())
				.setFactoryId(psrModel.getFactoryId()).setFactoryName(psrModel.getFactoryName())
				.setCountryOrigin(psrModel.getCountryOrigin()).setBrand(psrModel.getBrand())
				.setStyle(psrModel.getStyle()).setStyleDesc(psrModel.getStyleDesc()).setNdcDate(psrModel.getNdcDate())
				.setVpoNltGacDate(psrModel.getVpoNltGacDate()).setPlanDate(null).setActualDate(null)
				.setCpoPlanDate(null).setCpoPlanDate(null);

		if (orderView.getLocations() != null && orderView.getLocations().size() <= 0) {

			orderView.setLocations(eventNameList);

		}
		return orderView;
	}

	public static boolean checkNotEmpty(String var) {
		return (null != var) && (var.trim().length() > 0) && (!var.trim().equalsIgnoreCase("undefined"))
				&& (!var.trim().equalsIgnoreCase("null"));
	}

	public static PsrEventMaster getPsrEventMasterSave(PsrEventForm psrEvent) {
		return new PsrEventMaster().setEventCode(psrEvent.getEventcode()).setEventCategory(psrEvent.getEventcategory())
				.setEventDesc(psrEvent.getDesc()).setModifyTs(new Date()).setCreateDate(new Date());
	}

	public static PsrEventMaster getPsrEventMasterUpdate(PsrEventForm psrEvent) {
		return new PsrEventMaster().setEventCode(psrEvent.getEventcode()).setEventCategory(psrEvent.getEventcategory())
				.setEventDesc(psrEvent.getDesc()).setModifyTs(new Date());
	}

	// Added method to create match criteria
	public static PsrMatchCriteria getPsrMatchCriteria(String modelName, PsrMatchCriteria matchList) {
		return new PsrMatchCriteria().setModelName(modelName).setMatchCase(matchList.getMatchCase())
				.setFieldValue(matchList.getFieldValue()).setModifyUser(matchList.getModifyUser());
	}

	// Jagadish
	public static PsrEventModel getPsrEventModelDetails(PsrEventModelView psrEventDetails, int modelId, String loginUserId) {
		return new PsrEventModel()
				.setId(new PsrEventModelIdentity().setModelId(modelId).setEventCode(psrEventDetails.getEventCode()))
				.setDescription(psrEventDetails.getDescription()).setPlanDate(psrEventDetails.getPlanDate())
				.setActualDate(psrEventDetails.getActualDate()).setRevisedDate(psrEventDetails.getRevisedDate())
				.setTriggerEvent(psrEventDetails.getTriggerEvent()).setTriggerDays(psrEventDetails.getTriggerDays())
				.setModifyTs(new Date()).setModifyUser(loginUserId).setDisableEvent(psrEventDetails.getDisableEvent());

	}

	public static PsrUserCustimozedView getPsrUserCustimozedView(UserCustimozedView customizedView) {
		return new PsrUserCustimozedView().setQueryName(customizedView.getQueryName())
				.setSelectedColumns(customizedView.getSelectedColumns()).setModifyUser(customizedView.getLoginUserId())
				.setModifyTs(new Date()).setCreateDate(new Date()).setLoginUser(customizedView.getLoginUserId())
				.setPublicView(customizedView.getPublicView()).setColumnReorder(customizedView.getColumnReorder());
	}

	public static List<Map> getPsrDataResultSetUtility(List<Object[]> list, List<String> eventDescList)
			throws JsonProcessingException {
		log.info(" entered into the method getPsrDataResultSetUtility .");
		List<Map> psrData1 = new LinkedList<>();
		Map<String, Object> psrJson = null;
		Map<Integer, Map> psrData = new LinkedHashMap<>();
		for (Object[] object : list) {
			Integer psrOrderPK = (Integer) object[87];
			if (psrData.containsKey(psrOrderPK)) {
				psrJson = psrData.get(psrOrderPK);
				for (String eventdesc : eventDescList) {
					if (eventdesc.trim().equalsIgnoreCase(((String) object[82]).trim())) {
						psrJson.put(eventdesc + ApplicationConstants.ACTUAL_DATE, object[83]);
						psrJson.put(eventdesc + ApplicationConstants.PLANNED_DATE, object[84]);
						psrJson.put(eventdesc + ApplicationConstants.REVISED_DATE, object[85]);
						psrJson.put(eventdesc + ApplicationConstants.EVENT_CODE, object[86]);
						psrJson.put(eventdesc + ApplicationConstants.HIGHLIGHT_REVISED_DATE, object[90]);
						psrJson.put(eventdesc + "disableEvent", object[145]);
					}
				}
			} else {
					psrJson = new LinkedHashMap<>();
					psrJson.put("image", object[42]);
					psrJson.put("vpo", object[3]);
					psrJson.put("colorWashName", object[0]);
					psrJson.put("shipTo", object[1]);
					psrJson.put("soId", object[2]);
					psrJson.put("prodOffice", object[61]);
					psrJson.put("vendorId", object[74]);
					psrJson.put("vendorName", object[75]);
					psrJson.put("factoryId", object[30]);
					psrJson.put("factoryName", object[31]);
					psrJson.put("countryOrigin", object[13]);
					psrJson.put("brand", object[8]);
					psrJson.put("dept", object[23]);
					psrJson.put("merchant", object[52]);
					psrJson.put("style", object[70]);
					psrJson.put("styleDesc", object[71]);
					psrJson.put("fob", object[34]);
					psrJson.put("grossSellPrice", object[41]);
					psrJson.put("margin", object[51]);
					psrJson.put("productType", object[62]);
					psrJson.put("programName", object[64]);
					psrJson.put("shipMode", object[68]);
					psrJson.put("gender", object[39]);
					psrJson.put("orderQty", object[57]);
					psrJson.put("shippedQty", object[69]);
					psrJson.put("ocCommit", object[56]);
					psrJson.put("cpo", object[14]);
					psrJson.put("season", object[66]);
					psrJson.put("ndcDate", object[54]);
					psrJson.put("vpoNltGacDate", object[80]);
					psrJson.put("vpoFtyGacDate", object[76]);
					psrJson.put("cpoGacDate", object[36]);
					psrJson.put("plActualGac", object[35]);
					psrJson.put("asn", object[6]);
					psrJson.put("keyItem", object[44]);
					psrJson.put("vpoStatus", object[81]);
					psrJson.put("custFtyId", object[20]);
					psrJson.put("factoryRef", object[32]);
					psrJson.put("fabricCode", object[26]);
					psrJson.put("fiberContent", object[33]);
					psrJson.put("fabricMill", object[28]);
					psrJson.put("fabTrimOrder", object[25]);
					psrJson.put("colorCodePatrn", object[11]);
					psrJson.put("graphicName", object[40]);
					psrJson.put("orderType", object[58]);
					psrJson.put("ndcWk", object[55]);
					psrJson.put("apprFabricYarnLot", object[5]);
					if (object[24] != null) {
						if (((String) object[24]).equalsIgnoreCase("Y"))
							psrJson.put("extraProcess", true);
						else
							psrJson.put("extraProcess", false);
					} else {
						psrJson.put("extraProcess", false);
					}
					psrJson.put("actualPoundageAvailable", object[4]);
					psrJson.put("cutApprLetter", object[21]);
					psrJson.put("careLabelCode", object[9]);
					psrJson.put("gauge", object[38]);
					psrJson.put("knittingPcs", object[47]);
					psrJson.put("knitting", object[45]);
					psrJson.put("linkingPcs", object[50]);
					psrJson.put("linking", object[48]);
					psrJson.put("sewingLines", object[67]);
					psrJson.put("knittingMachines", object[46]);
					psrJson.put("linkingMachines", object[49]);
					psrJson.put("dailyOutputPerLine", object[22]);
					psrJson.put("fabricEstDate", object[27]);
					psrJson.put("fabricTestReportNumeric", object[29]);
					psrJson.put("garmentTestActualStr", object[37]);
					psrJson.put("packType", object[59]);
					psrJson.put("packagingReadyDateStr", object[60]);
					psrJson.put("comment", object[12]);
					psrJson.put("ticketColorCode", object[72]);
					psrJson.put("ticketStyleNumeric", object[73]);
					psrJson.put("salesAmount", object[65]);
					psrJson.put("balanceShip", object[7]);
					psrJson.put("cpoOrderQty", object[16]);
					psrJson.put("cpoShipMode", object[18]);
					psrJson.put("cpoColorDesc", object[15]);
					psrJson.put("cpoPackType", object[17]);
					// psrJson.put("modifyTs", object[0]);//required or not
					psrJson.put("modifyUser", object[53]);
					psrJson.put("createDate", object[19]);
					psrJson.put("vpoLineStatus", object[79]);
					psrJson.put("interfaceTs", object[43]);
					psrJson.put("category", object[10]);
					psrJson.put("profomaPo", object[63]);
					psrJson.put("vpoFtyGacDateTemp", object[77]);
					psrJson.put("vpoHeadStatus", object[78]);
					psrJson.put("psrOrderId", object[87]);
					psrJson.put("planId", object[88]);
					psrJson.put("initFlow", object[89]);
					psrJson.put("vpoOrderType", object[91]);
					psrJson.put("speedIndicator", object[92]);
					psrJson.put("garmentTestReportNo", object[93]);
					psrJson.put("plShipDate", object[94]);
					psrJson.put("cpoStyleNo", object[95]);
					psrJson.put("requestNo", object[96]);
					psrJson.put("ladingPoint", object[97]);
					psrJson.put("techDesigner", object[98]);
					psrJson.put("plmNo", object[99]);
					if (object[100] != null) {
						if (((String) object[100]).equalsIgnoreCase("Y") || ((String) object[100]).equals("1"))
							psrJson.put("washYn", true);
						else
							psrJson.put("washYn", false);
					} else {
						psrJson.put("washYn", false);
					}
					psrJson.put("washFacility", object[101]);
					psrJson.put("fabricShipMode", object[102]);
					psrJson.put("techPackReceivedDateStr", object[103]);
					psrJson.put("sampleMaterialStatus", object[104]);
					psrJson.put("retailPrice", object[105]);
					psrJson.put("fabricPpDateStr", object[106]);
					psrJson.put("deliveryMonth", object[107]);
					psrJson.put("cpoAccDateByVendorStr", object[108]);
					psrJson.put("distChannel", object[109]);
					if (object[110] != null) {
						if (((String) object[110]).equalsIgnoreCase("Y"))
							psrJson.put("floorSet", true);
						else
							psrJson.put("floorSet", false);
					} else {
						psrJson.put("floorSet", false);
					}
					if (object[111] != null) {
						if (((String) object[111]).equalsIgnoreCase("Y"))
							psrJson.put("styleAtRisk", true);
						else
							psrJson.put("styleAtRisk", false);
					} else {
						psrJson.put("styleAtRisk", false);
					}
					psrJson.put("sampleMerchEtaStr", object[112]);
					psrJson.put("sampleFloorSetEtaStr", object[113]);
					psrJson.put("sampleDcomEtaStr", object[114]);
					if (object[115] != null) {
						if (((String) object[115]).equalsIgnoreCase("Y"))
							psrJson.put("sampleMailer", true);
						else
							psrJson.put("sampleMailer", false);
					} else {
						psrJson.put("sampleMailer", false);
					}
					psrJson.put("sampleMailerEtaStr", object[116]);
					psrJson.put("topSampleEtaDateStr", object[117]);
					psrJson.put("topSampleComment", object[118]);
					psrJson.put("photoMerchantSampleSendDateStr", object[119]);
					psrJson.put("photoMerchantSampleEtaDateStr", object[120]);
					psrJson.put("photoMerchantSampleComment", object[121]);
					psrJson.put("marketingSampleSendDateStr", object[122]);
					psrJson.put("marketingSampleEtaDateStr", object[123]);
					psrJson.put("marketingSampleComment", object[124]);
					psrJson.put("visualSampleSendDateStr", object[125]);
					psrJson.put("visualSampleEtaDateStr", object[126]);
					psrJson.put("visualSampleComment", object[127]);
					psrJson.put("copyrightSampleSendDateStr", object[128]);
					psrJson.put("copyrightSampleEtaDateStr", object[129]);
					psrJson.put("copyrightSampleComment", object[130]);
					psrJson.put("reasonForLateGac", object[131]);
					psrJson.put("cpoDept", object[132]);
					psrJson.put("additionalBulkLotApproveStr", object[133]);
					psrJson.put("colorComment", object[134]);
					psrJson.put("fabricComment", object[135]);
					psrJson.put("fabricTestComment", object[136]);
					psrJson.put("garmentTestComment", object[137]);
					psrJson.put("trimComment", object[138]);
					psrJson.put("sampleComment", object[139]);
					psrJson.put("fitSampleComment", object[140]);
					psrJson.put("ppSampleComment", object[141]);
					psrJson.put("garmentTreatmentPcs", object[142]);
					psrJson.put("finishedGarmentPcs", object[143]);
					psrJson.put("packedUnitsPcs", object[144]);
				for (String eventdesc : eventDescList) {
					if (eventdesc.trim().equalsIgnoreCase(((String) object[82]).trim())) {
						psrJson.put(eventdesc + ApplicationConstants.ACTUAL_DATE, object[83]);
						psrJson.put(eventdesc + ApplicationConstants.PLANNED_DATE, object[84]);
						psrJson.put(eventdesc + ApplicationConstants.REVISED_DATE, object[85]);
						psrJson.put(eventdesc + ApplicationConstants.EVENT_CODE, object[86]);
						psrJson.put(eventdesc + ApplicationConstants.HIGHLIGHT_REVISED_DATE, object[90]);
						psrJson.put(eventdesc + "disableEvent", object[145]);
					} else {
						psrJson.put(eventdesc + ApplicationConstants.ACTUAL_DATE, "");
						psrJson.put(eventdesc + ApplicationConstants.PLANNED_DATE, "");
						psrJson.put(eventdesc + ApplicationConstants.REVISED_DATE, "");
						psrJson.put(eventdesc + ApplicationConstants.EVENT_CODE, "");
						psrJson.put(eventdesc + ApplicationConstants.HIGHLIGHT_REVISED_DATE, "");
						psrJson.put(eventdesc + "disableEvent", "");
					}
				}
				psrData.put((Integer) object[87], psrJson);

			}
		}
		for (Entry<Integer, Map> entry : psrData.entrySet()) {
			psrData1.add(entry.getValue());
		}

		if (log.isDebugEnabled()) {
			log.debug(String.format(" size : %d", psrData.size()));
		}
		return psrData1;

	}

	public static List<EventModelName> getModelNames(List<Object[]> eventModelNames) {
		List<EventModelName> emnList = new ArrayList<>();
		for (Object[] objects : eventModelNames) {
//			String dynamicLink = "<div width=100% height=100% ><a [routerLink]='event-model-details/" + modelName
//					+ "' id='nexturl' style='display: none'>" + modelName + "</a></div>";
			emnList.add(new EventModelName().setModelname((String) objects[0]).setDisable((Boolean) objects[1]));
		}
		return emnList;
	}

	public static List<PsrEventModelView> getModeldetailsForm(List<PsrEventModel> modeldetails) {
		List<PsrEventModelView> psrModelDetailsList = new ArrayList<>();
		for (PsrEventModel psrEventModel : modeldetails) {
			psrModelDetailsList.add(new PsrEventModelView().setEventCode(psrEventModel.getId().getEventCode())
					.setDescription(psrEventModel.getDescription()).setPlanDate(psrEventModel.getPlanDate())
					.setActualDate(psrEventModel.getActualDate()).setRevisedDate(psrEventModel.getRevisedDate())
					.setTriggerEvent(psrEventModel.getTriggerEvent()).setTriggerDays(psrEventModel.getTriggerDays())
					.setDisableEvent(psrEventModel.getDisableEvent()));
		}
		return psrModelDetailsList;
	}

	public static List<PsrEventModel> constructPsrEventModel(List<Object[]> eventModelNameList) {
		List<PsrEventModel> modelNameList = new ArrayList<>();
		for (Object[] objects : eventModelNameList) {

			modelNameList.add(new PsrEventModel().setId(new PsrEventModelIdentity().setEventCode((String) objects[0]))
					.setDescription((String) objects[1]).setPlanDate((Date) objects[2]).setActualDate((Date) objects[3])
					.setRevisedDate((Date) objects[4]).setTriggerEvent((String) objects[5])
					.setTriggerDays((String) objects[6]).setDisableEvent((Boolean) objects[7]));
		}
		return modelNameList;
	}

	public static List<PsrMatchCriteria> constructPsrEventMatchCriteria(List<Object[]> eventMatchCriteria) {
		List<PsrMatchCriteria> modelNameList = new ArrayList<>();
		for (Object[] objects : eventMatchCriteria) {

			modelNameList
					.add(new PsrMatchCriteria().setMatchCase((String) objects[0]).setFieldValue((String) objects[1]));

		}
		return modelNameList;
	}

	public static List<PsrEventForm> getEventCodeDetailsForm(List<PsrEventMaster> eventDetails) {
		List<PsrEventForm> eventCodeList = new ArrayList<>();
		for (PsrEventMaster psrEventMaster : eventDetails) {
			eventCodeList.add(new PsrEventForm().setEventcode(psrEventMaster.getEventCode())
					.setDesc(psrEventMaster.getEventDesc()).setEventcategory(psrEventMaster.getEventCategory()));
		}
		return eventCodeList;
	}

	public static List<EventModel> getMatchCriteriaForm(List<PsrMatchCriteria> eventDetails) {
		List<EventModel> eventMatch = new ArrayList<>();
		for (PsrMatchCriteria psrEventMaster : eventDetails) {
			eventMatch.add(
					new EventModel().setField(psrEventMaster.getMatchCase()).setValue(psrEventMaster.getFieldValue()));
		}
		return eventMatch;
	}

	public static List<PsrEventMaster> constructEventCodeDetails(List<Object[]> eventCodeList) {
		List<PsrEventMaster> modelNameList = new ArrayList<>();
		for (Object[] objects : eventCodeList) {
			modelNameList.add(new PsrEventMaster().setEventCode((String) objects[0]).setEventDesc((String) objects[1])
					.setEventCategory((String) objects[2]));
		}
		return modelNameList;
	}

	public static List<UserCustimozedView> getCustimozedViews(List<String> customizedView) {
		List<UserCustimozedView> ucViews = new ArrayList<>();
		for (String queryName : customizedView) {
			ucViews.add(new UserCustimozedView().setQueryName(queryName));
		}
		return ucViews;
	}

	public static PsrEvents getPsrEvents(List<Object[]> autoAttachEvents, PsrOrder psrOrder) {
		PsrEvents events = null;
		for (Object[] objects : autoAttachEvents) {
			events = new PsrEvents()
					.setId(new PsrEventsPK().setPsrOrderId(psrOrder.getPsrOrderId()).setEventCode((String) objects[1]))
					.setEventDesc((String) objects[2]).setModelName((String) (objects[0]))
					.setPlannedDate((Date) objects[3]).setActualDate((Date) objects[4])
					.setRevisedDate((Date) objects[5]).setModifyTs(new Date());
		}
		return events;
	}

	public static PsrGridView getObject(Object[] object) {
		return new PsrGridView().setColorWashName((String) object[0]).setShipTo((String) object[1])
				.setSoId(PSRHelper.setSoIdToString((BigDecimal) object[2])).setVpo((String) object[3])
				.setActualPoundageAvailable((BigDecimal) object[4]).setApprFabricYarnLot((String) object[5])
				.setAsn((String) object[6]).setBalanceShip((Integer) object[7]).setBrand((String) object[8])
				.setCareLabelCode((String) object[9]).setCategory((String) object[10])
				.setColorCodePatrn((String) object[11]).setComment((String) object[12])
				.setCountryOrigin((String) object[13]).setCpo((String) object[14]).setCpoColorDesc((String) object[15])
				.setCpoOrderQty((BigDecimal) object[16]).setCpoPackType((String) object[17])
				.setCpoShipMode((String) object[18]).setCreateDate((Date) object[19]).setCustFtyId((String) object[20])
				.setCutApprLetter((String) object[21]).setDailyOutputPerLine((Integer) object[22])
				.setDept((String) object[23]).setExtraProcess((String) object[24]).setFabTrimOrder((String) object[25])
				.setFabricCode((String) object[26]).setFabricEstDate((Date) object[27])
				.setFabricMill((String) object[28]).setFabricTestReportNumeric((String) object[29])
				.setFactoryId((String) object[30]).setFactoryName((String) object[31])
				.setFactoryRef((String) object[32]).setFiberContent((String) object[33]).setFob((BigDecimal) object[34])
				.setPlActualGac((Date) object[35]).setCpoGacDate((Date) object[36])
				.setGarmentTestActual((Date) object[37]).setGauge((String) object[38]).setGender((String) object[39])
				.setGraphicName((String) object[40]).setGrossSellPrice((BigDecimal) object[41])
				.setImage((String) object[42]).setInterfaceTs((Date) object[43]).setKeyItem((String) object[44])
				.setKnitting(PSRHelper.isEmptyOrNull(object[46])).setKnittingMachines((Integer) object[46])
				.setKnittingPcs((Integer) object[47]).setLinking((BigDecimal) object[48])
				.setLinkingMachines((Integer) object[49]).setLinkingPcs((Integer) object[50])
				.setMargin((BigDecimal) object[51]).setMerchant((String) object[52]).setModifyUser((String) object[53])
				.setNdcDate((Date) object[54]).setNdcWk((String) object[55]).setOcCommit((String) object[56])
				.setOrderQty((BigDecimal) object[57]).setOrderType((String) object[58]).setPackType((String) object[59])
				.setPackagingReadyDate((Date) object[60]).setProdOffice((String) object[61])
				.setProductType((String) object[62]).setProfomaPo((String) object[63])
				.setProgramName((String) object[64]).setSalesAmount((BigDecimal) object[65])
				.setSeason((String) object[66]).setSewingLines((Integer) object[67]).setShipMode((String) object[68])
				.setShippedQty((BigDecimal) object[69]).setStyle((String) object[70]).setStyleDesc((String) object[71])
				.setTicketColorCode((String) object[72]).setTicketStyleNumeric((String) object[73])
				.setVendorId((String) object[74]).setVendorName((String) object[75]).setVpoFtyGacDate((Date) object[76])
				.setVpoFtyGacDateTemp((Date) object[77]).setVpoHeadStatus((Date) object[78])
				.setVpoLineStatus((String) object[79]).setVpoNltGacDate((Date) object[80])
				.setVpoStatus((String) object[81])
				// .setEventDesc((String) object[82])
				.setActualDate((Date) object[83]).setPlannedDate((Date) object[84]).setRevisedDate((Date) object[85]);

	}

	public static PsrOrder getPsrOrder(PsrGridView gridView) {
		return new PsrOrder().setPsrOrderId(gridView.getPsrOrderId()).setVpo(gridView.getVpo())
				.setColorWashName(gridView.getColorWashName()).setShipTo(gridView.getShipTo())
				.setSoId(PSRHelper.isEmptyOrNull(gridView.getSoId())).setCustFtyId(gridView.getCustFtyId())
				.setFactoryRef(gridView.getFactoryRef()).setImage(gridView.getImage())
				.setFabricCode(gridView.getFabricCode()).setFiberContent(gridView.getFiberContent())
				.setFabricMill(gridView.getFabricMill()).setFabTrimOrder(gridView.getFabTrimOrder())
				.setColorCodePatrn(gridView.getColorCodePatrn()).setGraphicName(gridView.getGraphicName())
				.setOrderType(gridView.getOrderType()).setNdcWk(gridView.getNdcWk())
				.setApprFabricYarnLot(gridView.getApprFabricYarnLot()).setExtraProcess(gridView.getExtraProcess())
				.setActualPoundageAvailable(gridView.getActualPoundageAvailable())
				.setCutApprLetter(gridView.getCutApprLetter()).setCareLabelCode(gridView.getCareLabelCode())
				.setGauge(gridView.getGauge()).setKnittingPcs(PSRHelper.isEmptyOrNull(gridView.getKnittingPcs()))
				.setKnitting(gridView.getKnitting()).setLinkingPcs(PSRHelper.isEmptyOrNull(gridView.getLinkingPcs()))
				.setLinking(gridView.getLinking()).setSewingLines(PSRHelper.isEmptyOrNull(gridView.getSewingLines()))
				.setLinkingMachines(PSRHelper.isEmptyOrNull(gridView.getLinkingMachines()))
				.setKnittingMachines(PSRHelper.isEmptyOrNull(gridView.getKnittingMachines()))
				.setDailyOutputPerLine(PSRHelper.isEmptyOrNull(gridView.getDailyOutputPerLine()))
				.setFabricEstDate(gridView.getFabricEstDate())
				.setFabricTestReportNumeric(gridView.getFabricTestReportNumeric())
				.setGarmentTestActual(gridView.getGarmentTestActual()).setPackType(gridView.getPackType())
				.setPackagingReadyDate(gridView.getPackagingReadyDate()).setComment(gridView.getComment())
				.setTicketColorCode(gridView.getTicketColorCode())
				.setTicketStyleNumeric(gridView.getTicketStyleNumeric());
	}

	public static PsrEvents getPsrEvents(PsrGridView gridView, String eventCode) {
		PsrEventsPK id = new PsrEventsPK().setPsrOrderId(gridView.getPsrOrderId()).setEventCode(eventCode);
		return new PsrEvents().setId(id).setActualDate(gridView.getActualDate())
				.setRevisedDate(gridView.getRevisedDate());
	}

	public static PsrModelMaster getPsrModelMaster(String modelName) {
		return new PsrModelMaster().setModelName(modelName);
	}

	public static String constructDelImageXmlStr(String psrOrderId) {
		StringBuilder sb = new StringBuilder();
		if (psrOrderId != null && !psrOrderId.isEmpty()) {
			sb.append("<ORDERS><PSR><PSR_ORDER_ID>" + psrOrderId + "</PSR_ORDER_ID><IMAGE></IMAGE></PSR></ORDERS>");
		}
		return sb.toString();
	}

	public static String constructXmlStr(List<PsrOrder> orders, Map<Integer, List<PsrEventsView>> eventsWRTOrder,
			String imageurl) throws Exception {
		StringBuilder sb = new StringBuilder();
		try {
			if (orders != null && orders.size() > 0) {
				String start = "<ORDERS>";
				String end = "</ORDERS>";
				sb.append(start);
				String imageName = null;
				for (PsrOrder order : orders) {
					Integer orderId = order.getPsrOrderId();
					if (orderId != null) {
						sb.append("<PSR>");
						sb.append("<PSR_ORDER_ID>" + order.getPsrOrderId() + "</PSR_ORDER_ID>");
						if (order.getComment() != null && order.getComment().isEmpty() == FALSE) {
							sb.append("<COMMENT>" + StringEscapeUtils.escapeXml(order.getComment()) + "</COMMENT>");
						} else {
							sb.append("<COMMENT></COMMENT>");
						}
						if (PSRHelper.checkImage(order)) {
							try {
								imageName = PSRHelper.convertBase64ToImage(order.getImageType(), order.getImage(),
										order.getPsrOrderId(), imageurl);
								sb.append("<IMAGE>" + imageName + "</IMAGE>");
							} catch (IOException e) {
								log.error(String.format("Error in method convertBase64ToImage : %s", e.getMessage()));
								throw e;
							}
						}
						if (order.getCustFtyId() != null && order.getCustFtyId().isEmpty() == FALSE) {
							sb.append("<CUST_FTY_ID>" + StringEscapeUtils.escapeXml(order.getCustFtyId())
									+ "</CUST_FTY_ID>");
						} else {
							sb.append("<CUST_FTY_ID></CUST_FTY_ID>");
						}
						if (order.getCutApprLetter() != null && order.getCutApprLetter().isEmpty() == FALSE) {
							sb.append("<CUT_APPR_LETTER>" + StringEscapeUtils.escapeXml(order.getCutApprLetter())
									+ "</CUT_APPR_LETTER>");
						} else {
							sb.append("<CUT_APPR_LETTER></CUT_APPR_LETTER>");
						}
						if (order.getDailyOutputPerLine() != null) {
							sb.append("<DAILY_OUTPUT_PER_LINE>" + order.getDailyOutputPerLine()
									+ "</DAILY_OUTPUT_PER_LINE>");
						} else {
							sb.append("<DAILY_OUTPUT_PER_LINE></DAILY_OUTPUT_PER_LINE>");
						}
						if (PSRHelper.handleOnlyYesOrTrueOrOne(order.getExtraProcess())) {
							sb.append("<EXTRA_PROCESS>Y</EXTRA_PROCESS>");
						} else {
							sb.append("<EXTRA_PROCESS>N</EXTRA_PROCESS>");
						}
						if (order.getCpoPackType() != null && order.getCpoPackType().isEmpty() == FALSE) {
							sb.append("<CPO_PACK_TYPE>" + StringEscapeUtils.escapeXml(order.getCpoPackType())
									+ "</CPO_PACK_TYPE>");
						} else {
							sb.append("<CPO_PACK_TYPE></CPO_PACK_TYPE>");
						}
						if (order.getFabTrimOrder() != null && order.getFabTrimOrder().isEmpty() == FALSE) {
							sb.append("<FAB_TRIM_ORDER>" + StringEscapeUtils.escapeXml(order.getFabTrimOrder())
									+ "</FAB_TRIM_ORDER>");
						} else {
							sb.append("<FAB_TRIM_ORDER></FAB_TRIM_ORDER>");
						}
						if (order.getFabricCode() != null && order.getFabricCode().isEmpty() == FALSE) {
							sb.append("<FABRIC_CODE>" + StringEscapeUtils.escapeXml(order.getFabricCode())
									+ "</FABRIC_CODE>");
						} else {
							sb.append("<FABRIC_CODE></FABRIC_CODE>");
						}
						if (order.getFabricEstDateStr() != null && order.getFabricEstDateStr().isEmpty() == FALSE) {
							sb.append("<FABRIC_EST_DATE>" + order.getFabricEstDateStr() + "</FABRIC_EST_DATE>");
						} else {
							sb.append("<FABRIC_EST_DATE></FABRIC_EST_DATE>");
						}
						if (order.getFabricMill() != null && order.getFabricMill().isEmpty() == FALSE) {
							sb.append("<FABRIC_MILL>" + StringEscapeUtils.escapeXml(order.getFabricMill())
									+ "</FABRIC_MILL>");
						} else {
							sb.append("<FABRIC_MILL></FABRIC_MILL>");
						}
						if (order.getFabricTestReportNumeric() != null
								&& order.getFabricTestReportNumeric().isEmpty() == FALSE) {
							sb.append("<FABRIC_TEST_REPORT_NUMERIC>"
									+ StringEscapeUtils.escapeXml(order.getFabricTestReportNumeric())
									+ "</FABRIC_TEST_REPORT_NUMERIC>");
						} else {
							sb.append("<FABRIC_TEST_REPORT_NUMERIC></FABRIC_TEST_REPORT_NUMERIC>");
						}
						if (order.getFactoryRef() != null && order.getFactoryRef().isEmpty() == FALSE) {
							sb.append("<FACTORY_REF>" + StringEscapeUtils.escapeXml(order.getFactoryRef())
									+ "</FACTORY_REF>");
						} else {
							sb.append("<FACTORY_REF></FACTORY_REF>");
						}
						if (order.getFiberContent() != null && order.getFiberContent().isEmpty() == FALSE) {
							sb.append("<FIBER_CONTENT>" + StringEscapeUtils.escapeXml(order.getFiberContent())
									+ "</FIBER_CONTENT>");
						} else {
							sb.append("<FIBER_CONTENT></FIBER_CONTENT>");
						}
						if (order.getGarmentTestActualStr() != null
								&& order.getGarmentTestActualStr().isEmpty() == FALSE) {
							sb.append("<GARMENT_TEST_ACTUAL>" + order.getGarmentTestActualStr()
									+ "</GARMENT_TEST_ACTUAL>");
						} else {
							sb.append("<GARMENT_TEST_ACTUAL></GARMENT_TEST_ACTUAL>");
						}
						if (order.getGauge() != null && order.getGauge().isEmpty() == FALSE) {
							sb.append("<GAUGE>" + StringEscapeUtils.escapeXml(order.getGauge()) + "</GAUGE>");
						} else {
							sb.append("<GAUGE></GAUGE>");
						}
						if (order.getGraphicName() != null && order.getGraphicName().isEmpty() == FALSE) {
							sb.append("<GRAPHIC_NAME>" + StringEscapeUtils.escapeXml(order.getGraphicName())
									+ "</GRAPHIC_NAME>");
						} else {
							sb.append("<GRAPHIC_NAME></GRAPHIC_NAME>");
						}
						if (order.getKnitting() != null) {
							sb.append("<KNITTING>" + order.getKnitting() + "</KNITTING>");
						} else {
							sb.append("<KNITTING></KNITTING>");
						}
						if (order.getKnittingMachines() != null) {
							sb.append("<KNITTING_MACHINES>" + order.getKnittingMachines() + "</KNITTING_MACHINES>");
						} else {
							sb.append("<KNITTING_MACHINES></KNITTING_MACHINES>");
						}
						if (order.getKnittingPcs() != null) {
							sb.append("<KNITTING_PCS>" + order.getKnittingPcs() + "</KNITTING_PCS>");
						} else {
							sb.append("<KNITTING_PCS></KNITTING_PCS>");
						}
						if (order.getLinking() != null) {
							sb.append("<LINKING>" + order.getLinking() + "</LINKING>");
						} else {
							sb.append("<LINKING></LINKING>");
						}
						if (order.getLinkingMachines() != null) {
							sb.append("<LINKING_MACHINES>" + order.getLinkingMachines() + "</LINKING_MACHINES>");
						} else {
							sb.append("<LINKING_MACHINES></LINKING_MACHINES>");
						}
						if (order.getLinkingPcs() != null) {
							sb.append("<LINKING_PCS>" + order.getLinkingPcs() + "</LINKING_PCS>");
						} else {
							sb.append("<LINKING_PCS></LINKING_PCS>");
						}
						if (order.getNdcWk() != null && order.getNdcWk().isEmpty() == FALSE) {
							sb.append("<NDC_WK>" + StringEscapeUtils.escapeXml(order.getNdcWk()) + "</NDC_WK>");
						} else {
							sb.append("<NDC_WK></NDC_WK>");
						}
						if (order.getOrderType() != null && order.getOrderType().isEmpty() == FALSE) {
							sb.append("<ORDER_TYPE>" + StringEscapeUtils.escapeXml(order.getOrderType())
									+ "</ORDER_TYPE>");
						} else {
							sb.append("<ORDER_TYPE></ORDER_TYPE>");
						}
						if (order.getPackType() != null && order.getPackType().isEmpty() == FALSE) {
							sb.append(
									"<PACK_TYPE>" + StringEscapeUtils.escapeXml(order.getPackType()) + "</PACK_TYPE>");
						} else {
							sb.append("<PACK_TYPE></PACK_TYPE>");
						}
						if (order.getPackagingReadyDateStr() != null
								&& order.getPackagingReadyDateStr().isEmpty() == FALSE) {
							sb.append("<PACKAGING_READY_DATE>" + order.getPackagingReadyDateStr()
									+ "</PACKAGING_READY_DATE>");
						} else {
							sb.append("<PACKAGING_READY_DATE></PACKAGING_READY_DATE>");
						}
						if (order.getColorCodePatrn() != null && order.getColorCodePatrn().isEmpty() == FALSE) {
							sb.append("<COLOR_CODE_PATRN>" + StringEscapeUtils.escapeXml(order.getColorCodePatrn())
									+ "</COLOR_CODE_PATRN>");
						} else {
							sb.append("<COLOR_CODE_PATRN></COLOR_CODE_PATRN>");
						}
						if (order.getApprFabricYarnLot() != null && order.getApprFabricYarnLot().isEmpty() == FALSE) {
							sb.append(
									"<APPR_FABRIC_YARN_LOT>" + StringEscapeUtils.escapeXml(order.getApprFabricYarnLot())
											+ "</APPR_FABRIC_YARN_LOT>");
						} else {
							sb.append("<APPR_FABRIC_YARN_LOT></APPR_FABRIC_YARN_LOT>");
						}
						if (order.getActualPoundageAvailable() != null) {
							sb.append("<ACTUAL_POUNDAGE_AVAILABLE>" + order.getActualPoundageAvailable()
									+ "</ACTUAL_POUNDAGE_AVAILABLE>");
						} else {
							sb.append("<ACTUAL_POUNDAGE_AVAILABLE></ACTUAL_POUNDAGE_AVAILABLE>");
						}
						if (order.getCareLabelCode() != null && order.getCareLabelCode().isEmpty() == FALSE) {
							sb.append("<CARE_LABEL_CODE>" + StringEscapeUtils.escapeXml(order.getCareLabelCode())
									+ "</CARE_LABEL_CODE>");
						} else {
							sb.append("<CARE_LABEL_CODE></CARE_LABEL_CODE>");
						}
						if (order.getSewingLines() != null) {
							sb.append("<SEWING_LINES>" + order.getSewingLines() + "</SEWING_LINES>");
						} else {
							sb.append("<SEWING_LINES></SEWING_LINES>");
						}
						if (order.getTicketColorCode() != null && order.getTicketColorCode().isEmpty() == FALSE) {
							sb.append("<TICKET_COLOR_CODE>" + StringEscapeUtils.escapeXml(order.getTicketColorCode())
									+ "</TICKET_COLOR_CODE>");
						} else {
							sb.append("<TICKET_COLOR_CODE></TICKET_COLOR_CODE>");
						}
						if (order.getTicketStyleNumeric() != null && order.getTicketStyleNumeric().isEmpty() == FALSE) {
							sb.append("<TICKET_STYLE_NUMERIC>"
									+ StringEscapeUtils.escapeXml(order.getTicketStyleNumeric())
									+ "</TICKET_STYLE_NUMERIC>");
						} else {
							sb.append("<TICKET_STYLE_NUMERIC></TICKET_STYLE_NUMERIC>");
						}
						if (order.getGarmentTestReportNo() != null
								&& order.getGarmentTestReportNo().isEmpty() == FALSE) {
							sb.append("<GARMENT_TEST_REPORT_NO>"
									+ StringEscapeUtils.escapeXml(order.getGarmentTestReportNo())
									+ "</GARMENT_TEST_REPORT_NO>");
						} else {
							sb.append("<GARMENT_TEST_REPORT_NO></GARMENT_TEST_REPORT_NO>");
						}
						if (order.getPlmNo() != null && order.getPlmNo().isEmpty() == FALSE) {
							sb.append("<PLM_NO>" + StringEscapeUtils.escapeXml(order.getPlmNo()) + "</PLM_NO>");
						} else {
							sb.append("<PLM_NO></PLM_NO>");
						}
						if (PSRHelper.handleOnlyYesOrTrueOrOne(order.getWashYn())) {
							sb.append("<WASH_YN>Y</WASH_YN>");
						} else {
							sb.append("<WASH_YN>N</WASH_YN>");
						}
						if (order.getWashFacility() != null && order.getWashFacility().isEmpty() == FALSE) {
							sb.append("<WASH_FACILITY>" + StringEscapeUtils.escapeXml(order.getWashFacility())
									+ "</WASH_FACILITY>");
						} else {
							sb.append("<WASH_FACILITY></WASH_FACILITY>");
						}
						if (order.getFabricShipMode() != null && order.getFabricShipMode().isEmpty() == FALSE) {
							sb.append("<FABRIC_SHIP_MODE>" + StringEscapeUtils.escapeXml(order.getFabricShipMode())
									+ "</FABRIC_SHIP_MODE>");
						} else {
							sb.append("<FABRIC_SHIP_MODE></FABRIC_SHIP_MODE>");
						}
						if (order.getTechPackReceivedDateStr() != null
								&& order.getTechPackReceivedDateStr().isEmpty() == FALSE) {
							sb.append("<TECH_PACK_RECEIVED_DATE>" + order.getTechPackReceivedDateStr()
									+ "</TECH_PACK_RECEIVED_DATE>");
						} else {
							sb.append("<TECH_PACK_RECEIVED_DATE></TECH_PACK_RECEIVED_DATE>");
						}
						if (order.getSampleMaterialStatus() != null
								&& order.getSampleMaterialStatus().isEmpty() == FALSE) {
							sb.append("<SAMPLE_MATERIAL_STATUS>"
									+ StringEscapeUtils.escapeXml(order.getSampleMaterialStatus())
									+ "</SAMPLE_MATERIAL_STATUS>");
						} else {
							sb.append("<SAMPLE_MATERIAL_STATUS></SAMPLE_MATERIAL_STATUS>");
						}
						if (order.getRetailPrice() != null) {
							sb.append("<RETAIL_PRICE>" + order.getRetailPrice().setScale(2, RoundingMode.HALF_EVEN)
									+ "</RETAIL_PRICE>");
						} else {
							sb.append("<RETAIL_PRICE></RETAIL_PRICE>");
						}
						if (order.getFabricPpDateStr() != null && order.getFabricPpDateStr().isEmpty() == FALSE) {
							sb.append("<FABRIC_PP_DATE>" + order.getFabricPpDateStr() + "</FABRIC_PP_DATE>");
						} else {
							sb.append("<FABRIC_PP_DATE></FABRIC_PP_DATE>");
						}
						if (order.getDeliveryMonth() != null && order.getDeliveryMonth().isEmpty() == FALSE) {
							sb.append("<DELIVERY_MONTH>" + StringEscapeUtils.escapeXml(order.getDeliveryMonth())
									+ "</DELIVERY_MONTH>");
						} else {
							sb.append("<DELIVERY_MONTH></DELIVERY_MONTH>");
						}
						if (order.getCpoAccDateByVendorStr() != null
								&& order.getCpoAccDateByVendorStr().isEmpty() == FALSE) {
							sb.append("<CPO_ACC_DATE_BY_VENDOR>" + order.getCpoAccDateByVendorStr()
									+ "</CPO_ACC_DATE_BY_VENDOR>");
						} else {
							sb.append("<CPO_ACC_DATE_BY_VENDOR></CPO_ACC_DATE_BY_VENDOR>");
						}
						if (order.getDistChannel() != null && order.getDistChannel().isEmpty() == FALSE) {
							sb.append("<DIST_CHANNEL>" + StringEscapeUtils.escapeXml(order.getDistChannel())
									+ "</DIST_CHANNEL>");
						} else {
							sb.append("<DIST_CHANNEL></DIST_CHANNEL>");
						}
						if (PSRHelper.handleOnlyYesOrTrueOrOne(order.getFloorSet())) {
							sb.append("<FLOOR_SET>Y</FLOOR_SET>");
						} else {
							sb.append("<FLOOR_SET>N</FLOOR_SET>");
						}
						if (PSRHelper.handleOnlyYesOrTrueOrOne(order.getStyleAtRisk())) {
							sb.append("<STYLE_AT_RISK>Y</STYLE_AT_RISK>");
						} else {
							sb.append("<STYLE_AT_RISK>N</STYLE_AT_RISK>");
						}
						if (order.getSampleMerchEtaStr() != null && order.getSampleMerchEtaStr().isEmpty() == FALSE) {
							sb.append("<SAMPLE_MERCH_ETA>" + order.getSampleMerchEtaStr() + "</SAMPLE_MERCH_ETA>");
						} else {
							sb.append("<SAMPLE_MERCH_ETA></SAMPLE_MERCH_ETA>");
						}
						if (order.getSampleFloorSetEtaStr() != null
								&& order.getSampleFloorSetEtaStr().isEmpty() == FALSE) {
							sb.append("<SAMPLE_FLOOR_SET_ETA>" + order.getSampleFloorSetEtaStr()
									+ "</SAMPLE_FLOOR_SET_ETA>");
						} else {
							sb.append("<SAMPLE_FLOOR_SET_ETA></SAMPLE_FLOOR_SET_ETA>");
						}
						if (order.getSampleDcomEtaStr() != null && order.getSampleDcomEtaStr().isEmpty() == FALSE) {
							sb.append("<SAMPLE_DCOM_ETA>" + order.getSampleDcomEtaStr() + "</SAMPLE_DCOM_ETA>");
						} else {
							sb.append("<SAMPLE_DCOM_ETA></SAMPLE_DCOM_ETA>");
						}
						if (PSRHelper.handleOnlyYesOrTrueOrOne(order.getSampleMailer())) {
							sb.append("<SAMPLE_MAILER>Y</SAMPLE_MAILER>");
						} else {
							sb.append("<SAMPLE_MAILER>N</SAMPLE_MAILER>");
						}
						if (order.getSampleMailerEtaStr() != null && order.getSampleMailerEtaStr().isEmpty() == FALSE) {
							sb.append("<SAMPLE_MAILER_ETA>" + order.getSampleMailerEtaStr() + "</SAMPLE_MAILER_ETA>");
						} else {
							sb.append("<SAMPLE_MAILER_ETA></SAMPLE_MAILER_ETA>");
						}
						if (order.getTopSampleEtaDateStr() != null
								&& order.getTopSampleEtaDateStr().isEmpty() == FALSE) {
							sb.append("<TOP_SAMPLE_ETA_DATE>" + order.getTopSampleEtaDateStr()
									+ "</TOP_SAMPLE_ETA_DATE>");
						} else {
							sb.append("<TOP_SAMPLE_ETA_DATE></TOP_SAMPLE_ETA_DATE>");
						}
						if (order.getTopSampleComment() != null && order.getTopSampleComment().isEmpty() == FALSE) {
							sb.append("<TOP_SAMPLE_COMMENT>" +  StringEscapeUtils.escapeXml(order.getTopSampleComment()) + "</TOP_SAMPLE_COMMENT>");
						} else {
							sb.append("<TOP_SAMPLE_COMMENT></TOP_SAMPLE_COMMENT>");
						}
						if (order.getPhotoMerchantSampleSendDateStr() != null
								&& order.getPhotoMerchantSampleSendDateStr().isEmpty() == FALSE) {
							sb.append("<PHOTO_MERCHANT_SAMPLE_SEND_DATE>" + order.getPhotoMerchantSampleSendDateStr()
									+ "</PHOTO_MERCHANT_SAMPLE_SEND_DATE>");
						} else {
							sb.append("<PHOTO_MERCHANT_SAMPLE_SEND_DATE></PHOTO_MERCHANT_SAMPLE_SEND_DATE>");
						}
						if (order.getPhotoMerchantSampleEtaDateStr() != null
								&& order.getPhotoMerchantSampleEtaDateStr().isEmpty() == FALSE) {
							sb.append("<PHOTO_MERCHANT_SAMPLE_ETA_DATE>" + order.getPhotoMerchantSampleEtaDateStr()
									+ "</PHOTO_MERCHANT_SAMPLE_ETA_DATE>");
						} else {
							sb.append("<PHOTO_MERCHANT_SAMPLE_ETA_DATE></PHOTO_MERCHANT_SAMPLE_ETA_DATE>");
						}
						if (order.getPhotoMerchantSampleComment() != null
								&& order.getPhotoMerchantSampleComment().isEmpty() == FALSE) {
							sb.append("<PHOTO_MERCHANT_SAMPLE_COMMENT>" + StringEscapeUtils.escapeXml(order.getPhotoMerchantSampleComment())
									+ "</PHOTO_MERCHANT_SAMPLE_COMMENT>");
						} else {
							sb.append("<PHOTO_MERCHANT_SAMPLE_COMMENT></PHOTO_MERCHANT_SAMPLE_COMMENT>");
						}
						if (order.getMarketingSampleSendDateStr() != null
								&& order.getMarketingSampleSendDateStr().isEmpty() == FALSE) {
							sb.append("<MARKETING_SAMPLE_SEND_DATE>" + order.getMarketingSampleSendDateStr()
									+ "</MARKETING_SAMPLE_SEND_DATE>");
						} else {
							sb.append("<MARKETING_SAMPLE_SEND_DATE></MARKETING_SAMPLE_SEND_DATE>");
						}
						if (order.getMarketingSampleEtaDateStr() != null
								&& order.getMarketingSampleEtaDateStr().isEmpty() == FALSE) {
							sb.append("<MARKETING_SAMPLE_ETA_DATE>" + order.getMarketingSampleEtaDateStr()
									+ "</MARKETING_SAMPLE_ETA_DATE>");
						} else {
							sb.append("<MARKETING_SAMPLE_ETA_DATE></MARKETING_SAMPLE_ETA_DATE>");
						}
						if (order.getMarketingSampleComment() != null
								&& order.getMarketingSampleComment().isEmpty() == FALSE) {
							sb.append("<MARKETING_SAMPLE_COMMENT>" + StringEscapeUtils.escapeXml(order.getMarketingSampleComment())
									+ "</MARKETING_SAMPLE_COMMENT>");
						} else {
							sb.append("<MARKETING_SAMPLE_COMMENT></MARKETING_SAMPLE_COMMENT>");
						}
						if (order.getVisualSampleSendDateStr() != null
								&& order.getVisualSampleSendDateStr().isEmpty() == FALSE) {
							sb.append("<VISUAL_SAMPLE_SEND_DATE>" + order.getVisualSampleSendDateStr()
									+ "</VISUAL_SAMPLE_SEND_DATE>");
						} else {
							sb.append("<VISUAL_SAMPLE_SEND_DATE></VISUAL_SAMPLE_SEND_DATE>");
						}
						if (order.getVisualSampleEtaDateStr() != null
								&& order.getVisualSampleEtaDateStr().isEmpty() == FALSE) {
							sb.append("<VISUAL_SAMPLE_ETA_DATE>" + order.getVisualSampleEtaDateStr()
									+ "</VISUAL_SAMPLE_ETA_DATE>");
						} else {
							sb.append("<VISUAL_SAMPLE_ETA_DATE></VISUAL_SAMPLE_ETA_DATE>");
						}
						if (order.getVisualSampleComment() != null
								&& order.getVisualSampleComment().isEmpty() == FALSE) {
							sb.append("<VISUAL_SAMPLE_COMMENT>" + StringEscapeUtils.escapeXml(order.getVisualSampleComment())
									+ "</VISUAL_SAMPLE_COMMENT>");
						} else {
							sb.append("<VISUAL_SAMPLE_COMMENT></VISUAL_SAMPLE_COMMENT>");
						}
						if (order.getCopyrightSampleSendDateStr() != null
								&& order.getCopyrightSampleSendDateStr().isEmpty() == FALSE) {
							sb.append("<COPYRIGHT_SAMPLE_SEND_DATE>" + order.getCopyrightSampleSendDateStr()
									+ "</COPYRIGHT_SAMPLE_SEND_DATE>");
						} else {
							sb.append("<COPYRIGHT_SAMPLE_SEND_DATE></COPYRIGHT_SAMPLE_SEND_DATE>");
						}
						if (order.getCopyrightSampleEtaDateStr() != null
								&& order.getCopyrightSampleEtaDateStr().isEmpty() == FALSE) {
							sb.append("<COPYRIGHT_SAMPLE_ETA_DATE>" + order.getCopyrightSampleEtaDateStr()
									+ "</COPYRIGHT_SAMPLE_ETA_DATE>");
						} else {
							sb.append("<COPYRIGHT_SAMPLE_ETA_DATE></COPYRIGHT_SAMPLE_ETA_DATE>");
						}
						if (order.getCopyrightSampleComment() != null
								&& order.getCopyrightSampleComment().isEmpty() == FALSE) {
							sb.append("<COPYRIGHT_SAMPLE_COMMENT>" + StringEscapeUtils.escapeXml(order.getCopyrightSampleComment())
									+ "</COPYRIGHT_SAMPLE_COMMENT>");
						} else {
							sb.append("<COPYRIGHT_SAMPLE_COMMENT></COPYRIGHT_SAMPLE_COMMENT>");
						}
						if (order.getReasonForLateGac() != null && order.getReasonForLateGac().isEmpty() == FALSE) {
							sb.append("<REASON_FOR_LATE_GAC>" + StringEscapeUtils.escapeXml(order.getReasonForLateGac()) + "</REASON_FOR_LATE_GAC>");
						} else {
							sb.append("<REASON_FOR_LATE_GAC></REASON_FOR_LATE_GAC>");
						}
						if (order.getAdditionalBulkLotApproveStr() != null
								&& order.getAdditionalBulkLotApproveStr().isEmpty() == FALSE) {
							sb.append("<ADDITIONAL_BULK_LOT_APPROVE>" + order.getAdditionalBulkLotApproveStr()
									+ "</ADDITIONAL_BULK_LOT_APPROVE>");
						} else {
							sb.append("<ADDITIONAL_BULK_LOT_APPROVE></ADDITIONAL_BULK_LOT_APPROVE>");
						}
						if (order.getColorComment() != null
								&& order.getColorComment().isEmpty() == FALSE) {
							sb.append("<COLOR_COMMENT>" + StringEscapeUtils.escapeXml(order.getColorComment())
									+ "</COLOR_COMMENT>");
						} else {
							sb.append("<COLOR_COMMENT></COLOR_COMMENT>");
						}
						if (order.getFabricComment() != null
								&& order.getFabricComment().isEmpty() == FALSE) {
							sb.append("<FABRIC_COMMENT>" + StringEscapeUtils.escapeXml(order.getFabricComment())
									+ "</FABRIC_COMMENT>");
						} else {
							sb.append("<FABRIC_COMMENT></FABRIC_COMMENT>");
						}
						
						if (order.getFabricTestComment() != null
								&& order.getFabricTestComment().isEmpty() == FALSE) {
							sb.append("<FABRIC_TEST_COMMENT>" + StringEscapeUtils.escapeXml(order.getFabricTestComment())
									+ "</FABRIC_TEST_COMMENT>");
						} else {
							sb.append("<FABRIC_TEST_COMMENT></FABRIC_TEST_COMMENT>");
						}
						if (order.getGarmentTestComment() != null
								&& order.getGarmentTestComment().isEmpty() == FALSE) {
							sb.append("<GARMENT_TEST_COMMENT>" + StringEscapeUtils.escapeXml(order.getGarmentTestComment())
									+ "</GARMENT_TEST_COMMENT>");
						} else {
							sb.append("<GARMENT_TEST_COMMENT></GARMENT_TEST_COMMENT>");
						}
						if (order.getTrimComment() != null
								&& order.getTrimComment().isEmpty() == FALSE) {
							sb.append("<TRIM_COMMENT>" + StringEscapeUtils.escapeXml(order.getTrimComment())
									+ "</TRIM_COMMENT>");
						} else {
							sb.append("<TRIM_COMMENT></TRIM_COMMENT>");
						}
						if (order.getSampleComment() != null
								&& order.getSampleComment().isEmpty() == FALSE) {
							sb.append("<SAMPLE_COMMENT>" + StringEscapeUtils.escapeXml(order.getSampleComment())
									+ "</SAMPLE_COMMENT>");
						} else {
							sb.append("<SAMPLE_COMMENT></SAMPLE_COMMENT>");
						}
						if (order.getFitSampleComment() != null
								&& order.getFitSampleComment().isEmpty() == FALSE) {
							sb.append("<FIT_SAMPLE_COMMENT>" + StringEscapeUtils.escapeXml(order.getFitSampleComment())
									+ "</FIT_SAMPLE_COMMENT>");
						} else {
							sb.append("<FIT_SAMPLE_COMMENT></FIT_SAMPLE_COMMENT>");
						}
						if (order.getPpSampleComment() != null
								&& order.getPpSampleComment().isEmpty() == FALSE) {
							sb.append("<PP_SAMPLE_COMMENT>" + StringEscapeUtils.escapeXml(order.getPpSampleComment())
									+ "</PP_SAMPLE_COMMENT>");
						} else {
							sb.append("<PP_SAMPLE_COMMENT></PP_SAMPLE_COMMENT>");
						}
						if (order.getGarmentTreatmentPcs() != null) {
							sb.append("<GARMENT_TREATEMENT_PCS>" + order.getGarmentTreatmentPcs()
									+ "</GARMENT_TREATEMENT_PCS>");
						} else {
							sb.append("<GARMENT_TREATEMENT_PCS></GARMENT_TREATEMENT_PCS>");
						}
						if (order.getFinishedGarmentPcs() != null) {
							sb.append("<FINISHED_GARMENT_PCS>" + order.getFinishedGarmentPcs()
									+ "</FINISHED_GARMENT_PCS>");
						} else {
							sb.append("<FINISHED_GARMENT_PCS></FINISHED_GARMENT_PCS>");
						}
						if (order.getPackedUnitsPcs() != null) {
							sb.append("<PACKED_UNITS_PCS>" + order.getPackedUnitsPcs()
									+ "</PACKED_UNITS_PCS>");
						} else {
							sb.append("<PACKED_UNITS_PCS></PACKED_UNITS_PCS>");
						}
						if (eventsWRTOrder != null && eventsWRTOrder.size() > 0) {
							List<PsrEventsView> events = eventsWRTOrder.get(orderId);
							if (events != null && events.size() > 0) {
								sb.append("<EVENTS>");
								for (PsrEventsView event : events) {
									if (event.getEventCode() != null && event.getEventCode().trim().length() > 0) {
										sb.append("<EVENT>");
										sb.append("<EVENT_CODE>" + event.getEventCode() + "</EVENT_CODE> ");
										if (!event.getEventCode().trim().equalsIgnoreCase("E000") && event.getActualDateStr() != null
												&& event.getActualDateStr().isEmpty() == FALSE) {
											sb.append("<ACTUAL_DATE>" + event.getActualDateStr() + "</ACTUAL_DATE>");
										} 
										else if (!event.getEventCode().trim().equalsIgnoreCase("E000")) {
											sb.append("<ACTUAL_DATE></ACTUAL_DATE>");
										}
										if (event.getRevisedDateStr() != null
												&& event.getRevisedDateStr().isEmpty() == FALSE) {
											sb.append("<REVISED_DATE>" + event.getRevisedDateStr() + "</REVISED_DATE>");
										} else {
											sb.append("<REVISED_DATE></REVISED_DATE>");
										}
										sb.append("</EVENT>");
									}
								}
								sb.append("</EVENTS>");
							}
						}
						sb.append("</PSR>");
					}
				}
				sb.append(end);
			}
		} catch (Exception e) {
			log.error("Error occured in DAOHelper---" + e.getMessage());
			throw e;
		}
		return sb.toString();
	}

	public static Map<Integer, List<PsrEventsView>> getEventsWRTOrder(List<PsrEventsView> events) {
		try {
			Map<Integer, List<PsrEventsView>> eventsWRTOrder = new HashMap<Integer, List<PsrEventsView>>();
			if (events != null && events.isEmpty() == FALSE) {
				for (PsrEventsView event : events) {
					if (event != null) {
						int psrOrderId = event.getPsrOrderId();
						if (eventsWRTOrder.containsKey(psrOrderId)) {
							List<PsrEventsView> existingList = eventsWRTOrder.get(psrOrderId);
							existingList.add(event);
						} else {
							List<PsrEventsView> list = new ArrayList<PsrEventsView>();
							list.add(event);
							eventsWRTOrder.put(psrOrderId, list);
						}
					}
				}
			}
			return eventsWRTOrder;
		} catch (Exception e) {
			log.error("Error occured in getEventsWRTOrder", e.getMessage());
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public static List<Map> getExportData(SearchForm searchForm, List<Object[]> list, List<String> eventDescList,
			String selectedColoumns) {
		log.info(" entered into the method getExportData .");
		try {
			List<String> selctedColList = Arrays.asList(selectedColoumns.split(","));
			String roleName = searchForm.getRoleName();
			List<Map> psrData1 = new LinkedList<>();
			Map<String, Object> psrJson = null;
			Map<Integer, Map> psrData = new LinkedHashMap<>();
			for (Object[] object : list) {
				Integer psrOrderPK = (Integer) object[GridConstants.psrOrderId];
				if (psrData.containsKey(psrOrderPK)) {
					psrJson = psrData.get(psrOrderPK);
					for (String eventdesc : eventDescList) {
						if (!selctedColList.contains(eventdesc.replaceAll(" ", "").toLowerCase()) && eventdesc.trim()
								.equalsIgnoreCase(((String) object[GridConstants.eventDesc]).trim())) {
							
							psrJson.put(eventdesc + ApplicationConstants.DISABLE_EVENT,
									object[GridConstants.disableEvent]);
							psrJson.put(eventdesc + ApplicationConstants.HYPHEN_HIGHLIGHT_REVISED_DATE,
									object[GridConstants.highlightReviseDate]);
							psrJson.put(eventdesc + ApplicationConstants.HYPHEN_EVENT_CODE,
									object[GridConstants.EventCode]);
							psrJson.put(eventdesc + ApplicationConstants.HYPHEN_PLANNED_DATE,
									PSRHelper.getDateStr((Date) object[GridConstants.PlannedDate]));
							psrJson.put(eventdesc + ApplicationConstants.HYPHEN_REVISED_DATE,
									PSRHelper.getDateStr((Date) object[GridConstants.RevisedDate]));
							psrJson.put(eventdesc + ApplicationConstants.HYPHEN_ACTUAL_DATE,
									PSRHelper.getDateStr((Date) object[GridConstants.ActualDate]));
							
						}
					}
				} else {
					psrJson = new LinkedHashMap<>();
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "style"))
						psrJson.put("Style #", object[GridConstants.style]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "colorWashName"))
						psrJson.put("Color Wash Name", object[GridConstants.colorWashName]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "ocCommit"))
						psrJson.put("Order Commit #", object[GridConstants.ocCommit]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "vpo"))
						psrJson.put("Vpo #", object[GridConstants.vpo]);
//				if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Psr Order Id"))
					psrJson.put("Psr Order Id", object[GridConstants.psrOrderId]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Style Desc"))
						psrJson.put("Style Desc", object[GridConstants.styleDesc]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Init Flow"))
						psrJson.put("Init Flow", object[GridConstants.initFlow]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Image")) {
						if (object[GridConstants.image] == null
								|| object[GridConstants.image].toString().trim().equalsIgnoreCase("null"))
							psrJson.put("Image", "");
						else
							psrJson.put("Image", object[GridConstants.image]);
					}
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Order Qty"))
						psrJson.put("Order Qty", object[GridConstants.orderQty]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Cpo"))
						psrJson.put("Cpo", object[GridConstants.cpo]);
					if (!roleName.equalsIgnoreCase("PSR_VENDOR")) {
						if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Ndc Date"))
							psrJson.put("Ndc Date", PSRHelper.getDateStr((Date) object[GridConstants.ndcDate]));
					}
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Vpo Fty Gac Date"))
						psrJson.put("Vpo Fty Gac Date",
								PSRHelper.getDateStr((Date) object[GridConstants.vpoFtyGacDate]));
					if (!roleName.equalsIgnoreCase("PSR_VENDOR")) {
						if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Vpo Nlt Gac Date"))
							psrJson.put("Vpo Nlt Gac Date",
									PSRHelper.getDateStr((Date) object[GridConstants.vpoNltGacDate]));
					}
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Ndc Wk"))
						psrJson.put("Ndc Wk", object[GridConstants.ndcWk]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Vendor Id"))
						psrJson.put("Vendor Id", object[GridConstants.vendorId]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Factory Id"))
						psrJson.put("Factory Id", object[GridConstants.factoryId]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Factory Name"))
						psrJson.put("Factory Name", object[GridConstants.factoryName]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "prodOffice"))
						psrJson.put("Production Office", object[GridConstants.prodOffice]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Dept"))
						psrJson.put("Dept", object[GridConstants.dept]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Fob"))
						psrJson.put("Fob", object[GridConstants.fob]);
					if (!roleName.equalsIgnoreCase("PSR_VENDOR")) {
						if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Gross Sell Price"))
							psrJson.put("Gross Sell Price", object[GridConstants.grossSellPrice]);
					}
					if (!roleName.equalsIgnoreCase("PSR_VENDOR")) {
						if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Margin"))
							psrJson.put("Margin", object[GridConstants.margin]);
					}
					if (!roleName.equalsIgnoreCase("PSR_VENDOR")) {
						if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Sales Amount"))
							psrJson.put("Sales Amount", object[GridConstants.salesAmount]);
					}
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Ship To"))
						psrJson.put("Ship To", object[GridConstants.shipTo]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Ship Mode"))
						psrJson.put("Ship Mode", object[GridConstants.shipMode]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "CPO Style No"))
						psrJson.put("CPO Style No", object[GridConstants.cpoStyleNo]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Key Item"))
						psrJson.put("Key Item", object[GridConstants.keyItem]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "CPO Dept"))
						psrJson.put("CPO Dept", object[GridConstants.cpoDept]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "plmNo"))
						psrJson.put("Design Card Number/PLM Number", object[GridConstants.plmNo]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Cpo Color Desc"))
						psrJson.put("Cpo Color Desc", object[GridConstants.cpoColorDesc]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Cpo Order Qty"))
						psrJson.put("Cpo Order Qty", object[GridConstants.cpoOrderQty]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Cpo Ship Mode"))
						psrJson.put("Cpo Ship Mode", object[GridConstants.cpoShipMode]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "graphicName"))
						psrJson.put("Artwork name", object[GridConstants.graphicName]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "ticketStyleNumeric"))
						psrJson.put("Ticket Style Number", object[GridConstants.ticketStyleNumeric]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Ticket Color Code"))
						psrJson.put("Ticket Color Code", object[GridConstants.ticketColorCode]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Care Label Code"))
						psrJson.put("Care Label Code", object[GridConstants.careLabelCode]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "colorCodePatrn"))
						psrJson.put("Color Code Pattern", object[GridConstants.colorCodePatrn]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Cpo Pack Type"))
						psrJson.put("Cpo Pack Type", object[GridConstants.cpoPackType]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Cust Fty Id"))
						psrJson.put("Cust Fty Id", object[GridConstants.custFtyId]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Cut Appr Letter"))
						psrJson.put("Cut Appr Letter", object[GridConstants.cutApprLetter]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "extraProcess")) {
						if (object[GridConstants.extraProcess] != null) {
							if (((String) object[GridConstants.extraProcess]).equalsIgnoreCase("Y"))
								psrJson.put("Extra Process", "YES");
							else
								psrJson.put("Extra Process", null);
						} else {
							psrJson.put("Extra Process", null);
						}
					}
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Fabric Code"))
						psrJson.put("Fabric Code", object[GridConstants.fabricCode]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Fiber Content"))
						psrJson.put("Fiber Content", object[GridConstants.fiberContent]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Fabric Mill"))
						psrJson.put("Fabric Mill", object[GridConstants.fabricMill]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "requestNo"))
						psrJson.put("Request Number", object[GridConstants.requestNo]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Program Name"))
						psrJson.put("Program Name", object[GridConstants.programName]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Fab Trim Order"))
						psrJson.put("Fab Trim Order", object[GridConstants.fabTrimOrder]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Factory Ref"))
						psrJson.put("Factory Ref", object[GridConstants.factoryRef]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "orderType"))
						psrJson.put("Sell Channel", object[GridConstants.orderType]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Pack Type"))
						psrJson.put("Pack Type", object[GridConstants.packType]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "VPO Order Type"))
						psrJson.put("VPO Order Type", object[GridConstants.vpoOrderType]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "speedIndicator"))
						psrJson.put("VPO Speed Indicator", object[GridConstants.speedIndicator]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "asn"))
						psrJson.put("ANF ASN #", object[GridConstants.asn]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Shipped Qty"))
						psrJson.put("Shipped Qty", object[GridConstants.shippedQty]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Balance Ship"))
						psrJson.put("Balance Ship", object[GridConstants.balanceShip]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Comment"))
						psrJson.put("Comment", object[GridConstants.comment]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Season"))
						psrJson.put("Season", object[GridConstants.season]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Vpo Status"))
						psrJson.put("Vpo Status", object[GridConstants.vpoStatus]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Vpo Line Status"))
						psrJson.put("Vpo Line Status", object[GridConstants.vpoLineStatus]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Merchant"))
						psrJson.put("Merchant", object[GridConstants.merchant]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Product Type"))
						psrJson.put("Product Type", object[GridConstants.productType]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Gender"))
						psrJson.put("Gender", object[GridConstants.gender]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Country Origin"))
						psrJson.put("Country Origin", object[GridConstants.countryOrigin]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "ladingPoint"))
						psrJson.put("Port of Origin", object[GridConstants.ladingPoint]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "techDesigner"))
						psrJson.put("Fit Owner", object[GridConstants.techDesigner]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "washYn")) {
						if (object[GridConstants.washYn] != null) {
							if (((String) object[GridConstants.washYn]).equalsIgnoreCase("Y")
									|| ((String) object[GridConstants.washYn]).equals("YES"))
								psrJson.put("Wash", "YES");
							else
								psrJson.put("Wash", null);
						} else {
							psrJson.put("Wash", null);
						}
					}
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Wash Facility"))
						psrJson.put("Wash Facility", object[GridConstants.washFacility]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Fabric Ship Mode"))
						psrJson.put("Fabric Ship Mode", object[GridConstants.fabricShipMode]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "techPackReceivedDateStr"))
						psrJson.put("Tech Pack Received Date",
								PSRHelper.getDateStr((Date) object[GridConstants.techPackReceivedDate]));
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Sample Material Status"))
						psrJson.put("Sample Material Status", object[GridConstants.sampleMaterialStatus]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Daily Output Per Line"))
						psrJson.put("Daily Output Per Line", object[GridConstants.dailyOutputPerLine]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "fabricTestReportNumeric"))
						psrJson.put("Fabric Test Report#", object[GridConstants.fabricTestReportNumeric]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "garmentTestReportNo"))
						psrJson.put("Garment Test Report#", object[GridConstants.garmentTestReportNo]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Actual Poundage Available"))
						psrJson.put("Actual Poundage Available", object[GridConstants.actualPoundageAvailable]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Appr Fabric Yarn Lot"))
						psrJson.put("Appr Fabric Yarn Lot", object[GridConstants.apprFabricYarnLot]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Gauge"))
						psrJson.put("Gauge", object[GridConstants.gauge]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "knitting"))
						if (object[GridConstants.knitting] != null
								&& !object[GridConstants.knitting].toString().equalsIgnoreCase(""))
							psrJson.put("Knitting %", object[GridConstants.knitting] + "%");
						else
							psrJson.put("Knitting %", object[GridConstants.knitting]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "knittingMachines"))
						psrJson.put("# of Knitting Machines", object[GridConstants.knittingMachines]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Knitting Pcs"))
						psrJson.put("Knitting Pcs", object[GridConstants.knittingPcs]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "linking"))
						if (object[GridConstants.linking] != null
								&& !object[GridConstants.linking].toString().equalsIgnoreCase(""))
							psrJson.put("Linking %", object[GridConstants.linking] + "%");
						else
							psrJson.put("Linking %", object[GridConstants.linking]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "linkingMachines"))
						psrJson.put("# of Linking Machines", object[GridConstants.linkingMachines]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Linking Pcs"))
						psrJson.put("Linking Pcs", object[GridConstants.linkingPcs]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "sewingLines"))
						psrJson.put("# of Sewing Lines", object[GridConstants.sewingLines]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "garmentTreatmentPcs"))
						psrJson.put("Garment Treatment Pcs", object[GridConstants.garmentTreatmentPcs]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "finishedGarmentPcs"))
						psrJson.put("Finished Garment Pcs", object[GridConstants.finishedGarmentPcs]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "packagingReadyDateStr"))
						psrJson.put("Packaging Ready Date",
								PSRHelper.getDateStr((Date) object[GridConstants.packagingReadyDate]));
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "packedUnitsPcs"))
						psrJson.put("Packed Units Pcs", object[GridConstants.packedUnitsPcs]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "plShipDate"))
						psrJson.put("Ship Date", PSRHelper.getDateStr((Date) object[GridConstants.plShipDate]));
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "So Id"))
						psrJson.put("So Id", object[GridConstants.soId]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Retail Price"))
						psrJson.put("Retail Price", object[GridConstants.retailPrice]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "fabricPpDateStr"))
						psrJson.put("Fabric Pp Date", PSRHelper.getDateStr((Date) object[GridConstants.fabricPpDate]));
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Delivery Month"))
						psrJson.put("Delivery Month", object[GridConstants.deliveryMonth]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "cpoAccDateByVendorStr"))
						psrJson.put("Cpo Acc Date By Vendor",
								PSRHelper.getDateStr((Date) object[GridConstants.cpoAccDateByVendor]));
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Dist Channel"))
						psrJson.put("Dist Channel", object[GridConstants.distChannel]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "floorSet")) {
						if (object[GridConstants.floorSet] != null) {
							if (((String) object[GridConstants.floorSet]).equalsIgnoreCase("Y"))
								psrJson.put("Floor Set", "YES");
							else
								psrJson.put("Floor Set", null);
						} else {
							psrJson.put("Floor Set", null);
						}
					}
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "styleAtRisk")) {
						if (object[GridConstants.styleAtRisk] != null) {
							if (((String) object[GridConstants.styleAtRisk]).equalsIgnoreCase("Y"))
								psrJson.put("Style At Risk", "YES");
							else
								psrJson.put("Style At Risk", null);
						} else {
							psrJson.put("Style At Risk", null);
						}
					}
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "sampleMerchEtaStr"))
						psrJson.put("Sample Merch Eta",
								PSRHelper.getDateStr((Date) object[GridConstants.sampleMerchEta]));
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "sampleFloorSetEtaStr"))
						psrJson.put("Sample Floor Set Eta",
								PSRHelper.getDateStr((Date) object[GridConstants.sampleFloorSetEta]));
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "sampleDcomEtaStr"))
						psrJson.put("Sample Dcom Eta",
								PSRHelper.getDateStr((Date) object[GridConstants.sampleDcomEta]));
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "sampleMailer")) {
						if (object[GridConstants.sampleMailer] != null) {
							if (((String) object[GridConstants.sampleMailer]).equalsIgnoreCase("Y"))
								psrJson.put("Sample Mailer", "YES");
							else
								psrJson.put("Sample Mailer", null);
						} else {
							psrJson.put("Sample Mailer", null);
						}
					}
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "sampleMailerEtaStr"))
						psrJson.put("Sample Mailer Eta",
								PSRHelper.getDateStr((Date) object[GridConstants.sampleMailerEta]));
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "photoMerchantSampleSendDateStr"))
						psrJson.put("Photo/Merchant Sample Send date",
								PSRHelper.getDateStr((Date) object[GridConstants.photoMerchantSampleSendDate]));
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "photoMerchantSampleEtaDateStr"))
						psrJson.put("Photo/Merchant Sample ETA date",
								PSRHelper.getDateStr((Date) object[GridConstants.photoMerchantSampleEtaDate]));
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "photoMerchantSampleComment"))
						psrJson.put("Photo/Merchant Sample Comment", object[GridConstants.photoMerchantSampleComment]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "marketingSampleSendDateStr"))
						psrJson.put("Marketing Sample Send date",
								PSRHelper.getDateStr((Date) object[GridConstants.marketingSampleSendDate]));
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "marketingSampleEtaDateStr"))
						psrJson.put("Marketing Sample ETA date",
								PSRHelper.getDateStr((Date) object[GridConstants.marketingSampleEtaDate]));
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Marketing Sample Comment"))
						psrJson.put("Marketing Sample Comment", object[GridConstants.marketingSampleComment]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "visualSampleSendDateStr"))
						psrJson.put("Visual Sample Send date",
								PSRHelper.getDateStr((Date) object[GridConstants.visualSampleSendDate]));
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "visualSampleEtaDateStr"))
						psrJson.put("Visual Sample ETA date",
								PSRHelper.getDateStr((Date) object[GridConstants.visualSampleEtaDate]));
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Visual Sample Comment"))
						psrJson.put("Visual Sample Comment", object[GridConstants.visualSampleComment]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "copyrightSampleSendDateStr"))
						psrJson.put("Copyright Sample Send date",
								PSRHelper.getDateStr((Date) object[GridConstants.copyrightSampleSendDate]));
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "copyrightSampleEtaDateStr"))
						psrJson.put("Copyright Sample ETA date",
								PSRHelper.getDateStr((Date) object[GridConstants.copyrightSampleEtaDate]));
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Copyright Sample Comment"))
						psrJson.put("Copyright Sample Comment", object[GridConstants.copyrightSampleComment]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "additionalBulkLotApproveStr"))
						psrJson.put("Additional Bulk Lot Approve",
								PSRHelper.getDateStr((Date) object[GridConstants.additionalBulkLotApprove]));
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Reason For Late Gac"))
						psrJson.put("Reason For Late Gac", object[GridConstants.reasonForLateGac]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Color Comment"))
						psrJson.put("Color Comment", object[GridConstants.colorComment]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "Fabric Comment"))
						psrJson.put("Fabric Comment", object[GridConstants.fabricComment]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "fabricTestComment"))
						psrJson.put("Fabric Test Comment", object[GridConstants.fabricTestComment]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "garmentTestComment"))
						psrJson.put("Garment Test Comment", object[GridConstants.garmentTestComment]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "trimComment"))
						psrJson.put("Trim Comment", object[GridConstants.trimComment]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "sampleComment"))
						psrJson.put("Sample Comment", object[GridConstants.sampleComment]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "fitSampleComment"))
						psrJson.put("Fit Sample Comment", object[GridConstants.fitSampleComment]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "ppSampleComment"))
						psrJson.put("PP Sample Comment", object[GridConstants.ppSampleComment]);
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "topSampleEtaDateStr"))
						psrJson.put("TOP sample ETA Date",
								PSRHelper.getDateStr((Date) object[GridConstants.topSampleEtaDate]));
					if (PSRHelper.checkSelectColumnAvailable(selectedColoumns, "TOP sample Comment"))
						psrJson.put("TOP sample Comment", object[GridConstants.topSampleComment]);
					for (String eventdesc : eventDescList) {
						if (!selctedColList.contains(eventdesc.replaceAll(" ", "").toLowerCase())) {
							if (eventdesc.trim().equalsIgnoreCase(((String) object[GridConstants.eventDesc]).trim())) {
								psrJson.put(eventdesc + ApplicationConstants.DISABLE_EVENT,
										object[GridConstants.disableEvent]);
								psrJson.put(eventdesc + ApplicationConstants.HYPHEN_HIGHLIGHT_REVISED_DATE,
										object[GridConstants.highlightReviseDate]);
								psrJson.put(eventdesc + ApplicationConstants.HYPHEN_EVENT_CODE,
										object[GridConstants.EventCode]);
								psrJson.put(eventdesc + ApplicationConstants.HYPHEN_PLANNED_DATE,
										PSRHelper.getDateStr((Date) object[GridConstants.PlannedDate]));
								psrJson.put(eventdesc + ApplicationConstants.HYPHEN_REVISED_DATE,
										PSRHelper.getDateStr((Date) object[GridConstants.RevisedDate]));
								psrJson.put(eventdesc + ApplicationConstants.HYPHEN_ACTUAL_DATE,
										PSRHelper.getDateStr((Date) object[GridConstants.ActualDate]));
								
							} else {
								psrJson.put(eventdesc + ApplicationConstants.DISABLE_EVENT,"");
								psrJson.put(eventdesc + ApplicationConstants.HYPHEN_HIGHLIGHT_REVISED_DATE, "");
								psrJson.put(eventdesc + ApplicationConstants.HYPHEN_EVENT_CODE, "");
								psrJson.put(eventdesc + ApplicationConstants.HYPHEN_PLANNED_DATE, "");
								psrJson.put(eventdesc + ApplicationConstants.HYPHEN_REVISED_DATE, "");
								psrJson.put(eventdesc + ApplicationConstants.HYPHEN_ACTUAL_DATE, "");
							}
						}
					}
					psrData.put((Integer) object[87], psrJson);

				}
			}
			for (Entry<Integer, Map> entry : psrData.entrySet()) {
				psrData1.add(entry.getValue());
			}

			if (log.isDebugEnabled()) {
				log.debug(String.format(" size : %d", psrData.size()));
			}
			return psrData1;
		} catch (Exception e) {
			log.error(String.format(" Error occured in getExportData : %s", e.getMessage()));
			throw e;
		}

	}

	public static List<PtlUserProfileForm> constructUserProfileDetails(List<Object[]> userNameDetails) {
		List<PtlUserProfileForm> userProfileList = new ArrayList<>();
		for (Object[] objects : userNameDetails) {
			userProfileList.add(new PtlUserProfileForm().setUserId((String) objects[0]).setBrUserId((String) objects[1])
					.setUserName((String) objects[2]).setEmail((String) objects[3]).setRoleName((String) objects[4])
					.setCountry((String) objects[5]).setPswEffectiveDate((Date) objects[6])
					.setPswdEffectiveDays((Integer) objects[7]).setPswdNotifyDays((Integer) objects[8])
					.setPswdExpireDate((Date) objects[9]).setPswdNeverExpires((String) objects[10])
					.setDisableUser((String) objects[11]).setPswdChangeDate((Date) objects[12])
					.setPswdChgNxtLogin((String) objects[13]).setPswdResetDigest((String) objects[14])
					.setCreateDate((String) objects[15]).setQueryId((Integer) objects[16]));
		}
		return userProfileList;
	}

	public static PtlRoleProfile saveRoleProfileDetails(PtlRoleProfileForm roleProfile, String loginUserId) {
		return new PtlRoleProfile().setRoleId(roleProfile.getRoleId()).setRoleName(roleProfile.getRoleName())
				.setCreateDate(new Date()).setDescription(roleProfile.getDescription())
				.setAppName(roleProfile.getAppName()).setDisableRole(NO).setModifyTs(new Date())
				.setModifyUser(loginUserId);
	}

	public static PtlRoleProfile getRoleProfile(PtlRoleProfile roleProfile) {
		return new PtlRoleProfile().setRoleName(roleProfile.getRoleName()).setDescription(roleProfile.getDescription())
				.setAppName(roleProfile.getAppName()).setDisableRole(roleProfile.getDisableRole()).setBrUserIdStatus(roleProfile.getBrUserIdStatus());
	}

	/**
	 * Date:13/02/2019 Author:Randhir Saves User Profile Data
	 * 
	 * @param saveUserProfile
	 * @param userId
	 * @param loginUserId
	 * @return Object of PtlUserProfileTemp
	 */
	public static PtlUserProfileTemp saveUserProfileDetails(PtlUserProfileForm saveUserProfile, String loginUserId) {
		return new PtlUserProfileTemp().setUserId(saveUserProfile.getUserId())
				.setBrUserId(saveUserProfile.getBrUserId()).setPswd(saveUserProfile.getPswd())
				.setUserName(saveUserProfile.getUserName()).setEmail(saveUserProfile.getEmail())
				.setRoleId(saveUserProfile.getRoleIds().get(ZERO)).setCountry(saveUserProfile.getCountry())
				.setPswEffectiveDate(saveUserProfile.getPswEffectiveDate())
				.setPswdEffectiveDays(saveUserProfile.getPswdEffectiveDays())
				.setPswdNotifyDays(saveUserProfile.getPswdNotifyDays())
				.setPswdExpireDate(saveUserProfile.getPswdExpireDate())
				.setPswdNeverExpires(saveUserProfile.getPswdNeverExpires())
				.setPswdChangeDate(saveUserProfile.getPswdChangeDate())
				.setPswdChgNxtLogin(saveUserProfile.getPswdChgNxtLogin()).setDisableUser(NO)
				.setModifyTs(new Timestamp(new Date().getTime())).setModifyUser(loginUserId)
				.setCreateDate(new Timestamp(new Date().getTime())).setQueryId(saveUserProfile.getQueryId());
	}

	/**
	 * Date:13/02/2019 Author:Randhir Saves user role data via user profile screen
	 * 
	 * @param saveUserProfile
	 * @param userId
	 * @param loginUserId
	 * @return Object of PtlUserRole
	 */
	public static PtlUserRole saveUserRoleDetails(PtlUserProfileForm saveUserProfile, String loginUserId) {
		return new PtlUserRole().setId(new PtlUserRoleId().setRoleId(saveUserProfile.getRoleIds().get(ZERO))
				.setUserId(saveUserProfile.getUserId()));
	}
	
	public static String constructXmlStrExcel(List<PsrOrder> orders, Map<Integer, List<PsrEventsView>> eventsWRTOrder,
			String imageurl, String[] availableOrder) throws Exception {
		StringBuilder sb = new StringBuilder();
		try {
			if (orders != null && orders.size() > 0) {
				String start = "<ORDERS>";
				String end = "</ORDERS>";
				sb.append(start);
				String imageName = null;
				for (PsrOrder order : orders) {
					Integer orderId = order.getPsrOrderId();
					if (orderId != null) {
						sb.append("<PSR>");
						sb.append("<PSR_ORDER_ID>" + order.getPsrOrderId() + "</PSR_ORDER_ID>");
						
						if(Arrays.asList(availableOrder).contains("comment")) {
							
							if (order.getComment() != null && order.getComment().isEmpty() == FALSE) {
								sb.append("<COMMENT>" + StringEscapeUtils.escapeXml(order.getComment()) + "</COMMENT>");
							} else {
								sb.append("<COMMENT></COMMENT>");
							}
							
						}
						
						
						
						if (PSRHelper.checkImage(order)) {
							try {
								imageName = PSRHelper.convertBase64ToImage(order.getImageType(), order.getImage(),
										order.getPsrOrderId(), imageurl);
								sb.append("<IMAGE>" + imageName + "</IMAGE>");
							} catch (IOException e) {
								log.error(String.format("Error in method convertBase64ToImage : %s", e.getMessage()));
								throw e;
							}
						}
						
						if(Arrays.asList(availableOrder).contains("custFtyId")) {
							
							if (order.getCustFtyId() != null && order.getCustFtyId().isEmpty() == FALSE) {
								sb.append("<CUST_FTY_ID>" + StringEscapeUtils.escapeXml(order.getCustFtyId())
										+ "</CUST_FTY_ID>");
							} else {
								sb.append("<CUST_FTY_ID></CUST_FTY_ID>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("cutApprLetter")) { 
							
							if (order.getCutApprLetter() != null && order.getCutApprLetter().isEmpty() == FALSE) {
								sb.append("<CUT_APPR_LETTER>" + StringEscapeUtils.escapeXml(order.getCutApprLetter())
										+ "</CUT_APPR_LETTER>");
							} else {
								sb.append("<CUT_APPR_LETTER></CUT_APPR_LETTER>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("dailyOutputPerLine")) { 
							
							if (order.getDailyOutputPerLine() != null) {
								sb.append("<DAILY_OUTPUT_PER_LINE>" + order.getDailyOutputPerLine()
										+ "</DAILY_OUTPUT_PER_LINE>");
							} else {
								sb.append("<DAILY_OUTPUT_PER_LINE></DAILY_OUTPUT_PER_LINE>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("extraProcess")) { 
							
							if (PSRHelper.handleOnlyYesOrTrueOrOne(order.getExtraProcess())) {
								sb.append("<EXTRA_PROCESS>Y</EXTRA_PROCESS>");
							} else {
								sb.append("<EXTRA_PROCESS>N</EXTRA_PROCESS>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("cpoPackType")) {
							
							if (order.getCpoPackType() != null && order.getCpoPackType().isEmpty() == FALSE) {
								sb.append("<CPO_PACK_TYPE>" + StringEscapeUtils.escapeXml(order.getCpoPackType())
										+ "</CPO_PACK_TYPE>");
							} else {
								sb.append("<CPO_PACK_TYPE></CPO_PACK_TYPE>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("fabTrimOrder")) {
							
							if (order.getFabTrimOrder() != null && order.getFabTrimOrder().isEmpty() == FALSE) {
								sb.append("<FAB_TRIM_ORDER>" + StringEscapeUtils.escapeXml(order.getFabTrimOrder())
										+ "</FAB_TRIM_ORDER>");
							} else {
								sb.append("<FAB_TRIM_ORDER></FAB_TRIM_ORDER>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("fabricCode")) {
							
							if (order.getFabricCode() != null && order.getFabricCode().isEmpty() == FALSE) {
								sb.append("<FABRIC_CODE>" + StringEscapeUtils.escapeXml(order.getFabricCode())
										+ "</FABRIC_CODE>");
							} else {
								sb.append("<FABRIC_CODE></FABRIC_CODE>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("fabricEstDateStr")) {
							
							if (order.getFabricEstDateStr() != null && order.getFabricEstDateStr().isEmpty() == FALSE) {
								sb.append("<FABRIC_EST_DATE>" + order.getFabricEstDateStr() + "</FABRIC_EST_DATE>");
							} else {
								sb.append("<FABRIC_EST_DATE></FABRIC_EST_DATE>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("fabricMill")) { 
							
							if (order.getFabricMill() != null && order.getFabricMill().isEmpty() == FALSE) {
								sb.append("<FABRIC_MILL>" + StringEscapeUtils.escapeXml(order.getFabricMill())
										+ "</FABRIC_MILL>");
							} else {
								sb.append("<FABRIC_MILL></FABRIC_MILL>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("fabricTestReportNumeric")) {
							
							if (order.getFabricTestReportNumeric() != null
									&& order.getFabricTestReportNumeric().isEmpty() == FALSE) {
								sb.append("<FABRIC_TEST_REPORT_NUMERIC>"
										+ StringEscapeUtils.escapeXml(order.getFabricTestReportNumeric())
										+ "</FABRIC_TEST_REPORT_NUMERIC>");
							} else {
								sb.append("<FABRIC_TEST_REPORT_NUMERIC></FABRIC_TEST_REPORT_NUMERIC>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("factoryRef")) {
							
							if (order.getFactoryRef() != null && order.getFactoryRef().isEmpty() == FALSE) {
								sb.append("<FACTORY_REF>" + StringEscapeUtils.escapeXml(order.getFactoryRef())
										+ "</FACTORY_REF>");
							} else {
								sb.append("<FACTORY_REF></FACTORY_REF>");
							}
						}
						
						if(Arrays.asList(availableOrder).contains("fiberContent")) {
							
							if (order.getFiberContent() != null && order.getFiberContent().isEmpty() == FALSE) {
								sb.append("<FIBER_CONTENT>" + StringEscapeUtils.escapeXml(order.getFiberContent())
										+ "</FIBER_CONTENT>");
							} else {
								sb.append("<FIBER_CONTENT></FIBER_CONTENT>");
							}
							
						}
						
						
						if(Arrays.asList(availableOrder).contains("garmentTestActualStr")) {
							
							if (order.getGarmentTestActualStr() != null
									&& order.getGarmentTestActualStr().isEmpty() == FALSE) {
								sb.append("<GARMENT_TEST_ACTUAL>" + order.getGarmentTestActualStr()
										+ "</GARMENT_TEST_ACTUAL>");
							} else {
								sb.append("<GARMENT_TEST_ACTUAL></GARMENT_TEST_ACTUAL>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("gauge")) {
							
							if (order.getGauge() != null && order.getGauge().isEmpty() == FALSE) {
								sb.append("<GAUGE>" + StringEscapeUtils.escapeXml(order.getGauge()) + "</GAUGE>");
							} else {
								sb.append("<GAUGE></GAUGE>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("graphicName")) {
							
							if (order.getGraphicName() != null && order.getGraphicName().isEmpty() == FALSE) {
								sb.append("<GRAPHIC_NAME>" + StringEscapeUtils.escapeXml(order.getGraphicName())
										+ "</GRAPHIC_NAME>");
							} else {
								sb.append("<GRAPHIC_NAME></GRAPHIC_NAME>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("knitting")) {
							
							if (order.getKnitting() != null) {
								sb.append("<KNITTING>" + order.getKnitting() + "</KNITTING>");
							} else {
								sb.append("<KNITTING></KNITTING>");
							}
						}
						
						if(Arrays.asList(availableOrder).contains("knittingMachines")) {
							
							if (order.getKnittingMachines() != null) {
								sb.append("<KNITTING_MACHINES>" + order.getKnittingMachines() + "</KNITTING_MACHINES>");
							} else {
								sb.append("<KNITTING_MACHINES></KNITTING_MACHINES>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("knittingPcs")) {
							
							if (order.getKnittingPcs() != null) {
								sb.append("<KNITTING_PCS>" + order.getKnittingPcs() + "</KNITTING_PCS>");
							} else {
								sb.append("<KNITTING_PCS></KNITTING_PCS>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("linking")) {
							
							if (order.getLinking() != null) {
								sb.append("<LINKING>" + order.getLinking() + "</LINKING>");
							} else {
								sb.append("<LINKING></LINKING>");
							}
						}
						
						if(Arrays.asList(availableOrder).contains("linkingMachines")) {
							
							if (order.getLinkingMachines() != null) {
								sb.append("<LINKING_MACHINES>" + order.getLinkingMachines() + "</LINKING_MACHINES>");
							} else {
								sb.append("<LINKING_MACHINES></LINKING_MACHINES>");
							}
						}
						
						if(Arrays.asList(availableOrder).contains("linkingPcs")) {
							
							if (order.getLinkingPcs() != null) {
								sb.append("<LINKING_PCS>" + order.getLinkingPcs() + "</LINKING_PCS>");
							} else {
								sb.append("<LINKING_PCS></LINKING_PCS>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("ndcWk")) {
							
							if (order.getNdcWk() != null && order.getNdcWk().isEmpty() == FALSE) {
								sb.append("<NDC_WK>" + StringEscapeUtils.escapeXml(order.getNdcWk()) + "</NDC_WK>");
							} else {
								sb.append("<NDC_WK></NDC_WK>");
							}
						}
						
						if(Arrays.asList(availableOrder).contains("orderType")) {
							
							if (order.getOrderType() != null && order.getOrderType().isEmpty() == FALSE) {
								sb.append("<ORDER_TYPE>" + StringEscapeUtils.escapeXml(order.getOrderType())
										+ "</ORDER_TYPE>");
							} else {
								sb.append("<ORDER_TYPE></ORDER_TYPE>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("packType")) {
							
							if (order.getPackType() != null && order.getPackType().isEmpty() == FALSE) {
								sb.append(
										"<PACK_TYPE>" + StringEscapeUtils.escapeXml(order.getPackType()) + "</PACK_TYPE>");
							} else {
								sb.append("<PACK_TYPE></PACK_TYPE>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("packagingReadyDateStr")) {
							
							if (order.getPackagingReadyDateStr() != null
									&& order.getPackagingReadyDateStr().isEmpty() == FALSE) {
								sb.append("<PACKAGING_READY_DATE>" + order.getPackagingReadyDateStr()
										+ "</PACKAGING_READY_DATE>");
							} else {
								sb.append("<PACKAGING_READY_DATE></PACKAGING_READY_DATE>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("colorCodePatrn")) {
							
							if (order.getColorCodePatrn() != null && order.getColorCodePatrn().isEmpty() == FALSE) {
								sb.append("<COLOR_CODE_PATRN>" + StringEscapeUtils.escapeXml(order.getColorCodePatrn())
										+ "</COLOR_CODE_PATRN>");
							} else {
								sb.append("<COLOR_CODE_PATRN></COLOR_CODE_PATRN>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("apprFabricYarnLot")) {
							
							if (order.getApprFabricYarnLot() != null && order.getApprFabricYarnLot().isEmpty() == FALSE) {
								sb.append(
										"<APPR_FABRIC_YARN_LOT>" + StringEscapeUtils.escapeXml(order.getApprFabricYarnLot())
												+ "</APPR_FABRIC_YARN_LOT>");
							} else {
								sb.append("<APPR_FABRIC_YARN_LOT></APPR_FABRIC_YARN_LOT>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("actualPoundageAvailable")) {
							
							if (order.getActualPoundageAvailable() != null) {
								sb.append("<ACTUAL_POUNDAGE_AVAILABLE>" + order.getActualPoundageAvailable()
										+ "</ACTUAL_POUNDAGE_AVAILABLE>");
							} else {
								sb.append("<ACTUAL_POUNDAGE_AVAILABLE></ACTUAL_POUNDAGE_AVAILABLE>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("careLabelCode")) {
							
							if (order.getCareLabelCode() != null && order.getCareLabelCode().isEmpty() == FALSE) {
								sb.append("<CARE_LABEL_CODE>" + StringEscapeUtils.escapeXml(order.getCareLabelCode())
										+ "</CARE_LABEL_CODE>");
							} else {
								sb.append("<CARE_LABEL_CODE></CARE_LABEL_CODE>");
							}
						}
						
						if(Arrays.asList(availableOrder).contains("sewingLines")) {
							
							if (order.getSewingLines() != null) {
								sb.append("<SEWING_LINES>" + order.getSewingLines() + "</SEWING_LINES>");
							} else {
								sb.append("<SEWING_LINES></SEWING_LINES>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("ticketColorCode")) {
							
							if (order.getTicketColorCode() != null && order.getTicketColorCode().isEmpty() == FALSE) {
								sb.append("<TICKET_COLOR_CODE>" + StringEscapeUtils.escapeXml(order.getTicketColorCode())
										+ "</TICKET_COLOR_CODE>");
							} else {
								sb.append("<TICKET_COLOR_CODE></TICKET_COLOR_CODE>");
							}
							
							
						}
						
						if(Arrays.asList(availableOrder).contains("ticketStyleNumeric")) {
							
							if (order.getTicketStyleNumeric() != null && order.getTicketStyleNumeric().isEmpty() == FALSE) {
								sb.append("<TICKET_STYLE_NUMERIC>"
										+ StringEscapeUtils.escapeXml(order.getTicketStyleNumeric())
										+ "</TICKET_STYLE_NUMERIC>");
							} else {
								sb.append("<TICKET_STYLE_NUMERIC></TICKET_STYLE_NUMERIC>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("garmentTestReportNo")) {
							
							if (order.getGarmentTestReportNo() != null
									&& order.getGarmentTestReportNo().isEmpty() == FALSE) {
								sb.append("<GARMENT_TEST_REPORT_NO>"
										+ StringEscapeUtils.escapeXml(order.getGarmentTestReportNo())
										+ "</GARMENT_TEST_REPORT_NO>");
							} else {
								sb.append("<GARMENT_TEST_REPORT_NO></GARMENT_TEST_REPORT_NO>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("plmNo")) {
							
							if (order.getPlmNo() != null && order.getPlmNo().isEmpty() == FALSE) {
								sb.append("<PLM_NO>" + StringEscapeUtils.escapeXml(order.getPlmNo()) + "</PLM_NO>");
							} else {
								sb.append("<PLM_NO></PLM_NO>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("washYn")) {
							
							if (PSRHelper.handleOnlyYesOrTrueOrOne(order.getWashYn())) {
								sb.append("<WASH_YN>Y</WASH_YN>");
							} else {
								sb.append("<WASH_YN>N</WASH_YN>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("washFacility")) {
							
							if (order.getWashFacility() != null && order.getWashFacility().isEmpty() == FALSE) {
								sb.append("<WASH_FACILITY>" + StringEscapeUtils.escapeXml(order.getWashFacility())
										+ "</WASH_FACILITY>");
							} else {
								sb.append("<WASH_FACILITY></WASH_FACILITY>");
							}
							
							
						}
						
						if(Arrays.asList(availableOrder).contains("fabricShipMode")) {
							
							if (order.getFabricShipMode() != null && order.getFabricShipMode().isEmpty() == FALSE) {
								sb.append("<FABRIC_SHIP_MODE>" + StringEscapeUtils.escapeXml(order.getFabricShipMode())
										+ "</FABRIC_SHIP_MODE>");
							} else {
								sb.append("<FABRIC_SHIP_MODE></FABRIC_SHIP_MODE>");
							}
							
						}
						
						
						if(Arrays.asList(availableOrder).contains("techPackReceivedDateStr")) {
							
							if (order.getTechPackReceivedDateStr() != null
									&& order.getTechPackReceivedDateStr().isEmpty() == FALSE) {
								sb.append("<TECH_PACK_RECEIVED_DATE>" + order.getTechPackReceivedDateStr()
										+ "</TECH_PACK_RECEIVED_DATE>");
							} else {
								sb.append("<TECH_PACK_RECEIVED_DATE></TECH_PACK_RECEIVED_DATE>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("sampleMaterialStatus")) {
							
							if (order.getSampleMaterialStatus() != null
									&& order.getSampleMaterialStatus().isEmpty() == FALSE) {
								sb.append("<SAMPLE_MATERIAL_STATUS>"
										+ StringEscapeUtils.escapeXml(order.getSampleMaterialStatus())
										+ "</SAMPLE_MATERIAL_STATUS>");
							} else {
								sb.append("<SAMPLE_MATERIAL_STATUS></SAMPLE_MATERIAL_STATUS>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("retailPrice")) {
							
							if (order.getRetailPrice() != null) {
								sb.append("<RETAIL_PRICE>" + order.getRetailPrice().setScale(2, RoundingMode.HALF_EVEN)
										+ "</RETAIL_PRICE>");
							} else {
								sb.append("<RETAIL_PRICE></RETAIL_PRICE>");
							}
							
						}
						
						
						if(Arrays.asList(availableOrder).contains("fabricPpDateStr")) {
							
							if (order.getFabricPpDateStr() != null && order.getFabricPpDateStr().isEmpty() == FALSE) {
								sb.append("<FABRIC_PP_DATE>" + order.getFabricPpDateStr() + "</FABRIC_PP_DATE>");
							} else {
								sb.append("<FABRIC_PP_DATE></FABRIC_PP_DATE>");
							}
							
							
						}
						
						if(Arrays.asList(availableOrder).contains("deliveryMonth")) {
							
							if (order.getDeliveryMonth() != null && order.getDeliveryMonth().isEmpty() == FALSE) {
								sb.append("<DELIVERY_MONTH>" + StringEscapeUtils.escapeXml(order.getDeliveryMonth())
										+ "</DELIVERY_MONTH>");
							} else {
								sb.append("<DELIVERY_MONTH></DELIVERY_MONTH>");
							}
						}
						
						if(Arrays.asList(availableOrder).contains("cpoAccDateByVendorStr")) {
							
							if (order.getCpoAccDateByVendorStr() != null
									&& order.getCpoAccDateByVendorStr().isEmpty() == FALSE) {
								sb.append("<CPO_ACC_DATE_BY_VENDOR>" + order.getCpoAccDateByVendorStr()
										+ "</CPO_ACC_DATE_BY_VENDOR>");
							} else {
								sb.append("<CPO_ACC_DATE_BY_VENDOR></CPO_ACC_DATE_BY_VENDOR>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("distChannel")) {
							
							if (order.getDistChannel() != null && order.getDistChannel().isEmpty() == FALSE) {
								sb.append("<DIST_CHANNEL>" + StringEscapeUtils.escapeXml(order.getDistChannel())
										+ "</DIST_CHANNEL>");
							} else {
								sb.append("<DIST_CHANNEL></DIST_CHANNEL>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("floorSet")) {
							
							if (PSRHelper.handleOnlyYesOrTrueOrOne(order.getFloorSet())) {
								sb.append("<FLOOR_SET>Y</FLOOR_SET>");
							} else {
								sb.append("<FLOOR_SET>N</FLOOR_SET>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("styleAtRisk")) {
							
							if (PSRHelper.handleOnlyYesOrTrueOrOne(order.getStyleAtRisk())) {
								sb.append("<STYLE_AT_RISK>Y</STYLE_AT_RISK>");
							} else {
								sb.append("<STYLE_AT_RISK>N</STYLE_AT_RISK>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("sampleMerchEtaStr")) {
							
							if (order.getSampleMerchEtaStr() != null && order.getSampleMerchEtaStr().isEmpty() == FALSE) {
								sb.append("<SAMPLE_MERCH_ETA>" + order.getSampleMerchEtaStr() + "</SAMPLE_MERCH_ETA>");
							} else {
								sb.append("<SAMPLE_MERCH_ETA></SAMPLE_MERCH_ETA>");
							}
							
						}
						
						
						if(Arrays.asList(availableOrder).contains("sampleFloorSetEtaStr")) {
							
							if (order.getSampleFloorSetEtaStr() != null
									&& order.getSampleFloorSetEtaStr().isEmpty() == FALSE) {
								sb.append("<SAMPLE_FLOOR_SET_ETA>" + order.getSampleFloorSetEtaStr()
										+ "</SAMPLE_FLOOR_SET_ETA>");
							} else {
								sb.append("<SAMPLE_FLOOR_SET_ETA></SAMPLE_FLOOR_SET_ETA>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("sampleDcomEtaStr")) {
							
							if (order.getSampleDcomEtaStr() != null && order.getSampleDcomEtaStr().isEmpty() == FALSE) {
								sb.append("<SAMPLE_DCOM_ETA>" + order.getSampleDcomEtaStr() + "</SAMPLE_DCOM_ETA>");
							} else {
								sb.append("<SAMPLE_DCOM_ETA></SAMPLE_DCOM_ETA>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("sampleMailer")) {
							
							if (PSRHelper.handleOnlyYesOrTrueOrOne(order.getSampleMailer())) {
								sb.append("<SAMPLE_MAILER>Y</SAMPLE_MAILER>");
							} else {
								sb.append("<SAMPLE_MAILER>N</SAMPLE_MAILER>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("sampleMailerEtaStr")) {
							
							if (order.getSampleMailerEtaStr() != null && order.getSampleMailerEtaStr().isEmpty() == FALSE) {
								sb.append("<SAMPLE_MAILER_ETA>" + order.getSampleMailerEtaStr() + "</SAMPLE_MAILER_ETA>");
							} else {
								sb.append("<SAMPLE_MAILER_ETA></SAMPLE_MAILER_ETA>");
							}
							
						}
						
						
						if(Arrays.asList(availableOrder).contains("topSampleEtaDateStr")) {
							
							if (order.getTopSampleEtaDateStr() != null
									&& order.getTopSampleEtaDateStr().isEmpty() == FALSE) {
								sb.append("<TOP_SAMPLE_ETA_DATE>" + order.getTopSampleEtaDateStr()
										+ "</TOP_SAMPLE_ETA_DATE>");
							} else {
								sb.append("<TOP_SAMPLE_ETA_DATE></TOP_SAMPLE_ETA_DATE>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("topSampleComment")) {
							
							if (order.getTopSampleComment() != null && order.getTopSampleComment().isEmpty() == FALSE) {
								sb.append("<TOP_SAMPLE_COMMENT>" +  StringEscapeUtils.escapeXml(order.getTopSampleComment()) + "</TOP_SAMPLE_COMMENT>");
							} else {
								sb.append("<TOP_SAMPLE_COMMENT></TOP_SAMPLE_COMMENT>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("photoMerchantSampleSendDateStr")) {
							
							if (order.getPhotoMerchantSampleSendDateStr() != null
									&& order.getPhotoMerchantSampleSendDateStr().isEmpty() == FALSE) {
								sb.append("<PHOTO_MERCHANT_SAMPLE_SEND_DATE>" + order.getPhotoMerchantSampleSendDateStr()
										+ "</PHOTO_MERCHANT_SAMPLE_SEND_DATE>");
							} else {
								sb.append("<PHOTO_MERCHANT_SAMPLE_SEND_DATE></PHOTO_MERCHANT_SAMPLE_SEND_DATE>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("photoMerchantSampleEtaDateStr")) {
							
							if (order.getPhotoMerchantSampleEtaDateStr() != null
									&& order.getPhotoMerchantSampleEtaDateStr().isEmpty() == FALSE) {
								sb.append("<PHOTO_MERCHANT_SAMPLE_ETA_DATE>" + order.getPhotoMerchantSampleEtaDateStr()
										+ "</PHOTO_MERCHANT_SAMPLE_ETA_DATE>");
							} else {
								sb.append("<PHOTO_MERCHANT_SAMPLE_ETA_DATE></PHOTO_MERCHANT_SAMPLE_ETA_DATE>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("photoMerchantSampleComment")) {
							
							if (order.getPhotoMerchantSampleComment() != null
									&& order.getPhotoMerchantSampleComment().isEmpty() == FALSE) {
								sb.append("<PHOTO_MERCHANT_SAMPLE_COMMENT>" + StringEscapeUtils.escapeXml(order.getPhotoMerchantSampleComment())
										+ "</PHOTO_MERCHANT_SAMPLE_COMMENT>");
							} else {
								sb.append("<PHOTO_MERCHANT_SAMPLE_COMMENT></PHOTO_MERCHANT_SAMPLE_COMMENT>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("marketingSampleSendDateStr")) {
							
							if (order.getMarketingSampleSendDateStr() != null
									&& order.getMarketingSampleSendDateStr().isEmpty() == FALSE) {
								sb.append("<MARKETING_SAMPLE_SEND_DATE>" + order.getMarketingSampleSendDateStr()
										+ "</MARKETING_SAMPLE_SEND_DATE>");
							} else {
								sb.append("<MARKETING_SAMPLE_SEND_DATE></MARKETING_SAMPLE_SEND_DATE>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("marketingSampleEtaDateStr")) {
							
							if (order.getMarketingSampleEtaDateStr() != null
									&& order.getMarketingSampleEtaDateStr().isEmpty() == FALSE) {
								sb.append("<MARKETING_SAMPLE_ETA_DATE>" + order.getMarketingSampleEtaDateStr()
										+ "</MARKETING_SAMPLE_ETA_DATE>");
							} else {
								sb.append("<MARKETING_SAMPLE_ETA_DATE></MARKETING_SAMPLE_ETA_DATE>");
							}
							
						}
						
						
						if(Arrays.asList(availableOrder).contains("marketingSampleComment")) {
							
							if (order.getMarketingSampleComment() != null
									&& order.getMarketingSampleComment().isEmpty() == FALSE) {
								sb.append("<MARKETING_SAMPLE_COMMENT>" + StringEscapeUtils.escapeXml(order.getMarketingSampleComment())
										+ "</MARKETING_SAMPLE_COMMENT>");
							} else {
								sb.append("<MARKETING_SAMPLE_COMMENT></MARKETING_SAMPLE_COMMENT>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("visualSampleSendDateStr")) {
							
							if (order.getVisualSampleSendDateStr() != null
									&& order.getVisualSampleSendDateStr().isEmpty() == FALSE) {
								sb.append("<VISUAL_SAMPLE_SEND_DATE>" + order.getVisualSampleSendDateStr()
										+ "</VISUAL_SAMPLE_SEND_DATE>");
							} else {
								sb.append("<VISUAL_SAMPLE_SEND_DATE></VISUAL_SAMPLE_SEND_DATE>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("visualSampleEtaDateStr")) {
							
							if (order.getVisualSampleEtaDateStr() != null
									&& order.getVisualSampleEtaDateStr().isEmpty() == FALSE) {
								sb.append("<VISUAL_SAMPLE_ETA_DATE>" + order.getVisualSampleEtaDateStr()
										+ "</VISUAL_SAMPLE_ETA_DATE>");
							} else {
								sb.append("<VISUAL_SAMPLE_ETA_DATE></VISUAL_SAMPLE_ETA_DATE>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("visualSampleComment")) {
							
							if (order.getVisualSampleComment() != null
									&& order.getVisualSampleComment().isEmpty() == FALSE) {
								sb.append("<VISUAL_SAMPLE_COMMENT>" + StringEscapeUtils.escapeXml(order.getVisualSampleComment())
										+ "</VISUAL_SAMPLE_COMMENT>");
							} else {
								sb.append("<VISUAL_SAMPLE_COMMENT></VISUAL_SAMPLE_COMMENT>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("copyrightSampleSendDateStr")) {
							
							if (order.getCopyrightSampleSendDateStr() != null
									&& order.getCopyrightSampleSendDateStr().isEmpty() == FALSE) {
								sb.append("<COPYRIGHT_SAMPLE_SEND_DATE>" + order.getCopyrightSampleSendDateStr()
										+ "</COPYRIGHT_SAMPLE_SEND_DATE>");
							} else {
								sb.append("<COPYRIGHT_SAMPLE_SEND_DATE></COPYRIGHT_SAMPLE_SEND_DATE>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("copyrightSampleEtaDateStr")) {
							
							if (order.getCopyrightSampleEtaDateStr() != null
									&& order.getCopyrightSampleEtaDateStr().isEmpty() == FALSE) {
								sb.append("<COPYRIGHT_SAMPLE_ETA_DATE>" + order.getCopyrightSampleEtaDateStr()
										+ "</COPYRIGHT_SAMPLE_ETA_DATE>");
							} else {
								sb.append("<COPYRIGHT_SAMPLE_ETA_DATE></COPYRIGHT_SAMPLE_ETA_DATE>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("copyrightSampleComment")) {
							
							if (order.getCopyrightSampleComment() != null
									&& order.getCopyrightSampleComment().isEmpty() == FALSE) {
								sb.append("<COPYRIGHT_SAMPLE_COMMENT>" + StringEscapeUtils.escapeXml(order.getCopyrightSampleComment())
										+ "</COPYRIGHT_SAMPLE_COMMENT>");
							} else {
								sb.append("<COPYRIGHT_SAMPLE_COMMENT></COPYRIGHT_SAMPLE_COMMENT>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("reasonForLateGac")) {
							
							if (order.getReasonForLateGac() != null && order.getReasonForLateGac().isEmpty() == FALSE) {
								sb.append("<REASON_FOR_LATE_GAC>" + StringEscapeUtils.escapeXml(order.getReasonForLateGac()) + "</REASON_FOR_LATE_GAC>");
							} else {
								sb.append("<REASON_FOR_LATE_GAC></REASON_FOR_LATE_GAC>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("additionalBulkLotApproveStr")) {
							
							if (order.getAdditionalBulkLotApproveStr() != null
									&& order.getAdditionalBulkLotApproveStr().isEmpty() == FALSE) {
								sb.append("<ADDITIONAL_BULK_LOT_APPROVE>" + order.getAdditionalBulkLotApproveStr()
										+ "</ADDITIONAL_BULK_LOT_APPROVE>");
							} else {
								sb.append("<ADDITIONAL_BULK_LOT_APPROVE></ADDITIONAL_BULK_LOT_APPROVE>");
							}
							
							
						}
						
						if(Arrays.asList(availableOrder).contains("colorComment")) {
							
							if (order.getColorComment() != null
									&& order.getColorComment().isEmpty() == FALSE) {
								sb.append("<COLOR_COMMENT>" + StringEscapeUtils.escapeXml(order.getColorComment())
										+ "</COLOR_COMMENT>");
							} else {
								sb.append("<COLOR_COMMENT></COLOR_COMMENT>");
							}
							
						}
						
						
						if(Arrays.asList(availableOrder).contains("fabricComment")) {
							
							if (order.getFabricComment() != null
									&& order.getFabricComment().isEmpty() == FALSE) {
								sb.append("<FABRIC_COMMENT>" + StringEscapeUtils.escapeXml(order.getFabricComment())
										+ "</FABRIC_COMMENT>");
							} else {
								sb.append("<FABRIC_COMMENT></FABRIC_COMMENT>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("fabricTestComment")) {
							
							if (order.getFabricTestComment() != null
									&& order.getFabricTestComment().isEmpty() == FALSE) {
								sb.append("<FABRIC_TEST_COMMENT>" + StringEscapeUtils.escapeXml(order.getFabricTestComment())
										+ "</FABRIC_TEST_COMMENT>");
							} else {
								sb.append("<FABRIC_TEST_COMMENT></FABRIC_TEST_COMMENT>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("garmentTestComment")) {
							
							if (order.getGarmentTestComment() != null
									&& order.getGarmentTestComment().isEmpty() == FALSE) {
								sb.append("<GARMENT_TEST_COMMENT>" + StringEscapeUtils.escapeXml(order.getGarmentTestComment())
										+ "</GARMENT_TEST_COMMENT>");
							} else {
								sb.append("<GARMENT_TEST_COMMENT></GARMENT_TEST_COMMENT>");
							}
							
						}
						
						
						if(Arrays.asList(availableOrder).contains("trimComment")) {
							
							if (order.getTrimComment() != null
									&& order.getTrimComment().isEmpty() == FALSE) {
								sb.append("<TRIM_COMMENT>" + StringEscapeUtils.escapeXml(order.getTrimComment())
										+ "</TRIM_COMMENT>");
							} else {
								sb.append("<TRIM_COMMENT></TRIM_COMMENT>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("sampleComment")) {
							
							if (order.getSampleComment() != null
									&& order.getSampleComment().isEmpty() == FALSE) {
								sb.append("<SAMPLE_COMMENT>" + StringEscapeUtils.escapeXml(order.getSampleComment())
										+ "</SAMPLE_COMMENT>");
							} else {
								sb.append("<SAMPLE_COMMENT></SAMPLE_COMMENT>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("fitSampleComment")) {
							
							if (order.getFitSampleComment() != null
									&& order.getFitSampleComment().isEmpty() == FALSE) {
								sb.append("<FIT_SAMPLE_COMMENT>" + StringEscapeUtils.escapeXml(order.getFitSampleComment())
										+ "</FIT_SAMPLE_COMMENT>");
							} else {
								sb.append("<FIT_SAMPLE_COMMENT></FIT_SAMPLE_COMMENT>");
							}
							
						}
						
						
						if(Arrays.asList(availableOrder).contains("ppSampleComment")) {
							
							if (order.getPpSampleComment() != null
									&& order.getPpSampleComment().isEmpty() == FALSE) {
								sb.append("<PP_SAMPLE_COMMENT>" + StringEscapeUtils.escapeXml(order.getPpSampleComment())
										+ "</PP_SAMPLE_COMMENT>");
							} else {
								sb.append("<PP_SAMPLE_COMMENT></PP_SAMPLE_COMMENT>");
							}
							
							
						}
						
						
						if(Arrays.asList(availableOrder).contains("garmentTreatmentPcs")) {
							
							if (order.getGarmentTreatmentPcs() != null) {
								sb.append("<GARMENT_TREATEMENT_PCS>" + order.getGarmentTreatmentPcs()
										+ "</GARMENT_TREATEMENT_PCS>");
							} else {
								sb.append("<GARMENT_TREATEMENT_PCS></GARMENT_TREATEMENT_PCS>");
							}
							
						}
						
						if(Arrays.asList(availableOrder).contains("finishedGarmentPcs")) {
							
							if (order.getFinishedGarmentPcs() != null) {
								sb.append("<FINISHED_GARMENT_PCS>" + order.getFinishedGarmentPcs()
										+ "</FINISHED_GARMENT_PCS>");
							} else {
								sb.append("<FINISHED_GARMENT_PCS></FINISHED_GARMENT_PCS>");
							}
						}
						
						if(Arrays.asList(availableOrder).contains("packedUnitsPcs")) {
							
							if (order.getPackedUnitsPcs() != null) {
								sb.append("<PACKED_UNITS_PCS>" + order.getPackedUnitsPcs()
										+ "</PACKED_UNITS_PCS>");
							} else {
								sb.append("<PACKED_UNITS_PCS></PACKED_UNITS_PCS>");
							}
							
						}
				
						
						if (eventsWRTOrder != null && eventsWRTOrder.size() > 0) {
							List<PsrEventsView> events = eventsWRTOrder.get(orderId);
							if (events != null && events.size() > 0) {
								sb.append("<EVENTS>");
								for (PsrEventsView event : events) {
									if (event.getEventCode() != null && event.getEventCode().trim().length() > 0) {
										sb.append("<EVENT>");
										sb.append("<EVENT_CODE>" + event.getEventCode() + "</EVENT_CODE> ");
										if (!event.getEventCode().trim().equalsIgnoreCase("E000") && event.getActualDateStr() != null
												&& event.getActualDateStr().isEmpty() == FALSE) {
											sb.append("<ACTUAL_DATE>" + event.getActualDateStr() + "</ACTUAL_DATE>");
										} 
										else if (!event.getEventCode().trim().equalsIgnoreCase("E000")) {
											sb.append("<ACTUAL_DATE></ACTUAL_DATE>");
										}
										if (event.getRevisedDateStr() != null
												&& event.getRevisedDateStr().isEmpty() == FALSE) {
											sb.append("<REVISED_DATE>" + event.getRevisedDateStr() + "</REVISED_DATE>");
										} else {
											sb.append("<REVISED_DATE></REVISED_DATE>");
										}
										sb.append("</EVENT>");
									}
								}
								sb.append("</EVENTS>");
							}
						}
						sb.append("</PSR>");
					}
				}
				sb.append(end);
			}
		} catch (Exception e) {
			log.error("Error occured in DAOHelper---" + e.getMessage());
			throw e;
		}
		return sb.toString();
	}

}
