package com.igt.psr.common.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.igt.psr.common.constants.ApplicationConstants;

public class RoleHelper {
	public static final Map<String, Set<String>> URL_ROLE_MAP = new HashMap<>();
	static {
		URL_ROLE_MAP.put(ApplicationConstants.PTL_ADMIN, configuredUrlsforRole(ApplicationConstants.PTL_ADMIN));
	}

	public static Set<String> configuredUrlsforRole(String role) {
		Set<String> urlsList = new HashSet<>();
		urlsList.add("/rest/customisedViews");
		urlsList.add("/rest/eventMileStone");
		if (role != null && role.trim().equalsIgnoreCase(ApplicationConstants.PTL_ADMIN)) {
			urlsList.add(ApplicationConstants.REST_PSR_DYNAMIC_DATA);
		}
		if (role != null && role.trim().equalsIgnoreCase("PTL_USER")) {
			urlsList.add(ApplicationConstants.REST_PSR_DYNAMIC_DATA);
		}
		if (role != null && role.trim().equalsIgnoreCase("PSR_ADMIN")) {
			urlsList.add(ApplicationConstants.REST_PSR_DYNAMIC_DATA);
		}
		if (role != null && role.trim().equalsIgnoreCase("PSR_VENDOR")) {
			urlsList.add(ApplicationConstants.REST_PSR_DYNAMIC_DATA);
		}
		if (role != null && role.trim().equalsIgnoreCase("PSR_USER")) {
			urlsList.add(ApplicationConstants.REST_PSR_DYNAMIC_DATA);
		}
		if (role != null && role.trim().equalsIgnoreCase("BR_ADMIN")) {

		}
		if (role != null && role.trim().equalsIgnoreCase("BR_USER")) {

		}
		return urlsList;
	}

}
