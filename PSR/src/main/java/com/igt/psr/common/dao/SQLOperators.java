package com.igt.psr.common.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SQLOperators {

	private static final Logger log = LoggerFactory.getLogger(SQLOperators.class);

	public static final String WHERE_CLAUSE = "WHERE";
	public static final String AND = "AND";
	public static final String LIKE = " LIKE ";
	public static final String EQUALS = "=";
	public static final String GREATER = ">";
	public static final String LESS = "<";
	public static final String GREATER_EQUAL = ">=";
	public static final String LESS_EQUAL = "<=";
	public static final String NOT_EQUALS = "!=";
	public static final String NOT_LIKE = "NOT LIKE";
	public static final String NOT = "NOT";
	public static final String OR = "OR";
	public static final String BETWEEN = "BETWEEN";
	public static final String IN = "IN";
	public static final String NOT_IN = "NOT IN";
	public static final String JOIN = "JOIN";
	public static final String IS_NULL = "IS NULL";
	public static final String IS_NOT_NULL = "IS NOT NULL";
	public String operator = null;
}
