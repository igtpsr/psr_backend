package com.igt.psr.common.util;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PSRResponse {
	public String message;
	public String type;
	public String tokenId;
}
