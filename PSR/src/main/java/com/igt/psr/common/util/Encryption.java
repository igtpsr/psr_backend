package com.igt.psr.common.util;

import org.mindrot.jbcrypt.BCrypt;

public class Encryption {

	public static String getEncryptedPassword(String pswd) {
		return BCrypt.hashpw(pswd, BCrypt.gensalt());
	}

	public static boolean checkPassword(String pswd, String pswdFromDb) {
		return BCrypt.checkpw(pswd, pswdFromDb);
	}
}