package com.igt.psr.common.util;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.igt.psr.rest.resource.LoginResource;

public class LdapAuth {
	private static final Logger log = LoggerFactory.getLogger(LoginResource.class);

	private String ldapUrl;
	private String domain;

	public String authenticate(String userID, String pwd) throws Exception {
		try {
			// set up the LDAP parameters
			Hashtable<Object, Object> env = new Hashtable<Object, Object>();
			env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
			env.put(Context.PROVIDER_URL, ldapUrl);
			env.put(Context.SECURITY_AUTHENTICATION, "simple");
			env.put(Context.REFERRAL, "follow");
			env.put(Context.SECURITY_PRINCIPAL, domain + "\\" + userID);
			env.put(Context.SECURITY_CREDENTIALS, pwd);

			// attempt to authenticate
			DirContext ctx = new InitialDirContext(env);
			ctx.close();
			return userID;
		} catch (Exception e) {
			log.error(String.format("Ldap Authentication problem --> %s ", e.getMessage()));
		}
		return null;
	}

	public String getLdapUrl() {
		return ldapUrl;
	}

	public void setLdapUrl(String ldapUrl) {
		this.ldapUrl = ldapUrl;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

}
