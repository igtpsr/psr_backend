package com.igt.psr.common.util;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.igt.psr.db.model.PsrEventMaster;
import com.igt.psr.jaxb.model.Document;
import com.igt.psr.jaxb.model.Order;
import com.igt.psr.jaxb.model.OrderD;
import com.igt.psr.jaxb.model.OrderH;
import com.igt.psr.portal.model.BrUserProfile;
import com.igt.psr.portal.model.PtlRoleProfile;
import com.igt.psr.ui.view.BrUserProfileForm;
import com.igt.psr.ui.view.PsrEventForm;
import com.igt.psr.ui.view.PtlRoleProfileForm;
import com.igt.psr.ui.view.PtlUserProfileForm;

public class UIViewHelper {

	public static String SUCCESS = "success";
	public static String ERROR = "error";

	public static Document getDocumentObject(String owner, Map<String, List<String>> rowNos) {
		return getPostDocument(owner, rowNos);
	}

	public static XMLGregorianCalendar getDate(String strDate) {
		if (strDate != null && !strDate.isEmpty()) {
			String[] parseStr = strDate.split("-");
			String mon = parseStr[1];
			String day = parseStr[2];
			if (mon.length() == 1) {
				mon = "0" + mon;
			}
			if (day.length() == 1) {
				day = "0" + day;
			}
			String date = parseStr[0] + "-" + mon + "-" + day;
			try {
				return DatatypeFactory.newInstance().newXMLGregorianCalendar(date);
			} catch (DatatypeConfigurationException e) {
				throw new RuntimeException(e);
			}
		}
		return null;
	}

	private static Document getPostDocument(String owner, Map<String, List<String>> rowMap) {
		Document document = new Document();
		List<Order> orderlist = new ArrayList<>();
		rowMap.forEach((k, v) -> {
			Order order = createOrder(owner, k, v);
			orderlist.add(order);
		});
		// Order object initialization ends here
		document.order = orderlist;
		return document;
	}

	private static Order createOrder(String owner, String proformaPO, List<String> rowNos) {
		Order order = new Order();
		// OrderH object initialization starts here
		OrderH orderH = new OrderH();
		orderH.setOwner(owner);
		orderH.setProfomaPo(new BigInteger(proformaPO));
		List<OrderD> orderDList = new ArrayList<>();
		// OrderD object initialization starts here
		for (String rowNo : rowNos) {
			String[] rowNoWithRevisedDate = rowNo.split(":");
			OrderD orderD = createOrderD(owner, proformaPO, rowNoWithRevisedDate[0], rowNoWithRevisedDate[1]);
			orderDList.add(orderD);
		}
		// OrderD object initialization ends here
		orderH.orderD = orderDList;
		// OrderH object initialization ends here
		order.setOrderH(orderH);
		return order;
	}

	private static OrderD createOrderD(String owner, String proformaPO, String rowNo, String revisedDate) {
		OrderD orderD = new OrderD();
		orderD.setOwner(owner);
		orderD.setProfomaPo(new BigInteger(proformaPO));
		orderD.setRowNo(new BigInteger(rowNo));
		orderD.setDate5(revisedDate);
		return orderD;
	}

	public static List<PsrEventForm> getEventMasterForm(List<PsrEventMaster> eventMaster) {
		List<PsrEventForm> psrEventFormList = new ArrayList<>();
		for (PsrEventMaster psrEventMaster : eventMaster) {
			psrEventFormList.add(new PsrEventForm().setEventcode(psrEventMaster.getEventCode())
					.setDesc(psrEventMaster.getEventDesc()).setEventcategory(psrEventMaster.getEventCategory()));
		}
		return psrEventFormList;
	}

	
	public static List<PtlUserProfileForm> getUserProfileForm(List<PtlUserProfileForm> userPortal) {
		List<PtlUserProfileForm> userPortalList = new ArrayList<>();
		for (PtlUserProfileForm userPortaldetails : userPortal) {
			userPortalList.add(new PtlUserProfileForm().setUserId(userPortaldetails.getUserId())
					.setRoleName(userPortaldetails.getRoleName()));
		}
		return userPortalList;
	}

	public static List<PtlRoleProfileForm> getRoleProfileForm(List<PtlRoleProfile> rolePortal) {
		List<PtlRoleProfileForm> userPortalList = new ArrayList<>();
		for (PtlRoleProfile userPortaldetails : rolePortal) {
			userPortalList.add(new PtlRoleProfileForm().setRoleId(userPortaldetails.getRoleId())
					.setRoleName(userPortaldetails.getRoleName()).setDescription(userPortaldetails.getDescription())
					.setAppName(userPortaldetails.getAppName()).setBrUserIdStatus(userPortaldetails.getBrUserIdStatus()));
		}
		return userPortalList;
	}

	public static List<PtlRoleProfileForm> getRoleProfile(List<PtlRoleProfile> rolePortal) {
		List<PtlRoleProfileForm> userPortalList = new ArrayList<>();
		for (PtlRoleProfile userPortaldetails : rolePortal) {
			userPortalList.add(new PtlRoleProfileForm().setRoleId(userPortaldetails.getRoleId())
					.setRoleName(userPortaldetails.getRoleName()));
		}
		return userPortalList;
	}

	public static List<PtlRoleProfileForm> getRoleProfileDetails(List<PtlRoleProfile> roleList) {
		List<PtlRoleProfileForm> roleProfileList = new ArrayList<>();
		for (PtlRoleProfile roledetails : roleList) {
			roleProfileList.add(new PtlRoleProfileForm().setRoleId(roledetails.getRoleId())
					.setRoleName(roledetails.getRoleName()).setDescription(roledetails.getDescription())
					.setAppName(roledetails.getAppName()).setDisableRole(roledetails.getDisableRole()).setBrUserIdStatus(roledetails.getBrUserIdStatus()));
		}
		return roleProfileList;
	}

	public static List<BrUserProfileForm> getBrUserProfile(List<BrUserProfile> brUserList) {
		List<BrUserProfileForm> brUserProfileList = new ArrayList<>();
		for (BrUserProfile brUserDetails : brUserList) {
			brUserProfileList.add(new BrUserProfileForm().setBrUserID(brUserDetails.getBrUserId())
					.setBrUserName(brUserDetails.getBrUserName()).setBrRoleId(brUserDetails.getBrRoleId())
					.setBrRoleName(brUserDetails.getBrRoleName()).setBrAttrName(brUserDetails.getBrAttrName())
					.setBrAttrValue(brUserDetails.getBrAttrValue()));
		}
		return brUserProfileList;
	}

}
