package com.igt.psr.common.constants;

public class ApplicationErrorConstants {

	/* Query Name exists */
	public static final String QUERY_NAME = "Query Name Already Exists";
	public static final String QUERY_SAVE_SUCCESS = "New View has been saved successfully";

}
