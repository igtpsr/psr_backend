package com.igt.psr.common.util;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.poi.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ResourceUtils;

import com.google.common.collect.Lists;
import com.igt.psr.common.exception.ImageException;
import com.igt.psr.db.dto.PsrSaveSearchDto;
import com.igt.psr.db.model.PsrOrder;
import com.igt.psr.db.model.PsrSaveSearch;
import com.igt.psr.ui.view.PostXmlInputParams;

public class PSRHelper {

	private static final Logger log = LoggerFactory.getLogger(PSRHelper.class);

	public static boolean checkNotEmpty(String var) {
		return (null != var) && (var.trim().length() > 0) && (!var.trim().equalsIgnoreCase("undefined"));
	}

	public static BigDecimal handleValue(String var) {
		return (var != null && var.trim().length() > 0) ? BigDecimal.valueOf(Double.valueOf(var)) : null;
	}

	public static String setSoIdToString(BigDecimal var) {
		return (var.equals(new BigDecimal(0))) ? "" : var.toString() + "";
	}

	public static BigDecimal isEmptyOrNull(Object object) {
		if (object != null)
			return new BigDecimal(object + "");
		return null;
	}

	public static Boolean checkEmptyOrNull(String object) {
		return (object != null ? !(object.trim().isEmpty()) : false);
	}

	public static String getDateStr(Date date) {
		if (null != date)
			return new SimpleDateFormat("MM/dd/yyyy").format(date);
		else
			return null;
	}

	public static String getDateStrForSP(Date date) {
		if (null != date)
			return new SimpleDateFormat("yyyy-MM-dd").format(date);
		else
			return null;
	}

	public static String appendQuotesForGivenStr(String str) {
		if (null != str && !str.isEmpty()) {
			StringBuilder sb = new StringBuilder();
			String[] output = str.split("\\,");
			for (String val : output) {
				sb.append("'" + val.trim() + "',");
			}
			return sb.substring(0, sb.toString().length() - 1);
		}
		return null;
	}

	// Compression only on .jpg files
	private static String getFileName(String fileName) {
		try {
			String filName = fileName;
			if (!filName.endsWith(".jpg") || !filName.endsWith(".JPG")) {
				if (filName.endsWith(".bmp") || filName.endsWith(".BMP")) {
					filName = filName.replaceAll(".bmp", ".jpg");
				}
				if (filName.endsWith(".jpeg") || filName.endsWith(".JPEG")) {
					filName = filName.replaceAll(".jpeg", ".jpg");
				}
				if (filName.endsWith(".png") || filName.endsWith(".PNG")) {
					filName = filName.replaceAll(".png", ".jpg");
				}
				if (filName.endsWith(".gif") || filName.endsWith(".GIF")) {
					filName = filName.replaceAll(".gif", ".jpg");
				}
			}
			return filName;
		} catch (Exception e) {
			log.error("Error occured in getFileName ", e.getMessage());
			throw e;
		}
	}

	private static BufferedImage setImageRGB(BufferedImage bufferedImage) {
		BufferedImage newBufferedImage = null;

		try {
			// create a blank, RGB, same width and height, and a white
			// background
			newBufferedImage = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(),
					BufferedImage.TYPE_INT_RGB);
			newBufferedImage.createGraphics().drawImage(bufferedImage, 0, 0, Color.WHITE, null);

		} catch (Exception e) {
			log.error(String.format("Exception occured in Buffered Image method %s ", e.getMessage()));

		}
		return newBufferedImage;
	}

	private static String compressImage(BufferedImage bufferedImage, String imageFullName, String imageurl)
			throws IOException {
		log.info("Image Compression Started");
		String fileName = getFileName(imageFullName);
		deleteExistingFile(imageurl, imageFullName);
		File compressedImageFile = new File(imageurl + fileName);
		log.info(String.format("File save path : %s ", compressedImageFile.getAbsolutePath()));
		OutputStream outputStream = new FileOutputStream(compressedImageFile);
		Iterator<ImageWriter> imageWriters = ImageIO.getImageWritersByFormatName("jpg");
		if (!imageWriters.hasNext())
			throw new IllegalStateException(" Writers Not Found!!");

		ImageWriter imageWriter = imageWriters.next();
		ImageOutputStream imageOutputStream = ImageIO.createImageOutputStream(outputStream);
		imageWriter.setOutput(imageOutputStream);

		ImageWriteParam imageWriteParam = imageWriter.getDefaultWriteParam();

		imageWriteParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		float values[] = imageWriteParam.getCompressionQualityValues();
		imageWriteParam.setCompressionQuality(values[2]);

		imageWriter.write(null, new IIOImage(setImageRGB(bufferedImage), null, null), imageWriteParam);

		outputStream.close();
		imageOutputStream.close();
		imageWriter.dispose();
		log.info("Image Compression End");

		return fileName;

	}

	private static void deleteExistingFile(String imageurl, String imageFullName) {
		log.info("entered into a method deleteExistingFile ");
		try {
			String fileName = imageFullName.substring(imageFullName.indexOf("_"), imageFullName.length());
			Collection<File> list = FileUtils.listFiles(new File(imageurl), new WildcardFileFilter("*" + fileName),
					null);
			for (Iterator iterator = list.iterator(); iterator.hasNext();) {
				File file = (File) iterator.next();
				file.delete();
			}
		} catch (Exception e) {
			log.error(String.format("An error has been occured in deleteExistingFile --> %s ", e.getMessage()));
			throw e;
		}
	}

	public static String convertBase64ToImage(String imageType, String imageName, int orderId, String imageurl)
			throws Exception {
		log.info("Entered into the method convertBase64ToImage ");
		String imageFullName = null;
		String imageFormatJPG = null;
		try {
			int index = imageType.indexOf(',');
			String imgString = imageType.substring(index + 1, imageType.length());
			byte[] decodedBytes = Base64.getDecoder().decode(imgString);
			log.info(String.format(" Decoded upload data : %s", decodedBytes.length));
			String imageWithoutExt = imageName.substring(0, imageName.indexOf('.'));
			String imageAppendOrderId = imageWithoutExt + "_" + orderId;
			imageFullName = getImageNameWithExtention(imageName, imageAppendOrderId);
			ByteArrayInputStream bis = new ByteArrayInputStream(decodedBytes);
			BufferedImage image = ImageIO.read(bis);
			if (image == null) {
				log.error("Buffered Image is null");
				throw new ImageException(" Buffered Image is null ");
			}
			imageFormatJPG = compressImage(image, imageFullName, imageurl);
			log.info(" convertBase64ToImage Ends ");
		} catch (IOException e) {
			log.error("Error in reading Image  " + e.getMessage());
			throw new ImageException(orderId + "");
		} catch (Exception e) {
			log.error(String.format("An error has been occured while compressing the image error --> %s ",
					e.getMessage()));
			throw e;
		}
		return imageFormatJPG;
	}

	private static String getImageNameWithExtention(String imageName, String imageAppendOrderId) {
		if (imageName.toLowerCase().endsWith(".jpg"))
			return imageAppendOrderId + ".jpg";
		if (imageName.toLowerCase().endsWith(".png"))
			return imageAppendOrderId + ".png";
		if (imageName.toLowerCase().endsWith(".jpeg"))
			return imageAppendOrderId + ".jpeg";
		if (imageName.toLowerCase().endsWith(".gif"))
			return imageAppendOrderId + ".gif";
		if (imageName.toLowerCase().endsWith(".bmp"))
			return imageAppendOrderId + ".bmp";
		return null;
	}

	public static Map<String, String> getListToCommaSeparatedStr(List<PostXmlInputParams> postXmlInputParams) {
		Map<String, String> map = new HashMap<>();
		StringBuilder shipToStr = new StringBuilder();
		StringBuilder colorWashNameStr = new StringBuilder();
		StringBuilder profomaPoStr = new StringBuilder();
		for (PostXmlInputParams postXmlInputParam : postXmlInputParams) {
			shipToStr.append("'").append(postXmlInputParam.getShipTo().trim()).append("',");
			colorWashNameStr.append("'").append(postXmlInputParam.getColorWashName().trim()).append("',");
			profomaPoStr.append("'").append(postXmlInputParam.getProfomaPo().trim()).append("',");
		}
		map.put("shipToStr", shipToStr.substring(0, shipToStr.toString().trim().length() - 1));
		map.put("colorWashNameStr", colorWashNameStr.substring(0, colorWashNameStr.toString().trim().length() - 1));
		map.put("profomaPoStr", profomaPoStr.substring(0, profomaPoStr.toString().trim().length() - 1));
		return map;
	}

	public static String dateFormatter(Date dateStr) {
		log.info("dateFormatter method Started");
		DateFormat formatter = new SimpleDateFormat("E MMM dd HH:mm:ss Z yyyy");
		String strDate = formatter.format(dateStr);
		String formatedDate = null;
		try {
			Date date = formatter.parse(strDate);
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			formatedDate = (cal.get(Calendar.MONTH) + 1) + "/" + (cal.get(Calendar.DATE)) + "/" + cal.get(Calendar.YEAR)
					+ " Hour : " + cal.get(Calendar.HOUR_OF_DAY) + " Minute : " + cal.get(Calendar.MINUTE)
					+ " Seconds : " + cal.get(Calendar.SECOND);
			log.info(formatedDate);
		} catch (ParseException e) {
			log.error(String.format("Error in dateFormatter method : %s", e.getMessage()));
		}
		log.info("dateFormatter method End");
		return formatedDate;
	}

	/**
	 * Added this method to append the date and time to xml files for post back to
	 * BR
	 * 
	 * @return
	 */
	public static String dateFormatterXMLFile() {
		SimpleDateFormat sf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		return sf.format(new Date());
	}

	public static boolean checkImage(PsrOrder order) {
		String imageStr = order.getImage();
		if (imageStr == null || imageStr.trim().isEmpty() || imageStr == "null"
				|| imageStr.lastIndexOf(order.getPsrOrderId() + "") != -1) {
			return false;
		}
		return (order.getImageType() != null && !order.getImageType().trim().isEmpty());
	}

	public static void deleteImage(String path) {
		log.info(String.format(" eneteredinto the method delete image Path --> %s", path));
		File file = new File(path);
		if (file.exists()) {
			file.delete();
			log.info(String.format(path, " %s File deleted"));
		} else {
			log.info(String.format(path, " %s doesn't exists"));
		}
	}

	public static String getMonthName(int i) {
		switch (i) {
		case 1:
			return "Jan";
		case 2:
			return "Feb";
		case 3:
			return "Mar";
		case 4:
			return "Apr";
		case 5:
			return "May";
		case 6:
			return "Jun";
		case 7:
			return "July";
		case 8:
			return "Aug";
		case 9:
			return "Sep";
		case 10:
			return "Oct";
		case 11:
			return "Nov";
		case 12:
			return "Dec";
		default:
			return null;
		}
	}

	public static Date getDateWithAdditionOfYear() {
		Calendar c = Calendar.getInstance();
		c.set(9998, Calendar.DECEMBER, 31);
		c.add(Calendar.YEAR, 1);
		return c.getTime();
	}

	public static boolean checkSelectColumnAvailable(String selectedColoumns, String columnName) {
		selectedColoumns = "," + selectedColoumns.replaceAll(" ", "").toLowerCase() + ",";
		columnName = "," + columnName.replaceAll(" ", "").toLowerCase() + ",";
		return !selectedColoumns.contains(columnName);
	}

	/* Added by Thulasidhar on 01-05-2019 */
	public static String getDateWithCustomFormat(String date) {
		String[] temp;
		String tempDate;
		if (date != null) {
			if (date.contains("/")) {
				temp = date.split("/");
				tempDate = temp[0] + "/" + temp[1] + "/" + temp[2];
				return tempDate;
			} else if (date.contains("-")) {
				temp = date.split("-");
				tempDate = temp[1] + "/" + temp[2] + "/" + temp[0];
				return tempDate;
			} else {
				return date;
			}
		} else {
			return date;
		}
	}

	public static boolean handleOnlyYesOrTrueOrOne(String val) {
		if (val != null && (val.trim().equalsIgnoreCase("YES") || val.trim().equalsIgnoreCase("1")
				|| val.trim().equalsIgnoreCase("true")))
			return true;
		return false;
	}

	public static Calendar getCalenderVal(String timeStamp, DateFormat formatter) throws ParseException {
		Date endDate = formatter.parse(timeStamp);
		Calendar cal = Calendar.getInstance();
		cal.setTime(endDate);
		return cal;

	}

	public static String getTimestampKeyForFilleTrack(Calendar cal) {
		return (PSRHelper.getMonthName(cal.get(Calendar.MONTH) + 1)) + " " + (cal.get(Calendar.DATE)) + " "
				+ cal.get(Calendar.YEAR);

	}

	public static String setTimestampForFilleTrack(Calendar cal) {
		return (cal.get(Calendar.HOUR) < 10 ? "0" + cal.get(Calendar.HOUR) : cal.get(Calendar.HOUR)) + ":"
				+ (cal.get(Calendar.MINUTE) < 10 ? "0" + cal.get(Calendar.MINUTE) : cal.get(Calendar.MINUTE)) + ":"
				+ (cal.get(Calendar.SECOND) < 10 ? "0" + cal.get(Calendar.SECOND) : cal.get(Calendar.SECOND));
	}

	/* To reverse the List */
	public static <T> List<T> reverseList(List<T> list) {
		return new ArrayList<>(Lists.reverse(list));
	}

	public static <T> Set<T> reverse(Set<T> set) {
		if (set != null && set.size() > 0) {
			List<T> reverseSet = new ArrayList<T>();
			reverseSet.addAll(set);
			Collections.reverse(reverseSet);
			set.removeAll(set);
			set.addAll(reverseSet);
			return set;
		}
		return null;
	}

	public static File getTemplateFile(String path, String tempFileName) throws Exception {
		try {
			File fileToCopy = ResourceUtils.getFile(path);
			log.info("absolute path -- > " + fileToCopy.getAbsolutePath() + " get path ----> " + fileToCopy.getPath());
			String path1 = fileToCopy.getAbsolutePath();
			String filePath = path1.substring(0, path1.lastIndexOf("\\")) + "\\";
			File newFile = new File(filePath + tempFileName);
			log.info("path -- > " + newFile.getAbsolutePath());
			IOUtils.copy(new FileInputStream(fileToCopy), new FileOutputStream(newFile));
			return newFile;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}
	}

	public static PsrSaveSearch saveSearchResult(PsrSaveSearchDto psrSaveSearchDto, String loginUserId)
			throws Exception {
		try {
			PsrSaveSearch searchItems = new PsrSaveSearch();
			searchItems.setLoginUserId(loginUserId);
			searchItems.setVpo(psrSaveSearchDto.getVpo());
			searchItems.setProductionOffice(psrSaveSearchDto.getProductionOffice());
			searchItems.setVendorId(psrSaveSearchDto.getVendorId());
			searchItems.setOcCommit(psrSaveSearchDto.getOcCommit());
			searchItems.setFactoryId(psrSaveSearchDto.getFactoryId());
			searchItems.setFactoryName(psrSaveSearchDto.getFactoryName());
			searchItems.setCountryOrigin(psrSaveSearchDto.getCountryOrigin());
			searchItems.setBrand(psrSaveSearchDto.getBrand());
			searchItems.setStyle(psrSaveSearchDto.getStyle());
			searchItems.setStyleDescription(psrSaveSearchDto.getStyleDescription());
			searchItems.setCpo(psrSaveSearchDto.getCpo());
			searchItems.setDept(psrSaveSearchDto.getDept());
			searchItems.setSeason(psrSaveSearchDto.getSeason());
			searchItems.setColorWashName(psrSaveSearchDto.getColorWashName());
			searchItems.setNdcdate(psrSaveSearchDto.getNdcdate());
			searchItems.setVpoNltGacDate(psrSaveSearchDto.getVpoNltGacDate());
			searchItems.setVpoFtyGacDate(psrSaveSearchDto.getVpoFtyGacDate());
			searchItems.setNdcWk(psrSaveSearchDto.getNdcWk());
			searchItems.setVpoStatus(psrSaveSearchDto.getVpoStatus());
			searchItems.setVpoLineStatus(psrSaveSearchDto.getVpoLineStatus());
			searchItems.setCategory(psrSaveSearchDto.getCategory());
			searchItems.setCpoColorDesc(psrSaveSearchDto.getCpoColorDesc());
			searchItems.setCpoDept(psrSaveSearchDto.getCpoDept());
			searchItems.setInitFlow(psrSaveSearchDto.getInitFlow());
			searchItems.setVpoSelect(psrSaveSearchDto.getVpoSelect());
			searchItems.setVendorIdSelect(psrSaveSearchDto.getVendorIdSelect());
			searchItems.setOcCommitSelect(psrSaveSearchDto.getOcCommitSelect());
			searchItems.setFactoryIdSelect(psrSaveSearchDto.getFactoryIdSelect());
			searchItems.setFactoryNameSelect(psrSaveSearchDto.getFactoryNameSelect());
			searchItems.setBrandSelect(psrSaveSearchDto.getBrandSelect());
			searchItems.setStyleSelect(psrSaveSearchDto.getStyleSelect());
			searchItems.setStyleDescSelect(psrSaveSearchDto.getStyleDescSelect());
			searchItems.setProdOfficeSelect(psrSaveSearchDto.getProdOfficeSelect());
			searchItems.setCountryOriginSelect(psrSaveSearchDto.getCountryOriginSelect());
			searchItems.setMilestoneselect(psrSaveSearchDto.getMilestoneselect());
			searchItems.setCpoSelect(psrSaveSearchDto.getCpoSelect());
			searchItems.setDeptSelect(psrSaveSearchDto.getDeptSelect());
			searchItems.setSeasonSelect(psrSaveSearchDto.getSeasonSelect());
			searchItems.setColorWashNameSelect(psrSaveSearchDto.getColorWashNameSelect());
			searchItems.setNdcdateSelect(psrSaveSearchDto.getNdcdateSelect());
			searchItems.setVpoNltGacDateSelect(psrSaveSearchDto.getVpoNltGacDateSelect());
			searchItems.setVpoFtyGacDateSelect(psrSaveSearchDto.getVpoFtyGacDateSelect());
			searchItems.setNdcWkSelect(psrSaveSearchDto.getNdcWkSelect());
			searchItems.setVpoStatusSelect(psrSaveSearchDto.getVpoStatusSelect());
			searchItems.setVpoLineStatusSelect(psrSaveSearchDto.getVpoLineStatusSelect());
			searchItems.setCategorySelect(psrSaveSearchDto.getCategorySelect());
			searchItems.setCpoColorDescSelect(psrSaveSearchDto.getCpoColorDescSelect());
			searchItems.setCpoDeptSelect(psrSaveSearchDto.getCpoDeptSelect());
			searchItems.setInitFlowSelect(psrSaveSearchDto.getInitFlowSelect());
			return searchItems;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public static PsrSaveSearchDto getSavedResult(PsrSaveSearch result) {
		try {
			PsrSaveSearchDto showview = new PsrSaveSearchDto();
			showview.setVpo(result.getVpo());
			showview.setProductionOffice(result.getProductionOffice());
			showview.setVendorId(result.getVendorId());
			showview.setOcCommit(result.getOcCommit());
			showview.setFactoryId(result.getFactoryId());
			showview.setFactoryName(result.getFactoryName());
			showview.setCountryOrigin(result.getCountryOrigin());
			showview.setBrand(result.getBrand());
			showview.setStyle(result.getStyle());
			showview.setStyleDescription(result.getStyleDescription());
			showview.setCpo(result.getCpo());
			showview.setDept(result.getDept());
			showview.setSeason(result.getSeason());
			showview.setColorWashName(result.getColorWashName());
			showview.setNdcdate(result.getNdcdate());
			showview.setVpoNltGacDate(result.getVpoNltGacDate());
			showview.setVpoFtyGacDate(result.getVpoFtyGacDate());
			showview.setNdcWk(result.getNdcWk());
			showview.setVpoStatus(result.getVpoStatus());
			showview.setVpoLineStatus(result.getVpoLineStatus());
			showview.setCategory(result.getCategory());
			showview.setCpoColorDesc(result.getCpoColorDesc());
			showview.setCpoDept(result.getCpoDept());
			showview.setInitFlow(result.getInitFlow());
			showview.setVpoSelect(result.getVpoSelect());
			showview.setVendorIdSelect(result.getVendorIdSelect());
			showview.setOcCommitSelect(result.getOcCommitSelect());
			showview.setFactoryIdSelect(result.getFactoryIdSelect());
			showview.setFactoryNameSelect(result.getFactoryNameSelect());
			showview.setBrandSelect(result.getBrandSelect());
			showview.setStyleSelect(result.getStyleSelect());
			showview.setStyleDescSelect(result.getStyleDescSelect());
			showview.setProdOfficeSelect(result.getProdOfficeSelect());
			showview.setCountryOriginSelect(result.getCountryOriginSelect());
			showview.setMilestoneselect(result.getMilestoneselect());
			showview.setCpoSelect(result.getCpoSelect());
			showview.setDeptSelect(result.getDeptSelect());
			showview.setSeasonSelect(result.getSeasonSelect());
			showview.setColorWashNameSelect(result.getColorWashNameSelect());
			showview.setNdcdateSelect(result.getNdcdateSelect());
			showview.setVpoNltGacDateSelect(result.getVpoNltGacDateSelect());
			showview.setVpoFtyGacDateSelect(result.getVpoFtyGacDateSelect());
			showview.setNdcWkSelect(result.getNdcWkSelect());
			showview.setVpoStatusSelect(result.getVpoStatusSelect());
			showview.setVpoLineStatusSelect(result.getVpoLineStatusSelect());
			showview.setCategorySelect(result.getCategorySelect());
			showview.setCpoColorDescSelect(result.getCpoColorDescSelect());
			showview.setCpoDeptSelect(result.getCpoDeptSelect());
			showview.setInitFlowSelect(result.getInitFlowSelect());
			return showview;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
