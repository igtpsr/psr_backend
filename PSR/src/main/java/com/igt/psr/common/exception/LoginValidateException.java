package com.igt.psr.common.exception;

public class LoginValidateException extends Exception {

	private static final long serialVersionUID = 754834018703128676L;
	public String id;

	public LoginValidateException() {
		super();
	}

	public LoginValidateException(String msg) {
		super(msg);
	}

	public static String disableUser(String id) {
		return "This user " + id + " is a disabled user, could you please contact Admin!";
	}

	public static String expiredUser(String id) {
		return "Password for the user " + id + " has been expired, could you please contact Admin!";
	}

	public static String inActive(String id) {
		return "This user " + id + " is inactive, could you please contact Admin!";
	}

	public static String noRoleMapped(String id) {
		return "Authentication is passed for " + id + " but there is no role mapped to access any app in MGF Portal.";
	}
	
	public static String tokenMissed() {
		return "Authentication Token is missed in the request or in backend !";
	}
	
	public static String sessionExpired() {
		return " Your session in the server has been expired please relogin to continue ..!";
	}
	
}
