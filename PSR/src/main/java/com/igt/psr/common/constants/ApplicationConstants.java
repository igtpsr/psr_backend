package com.igt.psr.common.constants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class ApplicationConstants {

	public static Map<String, String> constantHeaders = new LinkedHashMap<>();

	static {
		constantHeaders.put("IMAGE", "Image");
		constantHeaders.put("NDC_WK", "Ndc Wk");
		constantHeaders.put("CPO_PACK_TYPE", "Cpo Pack Type");
		constantHeaders.put("FABRIC_CODE", "Fabric Code");
		constantHeaders.put("FABRIC_EST_DATE", "Fabric Est Date");
		constantHeaders.put("FABRIC_MILL", "Fabric Mill");
		constantHeaders.put("FIBER_CONTENT", "Fiber Content");
		constantHeaders.put("ACTUAL_POUNDAGE_AVAILABLE", "Actual Poundage Available");
		constantHeaders.put("APPR_FABRIC_YARN_LOT", "Appr Fabric Yarn Lot");
		constantHeaders.put("GAUGE", "Gauge");
		constantHeaders.put("KNITTING", "Knitting %");
		constantHeaders.put("KNITTING_MACHINES", "# of Knitting Machines");
		constantHeaders.put("KNITTING_PCS", "Knitting Pcs");
		constantHeaders.put("LINKING", "Linking %");
		constantHeaders.put("LINKING_MACHINES", "# of Linking Machines");
		constantHeaders.put("LINKING_PCS", "Linking Pcs");
		constantHeaders.put("SEWING_LINES", "# of Sewing Lines");
		constantHeaders.put("PACKAGING_READY_DATE", "Packaging Ready Date");
		constantHeaders.put("CARE_LABEL_CODE", "Care Label Code");
		constantHeaders.put("COLOR_CODE_PATRN", "Color Code Pattern");
		constantHeaders.put("COMMENT", "Comment");
		constantHeaders.put("CUST_FTY_ID", "Cust Fty Id");
		constantHeaders.put("CUT_APPR_LETTER", "Cut Appr Letter");
		constantHeaders.put("DAILY_OUTPUT_PER_LINE", "Daily Output Per Line");
		constantHeaders.put("EXTRA_PROCESS", "Extra Process");
		constantHeaders.put("FAB_TRIM_ORDER", "Fab Trim Order");
		constantHeaders.put("FACTORY_REF", "Factory Ref");
		constantHeaders.put("FABRIC_TEST_REPORT_NUMERIC", "Fabric Test Report#");
		constantHeaders.put("GRAPHIC_NAME", "Artwork name");
		constantHeaders.put("ORDER_TYPE", "Sell Channel");
		constantHeaders.put("PACK_TYPE", "Pack Type");
		constantHeaders.put("TICKET_COLOR_CODE", "Ticket Color Code");
		constantHeaders.put("TICKET_STYLE_NUMERIC", "Ticket Style Number");
		constantHeaders.put("GARMENT_TEST_REPORT_NO", "Garment Test Report#");
		constantHeaders.put("PLM_NO", "Design Card Number/PLM Number");
		constantHeaders.put("WASH_YN", "Wash");
		constantHeaders.put("WASH_FACILITY", "Wash Facility");
		constantHeaders.put("FABRIC_SHIP_MODE", "Fabric Ship Mode");
		constantHeaders.put("TECH_PACK_RECEIVED_DATE", "Tech Pack Received Date");
		constantHeaders.put("SAMPLE_MATERIAL_STATUS", "Sample Material Status");
		constantHeaders.put("RETAIL_PRICE", "Retail Price");
		constantHeaders.put("FABRIC_PP_DATE", "Fabric Pp Date");
		constantHeaders.put("DELIVERY_MONTH", "Delivery Month");
		constantHeaders.put("CPO_ACC_DATE_BY_VENDOR", "Cpo Acc Date By Vendor");
		constantHeaders.put("DIST_CHANNEL", "Dist Channel");
		constantHeaders.put("FLOOR_SET", "Floor Set");
		constantHeaders.put("STYLE_AT_RISK", "Style At Risk");
		constantHeaders.put("SAMPLE_MERCH_ETA", "Sample Merch Eta");
		constantHeaders.put("SAMPLE_FLOOR_SET_ETA", "Sample Floor Set Eta");
		constantHeaders.put("SAMPLE_DCOM_ETA", "Sample Dcom Eta");
		constantHeaders.put("SAMPLE_MAILER", "Sample Mailer");
		constantHeaders.put("SAMPLE_MAILER_ETA", "Sample Mailer Eta");
		constantHeaders.put("TOP_SAMPLE_ETA_DATE", "TOP sample ETA Date");
		constantHeaders.put("TOP_SAMPLE_COMMENT", "TOP sample Comment");
		constantHeaders.put("PHOTO_MERCHANT_SAMPLE_SEND_DATE", "Photo/Merchant Sample Send date");
		constantHeaders.put("PHOTO_MERCHANT_SAMPLE_ETA_DATE", "Photo/Merchant Sample ETA date");
		constantHeaders.put("PHOTO_MERCHANT_SAMPLE_COMMENT", "Photo/Merchant Sample Comment");
		constantHeaders.put("MARKETING_SAMPLE_SEND_DATE", "Marketing Sample Send date");
		constantHeaders.put("MARKETING_SAMPLE_ETA_DATE", "Marketing Sample ETA date");
		constantHeaders.put("MARKETING_SAMPLE_COMMENT", "Marketing Sample Comment");
		constantHeaders.put("VISUAL_SAMPLE_SEND_DATE", "Visual Sample Send date");
		constantHeaders.put("VISUAL_SAMPLE_ETA_DATE", "Visual Sample ETA date");
		constantHeaders.put("VISUAL_SAMPLE_COMMENT", "Visual Sample Comment");
		constantHeaders.put("COPYRIGHT_SAMPLE_SEND_DATE", "Copyright Sample Send date");
		constantHeaders.put("COPYRIGHT_SAMPLE_ETA_DATE", "Copyright Sample ETA date");
		constantHeaders.put("COPYRIGHT_SAMPLE_COMMENT", "Copyright Sample Comment");
		constantHeaders.put("REASON_FOR_LATE_GAC", "Reason For Late Gac");
		constantHeaders.put("ADDITIONAL_BULK_LOT_APPROVE", "Additional Bulk Lot Approve");
		constantHeaders.put("COLOR_COMMENT", "Color Comment");
		constantHeaders.put("FABRIC_COMMENT", "Fabric Comment");
		constantHeaders.put("FABRIC_TEST_COMMENT", "Fabric Test Comment");
		constantHeaders.put("GARMENT_TEST_COMMENT", "Garment Test Comment");
		constantHeaders.put("TRIM_COMMENT", "Trim Comment");
		constantHeaders.put("SAMPLE_COMMENT", "Sample Comment");
		constantHeaders.put("FIT_SAMPLE_COMMENT", "Fit Sample Comment");
		constantHeaders.put("PP_SAMPLE_COMMENT", "PP Sample Comment");
		constantHeaders.put("GARMENT_TREATEMENT_PCS", "Garment Treatment Pcs");
		constantHeaders.put("FINISHED_GARMENT_PCS", "Finished Garment Pcs");
		constantHeaders.put("PACKED_UNITS_PCS", "Packed Units Pcs");
		constantHeaders.put("GARMENT_TEST_ACTUAL", "Garment Test Actual");
	}

	// @Scheduled(cron = "0/60 * * * * *")
	// for every 30 minutes
	public static final long SCHEDULE_TIME = 1800000l;
	public static final String SUCCESS = "SUCCESS";
	public static final String ERROR = "ERROR";
	public static final String SUCCESS_SMALL_LETTERS = "success";
	public static final String ERROR_SMALL_LETTERS = "error";
	public static final String NO_ROLE_MAPPED = "NoRole";
	public static final String SAVE_SUCCESS = "Saved Successfully";
	public static final String UPLOAD_SUCCESS = "Uploaded Successfully";
	public static final String DELETE_SUCCESS = "Deleted Successfully";
	public static final Integer MINUS_ONE = -1;
	public static final String FAILURE = "FAILURE - This order has been updated recently, your orders will update automatically with latest data and after that try again.";
	public static final String YES = "Y";
	public static final String NO = "N";
	public static final Integer ZERO = 0;
	public static final String DEFAULT_EXPIRY_DATE = "12/31/9999";
	public static final boolean FALSE = false;

	/* added by Thulasidhar on 22-04-2019 */
	public static final String CASE_WHEN = "case when ";
	public static final String AND_MODEL_NAME = " AND pem.MODEL_NAME ";
	public static final String EQUAL_TO = "Equal to";
	public static final String NOT_EQUAL_TO = "Not equal to";
	public static final String NOT_LIKE = "Not like";
	public static final String IN_THE_LIST = "In the list";
	public static final String NOT_IN_THE_LIST = "Not in the list";
	public static final String STARTS_WITH = "Starts with";
	public static final String ENDS_WITH = "Ends with";
	public static final String EQUAL_TO_NULL = "Equal to null";
	public static final String IS_NOT_NULL = "Is not null";
	public static final String WHERE_EVENT_CODE = " where EVENT_CODE ";
	public static final String AND_USER_ID = " AND USER_ID ";
	public static final String AND_ROLE_NAME = " AND ROLE_NAME ";
	public static final String MODIFY_TS = " MODIFY_TS='";
	public static final String MODIFY_USER = " MODIFY_USER ='";
	public static final String ACTUAL_DATE = "ActualDate";
	public static final String PLANNED_DATE = "PlannedDate";
	public static final String REVISED_DATE = "RevisedDate";
	public static final String EVENT_CODE = "eventCode";
	public static final String HYPHEN_EVENT_CODE = "-eventCode";
	public static final String HIGHLIGHT_REVISED_DATE = "highlightRevisedDate";
	public static final String HYPHEN_HIGHLIGHT_REVISED_DATE = "-HighlightRevisedDate";
	public static final String HYPHEN_ACTUAL_DATE = "-ActualDate";
	public static final String HYPHEN_PLANNED_DATE = "-PlannedDate";
	public static final String HYPHEN_REVISED_DATE = "-RevisedDate";
	public static final String DISABLE_EVENT = "-disableEvent";
	public static final String PTL_ADMIN = "PTL_ADMIN";
	public static final String REST_PSR_DYNAMIC_DATA = "/rest/psrDynamicData";
	public static final String PSR_VENDOR = "PSR_VENDOR";
	public static final String SAMPLE_DCOM_ETA = "Sample Dcom Eta";
	public static final String ERROR_STRING = " Error : %s";
	public static final String ERROR_OCCURED = " Error has been occured : %s";
	public static final String AUTHENTICATION_FAILED = "Authentication is failed for user %s";
	public static final String LOGOUT = "logout";
	public static final String ERROR_OCCURED_IN_SAVE_USER_PROFILE = " Error has been occured in saveUserProfile: %s";
	public static final String ERROR_OCCURED_WHILE_SAVE_DATA = "Error occured while saving the data , please contact the Administrator . ";
	public static final String FILE_TYPE = ".xlsm";

	public static String getCurrentDateWithTime() {
		DateFormat formatterUTC = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		formatterUTC.setTimeZone(TimeZone.getTimeZone("UTC"));
		try {
			TimeUnit.MILLISECONDS.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return formatterUTC.format(new Date());
	}
	
	public static String getCurrentDateWithOutTimeDelay() {
		DateFormat formatterUTC = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		formatterUTC.setTimeZone(TimeZone.getTimeZone("UTC"));
		return formatterUTC.format(new Date());
	}

	public static String getLabel(String key) {
		return constantHeaders.get(key);
	}
}
