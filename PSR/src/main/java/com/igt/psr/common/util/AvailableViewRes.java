package com.igt.psr.common.util;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class AvailableViewRes extends PSRResponse {
	public String queryName;
}
