package com.igt.psr.common.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.igt.psr.common.util.DAOHelper;
import com.igt.psr.common.util.PSRHelper;
import com.igt.psr.ui.view.SearchForm;

public class SearchCriteriaGenerator extends SQLOperators {

	private static final Logger log = LoggerFactory.getLogger(SearchCriteriaGenerator.class);

	// Where clause to add user attributes
	public static String appendWhereClause(SearchForm searchForm) {
		log.info(" entered into the method appendWhereClause ");
		StringBuilder sb = new StringBuilder();
		if (searchForm != null) {
			sb.append(
					" WHERE ((																				                 ");
			sb.append(
					" 		  (SELECT TOP 1 BR_ATTR_NAME                                                                     ");
			sb.append(
					" 		   FROM BR_USER_PROFILE                                                                          ");
			sb.append(" 		   WHERE BR_USER_ID = '" + searchForm.getBrUserId()
					+ "' ) IS NULL)                                  ");
			sb.append(
					" 	   OR ((PROD_OFFICE IN                                                                               ");
			sb.append(
					" 			  (SELECT BR_ATTR_VALUE                                                                      ");
			sb.append(
					" 			   FROM BR_USER_PROFILE                                                                      ");
			sb.append(" 			   WHERE BR_USER_ID = '" + searchForm.getBrUserId()
					+ "'                                         ");
			sb.append(
					" 				 AND BR_ATTR_NAME = 'ATTRIB_2' )                                                         ");
			sb.append(
					" 			AND (BRAND IN                                                                                ");
			sb.append(
					" 				   (SELECT BR_ATTR_VALUE                                                                 ");
			sb.append(
					" 					FROM BR_USER_PROFILE                                                                 ");
			sb.append(" 					WHERE BR_USER_ID = '" + searchForm.getBrUserId()
					+ "'                                    ");
			sb.append(
					" 					  AND BR_ATTR_NAME = 'ATTRIB_1' )                                                    ");
			sb.append(
					" 				 OR                                                                                      ");
			sb.append(
					" 				   (SELECT TOP 1 BR_ATTR_NAME                                                            ");
			sb.append(
					" 					FROM BR_USER_PROFILE                                                                 ");
			sb.append(" 					WHERE BR_USER_ID = '" + searchForm.getBrUserId()
					+ "'                                    ");
			sb.append(
					" 					  AND BR_ATTR_NAME = 'ATTRIB_1') IS NULL))                                           ");
			sb.append(" 		   AND '" + searchForm.getRoleName()
					+ "'  IN ('PSR_USER',                                           ");
			sb.append(
					" 							'PSR_ADMIN'))                                                                ");
			sb.append(
					" 	   OR ((BRAND IN                                                                                     ");
			sb.append(
					" 			  (SELECT BR_ATTR_VALUE                                                                      ");
			sb.append(
					" 			   FROM BR_USER_PROFILE                                                                      ");
			sb.append(" 			   WHERE BR_USER_ID = '" + searchForm.getBrUserId()
					+ "'                                         ");
			sb.append(
					" 				 AND BR_ATTR_NAME = 'ATTRIB_1' )                                                         ");
			sb.append(
					" 			AND ((PROD_OFFICE IN                                                                         ");
			sb.append(
					" 					(SELECT BR_ATTR_VALUE                                                                ");
			sb.append(
					" 					 FROM BR_USER_PROFILE                                                                ");
			sb.append(" 					 WHERE BR_USER_ID = '" + searchForm.getBrUserId()
					+ "'                                   ");
			sb.append(
					" 					   AND BR_ATTR_NAME = 'ATTRIB_2' )                                                   ");
			sb.append(
					" 				  OR                                                                                     ");
			sb.append(
					" 					(SELECT TOP 1 BR_ATTR_NAME                                                           ");
			sb.append(
					" 					 FROM BR_USER_PROFILE                                                                ");
			sb.append(" 					 WHERE BR_USER_ID = '" + searchForm.getBrUserId()
					+ "'                                   ");
			sb.append(
					" 					   AND BR_ATTR_NAME = 'ATTRIB_2') IS NULL)))                                         ");
			sb.append(" 		   AND '" + searchForm.getRoleName()
					+ "'  IN ('PSR_USER',                                           ");
			sb.append(
					" 							'PSR_ADMIN'))                                                                ");
			sb.append(
					" 	   OR (VENDOR_ID IN                                                                                  ");
			sb.append(
					" 			 (SELECT BR_ATTR_VALUE                                                                       ");
			sb.append(
					" 			  FROM BR_USER_PROFILE                                                                       ");
			sb.append(" 			  WHERE BR_USER_ID = '" + searchForm.getBrUserId()
					+ "'                                          ");
			sb.append(
					" 				AND BR_ATTR_NAME = 'VENDOR_ID' )                                                         ");
			sb.append(" 		   AND '" + searchForm.getRoleName()
					+ "'  = 'PSR_VENDOR')                                           ");
			sb.append(
					" 	   OR (FACTORY_ID IN                                                                                 ");
			sb.append(
					" 			 (SELECT BR_ATTR_VALUE                                                                       ");
			sb.append(
					" 			  FROM BR_USER_PROFILE                                                                       ");
			sb.append(" 			  WHERE BR_USER_ID = '" + searchForm.getBrUserId()
					+ "'                                          ");
			sb.append(
					" 				AND BR_ATTR_NAME = 'MANUFACTURER_ID' )                                                   ");
			sb.append(" 		   AND '" + searchForm.getRoleName()
					+ "'  = 'PSR_VENDOR'))                                          ");
		}
		return sb.toString();
	}

	/* Vpo search */

	public static String addCriteria(SearchForm searchForm) {
		log.info(" enetered into the method addCriteria ");
		StringBuilder sb = new StringBuilder();
		if (searchForm != null) {
			/* Vpo search */
			sb.append(addVpoFilter(searchForm));
			/* Vendor Id search */
			sb.append(addVendorIdFilter(searchForm));
			/* Vendor Name search */
			sb.append(addVendorNameFilter(searchForm));
			/* Factory Id search */
			sb.append(addFactoryIdFilter(searchForm));
			/* Factory Name search */
			sb.append(addFactoryNameFilter(searchForm));
			/* Country Origin search */
			sb.append(addCountryOriginFilter(searchForm));
			/* Brand search */
			sb.append(addBrandFilter(searchForm));
			/* Style search */
			sb.append(addStyleFilter(searchForm));
			/* Production Office search */
			sb.append(addProductionOfficeFilter(searchForm));
			/* Style Description search */
			sb.append(addStyleDescriptionFilter(searchForm));
			/* CPO Search */
			sb.append(addCpoFilter(searchForm));
			/* Dept Search */
			sb.append(addDeptFilter(searchForm));
			/* Season Search */
			sb.append(addSeasonFilter(searchForm));
			/* ColorWashName Search */
			sb.append(addColorWashNameFilter(searchForm));
			/* NdcDate Search */
			sb.append(addNdcDateFilter(searchForm));
			/* VPO_NLT_GAC_DATE Search */
			sb.append(addVpoNltGacDateFilter(searchForm));
			/* VPO_FTY_GAC_DATE Search */
			sb.append(addVpoFtyGacDateFilter(searchForm));
			/* NdcWk search */
			sb.append(addNdcWkSearchFilter(searchForm));
			/* Vpo Status Search */
			sb.append(addVpoStatusFilter(searchForm));
			/* Vpo Line Status Search */
			sb.append(addVpoLineStatusFilter(searchForm));
			/* ocCommit Search */
			sb.append(addOcCommitFilter(searchForm));
			/* Category Search */
			sb.append(addCategoryFilter(searchForm));
			/* cpoColorDesc Search */
			sb.append(addCpoColorDescFilter(searchForm));
			/* CpoDept Search */
			sb.append(addCpoDeptFilter(searchForm));
			/* initFlow Search */
			sb.append(addInitFlowFilter(searchForm));
		}
		return sb.toString();
	}

	private static String addVpoFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getVpo())) {

			if (searchForm.getVposelect().equals("Like")) {
				sb.append(" AND PSR_ORDER.VPO  " + LIKE + "'%" + searchForm.getVpo() + "%'");
			} else if (searchForm.getVposelect().equals("Equal to")) {
				sb.append(" AND PSR_ORDER.VPO  " + EQUALS + "'" + searchForm.getVpo() + "'");
			} else if (searchForm.getVposelect().equals("Not equal to")) {
				sb.append(" AND PSR_ORDER.VPO  " + NOT_EQUALS + "'" + searchForm.getVpo() + "'");
			} else if (searchForm.getVposelect().equals("Not like")) {
				sb.append(" AND PSR_ORDER.VPO  " + NOT_LIKE + "'%" + searchForm.getVpo() + "%'");
			} else if (searchForm.getVposelect().equals("In the list")) {
				sb.append(" AND PSR_ORDER.VPO  " + IN + "(" + PSRHelper.appendQuotesForGivenStr(searchForm.getVpo())
						+ ")");
			} else if (searchForm.getVposelect().equals("Not in the list")) {
				sb.append(" AND PSR_ORDER.VPO  " + NOT_IN + "(" + PSRHelper.appendQuotesForGivenStr(searchForm.getVpo())
						+ ")");
			} else if (searchForm.getVposelect().equals("Starts with")) {
				sb.append(" AND PSR_ORDER.VPO  " + LIKE + "'" + searchForm.getVpo() + "%'");
			} else if (searchForm.getVposelect().equals("Ends with")) {
				sb.append(" AND PSR_ORDER.VPO  " + LIKE + "'%" + searchForm.getVpo() + "'");
			}
		} else {
			if (searchForm.getVposelect() != null) {
				if (searchForm.getVposelect().equals("Equal to null")) {
					sb.append(" AND (PSR_ORDER.VPO  " + IS_NULL + " OR PSR_ORDER.VPO = " + "''" + "  )");
				} else if (searchForm.getVposelect().equals("Is not null")) {
					sb.append("  AND (PSR_ORDER.VPO  " + IS_NOT_NULL + " AND PSR_ORDER.VPO != " + "''" + "  )");
				}
			}
		}
		return sb.toString();
	}

	private static String addVendorIdFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getVendorId())) {

			if (searchForm.getVendorIdSelect().equals("Like")) {
				sb.append(" AND PSR_ORDER.VENDOR_ID " + LIKE + "'%" + searchForm.getVendorId() + "%'");
			} else if (searchForm.getVendorIdSelect().equals("Equal to")) {
				sb.append(" AND PSR_ORDER.VENDOR_ID  " + EQUALS + "'" + searchForm.getVendorId() + "'");
			} else if (searchForm.getVendorIdSelect().equals("Not equal to")) {
				sb.append(" AND PSR_ORDER.VENDOR_ID " + NOT_EQUALS + "'" + searchForm.getVendorId() + "'");
			} else if (searchForm.getVendorIdSelect().equals("Not like")) {
				sb.append(" AND PSR_ORDER.VENDOR_ID  " + NOT_LIKE + "'%" + searchForm.getVendorId() + "%'");
			} else if (searchForm.getVendorIdSelect().equals("In the list")) {
				sb.append(" AND PSR_ORDER.VENDOR_ID " + IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getVendorId()) + ")");
			} else if (searchForm.getVendorIdSelect().equals("Not in the list")) {
				sb.append(" AND PSR_ORDER.VENDOR_ID " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getVendorId()) + ")");
			} else if (searchForm.getVendorIdSelect().equals("Starts with")) {
				sb.append(" AND PSR_ORDER.VENDOR_ID " + LIKE + "'" + searchForm.getVendorId() + "%'");
			} else if (searchForm.getVendorIdSelect().equals("Ends with")) {
				sb.append(" AND PSR_ORDER.VENDOR_ID " + LIKE + "'%" + searchForm.getVendorId() + "'");
			}
		} else {
			if (searchForm.getVendorIdSelect() != null) {

				if (searchForm.getVendorIdSelect().equals("Equal to null")) {
					sb.append(" AND (PSR_ORDER.VENDOR_ID  " + IS_NULL + " OR PSR_ORDER.VENDOR_ID = " + "''" + "  )");
				} else if (searchForm.getVendorIdSelect().equals("Is not null")) {
					sb.append("  AND (PSR_ORDER.VENDOR_ID  " + IS_NOT_NULL + " AND PSR_ORDER.VENDOR_ID != " + "''"
							+ "  )");
				}
			}
		}
		return sb.toString();
	}

	private static String addVendorNameFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getVendorName())) {

			if (searchForm.getVendorNameSelect().equals("Like")) {
				sb.append(" AND PSR_ORDER.VENDOR_NAME " + LIKE + "'%" + searchForm.getVendorName() + "%'");
			} else if (searchForm.getVendorNameSelect().equals("Equal to")) {
				sb.append(" AND PSR_ORDER.VENDOR_NAME  " + EQUALS + "'" + searchForm.getVendorName() + "'");
			} else if (searchForm.getVendorNameSelect().equals("Not equal to")) {
				sb.append(" AND PSR_ORDER.VENDOR_NAME " + NOT_EQUALS + "'" + searchForm.getVendorName() + "'");
			} else if (searchForm.getVendorNameSelect().equals("Not like")) {
				sb.append(" AND PSR_ORDER.VENDOR_NAME  " + NOT_LIKE + "'%" + searchForm.getVendorName() + "%'");
			} else if (searchForm.getVendorNameSelect().equals("In the list")) {
				sb.append(" AND PSR_ORDER.VENDOR_NAME " + IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getVendorName()) + ")");
			} else if (searchForm.getVendorNameSelect().equals("Not in the list")) {
				sb.append(" AND PSR_ORDER.VENDOR_NAME " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getVendorName()) + ")");
			} else if (searchForm.getVendorNameSelect().equals("Starts with")) {
				sb.append(" AND PSR_ORDER.VENDOR_NAME " + LIKE + "'" + searchForm.getVendorName() + "%'");
			} else if (searchForm.getVendorNameSelect().equals("Ends with")) {
				sb.append(" AND PSR_ORDER.VENDOR_NAME " + LIKE + "'%" + searchForm.getVendorName() + "'");
			}
		} else {
			if (searchForm.getVendorNameSelect() != null) {

				if (searchForm.getVendorNameSelect().equals("Equal to null")) {
					sb.append(
							" AND (PSR_ORDER.VENDOR_NAME  " + IS_NULL + " OR PSR_ORDER.VENDOR_NAME = " + "''" + "  )");
				} else if (searchForm.getVendorNameSelect().equals("Is not null")) {
					sb.append("  AND (PSR_ORDER.VENDOR_NAME  " + IS_NOT_NULL + " AND PSR_ORDER.VENDOR_NAME != " + "''"
							+ "  )");
				}
			}
		}
		return sb.toString();
	}

	private static String addFactoryIdFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getFactoryId())) {

			if (searchForm.getFactoryIdSelect().equals("Like")) {
				sb.append(" AND PSR_ORDER.FACTORY_ID " + LIKE + "'%" + searchForm.getFactoryId() + "%'");
			} else if (searchForm.getFactoryIdSelect().equals("Equal to")) {
				sb.append(" AND PSR_ORDER.FACTORY_ID  " + EQUALS + searchForm.getFactoryId() + "");
			} else if (searchForm.getFactoryIdSelect().equals("Not equal to")) {
				sb.append(" AND PSR_ORDER.FACTORY_ID " + NOT_EQUALS + searchForm.getFactoryId() + "");
			} else if (searchForm.getFactoryIdSelect().equals("Not like")) {
				sb.append(" AND PSR_ORDER.FACTORY_ID  " + NOT_LIKE + "'%" + searchForm.getFactoryId() + "%'");
			} else if (searchForm.getFactoryIdSelect().equals("In the list")) {
				sb.append(" AND PSR_ORDER.FACTORY_ID " + IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getFactoryId()) + ")");
			} else if (searchForm.getFactoryIdSelect().equals("Not in the list")) {
				sb.append(" AND PSR_ORDER.FACTORY_ID " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getFactoryId()) + ")");
			} else if (searchForm.getFactoryIdSelect().equals("Starts with")) {
				sb.append(" AND PSR_ORDER.FACTORY_ID " + LIKE + "'" + searchForm.getFactoryId() + "%'");
			} else if (searchForm.getFactoryIdSelect().equals("Ends with")) {
				sb.append(" AND PSR_ORDER.FACTORY_ID " + LIKE + "'%" + searchForm.getFactoryId() + "'");
			}

		} else {
			if (searchForm.getFactoryIdSelect() != null) {

				if (searchForm.getFactoryIdSelect().equals("Equal to null")) {
					sb.append(" AND (PSR_ORDER.FACTORY_ID  " + IS_NULL + " OR PSR_ORDER.FACTORY_ID = " + "''" + "  )");
				} else if (searchForm.getFactoryIdSelect().equals("Is not null")) {
					sb.append("  AND (PSR_ORDER.FACTORY_ID  " + IS_NOT_NULL + " AND PSR_ORDER.FACTORY_ID != " + "''"
							+ "  )");
				}
			}
		}
		return sb.toString();
	}

	private static String addFactoryNameFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getFactoryName())) {

			if (searchForm.getFactoryNameSelect().equals("Like")) {
				sb.append(" AND  PSR_ORDER.FACTORY_NAME " + LIKE + "'%" + searchForm.getFactoryName() + "%'");
			} else if (searchForm.getFactoryNameSelect().equals("Equal to")) {
				sb.append(" AND  PSR_ORDER.FACTORY_NAME " + EQUALS + "'" + searchForm.getFactoryName() + "'");
			} else if (searchForm.getFactoryNameSelect().equals("Not equal to")) {
				sb.append(" AND  PSR_ORDER.FACTORY_NAME " + NOT_EQUALS + "'" + searchForm.getFactoryName() + "'");
			} else if (searchForm.getFactoryNameSelect().equals("Not like")) {
				sb.append(" AND  PSR_ORDER.FACTORY_NAME  " + NOT_LIKE + "'%" + searchForm.getFactoryName() + "%'");
			} else if (searchForm.getFactoryNameSelect().equals("In the list")) {
				sb.append(" AND  PSR_ORDER.FACTORY_NAME " + IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getFactoryName()) + ")");
			} else if (searchForm.getFactoryNameSelect().equals("Not in the list")) {
				sb.append(" AND  PSR_ORDER.FACTORY_NAME " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getFactoryName()) + ")");
			} else if (searchForm.getFactoryNameSelect().equals("Starts with")) {
				sb.append(" AND  PSR_ORDER.FACTORY_NAME " + LIKE + "'" + searchForm.getFactoryName() + "%'");
			} else if (searchForm.getFactoryNameSelect().equals("Ends with")) {
				sb.append(" AND  PSR_ORDER.FACTORY_NAME " + LIKE + "'%" + searchForm.getFactoryName() + "'");
			}
		} else {
			if (searchForm.getFactoryNameSelect() != null) {

				if (searchForm.getFactoryNameSelect().equals("Equal to null")) {
					sb.append(" AND (PSR_ORDER.FACTORY_NAME  " + IS_NULL + " OR PSR_ORDER.FACTORY_NAME = " + "''"
							+ "  )");
				} else if (searchForm.getFactoryNameSelect().equals("Is not null")) {
					sb.append("  AND (PSR_ORDER.FACTORY_NAME  " + IS_NOT_NULL + " AND PSR_ORDER.FACTORY_NAME != " + "''"
							+ "  )");
				}
			}
		}
		return sb.toString();
	}

	private static String addCountryOriginFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getCountryOrigin())) {
			if (searchForm.getCountryOriginSelect().equals("Like")) {
				sb.append(" AND PSR_ORDER.COUNTRY_ORIGIN " + LIKE + "'%" + searchForm.getCountryOrigin() + "%'");
			} else if (searchForm.getCountryOriginSelect().equals("Equal to")) {
				sb.append(" AND PSR_ORDER.COUNTRY_ORIGIN  " + EQUALS + "'" + searchForm.getCountryOrigin() + "'");
			} else if (searchForm.getCountryOriginSelect().equals("Not equal to")) {
				sb.append(" AND PSR_ORDER.COUNTRY_ORIGIN " + NOT_EQUALS + "'" + searchForm.getCountryOrigin() + "'");
			} else if (searchForm.getCountryOriginSelect().equals("Not like")) {
				sb.append(" AND PSR_ORDER.COUNTRY_ORIGIN  " + NOT_LIKE + "'%" + searchForm.getCountryOrigin() + "%'");
			} else if (searchForm.getCountryOriginSelect().equals("In the list")) {
				sb.append(" AND PSR_ORDER.COUNTRY_ORIGIN " + IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getCountryOrigin()) + ")");
			} else if (searchForm.getCountryOriginSelect().equals("Not in the list")) {
				sb.append(" AND PSR_ORDER.COUNTRY_ORIGIN " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getCountryOrigin()) + ")");
			} else if (searchForm.getCountryOriginSelect().equals("Starts with")) {
				sb.append(" AND PSR_ORDER.COUNTRY_ORIGIN " + LIKE + "'" + searchForm.getCountryOrigin() + "%'");
			} else if (searchForm.getCountryOriginSelect().equals("Ends with")) {
				sb.append(" AND PSR_ORDER.COUNTRY_ORIGIN " + LIKE + "'%" + searchForm.getCountryOrigin() + "'");
			}

		} else {
			if (searchForm.getCountryOriginSelect() != null) {

				if (searchForm.getCountryOriginSelect().equals("Equal to null")) {
					sb.append(" AND (PSR_ORDER.COUNTRY_ORIGIN  " + IS_NULL + " OR PSR_ORDER.COUNTRY_ORIGIN = " + "''"
							+ "  )");
				} else if (searchForm.getCountryOriginSelect().equals("Is not null")) {
					sb.append("  AND (PSR_ORDER.COUNTRY_ORIGIN  " + IS_NOT_NULL + " AND PSR_ORDER.COUNTRY_ORIGIN != "
							+ "''" + "  )");
				}
			}
		}
		return sb.toString();
	}

	private static String addBrandFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getBrand())) {
			if (searchForm.getBrandSelect().equals("Like")) {
				sb.append(" AND  PSR_ORDER.BRAND " + LIKE + "'%" + searchForm.getBrand() + "%'");
			} else if (searchForm.getBrandSelect().equals("Equal to")) {
				sb.append(" AND  PSR_ORDER.BRAND " + EQUALS + "'" + searchForm.getBrand() + "'");
			} else if (searchForm.getBrandSelect().equals("Not equal to")) {
				sb.append(" AND PSR_ORDER.BRAND " + NOT_EQUALS + "'" + searchForm.getBrand() + "'");
			} else if (searchForm.getBrandSelect().equals("Not like")) {
				sb.append(" AND  PSR_ORDER.BRAND  " + NOT_LIKE + "'%" + searchForm.getBrand() + "%'");
			} else if (searchForm.getBrandSelect().equals("In the list")) {
				sb.append(" AND  PSR_ORDER.BRAND " + IN + "(" + PSRHelper.appendQuotesForGivenStr(searchForm.getBrand())
						+ ")");
			} else if (searchForm.getBrandSelect().equals("Not in the list")) {
				sb.append(" AND  PSR_ORDER.BRAND " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getBrand()) + ")");
			} else if (searchForm.getBrandSelect().equals("Starts with")) {
				sb.append(" AND  PSR_ORDER.BRAND " + LIKE + "'" + searchForm.getBrand() + "%'");
			} else if (searchForm.getBrandSelect().equals("Ends with")) {
				sb.append(" AND  PSR_ORDER.BRAND " + LIKE + "'%" + searchForm.getBrand() + "'");
			}
		} else {
			if (searchForm.getBrandSelect() != null) {

				if (searchForm.getBrandSelect().equals("Equal to null")) {
					sb.append(" AND (PSR_ORDER.BRAND  " + IS_NULL + " OR PSR_ORDER.BRAND = " + "''" + "  )");
				} else if (searchForm.getBrandSelect().equals("Is not null")) {
					sb.append("  AND (PSR_ORDER.BRAND  " + IS_NOT_NULL + " AND PSR_ORDER.BRAND != " + "''" + "  )");
				}
			}
		}
		return sb.toString();
	}

	private static String addStyleFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getStyle())) {
			if (searchForm.getStyleSelect().equals("Like")) {
				sb.append(" AND  PSR_ORDER.STYLE  " + LIKE + "'%" + searchForm.getStyle() + "%'");
			} else if (searchForm.getStyleSelect().equals("Equal to")) {
				sb.append(" AND  PSR_ORDER.STYLE   " + EQUALS + "'" + searchForm.getStyle() + "'");
			} else if (searchForm.getStyleSelect().equals("Not equal to")) {
				sb.append(" AND  PSR_ORDER.STYLE  " + NOT_EQUALS + "'" + searchForm.getStyle() + "'");
			} else if (searchForm.getStyleSelect().equals("Not like")) {
				sb.append(" AND  PSR_ORDER.STYLE   " + NOT_LIKE + "'%" + searchForm.getStyle() + "%'");
			} else if (searchForm.getStyleSelect().equals("In the list")) {
				sb.append(" AND  PSR_ORDER.STYLE  " + IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getStyle()) + ")");
			} else if (searchForm.getStyleSelect().equals("Not in the list")) {
				sb.append(" AND  PSR_ORDER.STYLE  " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getStyle()) + ")");
			} else if (searchForm.getStyleSelect().equals("Starts with")) {
				sb.append(" AND  PSR_ORDER.STYLE  " + LIKE + "'" + searchForm.getStyle() + "%'");
			} else if (searchForm.getStyleSelect().equals("Ends with")) {
				sb.append(" AND  PSR_ORDER.STYLE  " + LIKE + "'%" + searchForm.getStyle() + "'");
			}
		} else {
			if (searchForm.getStyleSelect() != null) {

				if (searchForm.getStyleSelect().equals("Equal to null")) {
					sb.append(" AND (PSR_ORDER.STYLE  " + IS_NULL + " OR PSR_ORDER.STYLE = " + "''" + "  )");
				} else if (searchForm.getStyleSelect().equals("Is not null")) {
					sb.append("  AND (PSR_ORDER.STYLE  " + IS_NOT_NULL + " AND PSR_ORDER.STYLE != " + "''" + "  )");
				}
			}
		}
		return sb.toString();
	}

	private static String addProductionOfficeFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getProductionOffice())) {
			if (searchForm.getProdOfficeSelect().equals("Like")) {
				sb.append(" AND  PSR_ORDER.PROD_OFFICE  " + LIKE + "'%" + searchForm.getProductionOffice() + "%'");
			} else if (searchForm.getProdOfficeSelect().equals("Equal to")) {
				sb.append(" AND  PSR_ORDER.PROD_OFFICE   " + EQUALS + "'" + searchForm.getProductionOffice() + "'");
			} else if (searchForm.getProdOfficeSelect().equals("Not equal to")) {
				sb.append(" AND  PSR_ORDER.PROD_OFFICE  " + NOT_EQUALS + "'" + searchForm.getProductionOffice() + "'");
			} else if (searchForm.getProdOfficeSelect().equals("Not like")) {
				sb.append(" AND  PSR_ORDER.PROD_OFFICE   " + NOT_LIKE + "'%" + searchForm.getProductionOffice() + "%'");
			} else if (searchForm.getProdOfficeSelect().equals("In the list")) {
				sb.append(" AND  PSR_ORDER.PROD_OFFICE  " + IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getProductionOffice()) + ")");
			} else if (searchForm.getProdOfficeSelect().equals("Not in the list")) {
				sb.append(" AND  PSR_ORDER.PROD_OFFICE  " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getProductionOffice()) + ")");
			} else if (searchForm.getProdOfficeSelect().equals("Starts with")) {
				sb.append(" AND  PSR_ORDER.PROD_OFFICE  " + LIKE + "'" + searchForm.getProductionOffice() + "%'");
			} else if (searchForm.getProdOfficeSelect().equals("Ends with")) {
				sb.append(" AND  PSR_ORDER.PROD_OFFICE  " + LIKE + "'%" + searchForm.getProductionOffice() + "'");
			}
		} else {
			if (searchForm.getProdOfficeSelect() != null) {

				if (searchForm.getProdOfficeSelect().equals("Equal to null")) {
					sb.append(
							" AND (PSR_ORDER.PROD_OFFICE  " + IS_NULL + " OR PSR_ORDER.PROD_OFFICE = " + "''" + "  )");
				} else if (searchForm.getProdOfficeSelect().equals("Is not null")) {
					sb.append("  AND (PSR_ORDER.PROD_OFFICE  " + IS_NOT_NULL + " AND PSR_ORDER.PROD_OFFICE != " + "''"
							+ "  )");
				}
			}
		}
		return sb.toString();
	}

	private static String addStyleDescriptionFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getStyleDescription())) {
			if (searchForm.getStyleDescSelect().equals("Like")) {
				sb.append(" AND  PSR_ORDER.STYLE_DESC  " + LIKE + "'%" + searchForm.getStyleDescription() + "%'");
			} else if (searchForm.getStyleDescSelect().equals("Equal to")) {
				sb.append(" AND  PSR_ORDER.STYLE_DESC   " + EQUALS + "'" + searchForm.getStyleDescription() + "'");
			} else if (searchForm.getStyleDescSelect().equals("Not equal to")) {
				sb.append(" AND  PSR_ORDER.STYLE_DESC  " + NOT_EQUALS + "'" + searchForm.getStyleDescription() + "'");
			} else if (searchForm.getStyleDescSelect().equals("Not like")) {
				sb.append(" AND  PSR_ORDER.STYLE_DESC   " + NOT_LIKE + "'%" + searchForm.getStyleDescription() + "%'");
			} else if (searchForm.getStyleDescSelect().equals("In the list")) {
				sb.append(" AND  PSR_ORDER.STYLE_DESC  " + IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getStyleDescription()) + ")");
			} else if (searchForm.getStyleDescSelect().equals("Not in the list")) {
				sb.append(" AND  PSR_ORDER.STYLE_DESC  " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getStyleDescription()) + ")");
			} else if (searchForm.getStyleDescSelect().equals("Starts with")) {
				sb.append(" AND  PSR_ORDER.STYLE_DESC  " + LIKE + "'" + searchForm.getStyleDescription() + "%'");
			} else if (searchForm.getStyleDescSelect().equals("Ends with")) {
				sb.append(" AND  PSR_ORDER.STYLE_DESC  " + LIKE + "'%" + searchForm.getStyleDescription() + "'");
			}
		} else {
			if (searchForm.getStyleDescSelect() != null) {

				if (searchForm.getStyleDescSelect().equals("Equal to null")) {
					sb.append(" AND (PSR_ORDER.STYLE_DESC  " + IS_NULL + " OR PSR_ORDER.STYLE_DESC = " + "''" + "  )");
				} else if (searchForm.getStyleDescSelect().equals("Is not null")) {
					sb.append("  AND (PSR_ORDER.STYLE_DESC  " + IS_NOT_NULL + " AND PSR_ORDER.STYLE_DESC != " + "''"
							+ "  )");
				}
			}
		}
		return sb.toString();
	}

	private static String addCpoFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getCpo())) {
			if (searchForm.getCpoSelect().equals("Like")) {
				sb.append(" AND  PSR_ORDER.CPO  " + LIKE + "'%" + searchForm.getCpo() + "%'");
			} else if (searchForm.getCpoSelect().equals("Equal to")) {
				sb.append(" AND  PSR_ORDER.CPO   " + EQUALS + "'" + searchForm.getCpo() + "'");
			} else if (searchForm.getCpoSelect().equals("Not equal to")) {
				sb.append(" AND  PSR_ORDER.CPO  " + NOT_EQUALS + "'" + searchForm.getCpo() + "'");
			} else if (searchForm.getCpoSelect().equals("Not like")) {
				sb.append(" AND  PSR_ORDER.CPO   " + NOT_LIKE + "'%" + searchForm.getCpo() + "%'");
			} else if (searchForm.getCpoSelect().equals("In the list")) {
				sb.append(" AND  PSR_ORDER.CPO  " + IN + "(" + PSRHelper.appendQuotesForGivenStr(searchForm.getCpo())
						+ ")");
			} else if (searchForm.getCpoSelect().equals("Not in the list")) {
				sb.append(" AND  PSR_ORDER.CPO  " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getCpo()) + ")");
			} else if (searchForm.getCpoSelect().equals("Starts with")) {
				sb.append(" AND  PSR_ORDER.CPO  " + LIKE + "'" + searchForm.getCpo() + "%'");
			} else if (searchForm.getCpoSelect().equals("Ends with")) {
				sb.append(" AND  PSR_ORDER.CPO  " + LIKE + "'%" + searchForm.getCpo() + "'");
			}
		} else {
			if (searchForm.getCpoSelect() != null) {

				if (searchForm.getCpoSelect().equals("Equal to null")) {
					sb.append(" AND (PSR_ORDER.CPO  " + IS_NULL + " OR PSR_ORDER.CPO = " + "''" + "  )");
				} else if (searchForm.getCpoSelect().equals("Is not null")) {
					sb.append("  AND (PSR_ORDER.CPO  " + IS_NOT_NULL + " AND PSR_ORDER.CPO != " + "''" + "  )");
				}
			}
		}
		return sb.toString();
	}

	private static String addDeptFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getDept())) {
			if (searchForm.getDeptSelect().equals("Like")) {
				sb.append(" AND  PSR_ORDER.DEPT  " + LIKE + "'%" + searchForm.getDept() + "%'");
			} else if (searchForm.getDeptSelect().equals("Equal to")) {
				sb.append(" AND  PSR_ORDER.DEPT   " + EQUALS + "'" + searchForm.getDept() + "'");
			} else if (searchForm.getDeptSelect().equals("Not equal to")) {
				sb.append(" AND  PSR_ORDER.DEPT  " + NOT_EQUALS + "'" + searchForm.getDept() + "'");
			} else if (searchForm.getDeptSelect().equals("Not like")) {
				sb.append(" AND  PSR_ORDER.DEPT   " + NOT_LIKE + "'%" + searchForm.getDept() + "%'");
			} else if (searchForm.getDeptSelect().equals("In the list")) {
				sb.append(" AND  PSR_ORDER.DEPT  " + IN + "(" + PSRHelper.appendQuotesForGivenStr(searchForm.getDept())
						+ ")");
			} else if (searchForm.getDeptSelect().equals("Not in the list")) {
				sb.append(" AND  PSR_ORDER.DEPT  " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getDept()) + ")");
			} else if (searchForm.getDeptSelect().equals("Starts with")) {
				sb.append(" AND  PSR_ORDER.DEPT  " + LIKE + "'" + searchForm.getDept() + "%'");
			} else if (searchForm.getDeptSelect().equals("Ends with")) {
				sb.append(" AND  PSR_ORDER.DEPT  " + LIKE + "'%" + searchForm.getDept() + "'");
			}
		} else {
			if (searchForm.getDeptSelect() != null) {

				if (searchForm.getDeptSelect().equals("Equal to null")) {
					sb.append(" AND (PSR_ORDER.DEPT  " + IS_NULL + " OR PSR_ORDER.DEPT = " + "''" + "  )");
				} else if (searchForm.getDeptSelect().equals("Is not null")) {
					sb.append("  AND (PSR_ORDER.DEPT  " + IS_NOT_NULL + " AND PSR_ORDER.DEPT != " + "''" + "  )");
				}
			}
		}
		return sb.toString();
	}

	private static String addSeasonFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getSeason())) {
			if (searchForm.getSeasonSelect().equals("Like")) {
				sb.append(" AND  PSR_ORDER.SEASON  " + LIKE + "'%" + searchForm.getSeason() + "%'");
			} else if (searchForm.getSeasonSelect().equals("Equal to")) {
				sb.append(" AND  PSR_ORDER.SEASON   " + EQUALS + "'" + searchForm.getSeason() + "'");
			} else if (searchForm.getSeasonSelect().equals("Not equal to")) {
				sb.append(" AND  PSR_ORDER.SEASON  " + NOT_EQUALS + "'" + searchForm.getSeason() + "'");
			} else if (searchForm.getSeasonSelect().equals("Not like")) {
				sb.append(" AND  PSR_ORDER.SEASON   " + NOT_LIKE + "'%" + searchForm.getSeason() + "%'");
			} else if (searchForm.getSeasonSelect().equals("In the list")) {
				sb.append(" AND  PSR_ORDER.SEASON  " + IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getSeason()) + ")");
			} else if (searchForm.getSeasonSelect().equals("Not in the list")) {
				sb.append(" AND  PSR_ORDER.SEASON  " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getSeason()) + ")");
			} else if (searchForm.getSeasonSelect().equals("Starts with")) {
				sb.append(" AND  PSR_ORDER.SEASON  " + LIKE + "'" + searchForm.getSeason() + "%'");
			} else if (searchForm.getSeasonSelect().equals("Ends with")) {
				sb.append(" AND  PSR_ORDER.SEASON  " + LIKE + "'%" + searchForm.getSeason() + "'");
			}
		} else {
			if (searchForm.getSeasonSelect() != null) {

				if (searchForm.getSeasonSelect().equals("Equal to null")) {
					sb.append(" AND (PSR_ORDER.SEASON  " + IS_NULL + " OR PSR_ORDER.SEASON = " + "''" + "  )");
				} else if (searchForm.getSeasonSelect().equals("Is not null")) {
					sb.append("  AND (PSR_ORDER.SEASON  " + IS_NOT_NULL + " AND PSR_ORDER.SEASON != " + "''" + "  )");
				}
			}
		}
		return sb.toString();
	}

	private static String addColorWashNameFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getColorWashName())) {
			if (searchForm.getColorWashNameSelect().equals("Like")) {
				sb.append(" AND  PSR_ORDER.COLOR_WASH_NAME  " + LIKE + "'%" + searchForm.getColorWashName() + "%'");
			} else if (searchForm.getColorWashNameSelect().equals("Equal to")) {
				sb.append(" AND  PSR_ORDER.COLOR_WASH_NAME   " + EQUALS + "'" + searchForm.getColorWashName() + "'");
			} else if (searchForm.getColorWashNameSelect().equals("Not equal to")) {
				sb.append(" AND  PSR_ORDER.COLOR_WASH_NAME  " + NOT_EQUALS + "'" + searchForm.getColorWashName() + "'");
			} else if (searchForm.getColorWashNameSelect().equals("Not like")) {
				sb.append(
						" AND  PSR_ORDER.COLOR_WASH_NAME   " + NOT_LIKE + "'%" + searchForm.getColorWashName() + "%'");
			} else if (searchForm.getColorWashNameSelect().equals("In the list")) {
				sb.append(" AND  PSR_ORDER.COLOR_WASH_NAME  " + IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getColorWashName()) + ")");
			} else if (searchForm.getColorWashNameSelect().equals("Not in the list")) {
				sb.append(" AND  PSR_ORDER.COLOR_WASH_NAME  " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getColorWashName()) + ")");
			} else if (searchForm.getColorWashNameSelect().equals("Starts with")) {
				sb.append(" AND  PSR_ORDER.COLOR_WASH_NAME  " + LIKE + "'" + searchForm.getColorWashName() + "%'");
			} else if (searchForm.getColorWashNameSelect().equals("Ends with")) {
				sb.append(" AND  PSR_ORDER.COLOR_WASH_NAME  " + LIKE + "'%" + searchForm.getColorWashName() + "'");
			}
		} else {
			if (searchForm.getColorWashNameSelect() != null) {

				if (searchForm.getColorWashNameSelect().equals("Equal to null")) {
					sb.append(" AND (PSR_ORDER.COLOR_WASH_NAME  " + IS_NULL + " OR PSR_ORDER.COLOR_WASH_NAME = " + "''"
							+ "  )");
				} else if (searchForm.getColorWashNameSelect().equals("Is not null")) {
					sb.append("  AND (PSR_ORDER.COLOR_WASH_NAME  " + IS_NOT_NULL + " AND PSR_ORDER.COLOR_WASH_NAME != "
							+ "''" + "  )");
				}
			}
		}
		return sb.toString();
	}

	private static String addNdcDateFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getNdcdate())) {
			if (searchForm.getNdcdateSelect().equals("Greater")) {
				sb.append(" AND  PSR_ORDER.NDC_DATE  " + GREATER + "'" + searchForm.getNdcdate() + "'");
			} else if (searchForm.getNdcdateSelect().equals("Equal to")) {
				sb.append(" AND  PSR_ORDER.NDC_DATE   " + EQUALS + "'" + searchForm.getNdcdate() + "'");
			} else if (searchForm.getNdcdateSelect().equals("Not equal to")) {
				sb.append(" AND  PSR_ORDER.NDC_DATE  " + NOT_EQUALS + "'" + searchForm.getNdcdate() + "'");
			} else if (searchForm.getNdcdateSelect().equals("Less")) {
				sb.append(" AND  PSR_ORDER.NDC_DATE   " + LESS + "'" + searchForm.getNdcdate() + "'");
			} else if (searchForm.getNdcdateSelect().equals("In the list")) {
				sb.append(" AND  PSR_ORDER.NDC_DATE  " + IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getNdcdate()) + ")");
			} else if (searchForm.getNdcdateSelect().equals("Not in the list")) {
				sb.append(" AND  PSR_ORDER.NDC_DATE  " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getNdcdate()) + ")");
			} else if (searchForm.getNdcdateSelect().equals("Starts with")) {
				sb.append(" AND  PSR_ORDER.NDC_DATE  " + LIKE + "'" + searchForm.getNdcdate() + "%'");
			} else if (searchForm.getNdcdateSelect().equals("Ends with")) {
				sb.append(" AND  PSR_ORDER.NDC_DATE  " + LIKE + "'%" + searchForm.getNdcdate() + "'");
			//CHG0016311 Rupan Start
			} else if (searchForm.getNdcdateSelect().equals("Between")) {
				sb.append(" AND  PSR_ORDER.NDC_DATE  " + BETWEEN + " " + searchForm.getNdcdate());
			//CHG0016311 Rupan End
			}
		} else {
			if (searchForm.getNdcdateSelect() != null) {

				if (searchForm.getNdcdateSelect().equals("Equal to null")) {
					sb.append(" AND PSR_ORDER.NDC_DATE  " + IS_NULL + "");
				} else if (searchForm.getNdcdateSelect().equals("Is not null")) {
					sb.append(" AND PSR_ORDER.NDC_DATE  " + IS_NOT_NULL + "");
				}
			}
		}
		return sb.toString();
	}

	private static String addVpoNltGacDateFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getVpoNltGacDate())) {
			if (searchForm.getVpoNltGacDateSelect().equals("Greater")) {
				sb.append(" AND  PSR_ORDER.VPO_NLT_GAC_DATE  " + GREATER + "'" + searchForm.getVpoNltGacDate() + "'");
			} else if (searchForm.getVpoNltGacDateSelect().equals("Equal to")) {
				sb.append(" AND  PSR_ORDER.VPO_NLT_GAC_DATE   " + EQUALS + "'" + searchForm.getVpoNltGacDate() + "'");
			} else if (searchForm.getVpoNltGacDateSelect().equals("Not equal to")) {
				sb.append(
						" AND  PSR_ORDER.VPO_NLT_GAC_DATE  " + NOT_EQUALS + "'" + searchForm.getVpoNltGacDate() + "'");
			} else if (searchForm.getVpoNltGacDateSelect().equals("Less")) {
				sb.append(" AND  PSR_ORDER.VPO_NLT_GAC_DATE   " + LESS + "'" + searchForm.getVpoNltGacDate() + "'");
			} else if (searchForm.getVpoNltGacDateSelect().equals("In the list")) {
				sb.append(" AND  PSR_ORDER.VPO_NLT_GAC_DATE  " + IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getVpoNltGacDate()) + ")");
			} else if (searchForm.getVpoNltGacDateSelect().equals("Not in the list")) {
				sb.append(" AND  PSR_ORDER.VPO_NLT_GAC_DATE  " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getVpoNltGacDate()) + ")");
			} else if (searchForm.getVpoNltGacDateSelect().equals("Starts with")) {
				sb.append(" AND  PSR_ORDER.VPO_NLT_GAC_DATE  " + LIKE + "'" + searchForm.getVpoNltGacDate() + "%'");
			} else if (searchForm.getVpoNltGacDateSelect().equals("Ends with")) {
				sb.append(" AND  PSR_ORDER.VPO_NLT_GAC_DATE  " + LIKE + "'%" + searchForm.getVpoNltGacDate() + "'");
			//CHG0016311 Rupan Start
			} else if (searchForm.getVpoNltGacDateSelect().equals("Between")) {
				sb.append(" AND  PSR_ORDER.VPO_NLT_GAC_DATE  " + BETWEEN + " " + searchForm.getVpoNltGacDate());
			//CHG0016311 Rupan End
			}
		} else {
			if (searchForm.getVpoNltGacDateSelect() != null) {

				if (searchForm.getVpoNltGacDateSelect().equals("Equal to null")) {
					sb.append(" AND PSR_ORDER.VPO_NLT_GAC_DATE  " + IS_NULL + "");
				} else if (searchForm.getVpoNltGacDateSelect().equals("Is not null")) {
					sb.append(" AND PSR_ORDER.VPO_NLT_GAC_DATE  " + IS_NOT_NULL + "");
				}
			}
		}
		return sb.toString();
	}

	private static String addVpoFtyGacDateFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getVpoFtyGacDate())) {
			if (searchForm.getVpoFtyGacDateSelect().equals("Greater")) {
				sb.append(" AND  PSR_ORDER.VPO_FTY_GAC_DATE  " + GREATER + "'" + searchForm.getVpoFtyGacDate() + "'");
			} else if (searchForm.getVpoFtyGacDateSelect().equals("Equal to")) {
				sb.append(" AND  PSR_ORDER.VPO_FTY_GAC_DATE   " + EQUALS + "'" + searchForm.getVpoFtyGacDate() + "'");
			} else if (searchForm.getVpoFtyGacDateSelect().equals("Not equal to")) {
				sb.append(
						" AND  PSR_ORDER.VPO_FTY_GAC_DATE  " + NOT_EQUALS + "'" + searchForm.getVpoFtyGacDate() + "'");
			} else if (searchForm.getVpoFtyGacDateSelect().equals("Less")) {
				sb.append(" AND  PSR_ORDER.VPO_FTY_GAC_DATE   " + LESS + "'" + searchForm.getVpoFtyGacDate() + "'");
			} else if (searchForm.getVpoFtyGacDateSelect().equals("In the list")) {
				sb.append(" AND  PSR_ORDER.VPO_FTY_GAC_DATE  " + IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getVpoFtyGacDate()) + ")");
			} else if (searchForm.getVpoFtyGacDateSelect().equals("Not in the list")) {
				sb.append(" AND  PSR_ORDER.VPO_FTY_GAC_DATE  " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getVpoFtyGacDate()) + ")");
			} else if (searchForm.getVpoFtyGacDateSelect().equals("Starts with")) {
				sb.append(" AND  PSR_ORDER.VPO_FTY_GAC_DATE  " + LIKE + "'" + searchForm.getVpoFtyGacDate() + "%'");
			} else if (searchForm.getVpoFtyGacDateSelect().equals("Ends with")) {
				sb.append(" AND  PSR_ORDER.VPO_FTY_GAC_DATE  " + LIKE + "'%" + searchForm.getVpoFtyGacDate() + "'");
			//CHG0016311 Rupan Start
			} else if (searchForm.getVpoFtyGacDateSelect().equals("Between")) {
				sb.append(" AND  PSR_ORDER.VPO_FTY_GAC_DATE  " + BETWEEN + " " + searchForm.getVpoFtyGacDate());
			//CHG0016311 Rupan End
			}
		} else {
			if (searchForm.getVpoFtyGacDateSelect() != null) {

				if (searchForm.getVpoFtyGacDateSelect().equals("Equal to null")) {
					sb.append(" AND PSR_ORDER.VPO_FTY_GAC_DATE  " + IS_NULL + "");
				} else if (searchForm.getVpoFtyGacDateSelect().equals("Is not null")) {
					sb.append(" AND PSR_ORDER.VPO_FTY_GAC_DATE  " + IS_NOT_NULL + "");
				}
			}
		}
		return sb.toString();
	}

	private static String addNdcWkSearchFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getNdcWk())) {
			if (searchForm.getNdcWkSelect().equals("Like")) {
				sb.append(" AND  PSR_ORDER.NDC_WK  " + LIKE + "'%" + searchForm.getNdcWk() + "%'");
			} else if (searchForm.getNdcWkSelect().equals("Equal to")) {
				sb.append(" AND  PSR_ORDER.NDC_WK   " + EQUALS + "'" + searchForm.getNdcWk() + "'");
			} else if (searchForm.getNdcWkSelect().equals("Not equal to")) {
				sb.append(" AND  PSR_ORDER.NDC_WK  " + NOT_EQUALS + "'" + searchForm.getNdcWk() + "'");
			} else if (searchForm.getNdcWkSelect().equals("Not like")) {
				sb.append(" AND  PSR_ORDER.NDC_WK   " + NOT_LIKE + "'%" + searchForm.getNdcWk() + "%'");
			} else if (searchForm.getNdcWkSelect().equals("In the list")) {
				sb.append(" AND  PSR_ORDER.NDC_WK  " + IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getNdcWk()) + ")");
			} else if (searchForm.getNdcWkSelect().equals("Not in the list")) {
				sb.append(" AND  PSR_ORDER.NDC_WK  " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getNdcWk()) + ")");
			} else if (searchForm.getNdcWkSelect().equals("Starts with")) {
				sb.append(" AND  PSR_ORDER.NDC_WK  " + LIKE + "'" + searchForm.getNdcWk() + "%'");
			} else if (searchForm.getNdcWkSelect().equals("Ends with")) {
				sb.append(" AND  PSR_ORDER.NDC_WK  " + LIKE + "'%" + searchForm.getNdcWk() + "'");
			}
		} else {
			if (searchForm.getNdcWkSelect() != null) {
				if (searchForm.getNdcWkSelect().equals("Equal to null")) {
					sb.append(" AND (PSR_ORDER.NDC_WK  " + IS_NULL + " OR PSR_ORDER.NDC_WK = " + "''" + "  )");
				} else if (searchForm.getNdcWkSelect().equals("Is not null")) {
					sb.append("  AND (PSR_ORDER.NDC_WK  " + IS_NOT_NULL + " AND PSR_ORDER.NDC_WK != " + "''" + "  )");
				}
			}
		}
		return sb.toString();
	}

	private static String addVpoStatusFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getVpoStatus())) {
			if (searchForm.getVpoStatusSelect().equals("Like")) {
				sb.append(" AND  PSR_ORDER.VPO_STATUS  " + LIKE + "'%" + searchForm.getVpoStatus() + "%'");
			} else if (searchForm.getVpoStatusSelect().equals("Equal to")) {
				sb.append(" AND  PSR_ORDER.VPO_STATUS   " + EQUALS + "'" + searchForm.getVpoStatus() + "'");
			} else if (searchForm.getVpoStatusSelect().equals("Not equal to")) {
				sb.append(" AND  PSR_ORDER.VPO_STATUS  " + NOT_EQUALS + "'" + searchForm.getVpoStatus() + "'");
			} else if (searchForm.getVpoStatusSelect().equals("Not like")) {
				sb.append(" AND  PSR_ORDER.VPO_STATUS  " + NOT_LIKE + "'%" + searchForm.getVpoStatus() + "%'");
			} else if (searchForm.getVpoStatusSelect().equals("In the list")) {
				sb.append(" AND  PSR_ORDER.VPO_STATUS  " + IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getVpoStatus()) + ")");
			} else if (searchForm.getVpoStatusSelect().equals("Not in the list")) {
				sb.append(" AND  PSR_ORDER.VPO_STATUS  " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getVpoStatus()) + ")");
			} else if (searchForm.getVpoStatusSelect().equals("Starts with")) {
				sb.append(" AND  PSR_ORDER.VPO_STATUS  " + LIKE + "'" + searchForm.getVpoStatus() + "%'");
			} else if (searchForm.getVpoStatusSelect().equals("Ends with")) {
				sb.append(" AND  PSR_ORDER.VPO_STATUS  " + LIKE + "'%" + searchForm.getVpoStatus() + "'");
			}
		} else {
			if (searchForm.getVpoStatusSelect() != null) {
				if (searchForm.getVpoStatusSelect().equals("Equal to null")) {
					sb.append(" AND (PSR_ORDER.VPO_STATUS  " + IS_NULL + " OR PSR_ORDER.VPO_STATUS = " + "''" + "  )");
				} else if (searchForm.getVpoStatusSelect().equals("Is not null")) {
					sb.append("  AND (PSR_ORDER.VPO_STATUS  " + IS_NOT_NULL + " AND PSR_ORDER.VPO_STATUS != " + "''"
							+ "  )");
				}
			}
		}
		return sb.toString();
	}

	private static String addVpoLineStatusFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getVpoLineStatus())) {
			if (searchForm.getVpoLineStatusSelect().equals("Like")) {
				sb.append(" AND  PSR_ORDER.VPO_LINE_STATUS  " + LIKE + "'%" + searchForm.getVpoLineStatus() + "%'");
			} else if (searchForm.getVpoLineStatusSelect().equals("Equal to")) {
				sb.append(" AND  PSR_ORDER.VPO_LINE_STATUS   " + EQUALS + "'" + searchForm.getVpoLineStatus() + "'");
			} else if (searchForm.getVpoLineStatusSelect().equals("Not equal to")) {
				sb.append(" AND  PSR_ORDER.VPO_LINE_STATUS  " + NOT_EQUALS + "'" + searchForm.getVpoLineStatus() + "'");
			} else if (searchForm.getVpoLineStatusSelect().equals("Not like")) {
				sb.append(
						" AND  PSR_ORDER.VPO_LINE_STATUS   " + NOT_LIKE + "'%" + searchForm.getVpoLineStatus() + "%'");
			} else if (searchForm.getVpoLineStatusSelect().equals("In the list")) {
				sb.append(" AND  PSR_ORDER.VPO_LINE_STATUS  " + IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getVpoLineStatus()) + ")");
			} else if (searchForm.getVpoLineStatusSelect().equals("Not in the list")) {
				sb.append(" AND  PSR_ORDER.VPO_LINE_STATUS  " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getVpoLineStatus()) + ")");
			} else if (searchForm.getVpoLineStatusSelect().equals("Starts with")) {
				sb.append(" AND  PSR_ORDER.VPO_LINE_STATUS  " + LIKE + "'" + searchForm.getVpoLineStatus() + "%'");
			} else if (searchForm.getVpoLineStatusSelect().equals("Ends with")) {
				sb.append(" AND  PSR_ORDER.VPO_LINE_STATUS  " + LIKE + "'%" + searchForm.getVpoLineStatus() + "'");
			}
		} else {
			if (searchForm.getVpoLineStatusSelect() != null) {
				if (searchForm.getVpoLineStatusSelect().equals("Equal to null")) {
					sb.append(" AND (PSR_ORDER.VPO_LINE_STATUS  " + IS_NULL + " OR PSR_ORDER.VPO_LINE_STATUS = " + "''"
							+ "  )");
				} else if (searchForm.getVpoLineStatusSelect().equals("Is not null")) {
					sb.append("  AND (PSR_ORDER.VPO_LINE_STATUS  " + IS_NOT_NULL + " AND PSR_ORDER.VPO_LINE_STATUS != "
							+ "''" + "  )");
				}
			}
		}
		return sb.toString();
	}

	private static String addOcCommitFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getOcCommit())) {
			if (searchForm.getOcCommitSelect().equals("Like")) {
				sb.append(" AND  PSR_ORDER.OC_COMMIT  " + LIKE + "'%" + searchForm.getOcCommit() + "%'");
			} else if (searchForm.getOcCommitSelect().equals("Equal to")) {
				sb.append(" AND  PSR_ORDER.OC_COMMIT   " + EQUALS + "'" + searchForm.getOcCommit() + "'");
			} else if (searchForm.getOcCommitSelect().equals("Not equal to")) {
				sb.append(" AND  PSR_ORDER.OC_COMMIT  " + NOT_EQUALS + "'" + searchForm.getOcCommit() + "'");
			} else if (searchForm.getOcCommitSelect().equals("Not like")) {
				sb.append(" AND  PSR_ORDER.OC_COMMIT   " + NOT_LIKE + "'%" + searchForm.getOcCommit() + "%'");
			} else if (searchForm.getOcCommitSelect().equals("In the list")) {
				sb.append(" AND  PSR_ORDER.OC_COMMIT  " + IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getOcCommit()) + ")");
			} else if (searchForm.getOcCommitSelect().equals("Not in the list")) {
				sb.append(" AND  PSR_ORDER.OC_COMMIT  " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getOcCommit()) + ")");
			} else if (searchForm.getOcCommitSelect().equals("Starts with")) {
				sb.append(" AND  PSR_ORDER.OC_COMMIT  " + LIKE + "'" + searchForm.getOcCommit() + "%'");
			} else if (searchForm.getOcCommitSelect().equals("Ends with")) {
				sb.append(" AND  PSR_ORDER.OC_COMMIT  " + LIKE + "'%" + searchForm.getOcCommit() + "'");
			}
		} else {
			if (searchForm.getOcCommitSelect() != null) {
				if (searchForm.getOcCommitSelect().equals("Equal to null")) {
					sb.append(" AND (PSR_ORDER.OC_COMMIT  " + IS_NULL + " OR PSR_ORDER.OC_COMMIT = " + "''" + "  )");
				} else if (searchForm.getOcCommitSelect().equals("Is not null")) {
					sb.append("  AND (PSR_ORDER.OC_COMMIT  " + IS_NOT_NULL + " AND PSR_ORDER.OC_COMMIT != " + "''"
							+ "  )");
				}
			}
		}
		return sb.toString();
	}

	private static String addCategoryFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getCategory())) {
			if (searchForm.getCategorySelect().equals("Like")) {
				sb.append(" AND  PSR_ORDER.CATEGORY  " + LIKE + "'%" + searchForm.getCategory() + "%'");
			} else if (searchForm.getCategorySelect().equals("Equal to")) {
				sb.append(" AND  PSR_ORDER.CATEGORY   " + EQUALS + "'" + searchForm.getCategory() + "'");
			} else if (searchForm.getCategorySelect().equals("Not equal to")) {
				sb.append(" AND  PSR_ORDER.CATEGORY  " + NOT_EQUALS + "'" + searchForm.getCategory() + "'");
			} else if (searchForm.getCategorySelect().equals("Not like")) {
				sb.append(" AND  PSR_ORDER.CATEGORY   " + NOT_LIKE + "'%" + searchForm.getCategory() + "%'");
			} else if (searchForm.getCategorySelect().equals("In the list")) {
				sb.append(" AND  PSR_ORDER.CATEGORY  " + IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getCategory()) + ")");
			} else if (searchForm.getCategorySelect().equals("Not in the list")) {
				sb.append(" AND  PSR_ORDER.CATEGORY  " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getCategory()) + ")");
			} else if (searchForm.getCategorySelect().equals("Starts with")) {
				sb.append(" AND  PSR_ORDER.CATEGORY  " + LIKE + "'" + searchForm.getCategory() + "%'");
			} else if (searchForm.getCategorySelect().equals("Ends with")) {
				sb.append(" AND  PSR_ORDER.CATEGORY  " + LIKE + "'%" + searchForm.getCategory() + "'");
			}
		} else {
			if (searchForm.getCategorySelect() != null) {
				if (searchForm.getCategorySelect().equals("Equal to null")) {
					sb.append(" AND (PSR_ORDER.CATEGORY  " + IS_NULL + " OR PSR_ORDER.CATEGORY = " + "''" + "  )");
				} else if (searchForm.getCategorySelect().equals("Is not null")) {
					sb.append(
							"  AND (PSR_ORDER.CATEGORY  " + IS_NOT_NULL + " AND PSR_ORDER.CATEGORY != " + "''" + "  )");
				}
			}
		}
		return sb.toString();
	}

	private static String addCpoColorDescFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getCpoColorDesc())) {
			if (searchForm.getCpoColorDescSelect().equals("Like")) {
				sb.append(" AND  PSR_ORDER.CPO_COLOR_DESC  " + LIKE + "'%" + searchForm.getCpoColorDesc() + "%'");
			} else if (searchForm.getCpoColorDescSelect().equals("Equal to")) {
				sb.append(" AND  PSR_ORDER.CPO_COLOR_DESC   " + EQUALS + "'" + searchForm.getCpoColorDesc() + "'");
			} else if (searchForm.getCpoColorDescSelect().equals("Not equal to")) {
				sb.append(" AND  PSR_ORDER.CPO_COLOR_DESC  " + NOT_EQUALS + "'" + searchForm.getCpoColorDesc() + "'");
			} else if (searchForm.getCpoColorDescSelect().equals("Not like")) {
				sb.append(" AND  PSR_ORDER.CPO_COLOR_DESC   " + NOT_LIKE + "'%" + searchForm.getCpoColorDesc() + "%'");
			} else if (searchForm.getCpoColorDescSelect().equals("In the list")) {
				sb.append(" AND  PSR_ORDER.CPO_COLOR_DESC  " + IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getCpoColorDesc()) + ")");
			} else if (searchForm.getCpoColorDescSelect().equals("Not in the list")) {
				sb.append(" AND  PSR_ORDER.CPO_COLOR_DESC  " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getCpoColorDesc()) + ")");
			} else if (searchForm.getCpoColorDescSelect().equals("Starts with")) {
				sb.append(" AND  PSR_ORDER.CPO_COLOR_DESC  " + LIKE + "'" + searchForm.getCpoColorDesc() + "%'");
			} else if (searchForm.getCpoColorDescSelect().equals("Ends with")) {
				sb.append(" AND  PSR_ORDER.CPO_COLOR_DESC  " + LIKE + "'%" + searchForm.getCpoColorDesc() + "'");
			}
		} else {
			if (searchForm.getCpoColorDescSelect() != null) {
				if (searchForm.getCpoColorDescSelect().equals("Equal to null")) {
					sb.append(" AND (PSR_ORDER.CPO_COLOR_DESC  " + IS_NULL + " OR PSR_ORDER.CPO_COLOR_DESC = " + "''"
							+ "  )");
				} else if (searchForm.getCpoColorDescSelect().equals("Is not null")) {
					sb.append("  AND (PSR_ORDER.CPO_COLOR_DESC  " + IS_NOT_NULL + " AND PSR_ORDER.CPO_COLOR_DESC != "
							+ "''" + "  )");
				}
			}
		}
		return sb.toString();
	}
	
	private static String addCpoDeptFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getCpoDept())) {
			if (searchForm.getCpoDeptSelect().equals("Like")) {
				sb.append(" AND  PSR_ORDER.CPO_DEPT  " + LIKE + "'%" + searchForm.getCpoDept() + "%'");
			} else if (searchForm.getCpoDeptSelect().equals("Equal to")) {
				sb.append(" AND  PSR_ORDER.CPO_DEPT   " + EQUALS + "'" + searchForm.getCpoDept() + "'");
			} else if (searchForm.getCpoDeptSelect().equals("Not equal to")) {
				sb.append(" AND  PSR_ORDER.CPO_DEPT  " + NOT_EQUALS + "'" + searchForm.getCpoDept() + "'");
			} else if (searchForm.getCpoDeptSelect().equals("Not like")) {
				sb.append(" AND  PSR_ORDER.CPO_DEPT   " + NOT_LIKE + "'%" + searchForm.getCpoDept() + "%'");
			} else if (searchForm.getCpoDeptSelect().equals("In the list")) {
				sb.append(" AND  PSR_ORDER.CPO_DEPT  " + IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getCpoDept()) + ")");
			} else if (searchForm.getCpoDeptSelect().equals("Not in the list")) {
				sb.append(" AND  PSR_ORDER.CPO_DEPT  " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getCpoDept()) + ")");
			} else if (searchForm.getCpoDeptSelect().equals("Starts with")) {
				sb.append(" AND  PSR_ORDER.CPO_DEPT  " + LIKE + "'" + searchForm.getCpoDept() + "%'");
			} else if (searchForm.getCpoDeptSelect().equals("Ends with")) {
				sb.append(" AND  PSR_ORDER.CPO_DEPT  " + LIKE + "'%" + searchForm.getCpoDept() + "'");
			}
		} else {
			if (searchForm.getCpoDeptSelect() != null) {
				if (searchForm.getCpoDeptSelect().equals("Equal to null")) {
					sb.append(" AND (PSR_ORDER.CPO_DEPT  " + IS_NULL + " OR PSR_ORDER.CPO_DEPT = " + "''"
							+ "  )");
				} else if (searchForm.getCpoDeptSelect().equals("Is not null")) {
					sb.append("  AND (PSR_ORDER.CPO_DEPT  " + IS_NOT_NULL + " AND PSR_ORDER.CPO_DEPT != "
							+ "''" + "  )");
				}
			}
		}
		return sb.toString();
	}
	
	private static String addInitFlowFilter(SearchForm searchForm) {
		StringBuilder sb = new StringBuilder();
		if (DAOHelper.checkNotEmpty(searchForm.getInitFlow())) {
			if (searchForm.getInitFlowSelect().equals("Like")) {
				sb.append(" AND  PSR_ORDER.INIT_FLOW  " + LIKE + "'%" + searchForm.getInitFlow() + "%'");
			} else if (searchForm.getInitFlowSelect().equals("Equal to")) {
				sb.append(" AND  PSR_ORDER.INIT_FLOW   " + EQUALS + "'" + searchForm.getInitFlow() + "'");
			} else if (searchForm.getInitFlowSelect().equals("Not equal to")) {
				sb.append(" AND  PSR_ORDER.INIT_FLOW  " + NOT_EQUALS + "'" + searchForm.getInitFlow() + "'");
			} else if (searchForm.getInitFlowSelect().equals("Not like")) {
				sb.append(" AND  PSR_ORDER.INIT_FLOW   " + NOT_LIKE + "'%" + searchForm.getInitFlow() + "%'");
			} else if (searchForm.getInitFlowSelect().equals("In the list")) {
				sb.append(" AND  PSR_ORDER.INIT_FLOW  " + IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getInitFlow()) + ")");
			} else if (searchForm.getInitFlowSelect().equals("Not in the list")) {
				sb.append(" AND  PSR_ORDER.INIT_FLOW  " + NOT_IN + "("
						+ PSRHelper.appendQuotesForGivenStr(searchForm.getInitFlow()) + ")");
			} else if (searchForm.getInitFlowSelect().equals("Starts with")) {
				sb.append(" AND  PSR_ORDER.INIT_FLOW  " + LIKE + "'" + searchForm.getInitFlow() + "%'");
			} else if (searchForm.getInitFlowSelect().equals("Ends with")) {
				sb.append(" AND  PSR_ORDER.INIT_FLOW  " + LIKE + "'%" + searchForm.getInitFlow() + "'");
			}
		} else {
			if (searchForm.getInitFlowSelect() != null) {
				if (searchForm.getInitFlowSelect().equals("Equal to null")) {
					sb.append(" AND (PSR_ORDER.INIT_FLOW  " + IS_NULL + " OR PSR_ORDER.INIT_FLOW = " + "''"
							+ "  )");
				} else if (searchForm.getInitFlowSelect().equals("Is not null")) {
					sb.append("  AND (PSR_ORDER.INIT_FLOW  " + IS_NOT_NULL + " AND PSR_ORDER.INIT_FLOW != "
							+ "''" + "  )");
				}
			}
		}
		return sb.toString();
	}
}
