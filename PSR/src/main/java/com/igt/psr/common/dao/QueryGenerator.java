package com.igt.psr.common.dao;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.igt.psr.common.constants.ApplicationConstants;
import com.igt.psr.common.util.DAOHelper;
import com.igt.psr.common.util.PSRHelper;
import com.igt.psr.db.model.PsrMatchCriteria;
import com.igt.psr.db.model.PsrModelMaster;
import com.igt.psr.portal.model.PtlRoleProfile;
import com.igt.psr.ui.view.PtlUserProfileForm;
import com.igt.psr.ui.view.SearchForm;

/**
 * @author Thippeswamy
 *
 */
public class QueryGenerator extends SQLOperators {

	private static final Logger log = LoggerFactory.getLogger(QueryGenerator.class);

	public static String getPsrOrdersCount(SearchForm searchForm) {
		log.info(" enetered into the  method getPsrOrdersCount ");
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT  count(distinct PSR_ORDER.PSR_ORDER_ID) from PSR_ORDER PSR_ORDER ,PSR_EVENTS PSR_EVENTS "
				+ " ");
		sb.append(SearchCriteriaGenerator.appendWhereClause(searchForm));
		sb.append(" AND PSR_EVENTS.PSR_ORDER_ID =  PSR_ORDER.PSR_ORDER_ID ");
		sb.append(SearchCriteriaGenerator.addCriteria(searchForm));
		return sb.toString();
	}

	public static String getPsrOrderIds(SearchForm searchForm) {
		log.info(" enetered into the  method getPsrOrdersCount ");
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT DISTINCT PSR_ORDER.PSR_ORDER_ID from PSR_ORDER PSR_ORDER,PSR_EVENTS PSR_EVENTS ");
		sb.append(SearchCriteriaGenerator.appendWhereClause(searchForm));
		sb.append(" AND PSR_ORDER.PSR_ORDER_ID = PSR_EVENTS.PSR_ORDER_ID ");
		sb.append(SearchCriteriaGenerator.addCriteria(searchForm));
		// added pagination logic here
		sb.append(" ORDER BY 1 DESC ");
		sb.append(" OFFSET " + searchForm.getPageNo() + "*" + searchForm.getPageSize() + " ROWS ");
		sb.append(" FETCH NEXT 100 ROWS ONLY ");
		return sb.toString();
	}

	// Actual Finalized Query to get PSR GRID DYNAMIC DATA
	public static String getPsrGridDataQuery(String roleName) {
		log.info(" enetered into the  method getPsrGridDataQuery ");
		StringBuilder sb = new StringBuilder();
		sb.append("  SELECT  PSR_ORDER.COLOR_WASH_NAME                                                       ");
		sb.append(" ,PSR_ORDER.SHIP_TO                                                                       ");
		sb.append(" ,PSR_ORDER.SO_ID                                                                         ");
		sb.append(" ,PSR_ORDER.VPO                                                                           ");
		sb.append(" ,PSR_ORDER.ACTUAL_POUNDAGE_AVAILABLE                                                     ");
		sb.append(" ,PSR_ORDER.APPR_FABRIC_YARN_LOT                                                          ");
		sb.append(" ,PSR_ORDER.ASN                                                                           ");
		sb.append(" ,PSR_ORDER.BALANCE_SHIP                                                                  ");
		sb.append(" ,PSR_ORDER.BRAND                                                                         ");
		sb.append(" ,PSR_ORDER.CARE_LABEL_CODE                                                               ");
		sb.append(" ,PSR_ORDER.CATEGORY                                                                      ");
		sb.append(" ,PSR_ORDER.COLOR_CODE_PATRN                                                              ");
		sb.append(" ,PSR_ORDER.COMMENT                                                                       ");
		sb.append(" ,PSR_ORDER.COUNTRY_ORIGIN                                                                ");
		sb.append(" ,PSR_ORDER.CPO                                                                           ");
		sb.append(" ,PSR_ORDER.CPO_COLOR_DESC                                                                ");
		sb.append(" ,PSR_ORDER.CPO_ORDER_QTY                                                                 ");
		sb.append(" ,PSR_ORDER.CPO_PACK_TYPE                                                                 ");
		sb.append(" ,PSR_ORDER.CPO_SHIP_MODE                                                                 ");
		sb.append(" ,PSR_ORDER.CREATE_DATE                                                                   ");
		sb.append(" ,PSR_ORDER.CUST_FTY_ID                                                                   ");
		sb.append(" ,PSR_ORDER.CUT_APPR_LETTER                                                               ");
		sb.append(" ,PSR_ORDER.DAILY_OUTPUT_PER_LINE                                                         ");
		sb.append(" ,PSR_ORDER.DEPT                                                                          ");
		sb.append(" ,PSR_ORDER.EXTRA_PROCESS                                                                 ");
		sb.append(" ,PSR_ORDER.FAB_TRIM_ORDER                                                                ");
		sb.append(" ,PSR_ORDER.FABRIC_CODE                                                                   ");
		sb.append(" ,PSR_ORDER.FABRIC_EST_DATE                                                               ");
		sb.append(" ,PSR_ORDER.FABRIC_MILL                                                                   ");
		sb.append(" ,PSR_ORDER.FABRIC_TEST_REPORT_NUMERIC                                                    ");
		sb.append(" ,PSR_ORDER.FACTORY_ID                                                                    ");
		sb.append(" ,PSR_ORDER.FACTORY_NAME                                                                  ");
		sb.append(" ,PSR_ORDER.FACTORY_REF                                                                   ");
		sb.append(" ,PSR_ORDER.FIBER_CONTENT                                                                 ");
		sb.append(" ,PSR_ORDER.FOB                                                                           ");
		sb.append(" ,PSR_ORDER.PL_ACTUAL_GAC                                                         		 ");
		sb.append(" ,PSR_ORDER.CPO_GAC_DATE                                                        		     ");
		sb.append(" ,PSR_ORDER.GARMENT_TEST_ACTUAL                                                           ");
		sb.append(" ,PSR_ORDER.GAUGE                                                                         ");
		sb.append(" ,PSR_ORDER.GENDER                                                                        ");
		sb.append(" ,PSR_ORDER.GRAPHIC_NAME                                                                  ");
		sb.append(" ," + ApplicationConstants.CASE_WHEN + "'" + roleName
				+ "' = 'PSR_VENDOR' THEN 0 ELSE GROSS_SELL_PRICE END GROSS_SELL_PRICE ");
		sb.append(" ,PSR_ORDER.IMAGE                                                                         ");
		sb.append(" ,PSR_ORDER.INTERFACE_TS                                                                  ");
		sb.append(" ,PSR_ORDER.KEY_ITEM                                                                      ");
		sb.append(" ,PSR_ORDER.KNITTING                                                                      ");
		sb.append(" ,PSR_ORDER.KNITTING_MACHINES                                                             ");
		sb.append(" ,PSR_ORDER.KNITTING_PCS                                                                  ");
		sb.append(" ,PSR_ORDER.LINKING                                                                       ");
		sb.append(" ,PSR_ORDER.LINKING_MACHINES                                                              ");
		sb.append(" ,PSR_ORDER.LINKING_PCS                                                                   ");
		sb.append(" ," + ApplicationConstants.CASE_WHEN + "'" + roleName
				+ "' = 'PSR_VENDOR' THEN 0 ELSE MARGIN END MARGIN                       ");
		sb.append(" ,PSR_ORDER.MERCHANT                                                                      ");
		sb.append(" ,PSR_ORDER.MODIFY_USER                                                                   ");
		sb.append(" ," + ApplicationConstants.CASE_WHEN + "'" + roleName
				+ "' = 'PSR_VENDOR' THEN '' ELSE NDC_DATE END NDC_DATE              ");
		sb.append(" ,PSR_ORDER.NDC_WK                                                                        ");
		sb.append(" ,PSR_ORDER.OC_COMMIT                                                                     ");
		sb.append(" ,PSR_ORDER.ORDER_QTY                                                                     ");
		sb.append(" ,PSR_ORDER.ORDER_TYPE                                                                    ");
		sb.append(" ,PSR_ORDER.PACK_TYPE                                                                     ");
		sb.append(" ,PSR_ORDER.PACKAGING_READY_DATE                                                         ");
		sb.append(" ,PSR_ORDER.PROD_OFFICE                                                                   ");
		sb.append(" ,PSR_ORDER.PRODUCT_TYPE                                                                  ");
		sb.append(" ,PSR_ORDER.PROFOMA_PO                                                                    ");
		sb.append(" ,PSR_ORDER.PROGRAM_NAME                                                                  ");
		sb.append(" ," + ApplicationConstants.CASE_WHEN + "'" + roleName
				+ "' = 'PSR_VENDOR' THEN 0 ELSE SALES_AMOUNT END SALES_AMOUNT           ");
		sb.append(" ,PSR_ORDER.SEASON                                                                        ");
		sb.append(" ,PSR_ORDER.SEWING_LINES                                                                  ");
		sb.append(" ,PSR_ORDER.SHIP_MODE                                                                     ");
		sb.append(" ,PSR_ORDER.SHIPPED_QTY                                                                   ");
		sb.append(" ,PSR_ORDER.STYLE                                                                         ");
		sb.append(" ,PSR_ORDER.STYLE_DESC                                                                    ");
		sb.append(" ,PSR_ORDER.TICKET_COLOR_CODE                                                             ");
		sb.append(" ,PSR_ORDER.TICKET_STYLE_NUMERIC                                                          ");
		sb.append(" ,PSR_ORDER.VENDOR_ID                                                                     ");
		sb.append(" ,PSR_ORDER.VENDOR_NAME                                                                   ");
		sb.append(" ,PSR_ORDER.VPO_FTY_GAC_DATE                                                              ");
		sb.append(" ,PSR_ORDER.VPO_FTY_GAC_DATE_TEMP                                                         ");
		sb.append(" ,PSR_ORDER.VPO_HEAD_STATUS                                                               ");
		sb.append(" ,PSR_ORDER.VPO_LINE_STATUS                                                               ");
		sb.append(" ," + ApplicationConstants.CASE_WHEN + "'" + roleName
				+ "' = 'PSR_VENDOR' THEN '' ELSE VPO_NLT_GAC_DATE END VPO_NLT_GAC_DATE  ");
		sb.append(" ,PSR_ORDER.VPO_STATUS                                                                    ");
		sb.append(" ,PSR_EVENTS.EVENT_DESC                                                                   ");
		sb.append(" ,PSR_EVENTS.ACTUAL_DATE                                                                  ");
		sb.append(" ,PSR_EVENTS.PLANNED_DATE                                                                 ");
		sb.append(
				" ,PSR_EVENTS.REVISED_DATE,PSR_EVENTS.EVENT_CODE,PSR_EVENTS.PSR_ORDER_ID,PSR_ORDER.PLAN_ID,PSR_ORDER.INIT_FLOW,PSR_EVENTS.HIGHLIGHT_REVISED_DATE,PSR_ORDER.VPO_ORDER_TYPE,PSR_ORDER.SPEED_INDICATOR,PSR_ORDER.GARMENT_TEST_REPORT_NO");
		sb.append(" ,PSR_ORDER.PL_SHIP_DATE                                                                  ");
		sb.append(" ,PSR_ORDER.CPO_STYLE_NO                                                                  ");
		sb.append(" ,PSR_ORDER.REQUEST_NO                                                                    ");
		sb.append(" ,PSR_ORDER.LADING_POINT                                                                  ");
		sb.append(" ,PSR_ORDER.TECH_DESIGNER                                                                 ");
		sb.append(" ,PSR_ORDER.PLM_NO                                                                        ");
		sb.append(" ,PSR_ORDER.WASH_YN                                                                       ");
		sb.append(" ,PSR_ORDER.WASH_FACILITY                                                                 ");
		sb.append(" ,PSR_ORDER.FABRIC_SHIP_MODE                                                              ");
		sb.append(" ,PSR_ORDER.TECH_PACK_RECEIVED_DATE                                                       ");
		sb.append(" ,PSR_ORDER.SAMPLE_MATERIAL_STATUS                                                        ");
		sb.append(" ,PSR_ORDER.RETAIL_PRICE                                                                  ");
		sb.append(" ,PSR_ORDER.FABRIC_PP_DATE                                                                ");
		sb.append(" ,PSR_ORDER.DELIVERY_MONTH                                                                ");
		sb.append(" ,PSR_ORDER.CPO_ACC_DATE_BY_VENDOR                                                        ");
		sb.append(" ,PSR_ORDER.DIST_CHANNEL                                                                  ");
		sb.append(" ,PSR_ORDER.FLOOR_SET                                                                     ");
		sb.append(" ,PSR_ORDER.STYLE_AT_RISK                                                                 ");
		sb.append(" ,PSR_ORDER.SAMPLE_MERCH_ETA                                                              ");
		sb.append(" ,PSR_ORDER.SAMPLE_FLOOR_SET_ETA                                                          ");
		sb.append(" ,PSR_ORDER.SAMPLE_DCOM_ETA                                                               ");
		sb.append(" ,PSR_ORDER.SAMPLE_MAILER                                                                 ");
		sb.append(" ,PSR_ORDER.SAMPLE_MAILER_ETA                                                             ");
		sb.append(" ,PSR_ORDER.TOP_SAMPLE_ETA_DATE                                                           ");
		sb.append(" ,PSR_ORDER.TOP_SAMPLE_COMMENT                                                            ");
		sb.append(" ,PSR_ORDER.PHOTO_MERCHANT_SAMPLE_SEND_DATE                                               ");
		sb.append(" ,PSR_ORDER.PHOTO_MERCHANT_SAMPLE_ETA_DATE                                                ");
		sb.append(" ,PSR_ORDER.PHOTO_MERCHANT_SAMPLE_COMMENT                                                 ");
		sb.append(" ,PSR_ORDER.MARKETING_SAMPLE_SEND_DATE                                                    ");
		sb.append(" ,PSR_ORDER.MARKETING_SAMPLE_ETA_DATE                                                     ");
		sb.append(" ,PSR_ORDER.MARKETING_SAMPLE_COMMENT                                                      ");
		sb.append(" ,PSR_ORDER.VISUAL_SAMPLE_SEND_DATE                                                       ");
		sb.append(" ,PSR_ORDER.VISUAL_SAMPLE_ETA_DATE                                                        ");
		sb.append(" ,PSR_ORDER.VISUAL_SAMPLE_COMMENT                                                         ");
		sb.append(" ,PSR_ORDER.COPYRIGHT_SAMPLE_SEND_DATE                                                    ");
		sb.append(" ,PSR_ORDER.COPYRIGHT_SAMPLE_ETA_DATE                                                     ");
		sb.append(" ,PSR_ORDER.COPYRIGHT_SAMPLE_COMMENT                                                      ");
		sb.append(" ,PSR_ORDER.REASON_FOR_LATE_GAC                                                      	 ");
		sb.append(" ,PSR_ORDER.CPO_DEPT			                                                             ");
		sb.append(" ,PSR_ORDER.ADDITIONAL_BULK_LOT_APPROVE			                                         ");
		sb.append(" ,PSR_ORDER.COLOR_COMMENT			                                                     ");
		sb.append(" ,PSR_ORDER.FABRIC_COMMENT			                                                     ");
		sb.append(" ,PSR_ORDER.FABRIC_TEST_COMMENT			                                                 ");
		sb.append(" ,PSR_ORDER.GARMENT_TEST_COMMENT			                                                 ");
		sb.append(" ,PSR_ORDER.TRIM_COMMENT			                                                         ");
		sb.append(" ,PSR_ORDER.SAMPLE_COMMENT			                                                     ");
		sb.append(" ,PSR_ORDER.FIT_SAMPLE_COMMENT			                                                 ");
		sb.append(" ,PSR_ORDER.PP_SAMPLE_COMMENT			                                                 ");
		sb.append(" ,PSR_ORDER.GARMENT_TREATEMENT_PCS			                                             ");
		sb.append(" ,PSR_ORDER.FINISHED_GARMENT_PCS			                                                 ");
		sb.append(" ,PSR_ORDER.PACKED_UNITS_PCS			                                                     ");
		sb.append(" ,PSR_EVENT_MODEL.DISABLE_EVENT			                                                 ");
		sb.append("  FROM PSR_ORDER PSR_ORDER ,PSR_EVENTS PSR_EVENTS,  PSR_EVENT_MODEL PSR_EVENT_MODEL       ");
		sb.append("  WHERE PSR_ORDER.PSR_ORDER_ID = PSR_EVENTS.PSR_ORDER_ID                      			 ");
		sb.append("  AND PSR_EVENT_MODEL.MODEL_ID = (SELECT MODEL_ID FROM PSR_MODEL_MASTER WHERE MODEL_NAME = CASE WHEN CHARINDEX(':',PSR_EVENTS.MODEL_NAME)>0 THEN (SELECT TOP 1 VALUE FROM SPLITSTRING (PSR_EVENTS.MODEL_NAME,':')) ELSE PSR_EVENTS.MODEL_NAME END) ");
		sb.append("  AND PSR_EVENTS.EVENT_CODE = PSR_EVENT_MODEL.EVENT_CODE                     			 ");
		return sb.toString();
	}

	public static String getPsrGridEvents(String brUserId, String loginUserId, String roleName) {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT DISTINCT PEM.EVENT_DESC,PSR_EVENTS.EVENT_CODE,PSR_EVENTS.EVENT_CATEGORY     ");
		sb.append("  FROM PSR_ORDER PSR_ORDER ,PSR_EVENTS PSR_EVENTS,PSR_EVENT_MASTER PEM                ");
		SearchForm searchForm = new SearchForm().setLoginUserId(loginUserId).setRoleName(roleName)
				.setBrUserId(brUserId);
		sb.append(SearchCriteriaGenerator.appendWhereClause(searchForm));
		sb.append("  AND PSR_ORDER.PSR_ORDER_ID = PSR_EVENTS.PSR_ORDER_ID    ");
		sb.append("  AND PEM.EVENT_CODE = PSR_EVENTS.EVENT_CODE    ");
		sb.append("  ORDER BY PSR_EVENTS.EVENT_CATEGORY ASC,PSR_EVENTS.EVENT_CODE ASC ");
		return sb.toString();
	}

	public static String getSearchQueryStr(SearchForm searchForm) {
		log.info(" enetered into the  method getSearchQueryStr and started appending the search fields");
		StringBuilder sb = new StringBuilder();
		sb.append(getPsrGridDataQuery(searchForm.getRoleName()));
		// add pagination logic below
		sb.append(" AND PSR_ORDER.PSR_ORDER_ID IN(" + getPsrOrderIds(searchForm)
				+ ") ORDER BY  PSR_ORDER.PSR_ORDER_ID DESC");
		return sb.toString();
	}

	public static String getModelNamesQuery(String model, String eventModelSearch) {
		StringBuilder sb = new StringBuilder();
		sb.append(
				" SELECT DISTINCT(pmc.MODEL_NAME), pem.DISABLE_MODEL FROM PSR_MODEL_MASTER pem ,PSR_MATCH_CRITERIA pmc where pmc.MODEL_ID = pem.MODEL_ID  ");
		if (DAOHelper.checkNotEmpty(model)) {
			if (eventModelSearch.equals("Like")) {
				sb.append(ApplicationConstants.AND_MODEL_NAME + LIKE + " '%" + model + "%'");
			} else if (eventModelSearch.equals(ApplicationConstants.EQUAL_TO)) {
				sb.append(ApplicationConstants.AND_MODEL_NAME + EQUALS + " '" + model + "'");
			} else if (eventModelSearch.equals(ApplicationConstants.NOT_EQUAL_TO)) {
				sb.append(ApplicationConstants.AND_MODEL_NAME + NOT_EQUALS + " '" + model + "'");
			} else if (eventModelSearch.equals(ApplicationConstants.NOT_LIKE)) {
				sb.append(ApplicationConstants.AND_MODEL_NAME + NOT_LIKE + " '%" + model + "%'");
			} else if (eventModelSearch.equals(ApplicationConstants.IN_THE_LIST)) {
				sb.append(ApplicationConstants.AND_MODEL_NAME + IN + " (" + PSRHelper.appendQuotesForGivenStr(model)
						+ ")");
			} else if (eventModelSearch.equals(ApplicationConstants.NOT_IN_THE_LIST)) {
				sb.append(ApplicationConstants.AND_MODEL_NAME + NOT_IN + " (" + PSRHelper.appendQuotesForGivenStr(model)
						+ ")");
			} else if (eventModelSearch.equals(ApplicationConstants.STARTS_WITH)) {
				sb.append(ApplicationConstants.AND_MODEL_NAME + LIKE + " '" + model + "%'");
			} else if (eventModelSearch.equals(ApplicationConstants.ENDS_WITH)) {
				sb.append(ApplicationConstants.AND_MODEL_NAME + LIKE + " '%" + model + "'");
			}
		} else {

			if (eventModelSearch != null) {

				if (eventModelSearch.equals(ApplicationConstants.EQUAL_TO_NULL)) {
					sb.append(ApplicationConstants.AND_MODEL_NAME + IS_NULL + "");
				} else if (eventModelSearch.equals(ApplicationConstants.IS_NOT_NULL)) {
					sb.append(ApplicationConstants.AND_MODEL_NAME + IS_NOT_NULL + "");
				}
			}
		}
		return sb.toString();
	}

	public static String getEventCodeQuery(String eventcode, String eventcodeselect) {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT EVENT_CODE,EVENT_DESC,EVENT_CATEGORY FROM  PSR_EVENT_MASTER ");
		if (DAOHelper.checkNotEmpty(eventcode)) {
			if (eventcodeselect.equals("Like")) {
				sb.append(ApplicationConstants.WHERE_EVENT_CODE + LIKE + " '%" + eventcode + "%'");
			} else if (eventcodeselect.equals(ApplicationConstants.EQUAL_TO)) {
				sb.append(ApplicationConstants.WHERE_EVENT_CODE + EQUALS + " '" + eventcode + "'");
			} else if (eventcodeselect.equals(ApplicationConstants.NOT_EQUAL_TO)) {
				sb.append(ApplicationConstants.WHERE_EVENT_CODE + NOT_EQUALS + " '" + eventcode + "'");
			} else if (eventcodeselect.equals(ApplicationConstants.NOT_LIKE)) {
				sb.append(ApplicationConstants.WHERE_EVENT_CODE + NOT_LIKE + " '%" + eventcode + "%'");
			} else if (eventcodeselect.equals(ApplicationConstants.IN_THE_LIST)) {
				sb.append(ApplicationConstants.WHERE_EVENT_CODE + IN + " ("
						+ PSRHelper.appendQuotesForGivenStr(eventcode) + ")");
			} else if (eventcodeselect.equals(ApplicationConstants.NOT_IN_THE_LIST)) {
				sb.append(ApplicationConstants.WHERE_EVENT_CODE + NOT_IN + " ("
						+ PSRHelper.appendQuotesForGivenStr(eventcode) + ")");
			} else if (eventcodeselect.equals(ApplicationConstants.STARTS_WITH)) {
				sb.append(ApplicationConstants.WHERE_EVENT_CODE + LIKE + " '" + eventcode + "%'");
			} else if (eventcodeselect.equals(ApplicationConstants.ENDS_WITH)) {
				sb.append(" where EVENT_CODE" + LIKE + " '%" + eventcode + "'");
			}
		} else {

			if (eventcodeselect != null) {

				if (eventcodeselect.equals(ApplicationConstants.EQUAL_TO_NULL)) {
					sb.append(ApplicationConstants.WHERE_EVENT_CODE + IS_NULL + "");
				} else if (eventcodeselect.equals(ApplicationConstants.IS_NOT_NULL)) {
					sb.append(ApplicationConstants.WHERE_EVENT_CODE + IS_NOT_NULL + "");
				}
			}
		}
		// Jagadish[12/02/2019] added below order by. We need to restructure to single
		// method(search and list).
		sb.append(" ORDER BY EVENT_CATEGORY ASC,EVENT_CODE ASC ");
		return sb.toString();
	}

	public static String getCustomizedView() {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT em.Query_Name from PSR_USER_CUSTIMOZED_VIEW em");
		return sb.toString();
	}

	public static String getModelNamesdetailsQuery(String modelnames) {
		StringBuilder sb = new StringBuilder();
		sb.append(
				"  SELECT PSR_EVENT_MODEL.EVENT_CODE,PEM.EVENT_DESC AS DESCRIPTION,PSR_EVENT_MODEL.PLAN_DATE,PSR_EVENT_MODEL.ACTUAL_DATE,");
		sb.append("  PSR_EVENT_MODEL.REVISED_DATE,PSR_EVENT_MODEL.TRIGGER_EVENT,PSR_EVENT_MODEL.TRIGGER_DAYS, PSR_EVENT_MODEL.DISABLE_EVENT  ");
		sb.append(
				"  FROM [dbo].[PSR_MODEL_MASTER] PSR_MODEL_MASTER , PSR_EVENT_MODEL PSR_EVENT_MODEL, PSR_EVENT_MASTER PEM WHERE ");
		sb.append("  PSR_EVENT_MODEL.MODEL_ID = PSR_MODEL_MASTER.MODEL_ID ");
		sb.append("  AND PEM.EVENT_CODE =  PSR_EVENT_MODEL.EVENT_CODE ");
		sb.append("  AND PSR_MODEL_MASTER.MODEL_NAME = ? ");
		return sb.toString();
	}

	public static String getEventModelNameMatchCriteria(int modelId) {
		StringBuilder sb = new StringBuilder();
		sb.append(
				"SELECT PSR_MATCH_CRITERIA.MATCH_CASE,PSR_MATCH_CRITERIA.FIELD_VALUE FROM PSR_MATCH_CRITERIA WHERE PSR_MATCH_CRITERIA.MODEL_ID = '"
						+ modelId + "' ");
		return sb.toString();
	}

	public static String getEventCodedetailsQuery(String eventcode) {
		StringBuilder sb = new StringBuilder();
		// Jagadish[12/02/2019] added below order by. We need to restructure to single
		// method(search and list).
		sb.append(

				" SELECT EVENT_CODE,EVENT_DESC,EVENT_CATEGORY FROM PSR_EVENT_MASTER WHERE PSR_EVENT_MASTER.EVENT_CODE= ? ORDER BY EVENT_CATEGORY ASC,EVENT_CODE ASC");
		return sb.toString();
	}

	/*
	 * Bamboorose code base queries are added start upto end (Bamboorose code base
	 * queries ends ) add only queries related to bamboorose
	 */
	public static String getPsrOrderDataFromBroseQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT DISTINCT VORDER_H.ORDER_NO ,REFERENCE3_CODES.[DESCRIPTION] AS PROD_OFFICE , ");
		sb.append(" VORDER_H.SUPPLIER ,VORDER_H.VENDOR_NAME,VORDER_H.MANUFACTURER,VORDER_H.MEMO11 AS FACTORY_NAME,");
		sb.append(" COUNTRY.[DESCRIPTION] AS ORIGIN_COUNTRY,VORDER_H.MEMO5 AS BRAND,VORDER_D.DEPT,");
		sb.append(
				" VORDER_D.ITEM_NO AS STYLE_NO,VORDER_D.DESCRIPTION AS STYLE_DESC,VORDER_D.MEMO8 AS COLOR_WASH_NAME ,VORDER_D.PRICE AS ");
		sb.append(
				" FOB,VORDER_D.RESELL_PRICE AS GROSS_SELL_PRICE,VORDER_D.PROFIT_PCT AS MARGIN_PCT,VORDER_D.MEMO6 AS PRODUCT_TYPE ");
		sb.append(
				" ,QUOTE_ANC.ALT_DESC2 AS PROGRAM_NAME,TRANS_MODE.DESCRIPTION AS TRANS_MODE,VORDER_D.DELIVER_TO,VORDER_D.MEMO5 AS GENDER,");
		sb.append(" VORDER_H.MEMO7 AS OC_NO,VORDER_D.SEASON,VORDER_D.DATE1 AS NLT_GAC_DATE,VORDER_D.DATE5 AS ");
		sb.append(
				" FTY_GAC_DATE,VORDER_H.STATUS AS VPO_HEAD_STATUS,VORDER_D.MODIFY_TS,VORDER_D.MODIFY_USER,GETDATE() ");
		sb.append(
				" AS INTERFACE_TS FROM VORDER_H WITH(NOLOCK) INNER JOIN VORDER_D WITH(NOLOCK) ON VORDER_H.PROFOMA_PO=VORDER_D.PROFOMA_PO ");
		sb.append(
				" INNER JOIN QUOTE_ANC WITH(NOLOCK) ON VORDER_D.REQUEST_NO = QUOTE_ANC.REQUEST_NO INNER JOIN USER_PROFILE WITH(NOLOCK) ON VORDER_H.BUYER ");
		sb.append(
				" = USER_PROFILE.USER_ID INNER JOIN USER_ATTRS WITH(NOLOCK) ON USER_PROFILE.USER_ID = USER_ATTRS.USER_ID INNER JOIN REFERENCE3_CODES ");
		sb.append(
				" WITH(NOLOCK) ON VORDER_H.SELLING_AGENT = dbo.REFERENCE3_CODES.CODE INNER JOIN TRANS_MODE WITH(NOLOCK) ON VORDER_D.TRANS_MODE = ");
		sb.append(
				" TRANS_MODE.CODE INNER JOIN COUNTRY WITH(NOLOCK) ON VORDER_H.ORIGIN_CNTRY = COUNTRY.CODE   where USER_PROFILE.USER_ID='AFERNANDO' ");
		sb.append(" and VORDER_H.SUPPLIER ='900355' AND VORDER_D.MEMO8='539 TRUFFLE'");

		log.info(String.format("  getPsrOrderDataFromBroseQuery ::::%s ", sb.toString()));
		return sb.toString();
	}

	/* Bamboorose code base queries ends */
	public static String getBambooroseDataqry() {

		StringBuilder sb = new StringBuilder();
		sb.append(
				"  SELECT VORDER_H.ORDER_NO AS VPO																       ");
		sb.append(
				" ,VORDER_D.MEMO8 AS COLOR_WASH_NAME                                                                   ");
		sb.append(
				" ,VORDER_D.DELIVER_TO AS SHIP_TO                                                                      ");
		sb.append(
				" ,SALES_ORDER_H.SO_ID AS SO_ID                                                                        ");
		sb.append(
				" ,REFERENCE3_CODES.[DESCRIPTION] AS PROD_OFFICE                                                       ");
		sb.append(
				" ,VORDER_H.SUPPLIER AS VENDOR_ID                                                                      ");
		sb.append(
				" ,VORDER_H.VENDOR_NAME AS VENDOR_NAME                                                                 ");
		sb.append(
				" ,VORDER_H.MANUFACTURER AS FACTORY_ID                                                                 ");
		sb.append(
				" ,VORDER_H.MEMO11 AS FACTORY_NAME                                                                     ");
		sb.append(
				" ,COUNTRY.[DESCRIPTION] AS COUNTRY_ORIGIN                                                             ");
		sb.append(
				" ,VORDER_H.MEMO5 AS BRAND                                                                             ");
		sb.append(
				" ,VORDER_D.DEPT AS DEPT                                                                               ");
		sb.append(
				" ,VORDER_H.MEMO9 AS CATEGORY                                                                          ");
		sb.append(
				" ,USER_PROFILE.USER_NAME AS MERCHANT                                                                  ");
		sb.append(
				" ,VORDER_D.ITEM_NO AS STYLE                                                                           ");
		sb.append(
				" ,VORDER_D.DESCRIPTION AS STYLE_DESC                                                                  ");
		sb.append(
				" ,VORDER_D.PRICE AS FOB                                                                               ");
		sb.append(
				" ,VORDER_D.RESELL_PRICE AS GROSS_SELL_PRICE                                                           ");
		sb.append(
				" ,VORDER_D.PROFIT_PCT AS MARGIN                                                                       ");
		sb.append(
				" ,VORDER_D.MEMO6 AS PRODUCT_TYPE                                                                      ");
		sb.append(
				" ,QUOTE_ANC.ALT_DESC2 AS PROGRAM_NAME                                                                 ");
		sb.append(
				" ,TRANS_MODE.DESCRIPTION AS SHIP_MODE                                                                 ");
		sb.append(
				" ,VORDER_D.MEMO5 AS GENDER                                                                            ");
		sb.append(
				" ,SUM(VORDER_D.QTY) AS ORDER_QTY                                                                      ");
		sb.append(
				" ,SUM(VORDER_D.NUMBR1) AS SHIPPED_QTY                                                                 ");
		sb.append(
				" ,VORDER_H.MEMO7 AS OC_COMMIT                                                                         ");
		sb.append(
				" ,SALES_ORDER_H.SALES_ORDER_NO AS CPO                                                                 ");
		sb.append(
				" ,VORDER_D.SEASON  AS SEASON                                                                          ");
		sb.append(
				" ,VORDER_D.FIRST_SHIP_DATE AS NDC_DATE                                                                ");
		sb.append(
				" ,VORDER_D.DATE1 AS VPO_NLT_GAC_DATE                                                                  ");
		sb.append(
				" ,VORDER_D.DATE5 AS VPO_FTY_GAC_DATE                                                                  ");
		sb.append(
				" ,SALES_ORDER_D.[DESCRIPTION] AS KEY_ITEM                                                             ");
		sb.append(
				" ,VORDER_H.STATUS AS VPO_STATUS                                                                       ");
		sb.append(
				" ,VORDER_D.STATUS AS VPO_LINE_STATUS                                                                  ");
		sb.append(
				" ,GETDATE() AS INTERFACE_TS                                                                           ");
		sb.append(
				" ,SUM(SALES_ORDER_D.ORDER_QTY) AS CPO_ORDER_QTY                                                       ");
		sb.append(
				" ,SALES_ORDER_D.MEMO5 AS CPO_SHIP_MODE                                                                ");
		sb.append(
				" ,SALES_ORDER_D.COLOR_DESC AS CPO_COLOR_DESC                                                          ");
		sb.append(
				" ,SALES_ORDER_D.PACK_TYPE AS CPO_PACK_TYPE                                                            ");
		sb.append(
				" ,VORDER_D.MODIFY_TS AS MODIFY_TS                                                                     ");
		sb.append(
				" ,VORDER_D.MODIFY_USER AS MODIFY_USER                                                                 ");
		sb.append(
				" ,VORDER_H.PROFOMA_PO AS PROFOMA_PO                                                                   ");
		sb.append(
				" FROM VORDER_H WITH(NOLOCK)                                                                           ");
		sb.append(
				" INNER JOIN VORDER_D WITH(NOLOCK) ON VORDER_H.PROFOMA_PO=                                             ");
		sb.append(
				" VORDER_D.PROFOMA_PO AND VORDER_D.MODIFY_TS between '08/01/2017 4:00:46' and '8/02/2017 4:00:46'      ");
		sb.append(
				" INNER JOIN QUOTE_ANC WITH(NOLOCK) ON VORDER_D.REQUEST_NO =                                           ");
		sb.append(
				" QUOTE_ANC.REQUEST_NO INNER JOIN REFERENCE3_CODES WITH(NOLOCK) ON                                     ");
		sb.append(
				" VORDER_H.SELLING_AGENT = dbo.REFERENCE3_CODES.CODE INNER JOIN TRANS_MODE                             ");
		sb.append(
				" WITH(NOLOCK) ON VORDER_D.TRANS_MODE = TRANS_MODE.CODE                                                ");
		sb.append(
				" INNER JOIN USER_PROFILE WITH(NOLOCK) ON VORDER_H.BUYER = USER_PROFILE.USER_ID                        ");
		sb.append(
				" INNER JOIN COUNTRY WITH(NOLOCK) ON VORDER_H.ORIGIN_CNTRY = COUNTRY.CODE                              ");
		sb.append(
				" LEFT JOIN ORDER_ASSIGNMENT WITH(NOLOCK) ON ORDER_ASSIGNMENT.PROFOMA_PO=                              ");
		sb.append(
				" VORDER_D.PROFOMA_PO AND VORDER_D.ROW_NO = ORDER_ASSIGNMENT.PO_ROW_NO                                 ");
		sb.append(
				" LEFT JOIN SALES_ORDER_D WITH(NOLOCK) ON SALES_ORDER_D.SO_ID=ORDER_ASSIGNMENT.SO_ID                   ");
		sb.append(
				" AND ORDER_ASSIGNMENT.SO_ROW_NO = SALES_ORDER_D.ROW_NO                                                ");
		sb.append(
				" LEFT JOIN SALES_ORDER_H WITH(NOLOCK) ON SALES_ORDER_H.SO_ID=SALES_ORDER_D.SO_ID                      ");
		sb.append(
				" GROUP BY VORDER_H.PROFOMA_PO                                                                         ");
		sb.append(
				" ,VORDER_H.ORDER_NO                                                                                   ");
		sb.append(
				" ,REFERENCE3_CODES.[DESCRIPTION]                                                                      ");
		sb.append(
				" ,VORDER_H.SUPPLIER                                                                                   ");
		sb.append(
				" ,VORDER_H.VENDOR_NAME                                                                                ");
		sb.append(
				" ,VORDER_H.MANUFACTURER                                                                               ");
		sb.append(
				" ,VORDER_H.MEMO11                                                                                     ");
		sb.append(
				" ,VORDER_H.ORIGIN_CNTRY                                                                               ");
		sb.append(
				" ,COUNTRY.[DESCRIPTION]                                                                               ");
		sb.append(
				" ,VORDER_H.MEMO5                                                                                      ");
		sb.append(
				" ,VORDER_D.DEPT                                                                                       ");
		sb.append(
				" ,VORDER_H.MEMO9                                                                                      ");
		sb.append(
				" ,USER_PROFILE.USER_NAME                                                                              ");
		sb.append(
				" ,VORDER_D.ITEM_NO                                                                                    ");
		sb.append(
				" ,VORDER_D.DESCRIPTION                                                                                ");
		sb.append(
				" ,VORDER_D.MEMO8                                                                                      ");
		sb.append(
				" ,VORDER_D.PRICE                                                                                      ");
		sb.append(
				" ,VORDER_D.RESELL_PRICE                                                                               ");
		sb.append(
				" ,VORDER_D.PROFIT_PCT                                                                                 ");
		sb.append(
				" ,VORDER_D.MEMO6                                                                                      ");
		sb.append(
				" ,QUOTE_ANC.ALT_DESC2                                                                                 ");
		sb.append(
				" ,TRANS_MODE.DESCRIPTION                                                                              ");
		sb.append(
				" ,VORDER_D.DELIVER_TO                                                                                 ");
		sb.append(
				" ,VORDER_D.MEMO5                                                                                      ");
		sb.append(
				" ,VORDER_H.MEMO7                                                                                      ");
		sb.append(
				" ,VORDER_D.SEASON                                                                                     ");
		sb.append(
				" ,VORDER_D.FIRST_SHIP_DATE                                                                            ");
		sb.append(
				" ,VORDER_D.DATE1                                                                                      ");
		sb.append(
				" ,VORDER_D.DATE5                                                                                      ");
		sb.append(
				" ,SALES_ORDER_D.[DESCRIPTION]                                                                         ");
		sb.append(
				" ,SALES_ORDER_H.SO_ID                                                                                 ");
		sb.append(
				" ,SALES_ORDER_H.SALES_ORDER_NO                                                                        ");
		sb.append(
				" ,SALES_ORDER_D.PACK_TYPE                                                                             ");
		sb.append(
				" ,SALES_ORDER_D.MEMO5                                                                                 ");
		sb.append(
				" ,SALES_ORDER_D.COLOR_DESC                                                                            ");
		sb.append(
				" ,VORDER_D.TRANS_MODE                                                                                 ");
		sb.append(
				" ,VORDER_H.STATUS                                                                                     ");
		sb.append(
				" ,VORDER_D.STATUS                                                                                     ");
		sb.append(
				" ,VORDER_D.MODIFY_TS                                                                                  ");
		sb.append(
				" ,VORDER_D.MODIFY_USER                                                                                ");

		return sb.toString();
	}

	public static String getAutoAttachQuery(String brand, String category, String prodOffice) {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT M.MODEL_NAME,                       ");
		sb.append(" PEM.EVENT_CODE,                            ");
		sb.append(" PEM.DESCRIPTION,                           ");
		sb.append(" PEM.PLAN_DATE,                             ");
		sb.append(" PEM.ACTUAL_DATE,                           ");
		sb.append(" PEM.REVISED_DATE,                          ");
		sb.append(" PEM.TRIGGER_EVENT,                         ");
		sb.append(" PEM.TRIGGER_DAYS                           ");
		sb.append(" FROM PSR_EVENT_MODEL PEM,                  ");
		sb.append(" PSR_MATCH_CRITERIA M         			   ");
		sb.append(" WHERE M.MODEL_ID=PEM.MODEL_ID              ");
		sb.append("   AND ((M.MATCH_CASE = 'BRAND' AND M.FIELD_VALUE = '" + brand + "') ");
		sb.append("   OR (M.MATCH_CASE = 'CATEGORY' AND M.FIELD_VALUE = '" + category + "') ");
		sb.append("   OR (M.MATCH_CASE = 'PROD_OFFICE' AND M.FIELD_VALUE = '" + prodOffice + "')) ");

		return sb.toString();
	}

	public static String getMatchCriteriaRowNo() {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT MAX(MC_ROW_NO)+1 MC_ROW_NO FROM PSR_MATCH_CRITERIA");
		return sb.toString();
	}

	public static String getAddEventModelsToCriteria(List<PsrMatchCriteria> pmcList, PsrModelMaster pmm) {
		StringBuilder sb = new StringBuilder();
		sb.append(" INSERT INTO [dbo].[PSR_MODEL_MASTER] ([MODEL_NAME] ,[CREATE_DATE]) VALUES ('" + pmm.getModelName()
				+ "',getDate()) ");
		for (PsrMatchCriteria pmc : pmcList) {
			sb.append(
					" INSERT INTO PSR_MATCH_CRITERIA (FIELD_VALUE, MATCH_CASE, MODEL_NAME, MODIFY_USER, MC_ROW_NO, MODEL_ID, MODIFY_TS, CREATE_DATE) ");
			sb.append(" VALUES('" + pmc.getFieldValue() + "', '" + pmc.getMatchCase() + "','" + pmm.getModelName()
					+ "', user,(SELECT MAX(MC_ROW_NO)+1 FROM PSR_MATCH_CRITERIA), ");
			sb.append(" (SELECT MODEL_ID FROM [DBO].[PSR_MODEL_MASTER] WHERE MODEL_NAME ='" + pmm.getModelName()
					+ "'),GETDATE(),GETDATE()) ");
		}
		return sb.toString();
	}

	public static String getUserValidationQry(String username, String password, boolean isLdapPassed) {
		StringBuilder sb = new StringBuilder();
		sb.append(
				" SELECT [USER_ID],[BR_USER_ID],[USER_NAME],[PSWD],[EMAIL],[TITLE],[ROLE_ID],[COUNTRY],[PSWD_EFFECTIVE_DATE],[PSWD_EFFECTIVE_DAYS],");
		sb.append(
				" [PSWD_NOTIFY_DAYS],[PSWD_EXPIRE_DATE],[PSWD_NEVER_EXPIRES],[DISABLE_USER],[PSWD_CHANGE_DATE],[PSWD_CHG_NXT_LOGIN],[PSWD_RESET_DIGEST],");
		sb.append(
				" [LOGON_ATTEMPTS],[LAST_LOGIN_TS],[MODIFY_TS],[MODIFY_USER],[CREATE_DATE],[QUERY_ID] FROM PTL_USER_PROFILE where USER_ID ='"
						+ username + "'");

		if (isLdapPassed) {
			sb.append(" and PSWD = '" + password + "'");
		}
		return sb.toString();
	}

	public static String getUsersQry() {
		StringBuilder sb = new StringBuilder();
		sb.append(
				" SELECT [USER_ID],[BR_USER_ID],[USER_NAME],[PSWD],[EMAIL],[TITLE],[ROLE_ID],[COUNTRY],[PSWD_EFFECTIVE_DATE],[PSWD_EFFECTIVE_DAYS],");
		sb.append(
				" [PSWD_NOTIFY_DAYS],[PSWD_EXPIRE_DATE],[PSWD_NEVER_EXPIRES],[DISABLE_USER],[PSWD_CHANGE_DATE],[PSWD_CHG_NXT_LOGIN],[PSWD_RESET_DIGEST],");
		sb.append(
				" [LOGON_ATTEMPTS],[LAST_LOGIN_TS],[MODIFY_TS],[MODIFY_USER],[CREATE_DATE],[QUERY_ID] FROM PTL_USER_PROFILE ");
		return sb.toString();
	}

	public static String getPsrGridEventsAll(String loginUserId, String roleName) {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT DISTINCT PSR_EVENTS.EVENT_DESC,PSR_EVENTS.EVENT_CODE,PSR_EVENTS.EVENT_CATEGORY     ");
		sb.append("  FROM PSR_ORDER PSR_ORDER ,PSR_EVENTS PSR_EVENTS                ");
		SearchForm searchForm = new SearchForm().setLoginUserId(loginUserId).setRoleName(roleName);
		sb.append(SearchCriteriaGenerator.appendWhereClause(searchForm));
		sb.append(
				"  AND PSR_ORDER.PSR_ORDER_ID = PSR_EVENTS.PSR_ORDER_ID  ORDER BY PSR_EVENTS.EVENT_CATEGORY ASC,PSR_EVENTS.EVENT_CODE ASC  ");
		sb.append("  AND PSR_ORDER.MERCHANT = '" + loginUserId + "'   ");
		return sb.toString();
	}

	public static String getSearchQueryStrAllData(SearchForm searchForm, String checkedOrderId) {
		log.info(" entered into the  method getSearchQueryStrAllData ");
		StringBuilder sb = new StringBuilder();
		sb.append(getPsrGridDataQuery(searchForm.getRoleName()));
		// add pagination logic below
		sb.append(" AND PSR_ORDER.PSR_ORDER_ID IN(" + getPsrOrderIdsAllData(searchForm)+ "");
		int length_str = checkedOrderId.length();
		if(length_str>0) {
			sb.append(" AND PSR_ORDER.PSR_ORDER_ID IN ("+checkedOrderId+") ");
		}
		sb.append(") ORDER BY PSR_ORDER.PSR_ORDER_ID Desc");
		return sb.toString();
	}

	public static String getPsrOrderIdsAllData(SearchForm searchForm) {
		log.info(" entered into the  method getPsrOrderIdsAllData ");
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT DISTINCT PSR_ORDER.PSR_ORDER_ID from PSR_ORDER PSR_ORDER,PSR_EVENTS PSR_EVENTS ");
		sb.append(SearchCriteriaGenerator.appendWhereClause(searchForm));
		sb.append(" AND PSR_ORDER.PSR_ORDER_ID = PSR_EVENTS.PSR_ORDER_ID ");
		sb.append(SearchCriteriaGenerator.addCriteria(searchForm));
		return sb.toString();
	}

	public static String getUserProfileQuery(String userId, String disable, String searchOperator, String roleInput,
			String roleOperator) {
		StringBuilder sb = new StringBuilder();
		sb.append(
				"SELECT USER_ID,ROLE_NAME FROM PTL_USER_PROFILE,PTL_ROLE_PROFILE where PTL_USER_PROFILE.ROLE_ID=  PTL_ROLE_PROFILE.ROLE_ID AND DISABLE_USER ='"
						+ disable + "' ");
		if (DAOHelper.checkNotEmpty(userId)) {
			if (searchOperator.equals("Like")) {
				sb.append(ApplicationConstants.AND_USER_ID + LIKE + " '%" + userId + "%'");
			} else if (searchOperator.equals(ApplicationConstants.EQUAL_TO)) {
				sb.append(ApplicationConstants.AND_USER_ID + EQUALS + " '" + userId + "'");
			} else if (searchOperator.equals(ApplicationConstants.NOT_EQUAL_TO)) {
				sb.append(ApplicationConstants.AND_USER_ID + NOT_EQUALS + " '" + userId + "'");
			} else if (searchOperator.equals(ApplicationConstants.NOT_LIKE)) {
				sb.append(ApplicationConstants.AND_USER_ID + NOT_LIKE + " '%" + userId + "%'");
			} else if (searchOperator.equals(ApplicationConstants.IN_THE_LIST)) {
				sb.append(
						ApplicationConstants.AND_USER_ID + IN + " (" + PSRHelper.appendQuotesForGivenStr(userId) + ")");
			} else if (searchOperator.equals(ApplicationConstants.NOT_IN_THE_LIST)) {
				sb.append(ApplicationConstants.AND_USER_ID + NOT_IN + " (" + PSRHelper.appendQuotesForGivenStr(userId)
						+ ")");
			} else if (searchOperator.equals(ApplicationConstants.STARTS_WITH)) {
				sb.append(ApplicationConstants.AND_USER_ID + LIKE + " '" + userId + "%'");
			} else if (searchOperator.equals(ApplicationConstants.ENDS_WITH)) {
				sb.append(ApplicationConstants.AND_USER_ID + LIKE + " '%" + userId + "'");
			}
		} else {

			if (searchOperator != null) {

				if (searchOperator.equals(ApplicationConstants.EQUAL_TO_NULL)) {
					sb.append(ApplicationConstants.AND_USER_ID + IS_NULL + "");
				} else if (searchOperator.equals(ApplicationConstants.IS_NOT_NULL)) {
					sb.append(ApplicationConstants.AND_USER_ID + IS_NOT_NULL + "");
				}
			}
		}
		if (DAOHelper.checkNotEmpty(roleInput)) {
			if (roleOperator.equals("Like")) {
				sb.append(ApplicationConstants.AND_ROLE_NAME + LIKE + " '%" + roleInput + "%'");
			} else if (roleOperator.equals(ApplicationConstants.EQUAL_TO)) {
				sb.append(ApplicationConstants.AND_ROLE_NAME + EQUALS + " '" + roleInput + "'");
			} else if (roleOperator.equals(ApplicationConstants.NOT_EQUAL_TO)) {
				sb.append(ApplicationConstants.AND_ROLE_NAME + NOT_EQUALS + " '" + roleInput + "'");
			} else if (roleOperator.equals(ApplicationConstants.NOT_LIKE)) {
				sb.append(ApplicationConstants.AND_ROLE_NAME + NOT_LIKE + " '%" + roleInput + "%'");
			} else if (roleOperator.equals(ApplicationConstants.IN_THE_LIST)) {
				sb.append(ApplicationConstants.AND_ROLE_NAME + IN + " (" + PSRHelper.appendQuotesForGivenStr(roleInput)
						+ ")");
			} else if (roleOperator.equals(ApplicationConstants.NOT_IN_THE_LIST)) {
				sb.append(ApplicationConstants.AND_ROLE_NAME + NOT_IN + " ("
						+ PSRHelper.appendQuotesForGivenStr(roleInput) + ")");
			} else if (roleOperator.equals(ApplicationConstants.STARTS_WITH)) {
				sb.append(ApplicationConstants.AND_ROLE_NAME + LIKE + " '" + roleInput + "%'");
			} else if (roleOperator.equals(ApplicationConstants.ENDS_WITH)) {
				sb.append(ApplicationConstants.AND_ROLE_NAME + LIKE + " '%" + roleInput + "'");
			}
		} else {

			if (roleOperator != null) {

				if (roleOperator.equals(ApplicationConstants.EQUAL_TO_NULL)) {
					sb.append(ApplicationConstants.AND_ROLE_NAME + IS_NULL + "");
				} else if (roleOperator.equals(ApplicationConstants.IS_NOT_NULL)) {
					sb.append(ApplicationConstants.AND_ROLE_NAME + IS_NOT_NULL + "");
				}
			}
		}
		return sb.toString();
	}

	public static String getUserProfiledetailsQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append(

				" SELECT USER_Id,BR_USER_ID,USER_NAME,EMAIL,ROLE_NAME,COUNTRY,PSWD_EFFECTIVE_DATE,PSWD_EFFECTIVE_DAYS,PSWD_NOTIFY_DAYS,PSWD_EXPIRE_DATE,PSWD_NEVER_EXPIRES,DISABLE_USER,PSWD_CHANGE_DATE,PSWD_CHG_NXT_LOGIN,PSWD_RESET_DIGEST,CONVERT(VARCHAR(10), PTL_USER_PROFILE.CREATE_DATE , 126),QUERY_ID FROM PTL_USER_PROFILE,PTL_ROLE_PROFILE WHERE PTL_ROLE_PROFILE.ROLE_ID=PTL_USER_PROFILE.ROLE_ID AND USER_ID= ? ");
		return sb.toString();
	}

	public static String getRoleProfileQuery(String roleName, String disable, String searchOperator) {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT ROLE_ID,ROLE_NAME,DESCRIPTION,APP_NAME, MANDATORY_BR_ID from PTL_ROLE_PROFILE WHERE DISABLE_ROLE ='" + disable
				+ "' ");
		if (DAOHelper.checkNotEmpty(roleName)) {
			if (searchOperator.equals("Like")) {
				sb.append(ApplicationConstants.AND_ROLE_NAME + LIKE + " '%" + roleName + "%' ");
			} else if (searchOperator.equals(ApplicationConstants.EQUAL_TO)) {
				sb.append(ApplicationConstants.AND_ROLE_NAME + EQUALS + " '" + roleName + "' ");
			} else if (searchOperator.equals(ApplicationConstants.NOT_EQUAL_TO)) {
				sb.append(ApplicationConstants.AND_ROLE_NAME + NOT_EQUALS + " '" + roleName + "' ");
			} else if (searchOperator.equals(ApplicationConstants.NOT_LIKE)) {
				sb.append(ApplicationConstants.AND_ROLE_NAME + NOT_LIKE + " '%" + roleName + "%' ");
			} else if (searchOperator.equals(ApplicationConstants.IN_THE_LIST)) {
				sb.append(ApplicationConstants.AND_ROLE_NAME + IN + " (" + PSRHelper.appendQuotesForGivenStr(roleName)
						+ ") ");
			} else if (searchOperator.equals(ApplicationConstants.NOT_IN_THE_LIST)) {
				sb.append(ApplicationConstants.AND_ROLE_NAME + NOT_IN + " ("
						+ PSRHelper.appendQuotesForGivenStr(roleName) + ") ");
			} else if (searchOperator.equals(ApplicationConstants.STARTS_WITH)) {
				sb.append(ApplicationConstants.AND_ROLE_NAME + LIKE + " '" + roleName + "%' ");
			} else if (searchOperator.equals(ApplicationConstants.ENDS_WITH)) {
				sb.append(ApplicationConstants.AND_ROLE_NAME + LIKE + " '%" + roleName + "' ");
			}
		} else {

			if (searchOperator != null) {

				if (searchOperator.equals(ApplicationConstants.EQUAL_TO_NULL)) {
					sb.append(ApplicationConstants.AND_ROLE_NAME + IS_NULL + " ");
				} else if (searchOperator.equals(ApplicationConstants.IS_NOT_NULL)) {
					sb.append(ApplicationConstants.AND_ROLE_NAME + IS_NOT_NULL + " ");
				}
			}
		}
		return sb.toString();
	}

	public static String getRoleQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append(" SELECT ROLE_ID,ROLE_NAME from PTL_ROLE_PROFILE WHERE DISABLE_ROLE ='N' ");
		return sb.toString();
	}

	/**
	 * Date:13/02/2019 Author: Randhir Update User Profile
	 * 
	 * @param userUpdate
	 * @param UserId
	 * @param loginUserId
	 * @return update SQL Query
	 */
	public static String updateUserQuery(PtlUserProfileForm userUpdate, String UserId, String loginUserId) {

		StringBuilder sb = new StringBuilder();
		if (userUpdate.getPswdNeverExpires() != null && !userUpdate.getPswdNeverExpires().isEmpty()
				&& userUpdate.getPswdNeverExpires().trim().equalsIgnoreCase("Y")) {
			userUpdate.setPswEffectiveDate(new Date());
		}

		sb.append(" UPDATE PTL_USER_PROFILE SET                            ");
		sb.append(" BR_USER_ID='" + userUpdate.getBrUserId() + "' ,                                             ");
		if (userUpdate.getPswd() != null && !userUpdate.getPswd().trim().isEmpty()) {
			sb.append(" PSWD ='" + userUpdate.getPswd() + "',");
		}
		sb.append(" USER_NAME='" + userUpdate.getUserName() + "' ,                                              ");
		sb.append(" EMAIL='" + userUpdate.getEmail() + "' ,                                                     ");
		sb.append(" ROLE_ID ='" + userUpdate.getRoleIds().get(ApplicationConstants.ZERO) + "',                  ");
		sb.append(" COUNTRY='" + userUpdate.getCountry() + "',                                                  ");
		if (PSRHelper.getDateStr(userUpdate.getPswEffectiveDate()) == null) {
			sb.append(" PSWD_EFFECTIVE_DATE =" + PSRHelper.getDateStr(userUpdate.getPswEffectiveDate()) + ",    ");
		} else {
			sb.append(" PSWD_EFFECTIVE_DATE ='" + PSRHelper.getDateStr(userUpdate.getPswEffectiveDate()) + "',  ");
		}
		sb.append(" PSWD_EFFECTIVE_DAYS=" + userUpdate.getPswdEffectiveDays() + ",                              ");
		sb.append(" PSWD_NOTIFY_DAYS=" + userUpdate.getPswdNotifyDays() + ",  ");

		if (userUpdate.getPswdNeverExpires() != null && !userUpdate.getPswdNeverExpires().isEmpty()
				&& userUpdate.getPswdNeverExpires().trim().equalsIgnoreCase("Y")) {
			sb.append(" PSWD_EXPIRE_DATE = '" + ApplicationConstants.DEFAULT_EXPIRY_DATE + "' ,             ");
		} else {
			if (PSRHelper.getDateStr(userUpdate.getPswdExpireDate()) == null) {
				sb.append(" PSWD_EXPIRE_DATE =" + PSRHelper.getDateStr(userUpdate.getPswdExpireDate())
						+ ",             ");
			} else {
				sb.append(" PSWD_EXPIRE_DATE ='" + PSRHelper.getDateStr(userUpdate.getPswdExpireDate()) + "',   ");
			}

		}
		sb.append("	PSWD_NEVER_EXPIRES ='" + userUpdate.getPswdNeverExpires() + "',                             ");
		sb.append(" DISABLE_USER='" + userUpdate.getDisableUser() + "',                                         ");
		if (userUpdate.getPswd() != null && !userUpdate.getPswd().trim().isEmpty()) {
			sb.append(" PSWD_CHANGE_DATE ='" + new Timestamp(new Date().getTime()) + "',                            ");
		}
		sb.append(ApplicationConstants.MODIFY_TS + new Timestamp(new Date().getTime())
				+ "' ,                                   ");
		sb.append(" QUERY_ID='" + userUpdate.getQueryId() + "' ,                                                ");
		sb.append(ApplicationConstants.MODIFY_USER + loginUserId + "' WHERE USER_ID ='" + UserId
				+ "'                          ");

		for (Integer roleId : userUpdate.getRoleIds()) {
			sb.append(" UPDATE PTL_USER_ROLE SET ROLE_ID ='" + roleId + "' WHERE USER_ID ='" + UserId + "'      ");
		}

		return sb.toString();
	}

	public static String getCntryQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append("  SELECT CODE,DESCRIPTION FROM PSR_CNTRY ");
		return sb.toString();
	}

	public static String updateRoleQuery(PtlRoleProfile roleUpdate, String roleId, String loginUserId) {

		StringBuilder sb = new StringBuilder();

		sb.append(" UPDATE PTL_ROLE_PROFILE SET ROLE_NAME = '" + roleUpdate.getRoleName()
				+ "',                         ");
		sb.append(" DESCRIPTION = '" + roleUpdate.getDescription()
				+ "',                                                ");
		sb.append(" APP_NAME = '" + roleUpdate.getAppName()
				+ "',                                                       ");
		sb.append(" DISABLE_ROLE = '" + roleUpdate.getDisableRole()
				+ "',                                               ");
		sb.append(" MANDATORY_BR_ID = '" + roleUpdate.getBrUserIdStatus()
				+ "',                                               ");
		sb.append(ApplicationConstants.MODIFY_TS + new Timestamp(new Date().getTime())
				+ "' ,                                             ");
		sb.append(ApplicationConstants.MODIFY_USER + loginUserId + "' WHERE ROLE_ID ='" + roleId
				+ "'                                  ");
		if (roleUpdate.getDisableRole().equals("Y")) {
			sb.append(
					" UPDATE PTL_USER_PROFILE SET DISABLE_USER = 'Y',                                                 ");
			sb.append(ApplicationConstants.MODIFY_TS + new Timestamp(new Date().getTime())
					+ "' ,                                         ");
			sb.append(ApplicationConstants.MODIFY_USER + loginUserId + "' WHERE ROLE_ID ='" + roleId
					+ "'                              ");
		}
		return sb.toString();
	}

	public static String getQueryNamesQuery() {
		StringBuilder sb = new StringBuilder();
		sb.append(
				" SELECT QUERY_ID,CASE WHEN PUBLIC_VIEW = 1 THEN QUERY_NAME+' (PUBLIC)' ELSE QUERY_NAME END AS QUERYNAME FROM  PSR_USER_CUSTIMOZED_VIEW  ORDER BY QUERY_NAME ASC ");
		return sb.toString();
	}

	public static String updateAcknowledge(String psrOrderIds) {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE PSR_EVENTS SET HIGHLIGHT_REVISED_DATE ='0' WHERE PSR_ORDER_ID IN (" + psrOrderIds
				+ ") AND EVENT_CODE != 'E000'");
		return sb.toString();
	}

	public static String getSessionDetailsQry(String authKey, Integer timeout) {
		StringBuilder sb = new StringBuilder();
		sb.append(
				" select LOGIN_USER_ID,USER_TOKEN_ID,LAST_INACTIVE_INTERVAL_TIME,CASE WHEN DATEDIFF(minute, last_inactive_interval_time,CURRENT_TIMESTAMP) > "
						+ timeout);
		sb.append(
				" THEN 0 ELSE 1 END as isInactive,DATEDIFF(minute, last_inactive_interval_time,CURRENT_TIMESTAMP) as inactiveTime from PORTAL_USER_SESSION where USER_TOKEN_ID in( '"
						+ authKey + "') ");
		return sb.toString();
	}

	public static String getFileTrackQuery(String loginUserId, String roleName, String type, int since, String fromDate, String toDate) {
		StringBuilder sb = new StringBuilder();
		sb.append("		SELECT *											");
		sb.append("		FROM                                                ");
		sb.append("		  (SELECT export.FILE_NAME,                         ");
		sb.append("				  export.[DESC],                            ");
		sb.append("				  export.export_USER_ID IMPORTUSER,         ");
		sb.append("				  export.export_START_DATE IMPORTSD,        ");
		sb.append("				  export.export_END_DATE IMPORTED,          ");
		sb.append("				  export.MODIFY_TS MODIFYTS                 ");
		sb.append("		   FROM PSR_FILE_EXPORT export                      ");
		if(type!=null && !type.equalsIgnoreCase("ALL") && !type.equalsIgnoreCase("CUSTOM")) {
			sb.append("		   WHERE export.MODIFY_TS>=DATEADD(day, "+since+", CAST(GETDATE() AS date))       ");
		}
		else if(type!=null && type.equalsIgnoreCase("CUSTOM")) {
			sb.append("		   WHERE export.MODIFY_TS>= CAST('"+fromDate+"' as datetime2) and export.MODIFY_TS<= CAST('"+toDate+"' as datetime2)       ");
		}
		sb.append("		   UNION ALL SELECT IMP.FILE_NAME,                  ");
		sb.append("							IMP.[DESC],                     ");
		sb.append("							IMP.IMPORT_USER_ID IMPORTUSER,  ");
		sb.append("							IMP.IMPORT_START_DATE IMPORTSD, ");
		sb.append("							IMP.IMPORT_END_DATE IMPORTED,   ");
		sb.append("							IMP.MODIFY_TS MODIFYTS          ");
		sb.append("		   FROM PSR_FILE_IMPORT IMP                         ");
		if(type!=null && !type.equalsIgnoreCase("ALL") && !type.equalsIgnoreCase("CUSTOM")) {
			sb.append("		   WHERE IMP.MODIFY_TS>=DATEADD(day, "+since+", CAST(GETDATE() AS date))       ");
		}
		else if(type!=null && type.equalsIgnoreCase("CUSTOM")) {
			sb.append("		   WHERE IMP.MODIFY_TS>= CAST('"+fromDate+"' as datetime2) and IMP.MODIFY_TS<= CAST('"+toDate+"' as datetime2)       ");
		}
		sb.append("				) TEMP " );
		if (roleName != null
				&& !(roleName.trim().equalsIgnoreCase("PSR_ADMIN") || roleName.trim().equalsIgnoreCase("PTL_ADMIN"))) {
			sb.append("		  where temp.file_name in ( ");
			sb.append("	select distinct													 ");
			sb.append("	export.file_name                                                 ");
			sb.append("	from                                                             ");
			sb.append("	psr_file_export export where export.export_USER_ID = '"+loginUserId+"'  ");
			sb.append("	union all                                                        ");
			sb.append("	select distinct                                                  ");
			sb.append("	export.file_name                                                 ");
			sb.append("	from                                                             ");
			sb.append("	psr_file_import export where export.import_USER_ID = '"+loginUserId+"'  ");
			sb.append("			) ");
		}
		sb.append("	ORDER BY MODIFYTS DESC          ");
		return sb.toString();
	}
}