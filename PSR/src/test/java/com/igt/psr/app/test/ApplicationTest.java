package com.igt.psr.app.test;

import java.io.IOException;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.StringTokenizer;

public class ApplicationTest {

	public static void main(String[] args) {
		Decoder decoder = Base64.getDecoder();
		byte[] decodedByte = decoder.decode("TElXQU5HOmlndEAxMjM0");
		String usernameAndPassword = new String(decodedByte);
		System.out.println(usernameAndPassword);
		// testGETWithBasicAuth();
	}

	public boolean authenticate(String credential) {
		if (null == credential) {
			return false;
		}
		// header value format will be "Basic encodedstring" for Basic
		// authentication. Example "Basic YWRtaW46YWRtaW4="
		final String encodedUserPassword = credential.replaceFirst("Basic" + " ", "");
		String usernameAndPassword = null;
		try {
			byte[] decodedBytes = Base64.getDecoder().decode("TElXQU5HOmlndEAxMjM0");
			usernameAndPassword = new String(decodedBytes, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
		final String username = tokenizer.nextToken();
		final String password = tokenizer.nextToken();

		// we have fixed the userid and password as admin
		// call some UserService/LDAP here
		boolean authenticationStatus = "admin".equals(username) && "admin".equals(password);
		return authenticationStatus;
	}

	private static void testGETWithBasicAuth() {
		try {
			String name = "admin";
			String password = "admin";
			String authString = name + ":" + password;
			String authStringEnc = Base64.getEncoder().encodeToString(authString.getBytes());
			System.out.println(
					"Base64 encoded auth string: " + Base64.getEncoder().encode(new String("Hello World!").getBytes()));
			String test = "SGVsbG8gV29ybGQh";
		} catch (Exception e) {

			e.printStackTrace();

		} finally {
			System.out.println("=========================================================================");
		}
	}

}
