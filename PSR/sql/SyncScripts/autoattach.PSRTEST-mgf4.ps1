$Logfile = "D:\localapps\logs\psr_synchorders.log"
$appfolder = "d:\localapps\psr-files\syncscripts"
$psrdb = 'PSR_TEST'
$ptldb = 'PTL_TEST'
$brdb = 'TSAM_TEST'
$dbserver = 'TSSSQLTEST,1501'
$psrpwd = 'p$rTest2018'
$psruser = 'PSRTEST'
$ptlpwd = '2018Ptlt&st'
$ptluser = 'PTLTEST'
$brpwd  = 'P$rTestConnect2BR'
$bruser = 'BRTST2PSR'


Import-Module -name sqlserver

Start-Transcript -Append $Logfile
$now=Get-Date -format "dd-MMM-yyyy HH:mm"
Write-Host $now "Sync Process Started"

$result = Invoke-Sqlcmd -server $dbserver -Database $psrdb -Username $psruser -Password $psrpwd -Query "select convert(varchar(50),ordersynchlastrun) lastrun from PSR_LASTRUN"
$lastrun = $result.lastrun
$now=Get-Date -format "dd-MMM-yyyy HH:mm"
Write-Host $now "Sync Process Last Run time was " $lastrun

$query = 
@"
if OBJECT_ID('PTL_DEV.dbo.tmpusers') is not null
	drop table tmpusers
"@

$query = $query -replace "PTL_DEV", $ptldb
Invoke-Sqlcmd -server $dbserver -Database $ptldb -Username $ptluser -Password $ptlpwd -Query $query

$query =
@"
CREATE TABLE [tmpusers](
	[BR_USER_ID] [varchar](18) NOT NULL,
	[BR_USER_NAME] [varchar](50) NULL,
	[BR_ROLE_ID] [int] NULL,
	[BR_ROLE_NAME] [varchar](50) NOT NULL,
	[BR_ATTR_NAME] [varchar](35) NULL,
	[BR_ATTR_VALUE] [varchar](120) NULL
) 
"@

Invoke-Sqlcmd -server $dbserver -Database $ptldb -Username $ptluser -Password $ptlpwd -Query $query
Invoke-Sqlcmd -server $dbserver -Database $psrdb -Username $psruser -Password $psrpwd -Query "drop table tmpusers"
Invoke-Sqlcmd -server $dbserver -Database $psrdb -Username $psruser -Password $psrpwd -Query $query

$query = Get-Content "$appfolder\PSR_USER_EXTRACT_QUERY.SQL" |OUT-STRING
 	
$query = $query -replace "##lastrun##", $lastrun
$query = $query -replace "TSSDEV02SQL01", $brdb

#$query
$dt = Invoke-Sqlcmd -server $dbserver  -Database $brdb -Username $bruser -Password $brpwd -Query $query -OutputAs DataTables -querytimeout 0
$extracted = $dt.Rows.Count
$now=Get-Date -format "dd-MMM-yyyy HH:mm"
Write-Host $now "Sync User Extraction completed.   $extracted records"

$counter = 0
foreach ($Row in $dt)
{
$qstr = "insert into tmpusers values ("
$str = $Row.BR_USER_ID
$qstr = $qstr + "'" + $str + "'"
$str = $Row.BR_USER_NAME	
$qstr = $qstr + ",'" + $str + "'"
$str = $Row.BR_ROLE_ID	
$qstr = $qstr + "," + $str 
$str = $Row.BR_ROLE_NAME	
$qstr = $qstr + ",'" + $str + "'"
$str = $Row.BR_ATTR_NAME	
$qstr = $qstr + ",'" + $str + "'"
$str = $Row.BR_ATTR_VALUE	
$qstr = $qstr + ",'" + $str + "'"
$qstr = $qstr + ")"
Invoke-Sqlcmd -server $dbserver -Database $ptldb -Username $ptluser -Password $ptlpwd -Query $qstr
Invoke-Sqlcmd -server $dbserver -Database $psrdb -Username $psruser -Password $psrpwd -Query $qstr
$counter++
}


#Invoke-Sqlcmd -server $dbserver -Database $ptldb -Username $ptluser -Password $ptlpwd -Query "exec PSR_SYNCUSERS"
Invoke-Sqlcmd -server $dbserver -Database $psrdb -Username $psruser -Password $psrpwd -Query "exec PSR_SYNCUSERS"

$now=Get-Date -format "dd-MMM-yyyy HH:mm"
Write-Host $now "Sync Process Users Synched"

$query = Get-Content "$appfolder\\psr_extract_query.sql" |out-string
$query = $query -replace "##lastrun##", $lastrun   
$query = $query -replace "TSSDEV02SQL01", $brdb                                    
#$query

$dt = Invoke-Sqlcmd -server $dbserver  -Database $brdb -Username $bruser -Password $brpwd -Query $query -OutputAs DataTables -querytimeout 0
$extracted = $dt.Rows.Count
$now=Get-Date -format "dd-MMM-yyyy HH:mm"
Write-Host $now "Sync Process Extraction completed.   $extracted records"

# Drop and Create tmppsr

$query = 
@"
if OBJECT_ID('PSR_DEV2.dbo.tmppsr') is not null
	drop table tmppsr
"@

$query = $query -replace "PSR_DEV2", $psrdb

Invoke-Sqlcmd -server $dbserver -Database $psrdb -Username $psruser -Password $psrpwd -Query $query 
Write-Host $now "tmppsr dropped"
$query = Get-Content "$appfolder\\PSR_CREATE_EXTRACT_TABLE_v5.SQL" |OUT-STRING


Invoke-Sqlcmd -server $dbserver -Database $psrdb -Username $psruser -Password $psrpwd -Query $query 
	
Write-Host $now "tmppsr created"	
# Insert extracted data to tmppsr

$counter = 0
foreach ($Row in $dt)
{
$qstr = "insert into tmppsr values ("
$str = $Row.PROFOMA_PO
$qstr = $qstr + "'" + $str + "'"
$str = $Row.ORDER_NO	
$qstr = $qstr + ",'" + $str + "'"
	$str = $Row.PROD_OFFICE
$qstr = $qstr +  ",'" + $str + "'"           
	$str = $Row.SUPPLIER
$qstr = $qstr +  ",'" + $str + "'"  
	$str = $Row.VENDOR_NAME
$qstr = $qstr +  ",'" + $str + "'"           
	$str = $Row.MANUFACTURER
$qstr = $qstr +  ",'" + $str + "'"          
	$str = $Row.FACTORY_NAME
$qstr = $qstr +  ",'" + $str + "'"          
	$str = $Row.ORIGIN_CNTRY
$qstr = $qstr +  ",'" + $str + "'"          
	$str = $Row.ORIGIN_COUNTRY
$qstr = $qstr +  ",'" + $str + "'"        
	$str = $Row.BRAND
$qstr = $qstr +  ",'" + $str + "'"                 
	$str = $Row.DEPT
$qstr = $qstr +  ",'" + $str + "'"                  
	$str = $Row.CATEGORY
$qstr = $qstr +  ",'" + $str + "'"              
	$str = $Row.BUYER
$qstr = $qstr +  ",'" + $str + "'"  
	$str = $Row.STYLE_NO
$qstr = $qstr +  ",'" + $str + "'"              
	$str = $Row.STYLE_DESC -replace "'", "''"
$qstr = $qstr +  ",'" + $str + "'"            
	$str = $Row.COLOR_CODE
$qstr = $qstr +  ",'" + $str + "'"            
	$str = $Row.COLOR_WASH_NAME	
$qstr = $qstr +  ",'" + $str + "'"       
	$str = $Row.PLAN_ID
$qstr = $qstr +  ",'" + $str + "'"               
	$str =    $Row.FOB
$qstr = $qstr +  "," + $str	
	$str =    $Row.GROSS_SELL_PRICE
$qstr = $qstr +  "," + $str	
	$str =    $Row.MARGIN_PCT
$qstr = $qstr +  "," + $str	
	$str = $Row.PRODUCT_TYPE
$qstr = $qstr +  ",'" + $str + "'"          
	$str = $Row.PROGRAM_NAME
$qstr = $qstr +  ",'" + $str + "'"          
	$str = $Row.TRANS_MODE
$qstr = $qstr +  ",'" + $str + "'"            
	$str = $Row.DELIVER_TO
$qstr = $qstr +  ",'" + $str + "'"            
	$str = $Row.GENDER
$qstr = $qstr +  ",'" + $str + "'"                
	$str =  "0" + $Row.FINAL_QTY 
$qstr = $qstr +  "," + $str  	
	$str =  "0" + $Row.SHIPPED_QTY     
$qstr = $qstr +  "," + $str 	
	$str =  "0" + $Row.CPO_ORDER_QTY  
$qstr = $qstr +  "," + $str 	
	$str = $Row.OC_NO
$qstr = $qstr +  ",'" + $str + "'"                 
	$str = $Row.SEASON
$qstr = $qstr +  ",'" + $str + "'"                
	$str = $Row.CUST_DEL_DATE
$qstr = $qstr +  ",'" + $str + "'"         
	$str = $Row.NLT_GAC_DATE
$qstr = $qstr +  ",'" + $str + "'"          
	$str = $Row.FTY_GAC_DATE
$qstr = $qstr +  ",'" + $str + "'"          
	$str = $Row.CPO_STYLE_DESC  -replace "'", "''"
$qstr = $qstr +  ",'" + $str + "'"        
	$str =  "0" + $Row.SO_ID
$qstr = $qstr +  "," + $str 	
	$str = $Row.SALES_ORDER_NO
$qstr = $qstr +  ",'" + $str + "'"        
	$str = $Row.CPO_PACK_TYPE
$qstr = $qstr +  ",'" + $str + "'"         
	$str = $Row.CPO_SHIP_MODE
$qstr = $qstr +  ",'" + $str + "'"         
	$str = $Row.CPO_COLOR_DESC
$qstr = $qstr +  ",'" + $str + "'"        
	$str = $Row.VPO_HEAD_STATUS
$qstr = $qstr +  ",'" + $str + "'"       
	$str = $Row.VPO_LINE_STATUS
$qstr = $qstr +  ",'" + $str + "'"       
	$str = $Row.ASN_ID
$qstr = $qstr +  ",'" + $str + "'"                
	$str = $Row.MODIFY_TS
$qstr = $qstr +  ",'" + $str + "'"             
	$str = $Row.MODIFY_USER
$qstr = $qstr +  ",'" + $str + "'"           
	$str = $Row.INTERFACE_TS
$qstr = $qstr +  ",'" + $str + "'"    
	$str = $Row.INIT_FLOW
$qstr = $qstr +  ",'" + $str + "'" 
	$str = $Row.CPO_GAC_DATE
$qstr = $qstr +  ",'" + $str + "'"          
	$str = $Row.PL_ACTUAL_GAC     
$qstr = $qstr +  ",'" + $str + "'"          
	$str = $Row.VPO_ORDER_TYPE  
$qstr = $qstr +  ",'" + $str + "'"          
	$str = $Row.VPO_SPEED_IND  
$qstr = $qstr +  ",'" + $str + "'"          
	$str = $Row.PL_SHIP_DATE  
$qstr = $qstr +  ",'" + $str + "'"          
	$str = $Row.CPO_STYLE_NO  
$qstr = $qstr +  ",'" + $str + "'"          
	$str = $Row.REQUEST_NO  
$qstr = $qstr +  ",'" + $str + "'"          
	$str = $Row.LADING_POINT  
$qstr = $qstr +  ",'" + $str + "'"          
	$str = $Row.TECH_DESIGNER  
$qstr = $qstr +  ",'" + $str + "'"         
$qstr = $qstr + ")"

#$qstr

Invoke-Sqlcmd -server $dbserver -Database $psrdb -Username $psruser -Password $psrpwd -Query $qstr
$counter++
# If($counter -gt 200) {break}
}

Write-Host $now "$extracted records extracted"
Write-Host $now "tmppsr loaded.  records  $counter"

Invoke-Sqlcmd -server $dbserver -Database $psrdb -Username $psruser -Password $psrpwd -Query "exec PSR_SYNCORDERS"  -querytimeout 0

$now=Get-Date -format "dd-MMM-yyyy HH:mm"
Write-Host $now "Sync Process Orders Synched"

Invoke-Sqlcmd -server $dbserver -Database $psrdb -Username $psruser -Password $psrpwd  -Query "exec PSR_ATTACHEVENTS"  -querytimeout 0

$now=Get-Date -format "dd-MMM-yyyy HH:mm"
Write-Host $now "Sync Process Events Attached.  Sync Process completed"

Stop-Transcript