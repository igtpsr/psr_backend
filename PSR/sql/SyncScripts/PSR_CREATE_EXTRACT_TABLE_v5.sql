CREATE TABLE [dbo].[tmppsr](
	[PROFOMA_PO] [varchar](35) NOT NULL,
	[ORDER_NO] [varchar](35) NOT NULL,
	[PROD_OFFICE] [varchar](80) NULL,
	[SUPPLIER] [varchar](17) NULL,
	[VENDOR_NAME] [varchar](80) NULL,
	[MANUFACTURER] [varchar](17) NULL,
	[FACTORY_NAME] [varchar](35) NULL,
	[ORIGIN_CNTRY] [varchar](3) NULL,
	[ORIGIN_COUNTRY] [varchar](50) NULL,
	[BRAND] [varchar](35) NULL,
	[DEPT] [varchar](17) NULL,
	[CATEGORY] [varchar](35) NULL,
	[BUYER] [varchar](100) NULL,
	[STYLE_NO] [varchar](35) NOT NULL,
	[STYLE_DESC] [varchar](80) NULL,
	[COLOR_CODE] [varchar] (20) NULL,
	[COLOR_WASH_NAME] [varchar](35) NULL,
	[PLAN_ID] [varchar](35) NULL,
	[FOB] [decimal](30, 6) NULL,
	[GROSS_SELL_PRICE] [decimal](30, 6) NULL,
	[MARGIN_PCT] [decimal](30, 6) NULL,
	[PRODUCT_TYPE] [varchar](35) NULL,
	[PROGRAM_NAME] [varchar](80) NULL,
	[TRANS_MODE] [varchar](80) NULL,
	[DELIVER_TO] [varchar](17) NULL,
	[GENDER] [varchar](35) NULL,
	[FINAL_QTY] [decimal](38, 6) NULL,
	[SHIPPED_QTY] [decimal](38, 6) NULL,
	[CPO_ORDER_QTY] [decimal](38, 6) NULL,
	[OC_NO] [varchar](35) NULL,
	[SEASON] [varchar](35) NULL,
	[CUST_DEL_DATE] [datetime] NULL,
	[NLT_GAC_DATE] [datetime] NULL,
	[FTY_GAC_DATE] [datetime] NULL,
	[CPO_STYLE_DESC] [varchar](80) NULL,
	[SO_ID] [decimal](30, 0) NULL,
	[SALES_ORDER_NO] [varchar](35) NULL,
	[CPO_PACK_TYPE] [varchar](6) NOT NULL, -- Change from 1 to 6 by Ron
	[CPO_SHIP_MODE] [varchar](35) NOT NULL, -- Change from 1 to 35 by Ron
	[CPO_COLOR_DESC] [varchar](50) NULL,
	[VPO_HEAD_STATUS] [varchar](8) NULL,
	[VPO_LINE_STATUS] [varchar](8) NULL,
	[ASN_ID] [varchar](78) NULL,
	[MODIFY_TS] [datetime] NULL,
	[MODIFY_USER] [varchar](18) NULL,
	[INTERFACE_TS] [datetime] NOT NULL,
	[INIT_FLOW] [varchar ] (8) NULL,
	[CPO_GAC_DATE] [datetime] NULL,
	[PL_ACTUAL_GAC] [datetime] NULL,
	[VPO_ORDER_TYPE] [varchar](35) NULL,		-- added by Ron on 2/11/2019
	[VPO_SPEED_IND] [varchar](35) NULL,         -- added by Ron on 2/11/2019
    [PL_SHIP_DATE] [datetime] NULL,				-- added by Ron on 2/11/2019
	[CPO_STYLE_NO] [varchar](35) NULL,               -- added by Ron on 2/11/2019
	[REQUEST_NO] [varchar](35) NULL,            -- added by Ron on 2/11/2019
    [LADING_POINT] [varchar](25) NULL,          -- added by Ron on 2/11/2019
	[TECH_DESIGNER] [varchar](20) NULL 	        -- added by Ron on 2/11/2019
	)                                           