/****** Object:  UserDefinedFunction [dbo].[CompareXml]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[CompareXml]
(
    @xml1 XML,
    @xml2 XML
)
RETURNS INT
AS 
BEGIN
    DECLARE @ret INT
    SELECT @ret = 0


    -- -------------------------------------------------------------
    -- If one of the arguments is NULL then we assume that they are
    -- not equal. 
    -- -------------------------------------------------------------
    IF @xml1 IS NULL OR @xml2 IS NULL 
    BEGIN
        RETURN 1
    END

     ---------------------------------------------------------------
     --Match the value of the elements
     ---------------------------------------------------------------
    IF((@xml1.query('count(/*)').value('.','INT') = 1) AND (@xml2.query('count(/*)').value('.','INT') = 1))
    BEGIN
    DECLARE @elValue1 VARCHAR(MAX), @elValue2 VARCHAR(MAX)

    SELECT
        @elValue1 = @xml1.value('((/*)[1])','VARCHAR(MAX)'),
        @elValue2 = @xml2.value('((/*)[1])','VARCHAR(MAX)')

    IF  @elValue1 <> @elValue2
    BEGIN
        RETURN 1
    END
	END
    
    RETURN @ret
END

GO
/****** Object:  UserDefinedFunction [dbo].[GetMatchingCriteria]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[GetMatchingCriteria](@vpo varchar(35) ,@color_wash_name varchar(35),@ship_to varchar(17), @so_id numeric(30)) RETURNS  int
AS  
BEGIN 
	declare @model_id int = 0
	declare @cnt int = 0
	declare @condition varchar(2000)
	declare @sql varchar(2000)
	DECLARE cur CURSOR 
	FOR
		select model_id, count(*) cnt from PSR_MATCH_CRITERIA group by model_id order by count(*) desc
	OPEN cur
 
	FETCH NEXT FROM cur INTO @model_id, @cnt
 	WHILE @@FETCH_STATUS = 0
    BEGIN
			select @condition =
			SUBSTRING(
			(
				SELECT ' p.' + mc.MATCH_CASE  + '= ''' + mc.FIELD_VALUE + ''' AND '   AS 'data()'
					from psr_model_master m, PSR_MATCH_CRITERIA mc
					where m.MODEL_ID = mc.MODEL_ID AND M.MODEL_ID = 3
					FOR XML PATH('')
			), 2 , 9999) + ' 1=1'

			declare @variable   int
			set @sql  = 'SELECT @variable = 1 from psr_order where vpo = @vpo and color_wash_name = @color_wash_name and '
			+ 	'ship_to = @ship_to and so_id = @so_id '
			+   @condition
			EXEC sp_executesql @sql, 
				N'@variable int OUTPUT',
				@variable OUTPUT
			if @variable > 0
				break
			FETCH NEXT FROM cur INTO @model_id, @cnt
    END
 
	CLOSE cur
	DEALLOCATE cur
	return @model_id
END




GO
/****** Object:  UserDefinedFunction [dbo].[PSR_SAVEORDERDIFF]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE    function [dbo].[PSR_SAVEORDERDIFF]
(
    @Data1 xml,
    @Data2 xml,
	@ModifiedBy varchar(50)
)
returns XML
as
begin
			declare @bo1 as table ([NAME] VARCHAR(50), [VALUE] VARCHAR(50))
			declare @Ao1 as table ([NAME] VARCHAR(50), [VALUE] VARCHAR(50))
			DECLARE @BE1 AS TABLE (PSR_ORDER_ID VARCHAR(20), EVENT_CODE VARCHAR(20), PLANNED_DATE DATE, REVISED_DATE DATE, ACTUAL_DATE DATE)
			DECLARE @AE1 AS TABLE (PSR_ORDER_ID VARCHAR(20), EVENT_CODE VARCHAR(20), PLANNED_DATE DATE, REVISED_DATE DATE, ACTUAL_DATE DATE)
			DECLARE @logxml as xml
			--sELECT @Data1 = [before], @Data2 = [after] from V_ChangeLog where log_id =  13
			INSERT INTO @BO1
			select
				XC.value('local-name(.)', 'varchar(50)') AS NAME,
				XC.value('(.)[1]', 'varchar(50)') AS VALUE
			FROM
				@dATA1.nodes('/BEFORE/*') AS XT(XC)
				WHERE XC.value('local-name(.)', 'varchar(50)') <> 'EVENT'

			INSERT INTO @BE1
			SELECT
			   PSR_ORDER_ID = IXML.value('(./PSR_ORDER_ID)[1]', 'varchar(20)'),
			   EVENT_CODE = IXML.value('(./EVENT_CODE)[1]', 'varchar(20)'),
			   PLANNED_DATE = IXML.value('(./PLANNED_DATE)[1]', 'date'),
			   REVISED_DATE = IXML.value('(./REVISED_DATE)[1]', 'date'),
			   ACTUAL_DATE = IXML.value('(./ACTUAL_DATE)[1]', 'date')
			FROM @Data1.nodes('/BEFORE/EVENT') Book(IXML)

			INSERT INTO @AO1
			select
				XC.value('local-name(.)', 'varchar(50)') AS NAME,
				XC.value('(.)[1]', 'varchar(50)') AS VALUE
			FROM
				@dATA2.nodes('/AFTER/*') AS XT(XC)
				WHERE XC.value('local-name(.)', 'varchar(50)') <> 'EVENT'

			INSERT INTO @AE1
			SELECT
			   PSR_ORDER_ID = IXML.value('(./PSR_ORDER_ID)[1]', 'varchar(20)'),
			   EVENT_CODE = IXML.value('(./EVENT_CODE)[1]', 'varchar(20)'),
			   PLANNED_DATE = IXML.value('(./PLANNED_DATE)[1]', 'date'),
			   REVISED_DATE = IXML.value('(./REVISED_DATE)[1]', 'date'),
			   ACTUAL_DATE = IXML.value('(./ACTUAL_DATE)[1]', 'date')
			FROM @Data2.nodes('/AFTER/EVENT') Book(IXML)


SELECT @logxml = (
 SELECT AA.VALUE PSR_ORDER_ID,CONVERT(VARCHAR(12),getdate(),101) [MODIFIED_DATE], @ModifiedBy [MODFIED_BY], (
 select A.NAME, B.VALUE [BEFORE], A.VALUE [AFTER] from  @AO1 A LEFT OUTER JOIN @BO1 B ON B.NAME = A.NAME 
WHERE B.VALUE <> A.VALUE OR (B.VALUE IS NULL AND A.VALUE <> '' AND A.VALUE NOT LIKE '0%')
FOR XML PATH ('PSR') ,  type),
 ( select A.EVENT_CODE,  ISNULL(CONVERT(VARCHAR(12),B.REVISED_DATE,101),'NULL') [B_REVISED_DATE],
          ISNULL(CONVERT(VARCHAR(12),A.REVISED_DATE,101),'NULL')   [A_REVISED_DATE]
 from @AE1 A FULL OUTER JOIN  @BE1 B ON B.EVENT_CODE = A.EVENT_CODE  
 WHERE   B.REVISED_DATE <> A.REVISED_DATE OR (B.REVISED_DATE IS NULL AND A.REVISED_DATE IS NOT NULL) OR 
 (B.REVISED_DATE IS NOT NULL AND A.REVISED_DATE IS NULL) FOR XML PATH ('EVENT'), TYPE),
 ( select A.EVENT_CODE,  ISNULL(CONVERT(VARCHAR(12),B.ACTUAL_DATE,101),'NULL') [B_ACTUAL_DATE],
          ISNULL(CONVERT(VARCHAR(12),A.ACTUAL_DATE,101),'NULL')   [A_ACTUAL_DATE]
 from @AE1 A FULL OUTER JOIN  @BE1 B ON B.EVENT_CODE = A.EVENT_CODE  
 WHERE   B.ACTUAL_DATE <> A.ACTUAL_DATE OR (B.ACTUAL_DATE IS NULL AND A.ACTUAL_DATE IS NOT NULL) OR 
 (B.ACTUAL_DATE IS NOT NULL AND A.ACTUAL_DATE IS NULL) FOR XML PATH ('EVENT'), TYPE)
 FROM @AO1 AA WHERE [NAME] = 'PSR_ORDER_ID' FOR XML PATH ('LOG'), TYPE )

 RETURN @LOGXML

 END

GO
/****** Object:  UserDefinedFunction [dbo].[SplitString]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  FUNCTION [dbo].[SplitString](@text varchar(8000), @delimiter varchar(20) = ' ')

RETURNS @Strings TABLE

(    

  position int IDENTITY PRIMARY KEY,

  value varchar(8000)   

)

AS

BEGIN

 

DECLARE @index int 

SET @index = -1 

 

WHILE (LEN(@text) > 0) 

  BEGIN  

    SET @index = CHARINDEX(@delimiter , @text)  

    IF (@index = 0) AND (LEN(@text) > 0)  

      BEGIN   

        INSERT INTO @Strings VALUES (@text)

          BREAK  

      END  

    IF (@index > 1)  

      BEGIN   

        INSERT INTO @Strings VALUES (LEFT(@text, @index - 1))   

        SET @text = RIGHT(@text, (LEN(@text) - @index))  

      END  

    ELSE 

      SET @text = RIGHT(@text, (LEN(@text) - @index)) 

    END

  RETURN

END

 






GO
/****** Object:  Table [dbo].[BR_USER_PROFILE]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BR_USER_PROFILE](
	[BR_USER_ID] [varchar](18) NOT NULL,
	[BR_USER_NAME] [varchar](50) NULL,
	[BR_ROLE_ID] [int] NULL,
	[BR_ROLE_NAME] [varchar](50) NOT NULL,
	[BR_ATTR_NAME] [varchar](35) NULL,
	[BR_ATTR_VALUE] [varchar](120) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[debug_log]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[debug_log](
	[loginuser] [nvarchar](100) NULL,
	[datecreated] [datetime] NULL,
	[callingproc] [nvarchar](100) NULL,
	[logtext] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DUPPSRS]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DUPPSRS](
	[DATECREATED] [datetime] NOT NULL,
	[PROFOMA_PO] [varchar](35) NOT NULL,
	[ORDER_NO] [varchar](35) NOT NULL,
	[PROD_OFFICE] [varchar](80) NULL,
	[SUPPLIER] [varchar](17) NULL,
	[VENDOR_NAME] [varchar](80) NULL,
	[MANUFACTURER] [varchar](17) NULL,
	[FACTORY_NAME] [varchar](35) NULL,
	[ORIGIN_CNTRY] [varchar](3) NULL,
	[ORIGIN_COUNTRY] [varchar](50) NULL,
	[BRAND] [varchar](35) NULL,
	[DEPT] [varchar](17) NULL,
	[CATEGORY] [varchar](35) NULL,
	[BUYER] [varchar](100) NULL,
	[STYLE_NO] [varchar](35) NOT NULL,
	[STYLE_DESC] [varchar](80) NULL,
	[COLOR_CODE] [varchar](20) NULL,
	[COLOR_WASH_NAME] [varchar](35) NULL,
	[PLAN_ID] [varchar](35) NULL,
	[FOB] [decimal](30, 6) NULL,
	[GROSS_SELL_PRICE] [decimal](30, 6) NULL,
	[MARGIN_PCT] [decimal](30, 6) NULL,
	[PRODUCT_TYPE] [varchar](35) NULL,
	[PROGRAM_NAME] [varchar](80) NULL,
	[TRANS_MODE] [varchar](80) NULL,
	[DELIVER_TO] [varchar](17) NULL,
	[GENDER] [varchar](35) NULL,
	[FINAL_QTY] [decimal](38, 6) NULL,
	[SHIPPED_QTY] [decimal](38, 6) NULL,
	[CPO_ORDER_QTY] [decimal](38, 6) NULL,
	[OC_NO] [varchar](35) NULL,
	[SEASON] [varchar](35) NULL,
	[CUST_DEL_DATE] [datetime] NULL,
	[NLT_GAC_DATE] [datetime] NULL,
	[FTY_GAC_DATE] [datetime] NULL,
	[CPO_STYLE_DESC] [varchar](80) NULL,
	[SO_ID] [decimal](30, 0) NULL,
	[SALES_ORDER_NO] [varchar](35) NULL,
	[CPO_PACK_TYPE] [varchar](1) NOT NULL,
	[CPO_SHIP_MODE] [varchar](1) NOT NULL,
	[CPO_COLOR_DESC] [varchar](50) NULL,
	[VPO_HEAD_STATUS] [varchar](8) NULL,
	[VPO_LINE_STATUS] [varchar](8) NULL,
	[ASN_ID] [varchar](78) NULL,
	[MODIFY_TS] [datetime] NULL,
	[MODIFY_USER] [varchar](18) NULL,
	[INTERFACE_TS] [datetime] NOT NULL,
	[INIT_FLOW] [varchar](8) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PSR_CHANGE_LOG]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PSR_CHANGE_LOG](
	[log_id] [int] IDENTITY(1,1) NOT NULL,
	[TIMESTAMP] [datetime2](7) NULL,
	[PSR_ORDER_ID] [int] NULL,
	[MODIFIED_BY] [varchar](18) NULL,
	[LOG] [xml] NULL,
 CONSTRAINT [PK__PSR_CHAN__9E2397E09544FC51] PRIMARY KEY CLUSTERED 
(
	[log_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PSR_EVENT_MASTER]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PSR_EVENT_MASTER](
	[EVENT_CODE] [varchar](35) NOT NULL,
	[EVENT_DESC] [varchar](80) NULL,
	[EVENT_CATEGORY] [varchar](35) NULL,
	[MODIFY_TS] [datetime2](7) NULL,
	[MODIFY_USER] [varchar](20) NULL,
	[CREATE_DATE] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[EVENT_CODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PSR_EVENT_MODEL]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PSR_EVENT_MODEL](
	[MODEL_ID] [int] NOT NULL,
	[EVENT_CODE] [varchar](35) NOT NULL,
	[DESCRIPTION] [varchar](250) NULL,
	[PLAN_DATE] [datetime2](7) NULL,
	[ACTUAL_DATE] [datetime2](7) NULL,
	[REVISED_DATE] [datetime2](7) NULL,
	[TRIGGER_EVENT] [varchar](20) NULL,
	[TRIGGER_DAYS] [varchar](20) NULL,
	[MODIFY_TS] [datetime2](7) NULL,
	[MODIFY_USER] [varchar](20) NULL,
	[CREATE_DATE] [datetime2](7) NULL,
 CONSTRAINT [PK_PSR_EVENT_MODEL] PRIMARY KEY CLUSTERED 
(
	[MODEL_ID] ASC,
	[EVENT_CODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PSR_EVENTS]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PSR_EVENTS](
	[PSR_ORDER_ID] [int] NOT NULL,
	[EVENT_CODE] [varchar](35) NOT NULL,
	[EVENT_DESC] [varchar](80) NULL,
	[EVENT_CATEGORY] [varchar](35) NULL,
	[ACTUAL_DATE] [date] NULL,
	[PLANNED_DATE] [date] NULL,
	[REVISED_DATE] [date] NULL,
	[MODEL_NAME] [varchar](500) NULL,
	[MODIFY_TS] [datetime2](7) NULL,
	[MODIFY_USER] [varchar](20) NULL,
	[CREATE_DATE] [datetime2](7) NULL,
	[HIGHLIGHT_REVISED_DATE] [bit] NULL,
 CONSTRAINT [PK__PSR_EVEN__867597ABB1B75E94] PRIMARY KEY CLUSTERED 
(
	[PSR_ORDER_ID] ASC,
	[EVENT_CODE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[psr_eventslog]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[psr_eventslog](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[logtext] [nvarchar](2000) NULL,
	[createddate] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PSR_LASTRUN]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PSR_LASTRUN](
	[OrderSynchLastRun] [datetime] NULL,
	[EventSynchLastRun] [datetime] NULL,
	[UserSynchLastRun] [datetime] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PSR_LOG_OCEVENTS]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PSR_LOG_OCEVENTS](
	[psr_order_id] [int] NOT NULL,
	[OldComment] [varchar](250) NULL,
	[NewComment] [varchar](250) NULL,
	[OldOCNO] [varchar](35) NULL,
	[NewOCNO] [varchar](35) NULL,
	[OldBrand] [varchar](35) NULL,
	[NewBrand] [varchar](35) NULL,
	[OldStyle] [varchar](35) NULL,
	[NewStyle] [varchar](35) NULL,
	[OldPLActualGAC] [datetime] NULL,
	[NewPLActualGAC] [datetime] NULL,
	[old_psr_id] [int] NULL,
	[Logts] [datetime] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PSR_MATCH_CRITERIA]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PSR_MATCH_CRITERIA](
	[MODEL_ID] [int] NOT NULL,
	[MC_ROW_NO] [int] NOT NULL,
	[MODEL_NAME] [varchar](35) NOT NULL,
	[MATCH_CASE] [varchar](35) NULL,
	[FIELD_VALUE] [varchar](35) NULL,
	[MODIFY_TS] [datetime2](7) NULL,
	[MODIFY_USER] [varchar](20) NULL,
	[CREATE_DATE] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[MODEL_ID] ASC,
	[MC_ROW_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PSR_MODEL_MASTER]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PSR_MODEL_MASTER](
	[MODEL_ID] [int] IDENTITY(1,1) NOT NULL,
	[MODEL_NAME] [varchar](35) NOT NULL,
	[CREATE_DATE] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[MODEL_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PSR_ORDER]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PSR_ORDER](
	[PSR_ORDER_ID] [int] IDENTITY(1,1) NOT NULL,
	[VPO] [varchar](35) NOT NULL,
	[COLOR_WASH_NAME] [varchar](35) NOT NULL,
	[SHIP_TO] [varchar](17) NOT NULL,
	[SO_ID] [numeric](30, 0) NOT NULL,
	[PROD_OFFICE] [varchar](80) NULL,
	[VENDOR_ID] [varchar](17) NOT NULL,
	[VENDOR_NAME] [varchar](80) NOT NULL,
	[FACTORY_ID] [varchar](17) NOT NULL,
	[FACTORY_NAME] [varchar](35) NOT NULL,
	[COUNTRY_ORIGIN] [varchar](50) NULL,
	[BRAND] [varchar](35) NOT NULL,
	[DEPT] [varchar](17) NULL,
	[MERCHANT] [varchar](50) NULL,
	[STYLE] [varchar](35) NULL,
	[STYLE_DESC] [varchar](80) NULL,
	[FOB] [numeric](30, 6) NULL,
	[GROSS_SELL_PRICE] [numeric](30, 6) NULL,
	[MARGIN] [numeric](30, 6) NULL,
	[PRODUCT_TYPE] [varchar](35) NULL,
	[PROGRAM_NAME] [varchar](80) NULL,
	[SHIP_MODE] [varchar](80) NULL,
	[GENDER] [varchar](35) NULL,
	[ORDER_QTY] [numeric](30, 6) NULL,
	[SHIPPED_QTY] [numeric](30, 6) NULL,
	[OC_COMMIT] [varchar](35) NULL,
	[CPO] [varchar](35) NULL,
	[SEASON] [varchar](35) NULL,
	[NDC_DATE] [date] NULL,
	[VPO_NLT_GAC_DATE] [date] NULL,
	[VPO_FTY_GAC_DATE] [date] NULL,
	[CPO_GAC_DATE] [date] NULL,
	[PL_ACTUAL_GAC] [date] NULL,
	[ASN] [varchar](35) NULL,
	[KEY_ITEM] [varchar](80) NULL,
	[VPO_STATUS] [varchar](8) NULL,
	[CUST_FTY_ID] [varchar](20) NULL,
	[FACTORY_REF] [varchar](20) NULL,
	[IMAGE] [varchar](250) NULL,
	[FABRIC_CODE] [varchar](50) NULL,
	[FIBER_CONTENT] [varchar](80) NULL,
	[FABRIC_MILL] [varchar](80) NULL,
	[FAB_TRIM_ORDER] [varchar](20) NULL,
	[COLOR_CODE_PATRN] [varchar](20) NULL,
	[GRAPHIC_NAME] [varchar](50) NULL,
	[ORDER_TYPE] [varchar](20) NULL,
	[NDC_WK] [varchar](10) NULL,
	[APPR_FABRIC_YARN_LOT] [varchar](20) NULL,
	[EXTRA_PROCESS] [varchar](1) NULL,
	[ACTUAL_POUNDAGE_AVAILABLE] [numeric](10, 2) NULL,
	[CUT_APPR_LETTER] [varchar](20) NULL,
	[CARE_LABEL_CODE] [varchar](10) NULL,
	[GAUGE] [varchar](20) NULL,
	[KNITTING_PCS] [int] NULL,
	[KNITTING] [numeric](10, 2) NULL,
	[LINKING_PCS] [int] NULL,
	[LINKING] [numeric](10, 2) NULL,
	[SEWING_LINES] [int] NULL,
	[KNITTING_MACHINES] [int] NULL,
	[LINKING_MACHINES] [int] NULL,
	[DAILY_OUTPUT_PER_LINE] [int] NULL,
	[FABRIC_EST_DATE] [date] NULL,
	[FABRIC_TEST_REPORT_NUMERIC] [varchar](20) NULL,
	[GARMENT_TEST_ACTUAL] [date] NULL,
	[PACK_TYPE] [varchar](10) NULL,
	[PACKAGING_READY_DATE] [date] NULL,
	[COMMENT] [varchar](250) NULL,
	[TICKET_COLOR_CODE] [varchar](20) NULL,
	[TICKET_STYLE_NUMERIC] [varchar](20) NULL,
	[SALES_AMOUNT] [numeric](10, 2) NULL,
	[BALANCE_SHIP] [int] NULL,
	[CPO_ORDER_QTY] [decimal](30, 6) NULL,
	[CPO_SHIP_MODE] [varchar](35) NULL,
	[CPO_COLOR_DESC] [varchar](50) NULL,
	[CPO_PACK_TYPE] [varchar](6) NULL,
	[CREATE_DATE] [datetime2](7) NULL,
	[VPO_HEAD_STATUS] [varchar](8) NULL,
	[VPO_LINE_STATUS] [varchar](8) NULL,
	[INTERFACE_TS] [date] NULL,
	[CATEGORY] [varchar](35) NULL,
	[PROFOMA_PO] [varchar](35) NULL,
	[VPO_FTY_GAC_DATE_TEMP] [date] NULL,
	[CPO_STYLE_DESC] [varchar](80) NOT NULL,
	[MODIFY_USER] [varchar](35) NULL,
	[MODIFY_TS] [datetime] NULL,
	[ASN_ID] [varchar](78) NULL,
	[ORIGIN_CNTRY] [varchar](3) NULL,
	[PLAN_ID] [varchar](35) NULL,
	[OC_NO] [varchar](35) NULL,
	[COLOR_CODE] [varchar](20) NULL,
	[INIT_FLOW] [varchar](8) NULL,
	[INIT_FLOW_PREV] [varchar](8) NULL,
	[VPO_ORDER_TYPE] [varchar](35) NULL,
	[SPEED_INDICATOR] [varchar](35) NULL,
	[GARMENT_TEST_REPORT_NO] [varchar](20) NULL,
	[PL_SHIP_DATE] [datetime] NULL,
	[CPO_STYLE_NO] [varchar](35) NULL,
	[REQUEST_NO] [varchar](35) NULL,
	[LADING_POINT] [varchar](25) NULL,
	[TECH_DESIGNER] [varchar](18) NULL,
	[PLM_NO] [varchar](35) NULL,
	[WASH_YN] [varchar](1) NULL,
	[WASH_FACILITY] [varchar](80) NULL,
	[FABRIC_SHIP_MODE] [varchar](35) NULL,
	[TECH_PACK_RECEIVED_DATE] [date] NULL,
	[SAMPLE_MATERIAL_STATUS] [varchar](35) NULL,
	[CK1] [varchar](150) NULL,
	[CK2] [varchar](150) NULL,
	[CK3] [varchar](150) NULL,
	[CK4] [varchar](150) NULL,
	[CK5] [varchar](150) NULL,
	[CK6] [varchar](150) NULL,
	[RETAIL_PRICE] [numeric](30, 6) NULL,
	[FABRIC_PP_DATE] [date] NULL,
	[DELIVERY_MONTH] [varchar](12) NULL,
	[CPO_ACC_DATE_BY_VENDOR] [date] NULL,
	[DIST_CHANNEL] [varchar](20) NULL,
	[FLOOR_SET] [varchar](1) NULL,
	[STYLE_AT_RISK] [varchar](1) NULL,
	[SAMPLE_MERCH_ETA] [date] NULL,
	[SAMPLE_FLOOR_SET_ETA] [date] NULL,
	[SAMPLE_DCOM_ETA] [date] NULL,
	[SAMPLE_MAILER] [varchar](1) NULL,
	[SAMPLE_MAILER_ETA] [date] NULL,
 CONSTRAINT [PK_PSR_ORDER] PRIMARY KEY CLUSTERED 
(
	[PSR_ORDER_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PSR_USER_CUSTIMOZED_VIEW]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PSR_USER_CUSTIMOZED_VIEW](
	[Query_Id] [int] IDENTITY(1,1) NOT NULL,
	[Query_Name] [varchar](80) NULL,
	[Selected_Columns] [varchar](2000) NULL,
	[LOGIN_USER] [varchar](35) NULL,
	[MODIFY_TS] [datetime2](7) NULL,
	[MODIFY_USER] [varchar](20) NULL,
	[CREATE_DATE] [datetime2](7) NULL,
	[PUBLIC_VIEW] [varchar](1) NULL,
PRIMARY KEY CLUSTERED 
(
	[Query_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Query_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[PSR_V_EVENTS]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[PSR_V_EVENTS] 
AS
SELECT MM.MODEL_ID, E.* FROM PSR_EVENTS E, PSR_MODEL_MASTER MM,
(SELECT E.PSR_ORDER_ID, X.VALUE 
FROM PSR_EVENTS E CROSS APPLY DBO.SplitString(E.MODEL_NAME,' :OC') X
WHERE EVENT_CODE = 'E000' AND X.position = 1) Y 
WHERE E.PSR_ORDER_ID = Y.PSR_ORDER_ID AND MM.MODEL_NAME = Y.value



GO

ALTER TABLE [dbo].[PSR_EVENT_MODEL]  WITH CHECK ADD  CONSTRAINT [FK_EVENT_MODEL_EVENT_MASTER] FOREIGN KEY([EVENT_CODE])
REFERENCES [dbo].[PSR_EVENT_MASTER] ([EVENT_CODE])
GO
ALTER TABLE [dbo].[PSR_EVENT_MODEL] CHECK CONSTRAINT [FK_EVENT_MODEL_EVENT_MASTER]
GO
ALTER TABLE [dbo].[PSR_EVENTS]  WITH CHECK ADD  CONSTRAINT [FK_PSR_EVENTS_PSR_ORDER] FOREIGN KEY([PSR_ORDER_ID])
REFERENCES [dbo].[PSR_ORDER] ([PSR_ORDER_ID])
GO
ALTER TABLE [dbo].[PSR_EVENTS] CHECK CONSTRAINT [FK_PSR_EVENTS_PSR_ORDER]
GO
ALTER TABLE [dbo].[PSR_MATCH_CRITERIA]  WITH CHECK ADD  CONSTRAINT [FK_MATCH_CRITERIA_MODEL_MASTER] FOREIGN KEY([MODEL_ID])
REFERENCES [dbo].[PSR_MODEL_MASTER] ([MODEL_ID])
GO
ALTER TABLE [dbo].[PSR_MATCH_CRITERIA] CHECK CONSTRAINT [FK_MATCH_CRITERIA_MODEL_MASTER]
GO
ALTER TABLE [dbo].[PSR_ORDER]  WITH CHECK ADD  CONSTRAINT [FK_PSR_ORDER_PSR_ORDER] FOREIGN KEY([PSR_ORDER_ID])
REFERENCES [dbo].[PSR_ORDER] ([PSR_ORDER_ID])
GO
ALTER TABLE [dbo].[PSR_ORDER] CHECK CONSTRAINT [FK_PSR_ORDER_PSR_ORDER]
GO
/****** Object:  StoredProcedure [dbo].[PSR_ATTACHEVENTS]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[PSR_ATTACHEVENTS]
AS
BEGIN
		iF OBJECT_ID('tempdb..##TempMatchValues') IS NOT NULL
		    DROP TABLE ##TempMatchValues
		iF OBJECT_ID('tempdb..##TEMP_EVENTS') IS NOT NULL
		    DROP TABLE ##TEMP_EVENTS
		iF OBJECT_ID('tempdb..##TMPMC') IS NOT NULL
		    DROP TABLE ##TMPMC		
		iF OBJECT_ID('tempdb..##TEMPM') IS NOT NULL
		    DROP TABLE ##TEMPM

		DECLARE @GacEvent varchar(50) 
		DECLARE @ValidMatchCriteria varchar(500)
		SET @GacEvent = 'E000'   -- The base GAC Event is assumed to have the description value of 'GAC by Vendor' in PSR_EVENT_MODEL
		select distinct match_case [value]  into ##TempMatchValues from PSR_MATCH_CRITERIA where isnull(match_case,'') <> '' and match_case <> 'Please Choose:'

		-- prior to this run, an extract has been run in bamboo rose the the extracted data is available in tempdb..tmp

		DECLARE @LASTRUN DATETIME = getdate()
		DECLARE @THISRUN DATETIME = getdate()

		-- Step 3.
		--  Insert events into the psr events table for all new orders.
		SELECT @LASTRUN = EventSynchLastRun  FROM PSR_LASTRUN



		declare @sql varchar(2000), @psql varchar(2000)
		declare @matchcase varchar(50)

		CREATE TABLE ##TEMPM
		(model_id int, model_name varchar(50), ordseq int)
		CREATE TABLE ##TMPMC
		(model_id int, model_name varchar(50), mc_row_no int, match_case varchar(50), field_value varchar(50))
		CREATE TABLE ##TEMP_EVENTS
		(ordseq int, psr_order_id int, vpo_fty_gac_date date, split_order_parent_id int, oc_parent_id int,  model_id int)

		declare  pcur cursor for  select [value] from ##TempMatchValues

		insert into ##tmpmc ( model_id, model_name,  mc_row_no, match_case, field_value)
		select MC.MODEL_ID, MC.MODEL_NAME, MC.MC_ROW_NO, MC.MATCH_CASE, MC.FIELD_VALUE  from 
			PSR_MATCH_CRITERIA MC,
			(select  model_id, max(mc_row_no) mc_row_no, max(match_case) match_case from PSR_MATCH_CRITERIA mc1 group by  model_id, MATCH_CASE) MCE
			WHERE MC.MODEL_ID = MCE.MODEL_ID AND MC.MC_ROW_NO = MCE.mc_row_no
			AND EXISTS (select 1 from psr_event_model em where em.MODEL_ID = mc.MODEL_ID)

		insert into ##tempm (model_id, model_name, ordseq)
		SELECT M.MODEL_ID, m.MODEL_NAME,  COUNT(MC.mc_row_no) *100000 + m.MODEL_ID ordseq 
				FROM  PSR_MODEL_MASTER M,
				(select MC.MODEL_ID, MC.MODEL_NAME, MC.MC_ROW_NO, MC.MATCH_CASE, MC.FIELD_VALUE from 
				PSR_MATCH_CRITERIA MC,
				(select  model_id, max(mc_row_no) mc_row_no, max(match_case) match_case from PSR_MATCH_CRITERIA mc1 
				where MATCH_CASE  IN (select [value] from ##TempMatchValues)
				group by  model_id, MATCH_CASE) MCE
				WHERE MC.MODEL_ID = MCE.MODEL_ID AND MC.MC_ROW_NO = MCE.mc_row_no) mc
				WHERE M.MODEL_ID = MC.MODEL_ID
				AND EXISTS (select 1 from psr_event_model em where em.MODEL_ID = mc.MODEL_ID)
				group by m.MODEL_ID, m.MODEL_NAME
		--insert into ##temp_events (ordseq, psr_order_id, vpo_fty_gac_date, split_order_parent_id)	

		--  INIT/FLOW LOGIC --
		--  DELETE EVENTS WHERE INIT/FLOW CHANGED FOR PSR_ORDER AND RESET THE INIT/FLOW CHANGE MARKER
			DELETE E FROM PSR_EVENTS E, PSR_ORDER O WHERE E.PSR_ORDER_ID = O.PSR_ORDER_ID
					 AND  ISNULL(O.INIT_FLOW,'') <>  ISNULL(O.INIT_FLOW_PREV,'')
			UPDATE PSR_ORDER SET INIT_FLOW_PREV = INIT_FLOW WHERE ISNULL(INIT_FLOW,'') <> ISNULL(INIT_FLOW_PREV	,'')	
        --  END OF INIT_FLOW LOGIC
		
		set @sql = 						
		'select x.*,m2.Model_id from (
		select 
			MAX(M.ordseq) ordseq,
			po.PSR_ORDER_ID, PO.VPO_FTY_GAC_DATE, 0 SPLIT_ORDER_PARENT_ID, 0 OC_PARENT_ID   FROM 
			PSR_ORDER PO LEFT OUTER JOIN 
		(select PSR_ORDER_ID from PSR_EVENTS) PE
		 ON PO.PSR_ORDER_ID = PE.PSR_ORDER_ID,
		 ##tEmpm m
		 where 
		 ( '
			set @psql = ''
			OPEN pCur
			fetch next from pCur into @matchcase
			while @@fetch_status=0 
			begin
				set @psql = @psql + ' (po.' + @matchcase + ' in (select FIELD_VALUE from ##tmpmc mc where mc.match_case = ''' + @matchcase + '''AND mc.model_id = m.MODEL_ID)  OR NOT EXISTS (select FIELD_VALUE from ##tmpmc mc where mc.match_case = ''' + @matchcase + '''AND mc.model_id = m.MODEL_ID)) AND'
				fetch next from pCur into @matchcase
			end
			set	@sql = @sql + @psql + 		
				' 1=1 )
		 and pe.PSR_ORDER_ID is null
		  GROUP BY 
		  PO.PSR_ORDER_ID , PO.VPO_FTY_GAC_DATE) x, ##tempm m2 where x.ordseq = m2.ordseq'

	--print @sql
	--print @psql
	close  pcur
	deallocate pcur
	INSERT INTO ##TEMP_EVENTS
	EXEC (@SQL)

	-- select * from ##TEMP_EVENTS
-- *********************  Begin  PLAN_ID match logic 
-- ** An incoming psr with all keys matching except for PLAN_ID is considered a child of Plan Id split
-- ** and will inherit all events and dates from the PLAN_ID parent.

		UPDATE TE SET TE.SPLIT_ORDER_PARENT_ID = X.SPLIT_ORDER_PARENT_ID
		FROM ##TEMP_EVENTS TE,

		    (SELECT TOP 1 P2.PSR_ORDER_ID SPLIT_ORDER_PARENT_ID, P1.PSR_ORDER_ID 
				FROM PSR_ORDER P1, PSR_ORDER P2 WHERE 1 = 1 
				AND	ISNULL(P1.PROFOMA_PO                 ,'')        = ISNULL(P2.PROFOMA_PO                 ,'')
				AND ISNULL(P1.VPO                        ,'')        = ISNULL(P2.VPO                        ,'')
				AND ISNULL(P1.PROD_OFFICE                ,'')        = ISNULL(P2.PROD_OFFICE                ,'')
				AND ISNULL(P1.VENDOR_ID				   	 ,'')        = ISNULL(P2.VENDOR_ID				   	,'')
				AND ISNULL(P1.VENDOR_NAME                ,'')        = ISNULL(P2.VENDOR_NAME                ,'')
				AND ISNULL(P1.FACTORY_ID				 ,'')        = ISNULL(P2.FACTORY_ID				    ,'')
				AND ISNULL(P1.FACTORY_NAME               ,'')        = ISNULL(P2.FACTORY_NAME               ,'')
				AND ISNULL(P1.ORIGIN_CNTRY				 ,'')        = ISNULL(P2.ORIGIN_CNTRY				,'')
				AND ISNULL(P1.COUNTRY_ORIGIN             ,'')        = ISNULL(P2.COUNTRY_ORIGIN             ,'')
				AND ISNULL(P1.BRAND                      ,'')        = ISNULL(P2.BRAND                      ,'')
				AND ISNULL(P1.DEPT                       ,'')        = ISNULL(P2.DEPT                       ,'')
				AND ISNULL(P1.CATEGORY                   ,'')        = ISNULL(P2.CATEGORY                   ,'')
				AND ISNULL(P1.MERCHANT					 ,'')        = ISNULL(P2.MERCHANT					,'')
				AND ISNULL(P1.STYLE                      ,'')        = ISNULL(P2.STYLE                      ,'')
				AND ISNULL(P1.STYLE_DESC                 ,'')        = ISNULL(P2.STYLE_DESC                 ,'')
				AND ISNULL(P1.COLOR_CODE                 ,'')        = ISNULL(P2.COLOR_CODE                 ,'')
				AND ISNULL(P1.PRODUCT_TYPE               ,'')        = ISNULL(P2.PRODUCT_TYPE               ,'')
				-- AND ISNULL(P1.PROGRAM_NAME               ,'')        = ISNULL(P2.PROGRAM_NAME               ,'')
				AND ISNULL(P1.GENDER                     ,'')        = ISNULL(P2.GENDER                     ,'')
				-- AND ISNULL(P1.SEASON                     ,'')        = ISNULL(P2.SEASON                     ,'')
				AND ISNULL(P1.CPO_SHIP_MODE              ,'')        = ISNULL(P2.CPO_SHIP_MODE              ,'')
				--AND ISNULL(P1.CPO_COLOR_DESC             ,'')        = ISNULL(P2.CPO_COLOR_DESC             ,'')
				--  ONLY PLAN ID is different
				AND ISNULL(P1.PLAN_ID                    ,'')        <> ISNULL(P2.PLAN_ID                    ,'')
				--  AND CPO .  BUT CPO IS USER INPUTTED SO NOT RELIABLE FOR THIS TEST
	--			AND ISNULL(P1.CPO                        ,'')		 = ISNULL(P2.CPO                         ,'')
				-- Now check to see if the parent of the split has an attached event.  
				-- Otherwise all the splits will be attached with events based on the event model.
				AND NOT EXISTS (SELECT 1 FROM PSR_EVENTS PE WHERE PE.PSR_ORDER_ID = P2.PSR_ORDER_ID)    
			) X
			WHERE TE.PSR_ORDER_ID = X.PSR_ORDER_ID  
		
		-- *********************  Begin  OC_NO match logic 
		-- ##TEMP_EVENTS NOW HAS ALL THE INCOMING PSRS THAT ARE NEW AND THE ASSOCIATED EVENT MODELS FOR EVENT ATTACHMENT BASED ON THE EVENT MODEL MATCH CRITERIA BEST FIT.
		-- The block below will identify those orders which are considered related based on OC_NO match.  If an existing matching psr order is found with events already attached,
		-- That PSR will be considered as OC_PARENT and the incoming OC CHILDREN will inherit the same events as OC_PARENT regardless of match critereia.  

		UPDATE TE SET TE.OC_PARENT_ID = OPS.PSR_ORDER_ID
		FROM ##TEMP_EVENTS TE, PSR_ORDER TPS, 
		(SELECT MIN(PSR_ORDER_ID) PSR_ORDER_ID, OC_NO, BRAND,STYLE FROM PSR_ORDER GROUP BY OC_NO, BRAND,STYLE)  OPS
			WHERE TE.PSR_ORDER_ID = TPS.PSR_ORDER_ID 
			  AND TPS.OC_NO = OPS.OC_NO
			  AND TPS.BRAND = OPS.BRAND
			  AND TPS.STYLE = OPS.STYLE
			  AND TPS.PSR_ORDER_ID <> OPS.PSR_ORDER_ID
			 -- AND EXISTS (SELECT 1 FROM PSR_EVENTS OE WHERE OE.PSR_ORDER_ID = OPS.PSR_ORDER_ID)

		-- *********************  End  OC_NO match logic 

		  -- SELECT * FROM ##TEMP_EVENTS where oc_parent_id > 0

		insert into PSR_EVENTS
		SELECT X.psr_order_id, EM.EVENT_CODE, EM.EVENT_DESC, EM.EVENT_CATEGORY, 
		CASE WHEN E.EVENT_CODE = @GacEvent THEN PO.PL_ACTUAL_GAC ELSE NULL END ACTUAL_DATE,
		CASE WHEN EM.EVENT_CODE = @GacEvent THEN X.VPO_FTY_GAC_DATE 
			 WHEN ISNULL(E.TRIGGER_EVENT,'') = ''  OR ISNULL(E.TRIGGER_DAYS,'') = ''  THEN NULL
			 ELSE DATEADD(DAY,CONVERT(INTEGER,E.TRIGGER_DAYS),X.VPO_FTY_GAC_DATE) END PLAN_DATE,			 
		CASE WHEN EM.EVENT_CODE = @GacEvent THEN X.VPO_FTY_GAC_DATE 
			 WHEN ISNULL(E.TRIGGER_EVENT,'') = ''  OR ISNULL(E.TRIGGER_DAYS,'') = ''  THEN NULL
			 ELSE DATEADD(DAY,CONVERT(INTEGER,E.TRIGGER_DAYS),X.VPO_FTY_GAC_DATE) END REVISED_DATE,			 
          MM.MODEL_NAME, GETDATE() MODIFY_TS, NULL MODIFY_USER, GETDATE() CREATE_DATE, 0
		  FROM PSR_EVENT_MODEL E , PSR_EVENT_MASTER EM, PSR_MODEL_MASTER MM, ##TEMP_EVENTS X, PSR_ORDER PO
		  WHERE E.MODEL_ID = X.MODEL_ID
		  AND E.EVENT_CODE = EM.EVENT_CODE
		  AND X.MODEL_ID = MM.MODEL_ID
		  AND X.psr_order_id = PO.PSR_ORDER_ID
		  
		  
-- *********************  Begin  PLAN_ID match logic 		
		  -- for new splits update the event dates from parent of a split 

-- **  Inherit the same scheduled and planned date values for a PLAN_ID child from the PLAN_ID parent.
-- **  For PLAN_ID children, the dates are not calculated based on the Calc function but is copied directly from the PLAN_ID parent.
		   UPDATE PE1 SET 
				PE1.REVISED_DATE = PE2.REVISED_DATE,
				PE1.PLANNED_DATE = PE2.PLANNED_DATE,
				PE1.MODIFY_TS = @THISRUN,
				PE1.MODEL_NAME = PE2.MODEL_NAME + ' SPLIT PLAN_ID PARENT:' + CONVERT(VARCHAR(6),TE.SPLIT_ORDER_PARENT_ID)
		   FROM PSR_EVENTS PE1, PSR_EVENTS PE2, ##TEMP_EVENTS TE
		   WHERE PE1.PSR_ORDER_ID = TE.PSR_ORDER_ID
		   AND  PE1.PSR_ORDER_ID = PE2.PSR_ORDER_ID
		   AND  PE1.EVENT_CODE = PE2.EVENT_CODE
		   AND PE2.PSR_ORDER_ID = TE.SPLIT_ORDER_PARENT_ID
		   AND TE.split_order_parent_id > 0
-- *********************  End  PLAN_ID match logic 



-- *********************  Begin  OC_NO match logic 
		   -- Delete events attached in the first pass for psrs later identified as OC children.  
		   -- These psrs will inherit events from the OC parent.
		   
			DELETE FROM PE
			FROM PSR_EVENTS PE, ##TEMP_EVENTS TE
			WHERE PE.PSR_ORDER_ID = TE.psr_order_id
			  AND TE.oc_parent_id > 0

			-- create events for OC children based on OC parent in a holding area temp table #oc_psr_events

			SELECT TE.psr_order_id, XE.EVENT_CODE, XE.EVENT_DESC,XE.EVENT_CATEGORY, XE.ACTUAL_DATE,XE.PLANNED_DATE PLANNED_DATE,XE.REVISED_DATE,XE.MODEL_NAME + ' :OC Parent:' + convert(varchar(6),te.oc_Parent_id) MODEL_NAME,GETDATE() MODIFY_TS,'' MODIFY_USER,GETDATE() CREATE_DATE
			INTO #oc_psr_events
			 FROM PSR_EVENTS XE	, ##TEMP_EVENTS TE
			WHERE XE.PSR_ORDER_ID = TE.oc_parent_id

--SELECT * FROM #oc_psr_events

			-- Now update the OC children events based on the OC Match Event attachment logic.
			-- 1.  If OC parent and child share the same VPO_FTY_GAC_DATE then inherit the date values as well.
			-- 2.  If OC parent and child VPO_FTY_GAC_DATE are different, only the event codes are matched. 
			--      The Date values will be calculated by the parent event model logic but using the OC child VPO_FTY_GAC_DATE as base.

			 UPDATE OCE SET
			 OCE.PLANNED_DATE =  CASE WHEN PO.VPO_FTY_GAC_DATE = OPO.VPO_FTY_GAC_DATE  THEN  PE.PLANNED_DATE
									  WHEN ISNULL(E.TRIGGER_EVENT,'') = ''  OR ISNULL(E.TRIGGER_DAYS,'') = ''  THEN NULL
									  ELSE DATEADD(DAY,CONVERT(INTEGER,E.TRIGGER_DAYS),PO.VPO_FTY_GAC_DATE) END ,
			 OCE.REVISED_DATE =  CASE WHEN PO.VPO_FTY_GAC_DATE = PO.VPO_FTY_GAC_DATE AND PE.REVISED_DATE <> PE.PLANNED_DATE THEN  PE.REVISED_DATE
									  WHEN ISNULL(E.TRIGGER_EVENT,'') = ''  OR ISNULL(E.TRIGGER_DAYS,'') = ''  THEN NULL
									  ELSE DATEADD(DAY,CONVERT(INTEGER,E.TRIGGER_DAYS),PO.VPO_FTY_GAC_DATE) END ,
			OCE.ACTUAL_DATE =    PE.ACTUAL_DATE		
			FROM #oc_psr_events OCE, PSR_EVENTS PE, ##TEMP_EVENTS TE, PSR_ORDER PO, PSR_EVENT_MODEL E, PSR_MODEL_MASTER M, PSR_ORDER OPO
			WHERE OCE.psr_order_id = TE.psr_order_id 
			AND TE.oc_parent_id = PE.PSR_ORDER_ID
			AND PO.PSR_ORDER_ID = OCE.psr_order_id 
			AND PE.EVENT_CODE = OCE.EVENT_CODE
			AND OPO.PSR_ORDER_ID = PE.PSR_ORDER_ID
			AND OCE.EVENT_CODE = E.EVENT_CODE
			AND OCE.MODEL_NAME = M.MODEL_NAME AND M.MODEL_ID = E.MODEL_ID

			-- Now insert all the events for OC children in to PSR_EVENTS table.

			INSERT INTO PSR_EVENTS 
			SELECT *,0 FROM #oc_psr_events
		
-- *********************  End  OC_NO match logic 

		  UPDATE PSR_LASTRUN SET OrderSynchLastRun = @THISRUN
		  insert into psr_eventslog (logtext, createddate)
		  select 'AutoAttach events completed. Number of rows inserted =' + convert(varchar(8),@@ROWCOUNT), getdate()
END



GO
/****** Object:  StoredProcedure [dbo].[PSR_CHECKDUPORDERS]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[PSR_CHECKDUPORDERS]
AS
BEGIN
SELECT DISTINCT 
							CK1=ISNULL(pe.PROFOMA_PO,'')+ '|'+ISNULL(pe.ORDER_NO,'')+ '|'+ISNULL(pe.PROD_OFFICE,'')+ '|'+ISNULL(pe.SUPPLIER,'')+ '|'+ISNULL(pe.VENDOR_NAME,'')
						,	CK2=ISNULL(pe.MANUFACTURER,'')+ '|'+ISNULL(pe.FACTORY_NAME,'')+  '|' +  pe.ORIGIN_CNTRY +  '|'+ISNULL(pe.ORIGIN_COUNTRY,'')+'|'+ISNULL(pe.BRAND,'') 
						,	CK3=ISNULL(pe.DEPT,'')+ '|'+ISNULL(pe.CATEGORY,'')+ '|'+ISNULL(pe.BUYER,'')+ '|'+ISNULL(pe.STYLE_NO,'')+ '|'+ISNULL(pe.STYLE_DESC,'')+ '|'+ISNULL(pe.COLOR_CODE,'') 
						,	CK4=ISNULL(pe.PLAN_ID,'')+ '|'+ISNULL(pe.PRODUCT_TYPE,'')+ '|'+ISNULL(pe.PROGRAM_NAME,'')
						,	CK5=ISNULL(pe.GENDER,'')+  '|'+ISNULL(pe.SEASON,'')
INTO #TMPKEYS
FROM TMPPSR PE

INSERT INTO DUPPSRS
SELECT GETDATE(), PE.* 
FROM TMPPSR PE,
(
SELECT COUNT(*) CNT, TK.*  FROM TMPPSR PE, #TMPKEYS TK
WHERE 
							CK1=ISNULL(pe.PROFOMA_PO,'')+ '|'+ISNULL(pe.ORDER_NO,'')+ '|'+ISNULL(pe.PROD_OFFICE,'')+ '|'+ISNULL(pe.SUPPLIER,'')+ '|'+ISNULL(pe.VENDOR_NAME,'')
						AND	CK2=ISNULL(pe.MANUFACTURER,'')+ '|'+ISNULL(pe.FACTORY_NAME,'')+  '|' +  pe.ORIGIN_CNTRY +  '|'+ISNULL(pe.ORIGIN_COUNTRY,'')+'|'+ISNULL(pe.BRAND,'') 
						AND	CK3=ISNULL(pe.DEPT,'')+ '|'+ISNULL(pe.CATEGORY,'')+ '|'+ISNULL(pe.BUYER,'')+ '|'+ISNULL(pe.STYLE_NO,'')+ '|'+ISNULL(pe.STYLE_DESC,'')+ '|'+ISNULL(pe.COLOR_CODE,'') 
						AND	CK4=ISNULL(pe.PLAN_ID,'')+ '|'+ISNULL(pe.PRODUCT_TYPE,'')+ '|'+ISNULL(pe.PROGRAM_NAME,'')
						AND	CK5=ISNULL(pe.GENDER,'')+  '|'+ISNULL(pe.SEASON,'')
GROUP BY TK.CK1, TK.CK2, TK.CK3, TK.CK4, TK.CK5
HAVING COUNT(*) > 1
)X WHERE
							CK1=ISNULL(pe.PROFOMA_PO,'')+ '|'+ISNULL(pe.ORDER_NO,'')+ '|'+ISNULL(pe.PROD_OFFICE,'')+ '|'+ISNULL(pe.SUPPLIER,'')+ '|'+ISNULL(pe.VENDOR_NAME,'')
						AND	CK2=ISNULL(pe.MANUFACTURER,'')+ '|'+ISNULL(pe.FACTORY_NAME,'')+  '|' +  pe.ORIGIN_CNTRY +  '|'+ISNULL(pe.ORIGIN_COUNTRY,'')+'|'+ISNULL(pe.BRAND,'') 
						AND	CK3=ISNULL(pe.DEPT,'')+ '|'+ISNULL(pe.CATEGORY,'')+ '|'+ISNULL(pe.BUYER,'')+ '|'+ISNULL(pe.STYLE_NO,'')+ '|'+ISNULL(pe.STYLE_DESC,'')+ '|'+ISNULL(pe.COLOR_CODE,'') 
						AND	CK4=ISNULL(pe.PLAN_ID,'')+ '|'+ISNULL(pe.PRODUCT_TYPE,'')+ '|'+ISNULL(pe.PROGRAM_NAME,'')
						AND	CK5=ISNULL(pe.GENDER,'')+  '|'+ISNULL(pe.SEASON,'')

END

GO
/****** Object:  StoredProcedure [dbo].[PSR_EXTRACT_QUERY]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[PSR_EXTRACT_QUERY] 
AS
BEGIN
IF OBJECT_ID('TEMPDB.DBO.TMP') is not null
	drop table TEMPDB.DBO.TMP

SELECT  DISTINCT    -- Version: 2018.09.04  Version 2 with ASN ID logic
VORDER_H.PROFOMA_PO   -- RH 8/23: Key to use the generate PostOrder WebService  (PK)
,VORDER_H.ORDER_NO
,REFERENCE3_CODES.[DESCRIPTION] AS PROD_OFFICE
,VORDER_H.SUPPLIER
,VORDER_H.VENDOR_NAME
,VORDER_H.MANUFACTURER
,VORDER_H.MEMO11 AS FACTORY_NAME
,VORDER_H.ORIGIN_CNTRY
,COUNTRY.[DESCRIPTION] AS ORIGIN_COUNTRY
,VORDER_H.MEMO5 AS BRAND
,VORDER_D.DEPT DEPT                                        
,VORDER_H.MEMO9 AS CATEGORY
,VORDER_H.BUYER
--,USER_PROFILE.USER_NAME AS BUYER
,VORDER_D.ITEM_NO AS STYLE_NO                        
,VORDER_D.DESCRIPTION AS STYLE_DESC            
,VORDER_D.MEMO8 AS COLOR_WASH_NAME
,VORDER_D.PLAN_ID                        -- PK  -- Added by Ron on 2018.08.30
,MAX(VORDER_D.PRICE) AS FOB                                       -- Aggregated by Ganesh using Max  2018.08.30
,MAX(VORDER_D.RESELL_PRICE) AS GROSS_SELL_PRICE       -- Aggregated by Ganesh using Max  2018.08.30
,MAX(VORDER_D.PROFIT_PCT) AS MARGIN_PCT                     -- Aggregated by Ganesh using Max  2018.08.30
,VORDER_D.MEMO6 AS PRODUCT_TYPE                      
,QUOTE_ANC.ALT_DESC2 AS PROGRAM_NAME                 
,TRANS_MODE.DESCRIPTION AS TRANS_MODE                
,VORDER_D.DELIVER_TO
,VORDER_D.MEMO5 AS GENDER                                  
,SUM(VORDER_D.QTY) AS FINAL_QTY                            
,SUM(VORDER_D.NUMBR1) AS SHIPPED_QTY
,SUM(SALES_ORDER_D.ORDER_QTY) AS CPO_ORDER_QTY
,VORDER_H.MEMO7 AS OC_NO
,VORDER_D.SEASON SEASON                              
,MAX(VORDER_D.FIRST_SHIP_DATE) AS CUST_DEL_DATE       -- Aggregated by Ganesh using Max  2018.08.30
,MAX(VORDER_D.DATE1) AS NLT_GAC_DATE                        -- Aggregated by Ganesh using Max  2018.08.30
,MAX(VORDER_D.DATE5) AS FTY_GAC_DATE                        -- Aggregated by Ganesh using Max  2018.08.30
,MIN(SALES_ORDER_D.[DESCRIPTION]) AS CPO_STYLE_DESC  -- Aggregated by Ron using Min 2018.09.04
,SALES_ORDER_H.SO_ID
,SALES_ORDER_H.SALES_ORDER_NO
--,SALES_ORDER_D.LINE_NO
--,SALES_ORDER_D.PACK_TYPE AS CPO_PACK_TYPE
,'' AS CPO_PACK_TYPE
--,SALES_ORDER_D.MEMO5 AS CPO_SHIP_MODE               -- replace with '' by Ron 2018.09.04
,'' AS CPO_SHIP_MODE
,SALES_ORDER_D.COLOR_DESC AS CPO_COLOR_DESC    
,VORDER_H.STATUS AS VPO_HEAD_STATUS
,VORDER_D.STATUS AS VPO_LINE_STATUS            
,MIN(PACKING_LIST_H.STATUS_01 + PACKING_LIST_H.PACKING_LIST_NO + PACKING_LIST_D.SALES_ORD_NO) AS ASN_ID 
,MAX(VORDER_D.MODIFY_TS) MODIFY_TS
,MAX(VORDER_D.MODIFY_USER) AS MODIFY_USER  -- Aggregated by Ron using Max 2018.09.04
,GETDATE() AS INTERFACE_TS
into TEMPDB.DBO.TMP
FROM  TSAM_TEST..VORDER_H WITH(NOLOCK)
INNER JOIN TSAM_TEST..VORDER_D WITH(NOLOCK) ON
VORDER_H.PROFOMA_PO=VORDER_D.PROFOMA_PO AND VORDER_D.DATE5 > '$LASTRUUN'
INNER JOIN TSAM_TEST..QUOTE_ANC WITH(NOLOCK) ON VORDER_D.REQUEST_NO = QUOTE_ANC.REQUEST_NO
INNER JOIN TSAM_TEST..REFERENCE3_CODES WITH(NOLOCK) ON VORDER_H.SELLING_AGENT = REFERENCE3_CODES.CODE
INNER JOIN TSAM_TEST..TRANS_MODE WITH(NOLOCK) ON VORDER_D.TRANS_MODE = TRANS_MODE.CODE
--INNER JOIN TSAM_TEST..USER_PROFILE WITH(NOLOCK) ON VORDER_H.BUYER = USER_PROFILE.USER_ID
INNER JOIN TSAM_TEST..COUNTRY WITH(NOLOCK) ON VORDER_H.ORIGIN_CNTRY = COUNTRY.CODE
LEFT JOIN TSAM_TEST..ORDER_ASSIGNMENT WITH(NOLOCK) ON ORDER_ASSIGNMENT.PROFOMA_PO=VORDER_D.PROFOMA_PO AND VORDER_D.ROW_NO = ORDER_ASSIGNMENT.PO_ROW_NO
LEFT JOIN TSAM_TEST..SALES_ORDER_D WITH(NOLOCK) ON SALES_ORDER_D.SO_ID=ORDER_ASSIGNMENT.SO_ID AND ORDER_ASSIGNMENT.SO_ROW_NO = SALES_ORDER_D.ROW_NO
LEFT JOIN TSAM_TEST..SALES_ORDER_H WITH(NOLOCK) ON SALES_ORDER_H.SO_ID=SALES_ORDER_D.SO_ID
LEFT JOIN TSAM_TEST..PACKING_LIST_D ON VORDER_H.PROFOMA_PO = PACKING_LIST_D.PROFOMA_PO AND VORDER_D.ROW_NO = PACKING_LIST_D.ORDER_ROW_NO AND PACKING_LIST_D.CUSTOMER_ID = '101'
LEFT JOIN TSAM_TEST..PACKING_LIST_H ON PACKING_LIST_D.PACKING_LIST_NO = PACKING_LIST_H.PACKING_LIST_NO AND PACKING_LIST_H.STATUS = 'SUBMIT'   -- Logic to get ASN ID, base on PLD Profoma PO and row number and under 101 customer with PLH status = SUBMIT
WHERE (( VORDER_H.MEMO4 = '102' AND  VORDER_H.MEMO9 = 'DE-DEN') or  ( VORDER_H.MEMO4 = '101' AND  VORDER_H.MEMO9 = 'IN-INT') )
AND VORDER_H.STATUS NOT IN ('VOID','CLOSED')
GROUP BY
VORDER_H.PROFOMA_PO
,VORDER_H.ORDER_NO
--,VORDER_H.SELLING_AGENT
,REFERENCE3_CODES.[DESCRIPTION]
,VORDER_H.SUPPLIER
,VORDER_H.VENDOR_NAME
,VORDER_H.MANUFACTURER
,VORDER_H.MEMO11
,VORDER_H.ORIGIN_CNTRY
,COUNTRY.[DESCRIPTION]
,VORDER_H.MEMO5
,VORDER_D.DEPT                             
,VORDER_H.MEMO9                              
--,USER_PROFILE.USER_NAME                      
,VORDER_H.BUYER
,VORDER_D.ITEM_NO                          
,VORDER_D.DESCRIPTION                      
,VORDER_D.MEMO8   
,VORDER_D.PLAN_ID                             -- Added by Ron on 2018.08.30     (PK)      
--,VORDER_D.PRICE                             -- Aggregated by Ganesh using Max  2018.08.30
--,VORDER_D.RESELL_PRICE                      -- Aggregated by Ganesh using Max  2018.08.30
--,VORDER_D.PROFIT_PCT                        -- Aggregated by Ganesh using Max  2018.08.30
,VORDER_D.MEMO6                            
,QUOTE_ANC.ALT_DESC2                       
,TRANS_MODE.DESCRIPTION                    
,VORDER_D.DELIVER_TO                          
,VORDER_D.MEMO5                            
,VORDER_H.MEMO7                              
,VORDER_D.SEASON                          
--,VORDER_D.FIRST_SHIP_DATE                  
--,VORDER_D.DATE1                            
--,VORDER_D.DATE5                             
--,SALES_ORDER_D.[DESCRIPTION]                 
,SALES_ORDER_H.SO_ID                         
,SALES_ORDER_H.SALES_ORDER_NO              
--,SALES_ORDER_D.LINE_NO                     
--,SALES_ORDER_D.PACK_TYPE                   
-- ,SALES_ORDER_D.MEMO5                -- Removed by Ron on 2018.09.04       
,SALES_ORDER_D.COLOR_DESC                 
--,VORDER_D.TRANS_MODE                       
,VORDER_H.STATUS                              
,VORDER_D.STATUS                           
--,VORDER_D.MODIFY_TS                        
--,VORDER_D.MODIFY_USER  
--,PACKING_LIST_H.STATUS_01 , PACKING_LIST_H.PACKING_LIST_NO , PACKING_LIST_D.SALES_ORD_NO
END


GO
/****** Object:  StoredProcedure [dbo].[PSR_LOG_EXTRACT]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[PSR_LOG_EXTRACT] 
	@psrlist varchar(500) = '' --   '62245,62247,62248,64893,64896,67555,67802,67806'  --'75294'
AS
BEGIN

		select * into #psrlist from dbo.SplitString(@psrlist, ',')

		select			(ROW_NUMBER() OVER(ORDER BY PSR_ORDER_ID, MODIFIED_BY, TIMESTAMP) + 2) /3 AS Row#,
						L.PSR_ORDER_ID, L.MODIFIED_BY, L.TIMESTAMP,
						 XC.value('(./NAME)[1]', 'varchar(50)') [FIELD]
						,XC.value('(./BEFORE)[1]', 'varchar(50)')      BEFORE
						,XC.value('(./AFTER)[1]', 'varchar(50)')  AFTER
						INTO #PSR
					FROM
						PSR_CHANGE_LOG L
						CROSS APPLY LOG.nodes('/LOG/PSR') AS XT(XC)
						WHERE PSR_ORDER_ID in (SELECT VALUE FROM #psrlist)

        SELECT * INTO #EVENT FROM 
		(
		select			L.PSR_ORDER_ID, L.MODIFIED_BY, L.TIMESTAMP,
						 XC.value('(./EVENT_CODE)[1]', 'varchar(50)') + ' REVISED_DATE' [FIELD]
						,XC.value('(./B_REVISED_DATE)[1]', 'varchar(50)')  [BEFORE]
						,XC.value('(./A_REVISED_DATE)[1]', 'varchar(50)')  [AFTER]
					FROM
						PSR_CHANGE_LOG L
						CROSS APPLY LOG.nodes('/LOG/EVENT') AS XT(XC)
						WHERE PSR_ORDER_ID in (SELECT VALUE FROM #psrlist)
		UNION

		select			L.PSR_ORDER_ID, L.MODIFIED_BY, L.TIMESTAMP,
						 XC.value('(./EVENT_CODE)[1]', 'varchar(50)') + ' ACTUAL_DATE' [FIELD]
						,XC.value('(./B_ACTUAL_DATE)[1]', 'varchar(50)')  [BEFORE]
						,XC.value('(./A_ACTUAL_DATE)[1]', 'varchar(50)')  [AFTER]
					FROM
						PSR_CHANGE_LOG L
						CROSS APPLY LOG.nodes('/LOG/EVENT') AS XT(XC)
						WHERE PSR_ORDER_ID in (SELECT VALUE FROM #psrlist)
		) X  WHERE (X.BEFORE IS NOT NULL OR X.AFTER IS NOT NULL)

		SELECT PO.VPO, Convert(date,x.[TIMESTAMP]) LogDate, X.* FROM 
		(
		SELECT P1.PSR_ORDER_ID, P1.MODIFIED_BY, P1.TIMESTAMP, 'ORDER' [TYPE],  P1.[FIELD], P1.[BEFORE], P1.[AFTER] FROM #PSR P1
		UNION
		SELECT P1.PSR_ORDER_ID, P1.MODIFIED_BY, P1.TIMESTAMP, 'EVENT' [TYPE],  P1.[FIELD], P1.[BEFORE], P1.[AFTER] FROM #EVENT P1
		) X, PSR_ORDER PO
		WHERE X.PSR_ORDER_ID = PO.PSR_ORDER_ID
		ORDER BY VPO ASC,TIMESTAMP DESC
END


GO
/****** Object:  StoredProcedure [dbo].[PSR_RECALCDATES]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[PSR_RECALCDATES]
	@ORDER_ID VARCHAR(500) = NULL
AS
BEGIN
		DECLARE @LASTRUN DATETIME = getdate()
		DECLARE @THISRUN DATETIME = getdate()
		DECLARE @GacEvent varchar(50) 
		SET @GacEvent = 'E000'   -- The base GAC Event is assumed to have the description value of 'GAC by Vendor' in PSR_EVENT_MODEL

		BEGIN TRY

		-- First update the GAC by vendor dates from the incoming new vpo factory date.
			SELECT @LASTRUN = EventSynchLastRun  FROM PSR_LASTRUN

			UPDATE E SET
						-- CASE WHEN E.EVENT_CODE = @GacEvent THEN PO.PL_ACTUAL_GAC ELSE NULL END ACTUAL_DATE, 
						E.ACTUAL_DATE  = O.PL_ACTUAL_GAC,
						-- E.PLANNED_DATE = O.VPO_FTY_GAC_DATE,
						E.REVISED_DATE = O.VPO_FTY_GAC_DATE,
						E.MODIFY_TS = @THISRUN
						 
			FROM PSR_V_EVENTS E, PSR_ORDER O, PSR_EVENT_MODEL EM
			WHERE E.PSR_ORDER_ID = O.PSR_ORDER_ID
			AND E.MODEL_ID = EM.MODEL_ID
			AND E.EVENT_CODE = EM.EVENT_CODE
			 AND EM.EVENT_CODE = @GacEvent
			 AND E.ACTUAL_DATE IS NULL
			  AND
			(E.PSR_ORDER_ID IN (SELECT VALUE FROM DBO.SPLITSTRING(@ORDER_ID,',')) OR @ORDER_ID IS NULL )
			--AND E.MODIFY_TS > ISNULL(@LASTRUN,'')   -- Do not rely on this EventSynchLastRun since events can be selectively recalculated.

		--  now update all othere event revised date based on the new revised date 

			UPDATE E SET 
						   E.REVISED_DATE = DATEADD(DAY,CONVERT(INTEGER, M1.TRIGGER_DAYS), E2.REVISED_DATE),
						   -- E.PLANNED_DATE = DATEADD(DAY,CONVERT(INTEGER, M1.TRIGGER_DAYS), E2.PLANNED_DATE),
						   E.MODIFY_TS = @THISRUN
						 
			FROM PSR_V_EVENTS E, PSR_EVENT_MODEL M1, PSR_EVENT_MODEL M2, PSR_V_EVENTS E2
			 WHERE
				 M1.MODEL_ID = E.MODEL_iD AND M1.EVENT_CODE = E.EVENT_CODE
			 AND M2.MODEL_ID = E.MODEL_ID AND M2.EVENT_CODE = M1.TRIGGER_EVENT
			 AND E2.PSR_ORDER_ID = E.PSR_ORDER_ID AND E2.EVENT_CODE = M1.TRIGGER_EVENT
			 AND ISNULL(M1.TRIGGER_DAYS,'') <> ''
			 AND E.ACTUAL_DATE IS NULL
			  AND
			(E.PSR_ORDER_ID IN (SELECT VALUE FROM DBO.SPLITSTRING(@ORDER_ID,',')) OR @ORDER_ID IS NULL )
			-- AND E.MODIFY_TS > ISNULL(@LASTRUN,'')

			UPDATE O SET 
			O.VPO_FTY_GAC_DATE_TEMP = O.VPO_FTY_GAC_DATE
			FROM
			PSR_ORDER O
			WHERE 
				(O.PSR_ORDER_ID IN (SELECT VALUE FROM DBO.SPLITSTRING(@ORDER_ID,',')) OR @ORDER_ID IS NULL )
				-- AND O.MODIFY_TS > ISNULL(@LASTRUN,'')

			update PSR_LASTRUN set EventSynchLastRun = @THISRUN
		END TRY
		BEGIN CATCH
		END CATCH
 END

GO
/****** Object:  StoredProcedure [dbo].[PSR_SAVEORDERS]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


--DROP PROCEDURE [dbo].[PSR_SAVEORDERS] 

CREATE PROCEDURE [dbo].[PSR_SAVEORDERS] 

	@XMLSTR VARCHAR(MAX), @ModifiedBy varchar(100) = null, @userrole varchar(100) = null, @searchdate DATETIME, @result varchar(500) output
AS	

begin

 declare @mindate date = ''
 declare @modifydate datetime = getutcdate()
 if @ModifiedBy is null
	set @ModifiedBy = ''
 INSERT INTO DEBUG_LOG SELECT SYSTEM_USER, GETDATE(), 'PSR_SAVEORDERS',
',@XMLSTR=' + '''' + ISNULL(CAST(@XMLSTR AS VARCHAR(MAX)),'') +'''' +
',@ModifiedBy=' + '''' + ISNULL(CAST(@ModifiedBy AS VARCHAR(MAX)),'') +'''' +
',@userrole=' + '''' + ISNULL(CAST(@userrole AS VARCHAR(MAX)),'') +''''  +
',@searchdate=' + '''' + ISNULL(CAST(@searchdate AS VARCHAR(MAX)),'') +'''' 


	DECLARE @message as varchar(500) = 'SUCCESS'
	if @xmlstr is null
		return


		IF OBJECT_ID('tempdb..#TXML') IS NOT NULL
			drop table #txml
		IF OBJECT_ID('tempdb..#TEMPPSRORDER') IS NOT NULL
			drop table #TEMPPSRORDER
		IF OBJECT_ID('tempdb..#TEMPPSREVENTS') IS NOT NULL
			drop table #TEMPPSREVENTS
		IF OBJECT_ID('tempdb..#BEFOREORDER') IS NOT NULL
			drop table #BEFOREORDER
		IF OBJECT_ID('tempdb..#AFTERORDER') IS NOT NULL
			drop table #AFTERORDER
		IF OBJECT_ID('tmpo') IS NOT NULL
			drop table tmpo
		IF OBJECT_ID('tmpe') IS NOT NULL
			drop table tmpe

		BEGIN TRY
			create table #txml (id int identity, data xml);
			insert into #txml (data) values (@xmlstr)
			SELECT 
						 T2.psr.value('(./PSR_ORDER_ID)[1]', 'int')								   PSR_ORDER_ID
						,T2.psr.value('(./CUST_FTY_ID)[1]', 'varchar(20)')                         CUST_FTY_ID
						,T2.psr.value('(./FACTORY_REF)[1]', 'varchar(20)')                         FACTORY_REF
						,T2.psr.value('(./FABRIC_CODE)[1]', 'varchar(80)')                         FABRIC_CODE
						,T2.psr.value('(./FIBER_CONTENT)[1]', 'varchar(80)')                       FIBER_CONTENT
						,T2.psr.value('(./FABRIC_MILL)[1]', 'varchar(80)')                         FABRIC_MILL
						,T2.psr.value('(./FAB_TRIM_ORDER)[1]', 'varchar(20)')                      FAB_TRIM_ORDER
						,T2.psr.value('(./COLOR_CODE_PATRN)[1]', 'varchar(20)')                    COLOR_CODE_PATRN
						,T2.psr.value('(./GRAPHIC_NAME)[1]', 'varchar(50)')                        GRAPHIC_NAME
						,T2.psr.value('(./ORDER_TYPE)[1]', 'varchar(20)')                          ORDER_TYPE
						,T2.psr.value('(./NDC_WK)[1]', 'varchar(10)')                              NDC_WK
						,T2.psr.value('(./APPR_FABRIC_YARN_LOT)[1]', 'varchar(20)')                APPR_FABRIC_YARN_LOT
						,T2.psr.value('(./EXTRA_PROCESS)[1]', 'varchar(1)')                        EXTRA_PROCESS
						,T2.psr.value('(./ACTUAL_POUNDAGE_AVAILABLE)[1]', 'varchar(20)')   ACTUAL_POUNDAGE_AVAILABLE
						,T2.psr.value('(./CARE_LABEL_CODE)[1]', 'varchar(10)')                     CARE_LABEL_CODE
						,T2.psr.value('(./GAUGE)[1]', 'varchar(20)')                               GAUGE
						,T2.psr.value('(./KNITTING_PCS)[1]', 'varchar(20)')                        KNITTING_PCS
						,T2.psr.value('(./KNITTING)[1]',  'varchar(20)')                        KNITTING
						,T2.psr.value('(./LINKING_PCS)[1]', 'varchar(20)')                                 LINKING_PCS
						,T2.psr.value('(./LINKING)[1]', 'varchar(50)')                        LINKING
						,T2.psr.value('(./SEWING_LINES)[1]', 'varchar(50)')                        SEWING_LINES
						,T2.psr.value('(./KNITTING_MACHINES)[1]', 'varchar(20)')					   KNITTING_MACHINES
						,T2.psr.value('(./LINKING_MACHINES)[1]',  'varchar(20)')                          LINKING_MACHINES
						,T2.psr.value('(./DAILY_OUTPUT_PER_LINE)[1]',  'varchar(20)')                     DAILY_OUTPUT_PER_LINE
						,T2.psr.value('(./FABRIC_EST_DATE)[1]',  'date')                        FABRIC_EST_DATE
						,T2.psr.value('(./FABRIC_TEST_REPORT_NUMERIC)[1]', 'varchar(20)')          FABRIC_TEST_REPORT_NUMERIC
						,T2.psr.value('(./GARMENT_TEST_ACTUAL)[1]',  'date')                    GARMENT_TEST_ACTUAL
						,T2.psr.value('(./PACK_TYPE)[1]', 'varchar(10)')                            PACK_TYPE
						,T2.psr.value('(./PACKAGING_READY_DATE)[1]', 'date')				      PACKAGING_READY_DATE
						,T2.psr.value('(./COMMENT)[1]', 'varchar(250)')                            COMMENT
						,T2.psr.value('(./TICKET_COLOR_CODE)[1]', 'varchar(20)')                   TICKET_COLOR_CODE
						,T2.psr.value('(./TICKET_STYLE_NUMERIC)[1]', 'varchar(20)')                TICKET_STYLE_NUMERIC
						,T2.psr.value('(./CUT_APPR_LETTER)[1]', 'varchar(20)')					   CUT_APPR_LETTER
						,T2.psr.value('(./CPO_PACK_TYPE)[1]', 'varchar(6)')						   CPO_PACK_TYPE
						,T2.psr.value('(./IMAGE)[1]', 'varchar(250)')							   [IMAGE]	
						,T2.psr.value('(./GARMENT_TEST_REPORT_NO)[1]', 'varchar(20)')               GARMENT_TEST_REPORT_NO		
						,T2.psr.value('(./PLM_NO)                   [1]', 		'varchar(35)')           PLM_NO 						
						,T2.psr.value('(./WASH_YN)                  [1]', 		'varchar(1)')            WASH_YN 					
						,T2.psr.value('(./WASH_FACILITY)            [1]', 		'varchar(80)')           WASH_FACILITY 				
						,T2.psr.value('(./FABRIC_SHIP_MODE)         [1]', 		'varchar(35)')           FABRIC_SHIP_MODE			
						,T2.psr.value('(./TECH_PACK_RECEIVED_DATE)  [1]', 		'date')           	 	 TECH_PACK_RECEIVED_DATE 	
						,T2.psr.value('(./SAMPLE_MATERIAL_STATUS)   [1]',  		'varchar(35)')           SAMPLE_MATERIAL_STATUS
						,T2.psr.value('(./RETAIL_PRICE)   [1]',  		'varchar(60)')				 RETAIL_PRICE
						,T2.psr.value('(./FABRIC_PP_DATE)   [1]',  		'date')							 FABRIC_PP_DATE
						,T2.psr.value('(./DELIVERY_MONTH)   [1]',  		'varchar(12)')					 DELIVERY_MONTH
						,T2.psr.value('(./CPO_ACC_DATE_BY_VENDOR)   [1]', 'date')						 CPO_ACC_DATE_BY_VENDOR
						,T2.psr.value('(./DIST_CHANNEL)   [1]', 'varchar(20)')					 DIST_CHANNEL
						,T2.psr.value('(./FLOOR_SET)   [1]', 'varchar(1)')						 FLOOR_SET
						,T2.psr.value('(./STYLE_AT_RISK)   [1]', 'varchar(1)')					 STYLE_AT_RISK 
						,T2.psr.value('(./SAMPLE_MERCH_ETA)   [1]', 'date')						SAMPLE_MERCH_ETA
						,T2.psr.value('(./SAMPLE_FLOOR_SET_ETA)   [1]', 'date')					SAMPLE_FLOOR_SET_ETA
						,T2.psr.value('(./SAMPLE_DCOM_ETA)   [1]', 'date')						SAMPLE_DCOM_ETA						 
						,T2.psr.value('(./SAMPLE_MAILER)   [1]', 'varchar(1)')					SAMPLE_MAILER
						,T2.psr.value('(./SAMPLE_MAILER_ETA)   [1]', 'date')					SAMPLE_MAILER_ETA						
			into #temppsrorder
			FROM #txml TX
			CROSS  APPLY  data.nodes('/ORDERS/PSR') as T2(psr)

			SELECT 
					 T2.psr.value('(./PSR_ORDER_ID)[1]', 'int') PSR_ORDER_ID
					,T4.psr.value('(./EVENT_CODE)[1]', 'varchar(35)') EVENT_CODE
					,T4.psr.value('(./PLANNED_DATE)[1]', 'date')      PLANNED_DATE
					,T4.psr.value('(./REVISED_DATE)[1]', 'date')  REVISED_DATE
					,T4.psr.value('(./ACTUAL_DATE)[1]', 'date')   ACTUAL_DATE
			into #temppsrevents
			FROM #txml TX
			CROSS  APPLY  data.nodes('/ORDERS/PSR') as T2(psr)
			CROSS APPLY T2.psr.nodes('EVENTS/EVENT') as T4(psr)
			print 'step 1'
			SELECT * INTO TMPO FROM #temppsrorder
			DECLARE @ORDERCOUNT INT =0 , @EVENTCOUNT INT = 0
			PRINT 'STEP 1.3'
			select @ORDERCOUNT = COUNT(*) from psr_order po, #temppsrorder t where
					 po.psr_order_id = t.PSR_ORDER_ID and 
					 (
							   po.CUST_FTY_ID                             <>          t.CUST_FTY_ID                      	
							OR po.FACTORY_REF                             <>          t.FACTORY_REF                      
							OR po.FABRIC_CODE                             <>          t.FABRIC_CODE                      
							OR po.FIBER_CONTENT                           <>          t.FIBER_CONTENT                    
							OR po.FABRIC_MILL                             <>          t.FABRIC_MILL                      
							OR po.FAB_TRIM_ORDER                          <>          t.FAB_TRIM_ORDER                   
							OR po.COLOR_CODE_PATRN                        <>          t.COLOR_CODE_PATRN                 
							OR po.GRAPHIC_NAME                            <>          t.GRAPHIC_NAME                     
							OR po.ORDER_TYPE                              <>          t.ORDER_TYPE                       
							OR po.NDC_WK                                  <>          t.NDC_WK                           
							OR po.APPR_FABRIC_YARN_LOT                    <>          t.APPR_FABRIC_YARN_LOT             
							OR po.EXTRA_PROCESS                           <>          t.EXTRA_PROCESS                    
							OR po.ACTUAL_POUNDAGE_AVAILABLE               <>          TRY_PARSE(t.ACTUAL_POUNDAGE_AVAILABLE as  numeric(10,2))      
							OR po.CARE_LABEL_CODE                         <>          t.CARE_LABEL_CODE                  
							OR po.GAUGE                                   <>          t.GAUGE        
							OR po.KNITTING_PCS                            <>          TRY_PARSE(t.KNITTING_PCS as  numeric(10,2))                     
							OR PO.KNITTING								  <>          TRY_PARSE(T.KNITTING as  numeric(10,2))                         
							OR po.LINKING_PCS                             <>          TRY_PARSE(t.LINKING_PCS as  int)                    
							OR PO.LINKING							      <>          TRY_PARSE(T.LINKING as  numeric(10,2))   							
							OR po.SEWING_LINES                            <>          TRY_PARSE(t.SEWING_LINES as  int)                     
							OR po.KNITTING_MACHINES                       <>          TRY_PARSE(t.KNITTING_MACHINES as  int)                
							OR po.LINKING_MACHINES                        <>          TRY_PARSE(t.LINKING_MACHINES as  int)                
							OR po.DAILY_OUTPUT_PER_LINE                   <>          TRY_PARSE(t.DAILY_OUTPUT_PER_LINE as  int)          
							OR isnull(po.FABRIC_EST_DATE,@mindate)        <>          isnull(t.FABRIC_EST_DATE,@mindate)                  
							OR po.FABRIC_TEST_REPORT_NUMERIC              <>          t.FABRIC_TEST_REPORT_NUMERIC       
							OR isnull(po.GARMENT_TEST_ACTUAL,@mindate)    <>          isnull(t.GARMENT_TEST_ACTUAL,@mindate)              
							OR po.PACK_TYPE                               <>          t.PACK_TYPE                        
							OR isnull(po.PACKAGING_READY_DATE,@mindate)   <>          isnull(t.PACKAGING_READY_DATE,@mindate)             
							OR po.COMMENT                                 <>          t.COMMENT                          
							OR po.TICKET_COLOR_CODE                       <>          t.TICKET_COLOR_CODE                
							OR po.TICKET_STYLE_NUMERIC                    <>          t.TICKET_STYLE_NUMERIC             
							OR po.CUT_APPR_LETTER						  <>          t.CUT_APPR_LETTER					
							OR po.CPO_PACK_TYPE						      <>          t.CPO_PACK_TYPE					
							OR ISNULL(po.[IMAGE],'')					  <>          ISNULL(t.[IMAGE],'')							
							OR ISNULL(po.GARMENT_TEST_REPORT_NO,'')		  <>          ISNULL(t.GARMENT_TEST_REPORT_NO,'')			
							OR ISNULL(po.PLM_NO,'')	  						<>                ISNULL(t.PLM_NO,'')							  
							OR po.WASH_YN		  							<>                t.WASH_YN
							OR ISNULL(po.WASH_FACILITY,'')		  			<>                ISNULL(t.WASH_FACILITY,'')	
							OR ISNULL(po.FABRIC_SHIP_MODE,'')		  		<>                ISNULL(t.FABRIC_SHIP_MODE,'')	
							OR ISNULL(po.TECH_PACK_RECEIVED_DATE,@mindate)	<>                ISNULL(t.TECH_PACK_RECEIVED_DATE,@mindate)	
							OR ISNULL(po.SAMPLE_MATERIAL_STATUS,'')		  	<>                ISNULL(t.SAMPLE_MATERIAL_STATUS,'')
							OR po.RETAIL_PRICE									  <>          TRY_PARSE(t.RETAIL_PRICE as  numeric(30,6)) 
							OR ISNULL(po.FABRIC_PP_DATE,@mindate)				  <>          ISNULL(t.FABRIC_PP_DATE,@mindate)
							OR po.DELIVERY_MONTH								  <>          t.DELIVERY_MONTH 
							OR ISNULL(po.CPO_ACC_DATE_BY_VENDOR,@mindate)	      <>          ISNULL(t.CPO_ACC_DATE_BY_VENDOR ,@mindate)
							OR po.DIST_CHANNEL 									  <>               t.DIST_CHANNEL 			
							OR po.FLOOR_SET 									  <>               t.FLOOR_SET 			
							OR po.STYLE_AT_RISK  								  <>               t.STYLE_AT_RISK  		
							OR ISNULL(po.SAMPLE_MERCH_ETA 		,@mindate)        <>               ISNULL(t.SAMPLE_MERCH_ETA 	,@mindate)	
							OR ISNULL(po.SAMPLE_FLOOR_SET_ETA 	,@mindate)        <>               ISNULL(t.SAMPLE_FLOOR_SET_ETA 	,@mindate)
							OR ISNULL(po.SAMPLE_DCOM_ETA 		,@mindate)        <>               ISNULL(t.SAMPLE_DCOM_ETA ,@mindate)	
							OR po.SAMPLE_MAILER  								  <>               t.SAMPLE_MAILER  		
							OR ISNULL(po.SAMPLE_MAILER_ETA,@mindate) 		      <>               ISNULL(t.SAMPLE_MAILER_ETA ,@mindate)														
					 ) and
					 po.modify_ts > @searchdate
			PRINT 'STEP 1.4'
            SELECT * INTO TMPE FROM #temppsrevents
			Select @EVENTCOUNT = COUNT(*) from psr_EVENTS po, #temppsrevents t 
					where po.psr_order_id = t.PSR_ORDER_ID and po.EVENT_CODE = t.EVENT_CODE
					and ( isnull(po.REVISED_DATE,@mindate) <> isnull(t.REVISED_DATE,@mindate) 
					OR isnull(po.ACTUAL_DATE,@mindate) <> isnull(t.ACTUAL_DATE,@mindate))
					and po.modify_ts > @searchdate
			SELECT 'COUNTS ',@ORDERCOUNT, @EVENTCOUNT
		
			IF @ORDERCOUNT > 0 OR @EVENTCOUNT > 0
			begin
				set @message = 'FAILURE - This order has been updated recently.  Please refresh you screen and try again'
				--  SELECT @message AS RESULT
			end
			else
			begin
			print 'step 2'
				SELECT o.PSR_ORDER_ID  , 
				(SELECT 
							PO.PSR_ORDER_ID							   , 
							po.CUST_FTY_ID                             ,
							po.FACTORY_REF                             ,
							po.FABRIC_CODE                             ,
							po.FIBER_CONTENT                           ,
							po.FABRIC_MILL                             ,
							po.FAB_TRIM_ORDER                          ,
							po.COLOR_CODE_PATRN                        ,
							po.GRAPHIC_NAME                            ,
							po.ORDER_TYPE                              ,
							po.NDC_WK                                  ,
							po.APPR_FABRIC_YARN_LOT                    ,
							po.EXTRA_PROCESS                           ,
							po.ACTUAL_POUNDAGE_AVAILABLE               ,
							po.CARE_LABEL_CODE                         ,
							po.GAUGE                                   ,
							po.KNITTING_PCS                            ,
							po.KNITTING                                ,
							po.LINKING_PCS                             ,
							PO.LINKING                                 ,
							po.SEWING_LINES                            ,
							po.KNITTING_MACHINES                       ,
							po.LINKING_MACHINES                        ,
							po.DAILY_OUTPUT_PER_LINE                   ,
							po.FABRIC_EST_DATE                         ,
							po.FABRIC_TEST_REPORT_NUMERIC              ,
							po.GARMENT_TEST_ACTUAL                     ,
							po.PACK_TYPE                               ,
							po.PACKAGING_READY_DATE                    ,
							po.COMMENT                                 ,
							po.TICKET_COLOR_CODE                       ,
							po.TICKET_STYLE_NUMERIC                    ,
							po.CUT_APPR_LETTER						   ,
							po.CPO_PACK_TYPE						   ,
							po.[IMAGE]								   ,
							po.GARMENT_TEST_REPORT_NO				   ,
							po.PLM_NO 						,
							po.WASH_YN 						,
							po.WASH_FACILITY 				,
							po.FABRIC_SHIP_MODE				,
							po.TECH_PACK_RECEIVED_DATE 		,
							po.SAMPLE_MATERIAL_STATUS 		,
							po.RETAIL_PRICE					,
							po.FABRIC_PP_DATE				,
							po.DELIVERY_MONTH				,
							po.CPO_ACC_DATE_BY_VENDOR		,
							po.DIST_CHANNEL 				,
							po.FLOOR_SET 					,
							po.STYLE_AT_RISK  				,
							po.SAMPLE_MERCH_ETA 			,
							po.SAMPLE_FLOOR_SET_ETA 		,
							po.SAMPLE_DCOM_ETA 				,
							po.SAMPLE_MAILER  				,	
							po.SAMPLE_MAILER_ETA 			,						
							(select (			SELECT E.PSR_ORDER_ID, E.EVENT_CODE, E.PLANNED_DATE, E.REVISED_DATE, E.ACTUAL_DATE FROM 
										PSR_EVENTS E, #temppsrevents TE
										WHERE E.PSR_ORDER_ID = TE.PSR_ORDER_ID
										AND E.EVENT_CODE = TE.EVENT_CODE   
										AND E.PSR_ORDER_ID = O.PSR_ORDER_ID FOR XML PATH ('EVENT') , type) ) 			
				 FROM PSR_ORDER po WHERE po.PSR_ORDER_ID = o.PSR_ORDER_ID   FOR xml path(''), root('BEFORE'), type) psrorder
					   INTO #beforeorder
				FROM PSR_ORDER o, #temppsrorder t where o.PSR_ORDER_ID = t.PSR_ORDER_ID

				update po set 
				po.CUST_FTY_ID                       =  isnull(pe.CUST_FTY_ID                    ,po.CUST_FTY_ID),
				po.FACTORY_REF                       =  isnull(pe.FACTORY_REF                    ,po.FACTORY_REF),
				po.FABRIC_CODE                       =  isnull(pe.FABRIC_CODE                    ,po.FABRIC_CODE),
				po.FIBER_CONTENT                     =  isnull(pe.FIBER_CONTENT                  ,po.FIBER_CONTENT),
				po.FABRIC_MILL                       =  isnull(pe.FABRIC_MILL                    ,po.FABRIC_MILL),
				po.FAB_TRIM_ORDER                    =  isnull(pe.FAB_TRIM_ORDER                 ,po.FAB_TRIM_ORDER),
				po.COLOR_CODE_PATRN                  =  isnull(pe.COLOR_CODE_PATRN               ,po.COLOR_CODE_PATRN),
				po.GRAPHIC_NAME                      =  isnull(pe.GRAPHIC_NAME                   ,po.GRAPHIC_NAME),
				po.ORDER_TYPE                        =  isnull(pe.ORDER_TYPE                     ,po.ORDER_TYPE),
				po.NDC_WK                            =  isnull(pe.NDC_WK                         ,po.NDC_WK),
				po.APPR_FABRIC_YARN_LOT              =  isnull(pe.APPR_FABRIC_YARN_LOT           ,po.APPR_FABRIC_YARN_LOT),
				po.EXTRA_PROCESS                     =  isnull(pe.EXTRA_PROCESS                  ,po.EXTRA_PROCESS),
				po.ACTUAL_POUNDAGE_AVAILABLE         =  TRY_PARSE(PE.ACTUAL_POUNDAGE_AVAILABLE as  numeric(10,2)) ,
				po.CARE_LABEL_CODE                   =  isnull(pe.CARE_LABEL_CODE                ,po.CARE_LABEL_CODE),
				po.GAUGE                             =  isnull(pe.GAUGE                          ,po.GAUGE),
				po.KNITTING_PCS                      =  TRY_PARSE(PE.KNITTING_PCS as  int)  ,
				po.KNITTING                          =  TRY_PARSE(PE.KNITTING as  numeric(10,2))  ,
				po.LINKING_PCS                       =  TRY_PARSE(PE.LINKING_PCS as  int)  ,
				PO.LINKING							 =  TRY_PARSE(PE.LINKING as  numeric(10,2))  ,
				po.SEWING_LINES                      =  TRY_PARSE(PE.SEWING_LINES as  int)  ,
				po.KNITTING_MACHINES                 =  TRY_PARSE(PE.KNITTING_MACHINES as  int)  ,
				po.LINKING_MACHINES                  =  TRY_PARSE(PE.LINKING_MACHINES as  int)  ,
				po.DAILY_OUTPUT_PER_LINE             =  TRY_PARSE(PE.DAILY_OUTPUT_PER_LINE as  int)  ,
				po.FABRIC_EST_DATE                   =  isnull(pe.FABRIC_EST_DATE                ,po.FABRIC_EST_DATE),
				po.FABRIC_TEST_REPORT_NUMERIC        =  isnull(pe.FABRIC_TEST_REPORT_NUMERIC     ,po.FABRIC_TEST_REPORT_NUMERIC),
				po.GARMENT_TEST_ACTUAL               =  isnull(pe.GARMENT_TEST_ACTUAL            ,po.GARMENT_TEST_ACTUAL),
				po.PACK_TYPE                         =  isnull(pe.PACK_TYPE                      ,po.PACK_TYPE),
				po.PACKAGING_READY_DATE              =  isnull(pe.PACKAGING_READY_DATE           ,po.PACKAGING_READY_DATE),
				po.COMMENT                           =  isnull(pe.COMMENT                        ,po.COMMENT),
				po.TICKET_COLOR_CODE                 =  isnull(pe.TICKET_COLOR_CODE              ,po.TICKET_COLOR_CODE),
				po.TICKET_STYLE_NUMERIC              =  isnull(pe.TICKET_STYLE_NUMERIC           ,po.TICKET_STYLE_NUMERIC),
				po.[IMAGE]							 =  isnull(pe.[IMAGE]						 ,po.[IMAGE]),
				po.CUT_APPR_LETTER					 =  isnull(pe.CUT_APPR_LETTER				 ,po.CUT_APPR_LETTER),
				po.CPO_PACK_TYPE					 =  isnull(pe.CPO_PACK_TYPE					 ,po.CPO_PACK_TYPE),
				po.GARMENT_TEST_REPORT_NO			 =  isnull(pe.GARMENT_TEST_REPORT_NO		,po.GARMENT_TEST_REPORT_NO),
				po.PLM_NO 							 =  isnull(pe.PLM_NO 							,po.PLM_NO 						),	
				po.WASH_YN 							 =  isnull(pe.WASH_YN                        ,po.WASH_YN),
				po.WASH_FACILITY 					 =  isnull(pe.WASH_FACILITY 					,po.WASH_FACILITY 				),	
				po.FABRIC_SHIP_MODE					 =  isnull(pe.FABRIC_SHIP_MODE					,po.FABRIC_SHIP_MODE			),	
				po.TECH_PACK_RECEIVED_DATE 			 =  isnull(pe.TECH_PACK_RECEIVED_DATE 			,po.TECH_PACK_RECEIVED_DATE 	),	
				po.SAMPLE_MATERIAL_STATUS 			 =  isnull(pe.SAMPLE_MATERIAL_STATUS 			,po.SAMPLE_MATERIAL_STATUS 		),
				PO.RETAIL_PRICE						 =  TRY_PARSE(PE.RETAIL_PRICE as  numeric(30,6)) ,
				po.FABRIC_PP_DATE					 =  isnull(pe.FABRIC_PP_DATE		               	,po.FABRIC_PP_DATE		),
				po.DELIVERY_MONTH 					 =  isnull(pe.DELIVERY_MONTH 		                ,po.DELIVERY_MONTH 		),
				po.CPO_ACC_DATE_BY_VENDOR			=  isnull(pe.CPO_ACC_DATE_BY_VENDOR                 ,po.CPO_ACC_DATE_BY_VENDOR),
				po.DIST_CHANNEL 					=  isnull(pe.DIST_CHANNEL 			                ,po.DIST_CHANNEL 			),
				po.FLOOR_SET 						=  isnull(pe.FLOOR_SET 			                	,po.FLOOR_SET 			),
				po.STYLE_AT_RISK  					=  isnull(pe.STYLE_AT_RISK  		                ,po.STYLE_AT_RISK  		),
				po.SAMPLE_MERCH_ETA 				=  isnull(pe.SAMPLE_MERCH_ETA 		                ,po.SAMPLE_MERCH_ETA 		),
				po.SAMPLE_FLOOR_SET_ETA 			=  isnull(pe.SAMPLE_FLOOR_SET_ETA 	                ,po.SAMPLE_FLOOR_SET_ETA 	),
				po.SAMPLE_DCOM_ETA 					=  isnull(pe.SAMPLE_DCOM_ETA 		                ,po.SAMPLE_DCOM_ETA 		),
				po.SAMPLE_MAILER  					=  isnull(pe.SAMPLE_MAILER  		                ,po.SAMPLE_MAILER  		),
				po.SAMPLE_MAILER_ETA 				=  isnull(pe.SAMPLE_MAILER_ETA 	                	,po.SAMPLE_MAILER_ETA 	),				
				po.MODIFY_TS						 = 
				CASE WHEN 
		    			 (
							   ISNULL(po.CUST_FTY_ID      ,'')                       <>          ISNULL(pe.CUST_FTY_ID       ,'')               	
							OR ISNULL(po.FACTORY_REF      ,'')                       <>          ISNULL(pe.FACTORY_REF       ,'')               
							OR ISNULL(po.FABRIC_CODE      ,'')                       <>          ISNULL(pe.FABRIC_CODE       ,'')               
							OR ISNULL(po.FIBER_CONTENT    ,'')                       <>          ISNULL(pe.FIBER_CONTENT     ,'')               
							OR ISNULL(po.FABRIC_MILL      ,'')                       <>          ISNULL(pe.FABRIC_MILL       ,'')               
							OR ISNULL(po.FAB_TRIM_ORDER   ,'')                       <>          ISNULL(pe.FAB_TRIM_ORDER    ,'')               
							OR ISNULL(po.COLOR_CODE_PATRN ,'')                       <>          ISNULL(pe.COLOR_CODE_PATRN  ,'')               
							OR ISNULL(po.GRAPHIC_NAME     ,'')                       <>          ISNULL(pe.GRAPHIC_NAME      ,'')               
							OR ISNULL(po.ORDER_TYPE       ,'')                       <>          ISNULL(pe.ORDER_TYPE         ,'')                   
							OR ISNULL(po.NDC_WK           ,'')                       <>          ISNULL(pe.NDC_WK                ,'')           
							OR ISNULL(po.APPR_FABRIC_YARN_LOT,'')                    <>          ISNULL(pe.APPR_FABRIC_YARN_LOT  ,'')           
							OR ISNULL(po.EXTRA_PROCESS    ,'')                       <>          ISNULL(pe.EXTRA_PROCESS         ,'')           
							OR po.ACTUAL_POUNDAGE_AVAILABLE							 <>          TRY_PARSE(pe.ACTUAL_POUNDAGE_AVAILABLE as  numeric(10,2))    
							OR ISNULL(po.CARE_LABEL_CODE ,'')                        <>          ISNULL(pe.CARE_LABEL_CODE ,'')                 
							OR ISNULL(po.GAUGE           ,'')                        <>          ISNULL(pe.GAUGE           ,'')
							OR po.KNITTING_PCS                            <>          TRY_PARSE(pe.KNITTING_PCS as  numeric(10,2))                     
							OR PO.KNITTING								  <>          TRY_PARSE(pe.KNITTING as  numeric(10,2))                         
							OR po.LINKING_PCS                             <>          TRY_PARSE(pe.LINKING_PCS as  int)                    
							OR PO.LINKING							      <>          TRY_PARSE(pe.LINKING as  numeric(10,2))   							
							OR po.SEWING_LINES                            <>          TRY_PARSE(pe.SEWING_LINES as  int)                     
							OR po.KNITTING_MACHINES                       <>          TRY_PARSE(pe.KNITTING_MACHINES as  int)                
							OR po.LINKING_MACHINES                        <>          TRY_PARSE(pe.LINKING_MACHINES as  int)                
							OR po.DAILY_OUTPUT_PER_LINE                   <>          TRY_PARSE(pe.DAILY_OUTPUT_PER_LINE as  int)          
							OR isnull(po.FABRIC_EST_DATE,@mindate)        <>          isnull(pe.FABRIC_EST_DATE,@mindate)                  
							OR po.FABRIC_TEST_REPORT_NUMERIC              <>          pe.FABRIC_TEST_REPORT_NUMERIC       
							OR isnull(po.GARMENT_TEST_ACTUAL,@mindate)    <>          isnull(pe.GARMENT_TEST_ACTUAL,@mindate)              
							OR ISNULL(po.PACK_TYPE ,'')                              <>          ISNULL(pe.PACK_TYPE ,'')                       
							OR isnull(po.PACKAGING_READY_DATE,@mindate)   <>          isnull(pe.PACKAGING_READY_DATE,@mindate)             
							OR ISNULL(po.COMMENT              ,'')                   <>          ISNULL(pe.COMMENT              ,'')            
							OR ISNULL(po.TICKET_COLOR_CODE    ,'')                   <>          ISNULL(pe.TICKET_COLOR_CODE    ,'')            
							OR ISNULL(po.TICKET_STYLE_NUMERIC ,'')                   <>          ISNULL(pe.TICKET_STYLE_NUMERIC ,'')            
							OR ISNULL(po.CUT_APPR_LETTER		,'')				  <>         ISNULL( pe.CUT_APPR_LETTER		,'')			
							OR ISNULL(po.CPO_PACK_TYPE	,'')					      <>          ISNULL(pe.CPO_PACK_TYPE,'')					
							OR ISNULL(po.[IMAGE],'')					  <>          ISNULL(pe.[IMAGE],'')							
							OR ISNULL(po.GARMENT_TEST_REPORT_NO,'')		  <>          ISNULL(pe.GARMENT_TEST_REPORT_NO,'')				
							OR ISNULL(po.PLM_NO,'')	  						<>                ISNULL(pe.PLM_NO,'')							  
							OR ISNULL(po.WASH_YN,'')		  			    <>                ISNULL(pe.WASH_YN,'')	
							OR ISNULL(po.WASH_FACILITY,'')		  			<>                ISNULL(pe.WASH_FACILITY,'')	
							OR ISNULL(po.FABRIC_SHIP_MODE,'')		  		<>                ISNULL(pe.FABRIC_SHIP_MODE,'')	
							OR ISNULL(po.TECH_PACK_RECEIVED_DATE,@mindate)	<>                ISNULL(pe.TECH_PACK_RECEIVED_DATE,@mindate)	
							OR ISNULL(po.SAMPLE_MATERIAL_STATUS,'')		  	<>                ISNULL(pe.SAMPLE_MATERIAL_STATUS,'')	
							OR po.RETAIL_PRICE 					  			<>                TRY_PARSE(pe.RETAIL_PRICE as  numeric(30,6))   
							OR ISNULL(po.FABRIC_PP_DATE	,@mindate)		  	<>                ISNULL(pe.FABRIC_PP_DATE,@mindate)
							OR ISNULL(po.DELIVERY_MONTH 		,'')		<>                ISNULL(pe.DELIVERY_MONTH 		,'')
							OR ISNULL(po.CPO_ACC_DATE_BY_VENDOR,@mindate)	<>                ISNULL(pe.CPO_ACC_DATE_BY_VENDOR,@mindate)
							OR ISNULL(po.DIST_CHANNEL 			,'')		<>                ISNULL(pe.DIST_CHANNEL 			,'')
							OR ISNULL(po.FLOOR_SET 			,'')		  	<>                ISNULL(pe.FLOOR_SET 			,'')
							OR ISNULL(po.STYLE_AT_RISK  		,'')		<>                ISNULL(pe.STYLE_AT_RISK  		,'')
							OR ISNULL(po.SAMPLE_MERCH_ETA 		,@mindate)		<>            ISNULL(pe.SAMPLE_MERCH_ETA 		,@mindate)
							OR ISNULL(po.SAMPLE_FLOOR_SET_ETA 	,@mindate)		<>            ISNULL(pe.SAMPLE_FLOOR_SET_ETA 	,@mindate)
							OR ISNULL(po.SAMPLE_DCOM_ETA 		,@mindate)		<>            ISNULL(pe.SAMPLE_DCOM_ETA 		,@mindate)
							OR ISNULL(po.SAMPLE_MAILER  		,'')		<>                ISNULL(pe.SAMPLE_MAILER  		,'')
							OR ISNULL(po.SAMPLE_MAILER_ETA 	,@mindate)		  	<>            ISNULL(pe.SAMPLE_MAILER_ETA 	,@mindate)							
					 ) THEN 
				 @modifydate ELSE PO.MODIFY_TS END,
				po.MODIFY_USER						 =  
				CASE WHEN 
		    			 (
								ISNULL(po.CUST_FTY_ID      ,'')                       <>          ISNULL(pe.CUST_FTY_ID       ,'')               	
							OR ISNULL(po.FACTORY_REF      ,'')                       <>          ISNULL(pe.FACTORY_REF       ,'')               
							OR ISNULL(po.FABRIC_CODE      ,'')                       <>          ISNULL(pe.FABRIC_CODE       ,'')               
							OR ISNULL(po.FIBER_CONTENT    ,'')                       <>          ISNULL(pe.FIBER_CONTENT     ,'')               
							OR ISNULL(po.FABRIC_MILL      ,'')                       <>          ISNULL(pe.FABRIC_MILL       ,'')               
							OR ISNULL(po.FAB_TRIM_ORDER   ,'')                       <>          ISNULL(pe.FAB_TRIM_ORDER    ,'')               
							OR ISNULL(po.COLOR_CODE_PATRN ,'')                       <>          ISNULL(pe.COLOR_CODE_PATRN  ,'')               
							OR ISNULL(po.GRAPHIC_NAME     ,'')                       <>          ISNULL(pe.GRAPHIC_NAME      ,'')               
							OR ISNULL(po.ORDER_TYPE       ,'')                       <>          ISNULL(pe.ORDER_TYPE         ,'')                   
							OR ISNULL(po.NDC_WK           ,'')                       <>          ISNULL(pe.NDC_WK                ,'')           
							OR ISNULL(po.APPR_FABRIC_YARN_LOT,'')                    <>          ISNULL(pe.APPR_FABRIC_YARN_LOT  ,'')           
							OR ISNULL(po.EXTRA_PROCESS    ,'')                       <>          ISNULL(pe.EXTRA_PROCESS         ,'')           
							OR po.ACTUAL_POUNDAGE_AVAILABLE               <>          TRY_PARSE(pe.ACTUAL_POUNDAGE_AVAILABLE as  numeric(10,2))    
							OR ISNULL(po.CARE_LABEL_CODE ,'')                        <>          ISNULL(pe.CARE_LABEL_CODE ,'')                 
							OR ISNULL(po.GAUGE           ,'')                        <>          ISNULL(pe.GAUGE           ,'')
							OR po.KNITTING_PCS                            <>          TRY_PARSE(pe.KNITTING_PCS as  numeric(10,2))                     
							OR PO.KNITTING								  <>          TRY_PARSE(pe.KNITTING as  numeric(10,2))                         
							OR po.LINKING_PCS                             <>          TRY_PARSE(pe.LINKING_PCS as  int)                    
							OR PO.LINKING							      <>          TRY_PARSE(pe.LINKING as  numeric(10,2))   							
							OR po.SEWING_LINES                            <>          TRY_PARSE(pe.SEWING_LINES as  int)                     
							OR po.KNITTING_MACHINES                       <>          TRY_PARSE(pe.KNITTING_MACHINES as  int)                
							OR po.LINKING_MACHINES                        <>          TRY_PARSE(pe.LINKING_MACHINES as  int)                
							OR po.DAILY_OUTPUT_PER_LINE                   <>          TRY_PARSE(pe.DAILY_OUTPUT_PER_LINE as  int)          
							OR isnull(po.FABRIC_EST_DATE,@mindate)        <>          isnull(pe.FABRIC_EST_DATE,@mindate)                  
							OR po.FABRIC_TEST_REPORT_NUMERIC              <>          pe.FABRIC_TEST_REPORT_NUMERIC       
							OR isnull(po.GARMENT_TEST_ACTUAL,@mindate)    <>          isnull(pe.GARMENT_TEST_ACTUAL,@mindate)              
							OR ISNULL(po.PACK_TYPE ,'')                              <>          ISNULL(pe.PACK_TYPE ,'')                       
							OR isnull(po.PACKAGING_READY_DATE,@mindate)   <>          isnull(pe.PACKAGING_READY_DATE,@mindate)             
							OR ISNULL(po.COMMENT              ,'')                   <>          ISNULL(pe.COMMENT              ,'')            
							OR ISNULL(po.TICKET_COLOR_CODE    ,'')                   <>          ISNULL(pe.TICKET_COLOR_CODE    ,'')            
							OR ISNULL(po.TICKET_STYLE_NUMERIC ,'')                   <>          ISNULL(pe.TICKET_STYLE_NUMERIC ,'')            
							OR ISNULL(po.CUT_APPR_LETTER		,'')				  <>         ISNULL( pe.CUT_APPR_LETTER		,'')			
							OR ISNULL(po.CPO_PACK_TYPE	,'')					      <>          ISNULL(pe.CPO_PACK_TYPE,'')					
							OR ISNULL(po.[IMAGE],'')					  <>          ISNULL(pe.[IMAGE],'')							
							OR ISNULL(po.GARMENT_TEST_REPORT_NO,'')		  <>          ISNULL(pe.GARMENT_TEST_REPORT_NO,'')				
							OR ISNULL(po.GARMENT_TEST_REPORT_NO,'')		  <>          ISNULL(pe.GARMENT_TEST_REPORT_NO,'')				
							OR ISNULL(po.PLM_NO,'')	  						<>                ISNULL(pe.PLM_NO,'')							  
							OR ISNULL(po.WASH_YN,'')		  			    <>                ISNULL(pe.WASH_YN,'')	
							OR ISNULL(po.WASH_FACILITY,'')		  			<>                ISNULL(pe.WASH_FACILITY,'')	
							OR ISNULL(po.FABRIC_SHIP_MODE,'')		  		<>                ISNULL(pe.FABRIC_SHIP_MODE,'')	
							OR ISNULL(po.TECH_PACK_RECEIVED_DATE,@mindate)	<>                ISNULL(pe.TECH_PACK_RECEIVED_DATE,@mindate)	
							OR ISNULL(po.SAMPLE_MATERIAL_STATUS,'')		  	<>                ISNULL(pe.SAMPLE_MATERIAL_STATUS,'')								
							OR po.RETAIL_PRICE 					  			<>                TRY_PARSE(pe.RETAIL_PRICE as  numeric(30,6))   
							OR ISNULL(po.FABRIC_PP_DATE	,@mindate)		  	<>                ISNULL(pe.FABRIC_PP_DATE,@mindate)
							OR ISNULL(po.DELIVERY_MONTH 		,'')		<>                ISNULL(pe.DELIVERY_MONTH 		,'')
							OR ISNULL(po.CPO_ACC_DATE_BY_VENDOR,@mindate)	<>                ISNULL(pe.CPO_ACC_DATE_BY_VENDOR,@mindate)
							OR ISNULL(po.DIST_CHANNEL 			,'')		<>                ISNULL(pe.DIST_CHANNEL 			,'')
							OR ISNULL(po.FLOOR_SET 			,'')		  	<>                ISNULL(pe.FLOOR_SET 			,'')
							OR ISNULL(po.STYLE_AT_RISK  		,'')		<>                ISNULL(pe.STYLE_AT_RISK  		,'')
							OR ISNULL(po.SAMPLE_MERCH_ETA 		,@mindate)		<>            ISNULL(pe.SAMPLE_MERCH_ETA 		,@mindate)
							OR ISNULL(po.SAMPLE_FLOOR_SET_ETA 	,@mindate)		<>            ISNULL(pe.SAMPLE_FLOOR_SET_ETA 	,@mindate)
							OR ISNULL(po.SAMPLE_DCOM_ETA 		,@mindate)		<>            ISNULL(pe.SAMPLE_DCOM_ETA 		,@mindate)
							OR ISNULL(po.SAMPLE_MAILER  		,'')		<>                ISNULL(pe.SAMPLE_MAILER  		,'')	
							OR ISNULL(po.SAMPLE_MAILER_ETA 	,@mindate)		  	<>            ISNULL(pe.SAMPLE_MAILER_ETA 	,@mindate)						
					 ) THEN 								
				 @ModifiedBy ELSE PO.MODIFY_USER END				
				from psr_order po, #temppsrorder pe
				where po.psr_order_id = pe.psr_order_id

				update po 
					set PO.PACKAGING_READY_DATE = CASE WHEN PO.PACKAGING_READY_DATE = @mindate THEN NULL ELSE PO.PACKAGING_READY_DATE END,
						PO.FABRIC_EST_DATE = CASE WHEN PO.FABRIC_EST_DATE = @mindate THEN NULL ELSE PO.FABRIC_EST_DATE END  ,
						PO.GARMENT_TEST_ACTUAL = CASE WHEN PO.GARMENT_TEST_ACTUAL = @mindate THEN NULL ELSE PO.GARMENT_TEST_ACTUAL END ,
						PO.TECH_PACK_RECEIVED_DATE = CASE WHEN PO.TECH_PACK_RECEIVED_DATE = @mindate THEN NULL ELSE PO.TECH_PACK_RECEIVED_DATE END ,
						PO.FABRIC_PP_DATE 			 = CASE WHEN PO.FABRIC_PP_DATE 			 = @mindate THEN NULL ELSE PO.FABRIC_PP_DATE 			 END,
						PO.CPO_ACC_DATE_BY_VENDOR	 = CASE WHEN PO.CPO_ACC_DATE_BY_VENDOR	 = @mindate THEN NULL ELSE PO.CPO_ACC_DATE_BY_VENDOR	 END,
						PO.SAMPLE_MERCH_ETA 		 = CASE WHEN PO.SAMPLE_MERCH_ETA 		 = @mindate THEN NULL ELSE PO.SAMPLE_MERCH_ETA 			 END,
						PO.SAMPLE_FLOOR_SET_ETA 	 = CASE WHEN PO.SAMPLE_FLOOR_SET_ETA 	 = @mindate THEN NULL ELSE PO.SAMPLE_FLOOR_SET_ETA 	 	 END,
						PO.SAMPLE_DCOM_ETA 			 = CASE WHEN PO.SAMPLE_DCOM_ETA 		 = @mindate THEN NULL ELSE PO.SAMPLE_DCOM_ETA 		 END ,
						PO.SAMPLE_MAILER_ETA 		 = CASE WHEN PO.SAMPLE_MAILER_ETA 		 = @mindate THEN NULL ELSE PO.SAMPLE_MAILER_ETA 		 END
				from psr_order po, #temppsrorder pe
				where po.psr_order_id = pe.psr_order_id




				update e
				set 
					e.PLANNED_DATE = isnull(te.PLANNED_DATE, e.PLANNED_DATE),
					e.REVISED_DATE = isnull(te.REVISED_DATE, e.REVISED_DATE),
					e.ACTUAL_DATE = isnull(te.ACTUAL_DATE, e.ACTUAL_DATE),
					e.HIGHLIGHT_REVISED_DATE = 
					 --CASE WHEN E.REVISED_DATE <> TE.REVISED_DATE AND @userrole IN ('PSR_VENDOR','BR_USER')  THEN 1 ELSE 0 END
					CASE WHEN ((E.REVISED_DATE <>  TE.REVISED_DATE) or (E.REVISED_DATE IS NULL and  TE.REVISED_DATE IS NOT NULL)) AND @userrole NOT IN ('PSR_VENDOR','BR_USER')  THEN 0
					     WHEN ((E.REVISED_DATE <>  TE.REVISED_DATE) or (E.REVISED_DATE IS NULL and  TE.REVISED_DATE IS NOT NULL)) AND @userrole IN ('PSR_VENDOR','BR_USER')  THEN 1
						 ELSE e.HIGHLIGHT_REVISED_DATE  END,
--					e.MODIFY_TS = @modifydate,
					e.MODIFY_TS = CASE WHEN ( isnull(e.REVISED_DATE,@mindate) <> isnull(te.REVISED_DATE,@mindate) OR isnull(e.ACTUAL_DATE,@mindate) <> isnull(te.ACTUAL_DATE,@mindate)) THEN @modifydate else e.MODIFY_TS END,
					e.MODIFY_USER = CASE WHEN ( isnull(e.REVISED_DATE,@mindate) <> isnull(te.REVISED_DATE,@mindate) OR isnull(e.ACTUAL_DATE,@mindate) <> isnull(te.ACTUAL_DATE,@mindate)) THEN @ModifiedBy else e.MODIFY_USER END
				from psr_events e, #temppsrevents te
				where e.psr_order_id = te.psr_order_id
				  and e.EVENT_CODE    = te.EVENT_CODE

				update e
				set 
					e.PLANNED_DATE = CASE WHEN e.PLANNED_DATE = @mindate THEN NULL ELSE e.PLANNED_DATE END,
					e.REVISED_DATE = CASE WHEN e.REVISED_DATE = @mindate THEN NULL ELSE e.REVISED_DATE END,
					e.ACTUAL_DATE = CASE WHEN e.ACTUAL_DATE = @mindate THEN NULL ELSE e.ACTUAL_DATE END
				from psr_events e, #temppsrevents te
				where e.psr_order_id = te.psr_order_id
				  and e.EVENT_CODE    = te.EVENT_CODE

				SELECT o.PSR_ORDER_ID  , 
				(SELECT 
							PO.PSR_ORDER_ID							   , 
							po.CUST_FTY_ID                             ,
							po.FACTORY_REF                             ,
							po.FABRIC_CODE                             ,
							po.FIBER_CONTENT                           ,
							po.FABRIC_MILL                             ,
							po.FAB_TRIM_ORDER                          ,
							po.COLOR_CODE_PATRN                        ,
							po.GRAPHIC_NAME                            ,
							po.ORDER_TYPE                              ,
							po.NDC_WK                                  ,
							po.APPR_FABRIC_YARN_LOT                    ,
							po.EXTRA_PROCESS                           ,
							po.ACTUAL_POUNDAGE_AVAILABLE               ,
							po.CARE_LABEL_CODE                         ,
							po.GAUGE                                   ,
							po.KNITTING_PCS                            ,
							po.KNITTING                                ,
							po.LINKING_PCS                             ,
							po.LINKING                                 ,
							po.SEWING_LINES                            ,
							po.KNITTING_MACHINES                       ,
							po.LINKING_MACHINES                        ,
							po.DAILY_OUTPUT_PER_LINE                   ,
							po.FABRIC_EST_DATE                         ,
							po.FABRIC_TEST_REPORT_NUMERIC              ,
							po.GARMENT_TEST_ACTUAL                     ,
							po.PACK_TYPE                               ,
							po.PACKAGING_READY_DATE                    ,
							po.COMMENT                                 ,
							po.TICKET_COLOR_CODE                       ,
							po.TICKET_STYLE_NUMERIC                    ,
							po.CUT_APPR_LETTER						   ,
							po.CPO_PACK_TYPE						   ,
							po.[IMAGE]								   ,
							po.GARMENT_TEST_REPORT_NO                  ,	
							po.PLM_NO 						,
							po.WASH_YN 						,
							po.WASH_FACILITY 				,
							po.FABRIC_SHIP_MODE				,
							po.TECH_PACK_RECEIVED_DATE 		,
							po.SAMPLE_MATERIAL_STATUS 		,
							po.RETAIL_PRICE 			,
							po.FABRIC_PP_DATE 			,
							po.DELIVERY_MONTH 			,
							po.CPO_ACC_DATE_BY_VENDOR	,
							po.DIST_CHANNEL 			,
							po.FLOOR_SET 				,
							po.STYLE_AT_RISK  			,
							po.SAMPLE_MERCH_ETA 		,
							po.SAMPLE_FLOOR_SET_ETA 	,
							po.SAMPLE_DCOM_ETA 		    ,	
							po.SAMPLE_MAILER  			,							
							po.SAMPLE_MAILER_ETA 		,							
							(select (SELECT E.PSR_ORDER_ID, E.EVENT_CODE, E.PLANNED_DATE, E.REVISED_DATE, E.ACTUAL_DATE FROM 
										PSR_EVENTS E, #temppsrevents TE
										WHERE E.PSR_ORDER_ID = TE.PSR_ORDER_ID
										AND E.EVENT_CODE = TE.EVENT_CODE   
										AND E.PSR_ORDER_ID = O.PSR_ORDER_ID FOR XML PATH ('EVENT') , type)) 			
				 FROM PSR_ORDER po WHERE po.PSR_ORDER_ID = o.PSR_ORDER_ID   FOR xml path(''), root('AFTER'), type) psrorder
					   INTO #afterorder

				FROM PSR_ORDER o, #temppsrorder t where o.PSR_ORDER_ID = t.PSR_ORDER_ID


				insert into PSR_change_log (timestamp,PSR_ORDER_ID,MODIFIED_BY,[LOG])
				select getdate(),B.PSR_ORDER_ID,@ModifiedBy, DBO.PSR_SAVEORDERDIFF(convert(xml,b.psrorder),convert(xml,a.psrorder),@ModifiedBy)
				from #beforeorder b, #afterorder a where a.PSR_ORDER_ID = b.psr_order_id
				AND dbo.COMPAREXML(B.psrorder, A.psrorder) = 1

			end
		END TRY
		
		BEGIN CATCH
				--PRINT 'STEP 3'
				PRINT ERROR_MESSAGE()
				 SET @message = 'FAILED.  ERROR = ' + ERROR_MESSAGE()
		END CATCH
		--SELECT @message AS RESULT
		set @result = @message
END


GO
/****** Object:  StoredProcedure [dbo].[PSR_SYNCORDERS]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[PSR_SYNCORDERS]
AS
BEGIN

		-- prior to this run, an extract has been run in bamboo rose the the extracted data is available in tempdb..tmp

			DECLARE @DATECREATED DATETIME = getdate()
		    DECLARE @LASTRUN DATETIME = getdate()
			DECLARE @INSERTED int = 0
			DECLARE @UPDATED int = 0	
    		DECLARE @MESSAGE VARCHAR(500)		
			DECLARE @RowId int
			DECLARE @mindate date = ''
	   
			-- EXEC PSR_CHECKDUPORDERS @DATECREATED

			DECLARE @MyTableVar table(  
				psr_order_id int NOT NULL,  
				OldComment varchar(250), 
				NewComment varchar(250),
				OldOCNO varchar(35),
				NewOCNO varchar(35),
				OldBrand varchar(35),
				NewBrand varchar(35),				
				OldStyle varchar(35),
				NewStyle varchar(35),
				OldPLActualGAC datetime,	
				NewPLActualGAC datetime		
		);  

		SELECT IDENTITY(int, 1, 1) AS RowID, * INTO #TMPPSR FROM TMPPSR ORDER BY MODIFY_TS

	    DECLARE PCUR CURSOR FOR SELECT ROWID FROM #TMPPSR

		OPEN pCur
			fetch next from pCur into @RowId
			while @@fetch_status=0 
			begin
						IF EXISTS (SELECT 1 
						FROM PSR_ORDER PO, #tmppsr pe  WHERE
							CK1=ISNULL(pe.PROFOMA_PO,'')+ '|'+ISNULL(pe.ORDER_NO,'')+ '|'+ISNULL(pe.PROD_OFFICE,'')+ '|'+ISNULL(pe.SUPPLIER,'')+ '|'+ISNULL(pe.VENDOR_NAME,'')
						AND	CK2=ISNULL(pe.MANUFACTURER,'')+ '|'+ISNULL(pe.FACTORY_NAME,'')+  '|' +  pe.ORIGIN_CNTRY +  '|'+ISNULL(pe.ORIGIN_COUNTRY,'')+'|'+ISNULL(pe.BRAND,'') 
						AND	CK3=ISNULL(pe.DEPT,'')+ '|'+ISNULL(pe.CATEGORY,'')+ '|'+ISNULL(pe.BUYER,'')+ '|'+ISNULL(pe.STYLE_NO,'')+ '|'+ISNULL(pe.STYLE_DESC,'')+ '|'+ISNULL(pe.COLOR_CODE,'') 
						AND	CK4=ISNULL(pe.PLAN_ID,'')+ '|'+ISNULL(pe.PRODUCT_TYPE,'') --+ '|'+ISNULL(pe.PROGRAM_NAME,'')
						AND	CK5=ISNULL(pe.GENDER,'') +  '|'-- +ISNULL(pe.CPO_SHIP_MODE,'') --+  '|'+ISNULL(pe.SEASON,'')
						AND PE.RowID = @RowId)
						BEGIN

							update po
								set
								PROD_OFFICE	 = pe.	PROD_OFFICE	,
								VENDOR_ID	 = pe.	SUPPLIER	,
								VENDOR_NAME	 = pe.	VENDOR_NAME	,
								FACTORY_ID	 = pe.	MANUFACTURER	,
								FACTORY_NAME	 = pe.	FACTORY_NAME	,
								COUNTRY_ORIGIN	 = pe.	ORIGIN_CNTRY	,
								BRAND	 = ISNULL(pe.BRAND,'')	,
								DEPT	 = pe.	DEPT	,
								CATEGORY = pe.CATEGORY,
								MERCHANT	 = pe.	BUYER	,
								STYLE 	 = pe.	STYLE_NO	,
								STYLE_DESC	 = pe.	STYLE_DESC	,
								FOB	 = pe.	FOB	,
								GROSS_SELL_PRICE	 = pe.	GROSS_SELL_PRICE	,
								MARGIN	 = pe.	MARGIN_PCT	,
								PRODUCT_TYPE	 = pe.	PRODUCT_TYPE	,
								PROGRAM_NAME	 = pe.	PROGRAM_NAME	,
								SHIP_MODE	 = pe.	TRANS_MODE	,
								SHIP_TO		 = pe.DELIVER_TO	,
								GENDER	 = pe.	GENDER	,
								ORDER_QTY	 = pe.	FINAL_QTY	,
								SHIPPED_QTY	 = pe.	SHIPPED_QTY	,
								OC_COMMIT	 = pe.	OC_NO	,
								CPO	 = pe.	SALES_ORDER_NO	,
								SEASON	 = pe.	SEASON	,
								NDC_DATE 	 = pe.	CUST_DEL_DATE	,
								VPO_NLT_GAC_DATE	 = pe.	NLT_GAC_DATE	,
								VPO_FTY_GAC_DATE	 = pe.	FTY_GAC_DATE	,
								KEY_ITEM	 = pe.	CPO_STYLE_DESC	,
								VPO_STATUS	 = pe.	VPO_HEAD_STATUS	,
								SO_ID		 = isnull(pe.SO_ID,0),
								VPO_LINE_STATUS	 = pe.	VPO_LINE_STATUS	,
								INTERFACE_TS	 = pe.	INTERFACE_TS	,
								CPO_ORDER_QTY	 = pe.	CPO_ORDER_QTY	,
								CPO_SHIP_MODE	 = pe.	CPO_SHIP_MODE	,
								CPO_COLOR_DESC	 = pe.	CPO_COLOR_DESC	,
								CPO_STYLE_DESC	 = ISNULL(pe.CPO_STYLE_DESC,''),
								CPO_PACK_TYPE    = substring(pe.CPO_PACK_TYPE,1,6),
								VPO_FTY_GAC_DATE_TEMP = CASE WHEN pe.FTY_GAC_DATE <> VPO_FTY_GAC_DATE THEN VPO_FTY_GAC_DATE ELSE VPO_FTY_GAC_DATE_TEMP END,
								PROFOMA_PO        =  PE.PROFOMA_PO,
								MODIFY_TS	 = pe.	MODIFY_TS	,
								MODIFY_USER	 = pe.	MODIFY_USER	 ,
								[ASN_ID]	 	  = pe.[ASN_ID]	 	,
								[ASN]	 		  = pe.[ASN_ID]	 	,																				 
								[ORIGIN_CNTRY]	  = pe.[ORIGIN_CNTRY]	,
								[PLAN_ID] 		  = pe.[PLAN_ID] 		,
								[OC_NO]           =  pe.[OC_NO] ,
								[COLOR_WASH_NAME] = pe.[COLOR_WASH_NAME],
								[INIT_FLOW] = pe.[INIT_FLOW],
								[INIT_FLOW_PREV] = CASE WHEN pe.[INIT_FLOW] <> PO.[INIT_FLOW] THEN PO.[INIT_FLOW] ELSE PO.[INIT_FLOW_PREV] END,
								CPO_GAC_DATE	 = CASE WHEN PE.CPO_GAC_DATE = @mindate THEN NULL ELSE PE.CPO_GAC_DATE END ,
								PL_ACTUAL_GAC    = CASE WHEN PE.PL_ACTUAL_GAC = @mindate THEN NULL ELSE PE.PL_ACTUAL_GAC END ,
								PL_SHIP_DATE    = CASE WHEN PE.PL_SHIP_DATE = @mindate THEN NULL ELSE PE.PL_SHIP_DATE END ,	
								[VPO_ORDER_TYPE]  = pe.[VPO_ORDER_TYPE],
								[SPEED_INDICATOR]   = pe.[VPO_SPEED_IND],
								[CPO_STYLE_NO]   = pe.[CPO_STYLE_NO],
								[REQUEST_NO]     = pe.[REQUEST_NO],
								[LADING_POINT]   = pe.[LADING_POINT],
								[TECH_DESIGNER]  = pe.[TECH_DESIGNER],
								[SALES_AMOUNT] = PE.[FINAL_QTY] * PE.[GROSS_SELL_PRICE],
								[BALANCE_SHIP] = CASE WHEN PE.[FINAL_QTY] > PE.[SHIPPED_QTY]  THEN PE.[FINAL_QTY] - PE.[SHIPPED_QTY] ELSE 0 END
								--,PO.COMMENT =  '|' +  isnull(PO.COMMENT,'') + '|' + CONVERT(VARCHAR(6),PE.RowID)
							    OUTPUT inserted.PSR_ORDER_ID,  
									   deleted.Comment,  
									   inserted.Comment,
									   deleted.OC_NO,
									   inserted.OC_NO  ,
									   deleted.BRAND,
									   inserted.BRAND ,								-- where ( deleted.oc_no = inserted.oc_no and deleted.brand = inserted.brand and deleted.style = inserted.style)
									   deleted.STYLE,
									   inserted.STYLE,
									   deleted.PL_ACTUAL_GAC,
									   inserted.PL_ACTUAL_GAC 
								INTO @MyTableVar 
								
									FROM PSR_ORDER PO, #tmppsr pe  WHERE
										CK1=ISNULL(pe.PROFOMA_PO,'')+ '|'+ISNULL(pe.ORDER_NO,'')+ '|'+ISNULL(pe.PROD_OFFICE,'')+ '|'+ISNULL(pe.SUPPLIER,'')+ '|'+ISNULL(pe.VENDOR_NAME,'')
									AND	CK2=ISNULL(pe.MANUFACTURER,'')+ '|'+ISNULL(pe.FACTORY_NAME,'')+  '|' +  pe.ORIGIN_CNTRY +  '|'+ISNULL(pe.ORIGIN_COUNTRY,'')+'|'+ISNULL(pe.BRAND,'') 
									AND	CK3=ISNULL(pe.DEPT,'')+ '|'+ISNULL(pe.CATEGORY,'')+ '|'+ISNULL(pe.BUYER,'')+ '|'+ISNULL(pe.STYLE_NO,'')+ '|'+ISNULL(pe.STYLE_DESC,'')+ '|'+ISNULL(pe.COLOR_CODE,'') 
									AND	CK4=ISNULL(pe.PLAN_ID,'')+ '|'+ISNULL(pe.PRODUCT_TYPE,'') --+ '|'+ISNULL(pe.PROGRAM_NAME,'')
									AND	CK5=ISNULL(pe.GENDER,'')  +  '|' -- +ISNULL(pe.CPO_SHIP_MODE,'') --+  '|'+ISNULL(pe.SEASON,'')
									--AND CK6='|' --ISNULL(pe.SALES_ORDER_NO,'')+ '|'+ISNULL(pe.CPO_COLOR_DESC,'')
									AND  PE.RowID = @RowId

								 								
								SET @UPDATED = @UPDATED + 1
								--SELECT 'UPDATING ', @ROWID 

								-- Delete events for incoming PSR if OC_NO, BRAND or STYLE changed and there exists an older PSR with matching OC_NO criteria.
  
								delete from psr_events where psr_order_id in 
								(
									select p1.psr_order_id 
										from @MyTableVar p1 ,
										(select min(psr_order_id) psr_order_id, oc_no, brand, style from   psr_order  group by oc_no, brand, style) p2 
									where  NOT(NewOCNO = OldOCNO and NewBrand = OldBrand and newstyle = OldStyle)
									and (	(p2.OC_NO = p1.NewOCNO and p2.BRAND = p1.NewBrand and p2.STYLE = p1.NewStyle
											and p1.psr_order_id <> p2.psr_order_id)
											or
											(p2.OC_NO = p1.OldOCNO and p2.BRAND = p1.OldBrand and p2.STYLE = p1.OldStyle
											and p1.psr_order_id <> p2.psr_order_id)
										)
								)
							   

								update PE set ACTUAL_DATE = CASE WHEN m.NewPLActualGAC = @mindate THEN NULL ELSE m.NewPLActualGAC END 
									FROM PSR_EVENTS PE, @MyTableVar m
									where PE.PSR_ORDER_ID = M.psr_order_id 
									AND PE.EVENT_CODE = 'E000'
									and isnull(M.OldPLActualGAC,@mindate) <> isnull(M.NewPLActualGAC,@mindate)							   
							   
						END 
						ELSE

						BEGIN
							insert into PSR_ORDER
							(
							VPO	,
							PROD_OFFICE	,
							VENDOR_ID	,
							VENDOR_NAME	,
							FACTORY_ID	,
							FACTORY_NAME	,
							COUNTRY_ORIGIN	,
							BRAND	,
							DEPT	,
							CATEGORY,
							MERCHANT	,
							STYLE 	,
							STYLE_DESC	,
							COLOR_CODE,
							COLOR_WASH_NAME	,
							FOB	,
							GROSS_SELL_PRICE	,
							MARGIN	,
							PRODUCT_TYPE	,
							PROGRAM_NAME	,
							SHIP_MODE	,
							SHIP_TO	,
							GENDER	,
							ORDER_QTY	,
							SHIPPED_QTY	,
							OC_COMMIT	,
							CPO	,
							SEASON	,
							NDC_DATE ,
							VPO_NLT_GAC_DATE	,
							VPO_FTY_GAC_DATE	,
							KEY_ITEM	,
							VPO_STATUS	,
							SO_ID	,
							VPO_LINE_STATUS	,
							INTERFACE_TS	,
							CPO_ORDER_QTY	,
							CPO_SHIP_MODE	,
							CPO_COLOR_DESC	,
							CPO_STYLE_DESC	 ,
							CPO_PACK_TYPE ,
							VPO_FTY_GAC_DATE_TEMP,
							PROFOMA_PO,
							MODIFY_TS	,
							MODIFY_USER	,
							[ASN_ID] ,
			  
							[ORIGIN_CNTRY],
							[PLAN_ID] ,
							[OC_NO] ,
							[INIT_FLOW],
							CPO_GAC_DATE,
							PL_ACTUAL_GAC,
                            [VPO_ORDER_TYPE],
                            [SPEED_INDICATOR], 
                            [PL_SHIP_DATE],
                            [CPO_STYLE_NO],
                            [REQUEST_NO],
                            [LADING_POINT],
                            [TECH_DESIGNER], 
							SALES_AMOUNT,
							[BALANCE_SHIP]
							--COMMENT
							,[CK1]
							,[CK2]
							,[CK3]
							,[CK4]
							,[CK5]
							,[CK6]
							)

							select  
							ORDER_NO	,
							PROD_OFFICE	,
							SUPPLIER	,
							VENDOR_NAME	,
							MANUFACTURER	,
							FACTORY_NAME	,
							ORIGIN_CNTRY	,
							ISNULL(BRAND,'')	,
							DEPT	,
							CATEGORY,
							BUYER	,
							STYLE_NO	,
							STYLE_DESC	,
							isnull(COLOR_CODE,''),
							isnull(COLOR_WASH_NAME, '')	,
							FOB	,
							GROSS_SELL_PRICE	,
							MARGIN_PCT	,
							PRODUCT_TYPE	,
							PROGRAM_NAME	,
							TRANS_MODE	,
							DELIVER_TO	,
							GENDER	,
							FINAL_QTY	,
							SHIPPED_QTY	,
							OC_NO	,
							SALES_ORDER_NO	,
							SEASON	,
							CUST_DEL_DATE	,
							NLT_GAC_DATE	,
							FTY_GAC_DATE	,
							ISNULL(CPO_STYLE_DESC,'') KEY_ITEM	,
							VPO_HEAD_STATUS	,
							isnull(SO_ID,0),
							VPO_LINE_STATUS	,
							INTERFACE_TS	,
							cpo_order_qty  	,
							CPO_SHIP_MODE	,
							CPO_COLOR_DESC	,
							ISNULL(CPO_STYLE_DESC,'') ,
							SUBSTRING(CPO_PACK_TYPE,1	,6),
							FTY_GAC_DATE,
							PROFOMA_PO ,
							MODIFY_TS	,
							MODIFY_USER	,
							[ASN_ID] ,
  
							[ORIGIN_CNTRY],
							[PLAN_ID] ,
							[OC_NO] ,
							[INIT_FLOW],
							CASE WHEN CPO_GAC_DATE = @mindate THEN NULL ELSE CPO_GAC_DATE END,
							CASE WHEN PL_ACTUAL_GAC = @mindate THEN NULL ELSE PL_ACTUAL_GAC END,
                            [VPO_ORDER_TYPE],
                            [VPO_SPEED_IND], 
							CASE WHEN PL_SHIP_DATE = @mindate THEN NULL ELSE PL_SHIP_DATE END,
                            [CPO_STYLE_NO],
                            [REQUEST_NO],
                            [LADING_POINT],
                            [TECH_DESIGNER], 
							[FINAL_QTY] * [GROSS_SELL_PRICE],
							CASE WHEN [FINAL_QTY] > [SHIPPED_QTY]  THEN [FINAL_QTY] - [SHIPPED_QTY] ELSE 0 END
							--[CPO_ORDER_QTY] - [SHIPPED_QTY]
							--ORDER_NO + '-' + CONVERT(VARCHAR(6),@ROWID)
							,ISNULL(PROFOMA_PO,'')+ '|'+ISNULL(ORDER_NO,'')+ '|'+ISNULL(PROD_OFFICE,'')+ '|'+ISNULL(SUPPLIER,'')+ '|'+ISNULL(VENDOR_NAME,'')
							,ISNULL(MANUFACTURER,'')+ '|'+ISNULL(FACTORY_NAME,'')+ '|'+ISNULL(ORIGIN_CNTRY,'')+ '|'+ISNULL(ORIGIN_COUNTRY,'')+ '|'+ISNULL(BRAND,'')
							,ISNULL(DEPT,'')+ '|'+ISNULL(CATEGORY,'')+ '|'+ISNULL(BUYER,'')+ '|'+ISNULL(STYLE_NO,'')+ '|'+ISNULL(STYLE_DESC,'')+ '|'+ISNULL(COLOR_CODE,'')
							,ISNULL(PLAN_ID,'')+ '|'+ISNULL(PRODUCT_TYPE,'') --+ '|'+ISNULL(PROGRAM_NAME,'')
							,ISNULL(GENDER,'')  +  '|' --+ISNULL(pe.CPO_SHIP_MODE,'') --+ '|' +ISNULL(SEASON,'')
							,'!' --ISNULL(SALES_ORDER_NO,'')+ '|'+ISNULL(CPO_COLOR_DESC,'')				
							from #tmppsr pe
							where not exists (select 1 from PSR_ORDER po  where					
									CK1=ISNULL(pe.PROFOMA_PO,'')+ '|'+ISNULL(pe.ORDER_NO,'')+ '|'+ISNULL(pe.PROD_OFFICE,'')+ '|'+ISNULL(pe.SUPPLIER,'')+ '|'+ISNULL(pe.VENDOR_NAME,'')
								AND	CK2=ISNULL(pe.MANUFACTURER,'')+ '|'+ISNULL(pe.FACTORY_NAME,'')+  '|' +  pe.ORIGIN_CNTRY +  '|'+ISNULL(pe.ORIGIN_COUNTRY,'')+'|'+ISNULL(pe.BRAND,'') 
								AND	CK3=ISNULL(pe.DEPT,'')+ '|'+ISNULL(pe.CATEGORY,'')+ '|'+ISNULL(pe.BUYER,'')+ '|'+ISNULL(pe.STYLE_NO,'')+ '|'+ISNULL(pe.STYLE_DESC,'')+ '|'+ISNULL(pe.COLOR_CODE,'') 
								AND	CK4=ISNULL(pe.PLAN_ID,'')+ '|'+ISNULL(pe.PRODUCT_TYPE,'') --+ '|'+ISNULL(pe.PROGRAM_NAME,'') 
								AND	CK5=ISNULL(pe.GENDER,'')  +  '|' -- +ISNULL(pe.CPO_SHIP_MODE,'') --+ '|'+ ISNULL(pe.SEASON,'')
								AND CK6='|' ) --ISNULL(pe.SALES_ORDER_NO,'')+ '|'+ISNULL(pe.CPO_COLOR_DESC,''))	
								AND  PE.RowID = @RowId

							SET @INSERTED = @INSERTED + 1

						END

				fetch next from pCur into @RowId		
			end
		  -- select psr_order_id from @MyTableVar WHERE OldOCNO <> NewOCNO
		  -- SELECT * INTO TEMPHOLD FROM #TMPPSR
		  UPDATE PSR_LASTRUN SET OrderSynchLastRun = @LASTRUN
		  SELECT @MESSAGE = 'Order Synch completed. Number of rows inserted = ' + convert(varchar(10),@INSERTED) + ' Updated = ' + convert(varchar(10),@UPDATED) 
		  insert into psr_eventslog (logtext, createddate)
		  select @MESSAGE , getdate()
		  PRINT	@MESSAGE
		  SELECT @MESSAGE RESULT 
END




GO
/****** Object:  StoredProcedure [dbo].[PSR_SYNCusers]    Script Date: 5/3/2019 4:56:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[PSR_SYNCusers]
AS
BEGIN

		-- prior to this run, an extract has been run in bamboo rose the the extracted data is available in tempdb..tmp
		   DELETE FROM U
		   FROM BR_USER_PROFILE  U, tmpusers UE
			WHERE U.BR_USER_ID = UE.BR_USER_ID
			  AND U.BR_ROLE_ID = UE.BR_ROLE_ID
			  AND ISNULL(U.BR_ATTR_NAME,'') =ISNULL(UE.BR_ATTR_NAME,'')

			UPDATE U 
				SET
					BR_USER_NAME = UE.BR_USER_NAME, 
					BR_ROLE_NAME = UE.BR_ROLE_NAME,
					BR_ATTR_VALUE = UE.BR_ATTR_VALUE
			FROM BR_USER_PROFILE  U, tmpusers UE
			WHERE U.BR_USER_ID = UE.BR_USER_ID
			  AND U.BR_ROLE_ID = UE.BR_ROLE_ID
			  AND ISNULL(U.BR_ATTR_NAME,'') =ISNULL(UE.BR_ATTR_NAME,'')

			insert into BR_USER_PROFILE
			 (br_user_id, BR_user_name, BR_ROLE_ID, BR_ROLE_NAME, BR_ATTR_NAME, BR_ATTR_VALUE)
			select  br_user_id, BR_user_name, BR_ROLE_ID, BR_ROLE_NAME, BR_ATTR_NAME, BR_ATTR_VALUE
				from tmpusers UE 
					where NOT EXISTS (SELECT 1 FROM BR_USER_PROFILE U
					 WHERE U.BR_USER_ID = UE.BR_USER_ID
					   AND U.BR_ROLE_ID = UE.BR_ROLE_ID
					   AND isnull(U.BR_ATTR_NAME,'') = isnull(UE.BR_ATTR_NAME,''))

			UPDATE BR_USER_PROFILE SET BR_ATTR_NAME = NULL WHERE BR_ATTR_NAME = ''

		  insert into psr_eventslog (logtext, createddate)
		  select 'Order Synch completed. Number of rows inserted =' + convert(varchar(8),@@ROWCOUNT), getdate()
END



GO
