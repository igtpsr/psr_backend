ALTER PROCEDURE [dbo].[PSR_SYNCORDERS]
WITH EXEC AS CALLER
AS
BEGIN

		-- prior to this run, an extract has been run in bamboo rose the the extracted data is available in tempdb..tmp

			DECLARE @DATECREATED DATETIME = getdate()
		    DECLARE @LASTRUN DATETIME = getdate()
			DECLARE @INSERTED int = 0
			DECLARE @UPDATED int = 0	
    		DECLARE @MESSAGE VARCHAR(500)		
			DECLARE @RowId int
			DECLARE @mindate date = ''
	   
			-- EXEC PSR_CHECKDUPORDERS @DATECREATED

			DECLARE @MyTableVar table(  
				psr_order_id int NOT NULL,  
				OldComment varchar(250), 
				NewComment varchar(250),
				OldOCNO varchar(35),
				NewOCNO varchar(35),
				OldBrand varchar(35),
				NewBrand varchar(35),				
				OldStyle varchar(35),
				NewStyle varchar(35),
				OldPLActualGAC datetime,	
				NewPLActualGAC datetime		
		);  

		SELECT IDENTITY(int, 1, 1) AS RowID, * INTO #TMPPSR FROM TMPPSR ORDER BY MODIFY_TS

	    DECLARE PCUR CURSOR FOR SELECT ROWID FROM #TMPPSR

		OPEN pCur
			fetch next from pCur into @RowId
			while @@fetch_status=0 
			begin
						IF EXISTS (SELECT 1 
						FROM PSR_ORDER PO, #tmppsr pe  WHERE
							CK1=ISNULL(pe.PROFOMA_PO,'')+ '|'+ISNULL(pe.ORDER_NO,'')+ '|'+ISNULL(pe.PROD_OFFICE,'')+ '|'+ISNULL(pe.SUPPLIER,'')+ '|'+ISNULL(pe.VENDOR_NAME,'')
						AND	CK2=ISNULL(pe.MANUFACTURER,'')+ '|'+ISNULL(pe.FACTORY_NAME,'')+  '|' +  pe.ORIGIN_CNTRY +  '|'+ISNULL(pe.ORIGIN_COUNTRY,'')+'|'+ISNULL(pe.BRAND,'') 
						AND	CK3=ISNULL(pe.DEPT,'')+ '|'+ISNULL(pe.CATEGORY,'')+ '|'+ISNULL(pe.BUYER,'')+ '|'+ISNULL(pe.STYLE_NO,'')+ '|'+ISNULL(pe.STYLE_DESC,'')+ '|'+ISNULL(pe.COLOR_CODE,'') 
						AND	CK4=ISNULL(pe.PLAN_ID,'')+ '|'+ISNULL(pe.PRODUCT_TYPE,'') --+ '|'+ISNULL(pe.PROGRAM_NAME,'')
						AND	CK5=ISNULL(pe.GENDER,'') +  '|'-- +ISNULL(pe.CPO_SHIP_MODE,'') --+  '|'+ISNULL(pe.SEASON,'')
						AND PE.RowID = @RowId)
						BEGIN

							update po
								set
								PROD_OFFICE	 = pe.	PROD_OFFICE	,
								VENDOR_ID	 = pe.	SUPPLIER	,
								VENDOR_NAME	 = pe.	VENDOR_NAME	,
								FACTORY_ID	 = pe.	MANUFACTURER	,
								FACTORY_NAME	 = pe.	FACTORY_NAME	,
								COUNTRY_ORIGIN	 = pe.	ORIGIN_CNTRY	,
								BRAND	 = ISNULL(pe.BRAND,'')	,
								DEPT	 = pe.	DEPT	,
								CATEGORY = pe.CATEGORY,
								MERCHANT	 = pe.	BUYER	,
								STYLE 	 = pe.	STYLE_NO	,
								STYLE_DESC	 = pe.	STYLE_DESC	,
								FOB	 = pe.	FOB	,
								GROSS_SELL_PRICE	 = pe.	GROSS_SELL_PRICE	,
								MARGIN	 = pe.	MARGIN_PCT	,
								PRODUCT_TYPE	 = pe.	PRODUCT_TYPE	,
								PROGRAM_NAME	 = pe.	PROGRAM_NAME	,
								SHIP_MODE	 = pe.	TRANS_MODE	,
								SHIP_TO		 = pe.DELIVER_TO	,
								GENDER	 = pe.	GENDER	,
								ORDER_QTY	 = pe.	FINAL_QTY	,
								SHIPPED_QTY	 = pe.	SHIPPED_QTY	,
								OC_COMMIT	 = pe.	OC_NO	,
								CPO	 = pe.	SALES_ORDER_NO	,
								SEASON	 = pe.	SEASON	,
								NDC_DATE 	 = pe.	CUST_DEL_DATE	,
								VPO_NLT_GAC_DATE	 = pe.	NLT_GAC_DATE	,
								VPO_FTY_GAC_DATE	 = pe.	FTY_GAC_DATE	,
								KEY_ITEM	 = pe.	CPO_STYLE_DESC	,
								VPO_STATUS	 = pe.	VPO_HEAD_STATUS	,
								SO_ID		 = isnull(pe.SO_ID,0),
								VPO_LINE_STATUS	 = pe.	VPO_LINE_STATUS	,
								INTERFACE_TS	 = pe.	INTERFACE_TS	,
								CPO_ORDER_QTY	 = pe.	CPO_ORDER_QTY	,
								CPO_SHIP_MODE	 = pe.	CPO_SHIP_MODE	,
								CPO_COLOR_DESC	 = pe.	CPO_COLOR_DESC	,
								CPO_STYLE_DESC	 = ISNULL(pe.CPO_STYLE_DESC,''),
								CPO_PACK_TYPE    = substring(pe.CPO_PACK_TYPE,1,6),
								VPO_FTY_GAC_DATE_TEMP = CASE WHEN pe.FTY_GAC_DATE <> VPO_FTY_GAC_DATE THEN VPO_FTY_GAC_DATE ELSE VPO_FTY_GAC_DATE_TEMP END,
								PROFOMA_PO        =  PE.PROFOMA_PO,
								MODIFY_TS	 = pe.	MODIFY_TS	,
								MODIFY_USER	 = pe.	MODIFY_USER	 ,
								[ASN_ID]	 	  = pe.[ASN_ID]	 	,
								[ASN]	 		  = pe.[ASN_ID]	 	,																				 
								[ORIGIN_CNTRY]	  = pe.[ORIGIN_CNTRY]	,
								[PLAN_ID] 		  = pe.[PLAN_ID] 		,
								[OC_NO]           =  pe.[OC_NO] ,
								[COLOR_WASH_NAME] = pe.[COLOR_WASH_NAME],
								[INIT_FLOW] = pe.[INIT_FLOW],
								[INIT_FLOW_PREV] = CASE WHEN pe.[INIT_FLOW] <> PO.[INIT_FLOW] THEN PO.[INIT_FLOW] ELSE PO.[INIT_FLOW_PREV] END,
								CPO_GAC_DATE	 = CASE WHEN PE.CPO_GAC_DATE = @mindate THEN NULL ELSE PE.CPO_GAC_DATE END ,
								PL_ACTUAL_GAC    = CASE WHEN PE.PL_ACTUAL_GAC = @mindate THEN NULL ELSE PE.PL_ACTUAL_GAC END ,
								PL_SHIP_DATE    = CASE WHEN PE.PL_SHIP_DATE = @mindate THEN NULL ELSE PE.PL_SHIP_DATE END ,	
								[VPO_ORDER_TYPE]  = pe.[VPO_ORDER_TYPE],
								[SPEED_INDICATOR]   = pe.[VPO_SPEED_IND],
								[CPO_STYLE_NO]   = pe.[CPO_STYLE_NO],
								[REQUEST_NO]     = pe.[REQUEST_NO],
								[LADING_POINT]   = pe.[LADING_POINT],
								[TECH_DESIGNER]  = pe.[TECH_DESIGNER],
								[CPO_DEPT]  = pe.[CPO_DEPT],
								[SALES_AMOUNT] = PE.[FINAL_QTY] * PE.[GROSS_SELL_PRICE],
								[BALANCE_SHIP] = CASE WHEN PE.[FINAL_QTY] > PE.[SHIPPED_QTY]  THEN PE.[FINAL_QTY] - PE.[SHIPPED_QTY] ELSE 0 END
								--,PO.COMMENT =  '|' +  isnull(PO.COMMENT,'') + '|' + CONVERT(VARCHAR(6),PE.RowID)
							    OUTPUT inserted.PSR_ORDER_ID,  
									   deleted.Comment,  
									   inserted.Comment,
									   deleted.OC_NO,
									   inserted.OC_NO  ,
									   deleted.BRAND,
									   inserted.BRAND ,								-- where ( deleted.oc_no = inserted.oc_no and deleted.brand = inserted.brand and deleted.style = inserted.style)
									   deleted.STYLE,
									   inserted.STYLE,
									   deleted.PL_ACTUAL_GAC,
									   inserted.PL_ACTUAL_GAC 
								INTO @MyTableVar 
								
									FROM PSR_ORDER PO, #tmppsr pe  WHERE
										CK1=ISNULL(pe.PROFOMA_PO,'')+ '|'+ISNULL(pe.ORDER_NO,'')+ '|'+ISNULL(pe.PROD_OFFICE,'')+ '|'+ISNULL(pe.SUPPLIER,'')+ '|'+ISNULL(pe.VENDOR_NAME,'')
									AND	CK2=ISNULL(pe.MANUFACTURER,'')+ '|'+ISNULL(pe.FACTORY_NAME,'')+  '|' +  pe.ORIGIN_CNTRY +  '|'+ISNULL(pe.ORIGIN_COUNTRY,'')+'|'+ISNULL(pe.BRAND,'') 
									AND	CK3=ISNULL(pe.DEPT,'')+ '|'+ISNULL(pe.CATEGORY,'')+ '|'+ISNULL(pe.BUYER,'')+ '|'+ISNULL(pe.STYLE_NO,'')+ '|'+ISNULL(pe.STYLE_DESC,'')+ '|'+ISNULL(pe.COLOR_CODE,'') 
									AND	CK4=ISNULL(pe.PLAN_ID,'')+ '|'+ISNULL(pe.PRODUCT_TYPE,'') --+ '|'+ISNULL(pe.PROGRAM_NAME,'')
									AND	CK5=ISNULL(pe.GENDER,'')  +  '|' -- +ISNULL(pe.CPO_SHIP_MODE,'') --+  '|'+ISNULL(pe.SEASON,'')
									--AND CK6='|' --ISNULL(pe.SALES_ORDER_NO,'')+ '|'+ISNULL(pe.CPO_COLOR_DESC,'')
									AND  PE.RowID = @RowId

								 								
								SET @UPDATED = @UPDATED + 1
								--SELECT 'UPDATING ', @ROWID 

								-- Delete events for incoming PSR if OC_NO, BRAND or STYLE changed and there exists an older PSR with matching OC_NO criteria.
  
								delete from psr_events where psr_order_id in 
								(
									select p1.psr_order_id 
										from @MyTableVar p1 ,
										(select min(psr_order_id) psr_order_id, oc_no, brand, style from   psr_order  group by oc_no, brand, style) p2 
									where  NOT(NewOCNO = OldOCNO and NewBrand = OldBrand and newstyle = OldStyle)
									and (	(p2.OC_NO = p1.NewOCNO and p2.BRAND = p1.NewBrand and p2.STYLE = p1.NewStyle
											and p1.psr_order_id <> p2.psr_order_id)
											or
											(p2.OC_NO = p1.OldOCNO and p2.BRAND = p1.OldBrand and p2.STYLE = p1.OldStyle
											and p1.psr_order_id <> p2.psr_order_id)
										)
								)
							   

								update PE set ACTUAL_DATE = CASE WHEN m.NewPLActualGAC = @mindate THEN NULL ELSE m.NewPLActualGAC END 
									FROM PSR_EVENTS PE, @MyTableVar m
									where PE.PSR_ORDER_ID = M.psr_order_id 
									AND PE.EVENT_CODE = 'E000'
									and isnull(M.OldPLActualGAC,@mindate) <> isnull(M.NewPLActualGAC,@mindate)							   
							   
						END 
						ELSE

						BEGIN
							insert into PSR_ORDER
							(
							VPO	,
							PROD_OFFICE	,
							VENDOR_ID	,
							VENDOR_NAME	,
							FACTORY_ID	,
							FACTORY_NAME	,
							COUNTRY_ORIGIN	,
							BRAND	,
							DEPT	,
							CATEGORY,
							MERCHANT	,
							STYLE 	,
							STYLE_DESC	,
							COLOR_CODE,
							COLOR_WASH_NAME	,
							FOB	,
							GROSS_SELL_PRICE	,
							MARGIN	,
							PRODUCT_TYPE	,
							PROGRAM_NAME	,
							SHIP_MODE	,
							SHIP_TO	,
							GENDER	,
							ORDER_QTY	,
							SHIPPED_QTY	,
							OC_COMMIT	,
							CPO	,
							SEASON	,
							NDC_DATE ,
							VPO_NLT_GAC_DATE	,
							VPO_FTY_GAC_DATE	,
							KEY_ITEM	,
							VPO_STATUS	,
							SO_ID	,
							VPO_LINE_STATUS	,
							INTERFACE_TS	,
							CPO_ORDER_QTY	,
							CPO_SHIP_MODE	,
							CPO_COLOR_DESC	,
							CPO_STYLE_DESC	 ,
							CPO_PACK_TYPE ,
							VPO_FTY_GAC_DATE_TEMP,
							PROFOMA_PO,
							MODIFY_TS	,
							MODIFY_USER	,
							[ASN_ID] ,
			  
							[ORIGIN_CNTRY],
							[PLAN_ID] ,
							[OC_NO] ,
							[INIT_FLOW],
							CPO_GAC_DATE,
							PL_ACTUAL_GAC,
                            [VPO_ORDER_TYPE],
                            [SPEED_INDICATOR], 
                            [PL_SHIP_DATE],
                            [CPO_STYLE_NO],
                            [REQUEST_NO],
                            [LADING_POINT],
                            [TECH_DESIGNER], 
							[CPO_DEPT],
							SALES_AMOUNT,
							[BALANCE_SHIP]
							--COMMENT
							,[CK1]
							,[CK2]
							,[CK3]
							,[CK4]
							,[CK5]
							,[CK6]
							)

							select  
							ORDER_NO	,
							PROD_OFFICE	,
							SUPPLIER	,
							VENDOR_NAME	,
							MANUFACTURER	,
							FACTORY_NAME	,
							ORIGIN_CNTRY	,
							ISNULL(BRAND,'')	,
							DEPT	,
							CATEGORY,
							BUYER	,
							STYLE_NO	,
							STYLE_DESC	,
							isnull(COLOR_CODE,''),
							isnull(COLOR_WASH_NAME, '')	,
							FOB	,
							GROSS_SELL_PRICE	,
							MARGIN_PCT	,
							PRODUCT_TYPE	,
							PROGRAM_NAME	,
							TRANS_MODE	,
							DELIVER_TO	,
							GENDER	,
							FINAL_QTY	,
							SHIPPED_QTY	,
							OC_NO	,
							SALES_ORDER_NO	,
							SEASON	,
							CUST_DEL_DATE	,
							NLT_GAC_DATE	,
							FTY_GAC_DATE	,
							ISNULL(CPO_STYLE_DESC,'') KEY_ITEM	,
							VPO_HEAD_STATUS	,
							isnull(SO_ID,0),
							VPO_LINE_STATUS	,
							INTERFACE_TS	,
							cpo_order_qty  	,
							CPO_SHIP_MODE	,
							CPO_COLOR_DESC	,
							ISNULL(CPO_STYLE_DESC,'') ,
							SUBSTRING(CPO_PACK_TYPE,1	,6),
							FTY_GAC_DATE,
							PROFOMA_PO ,
							MODIFY_TS	,
							MODIFY_USER	,
							[ASN_ID] ,
  
							[ORIGIN_CNTRY],
							[PLAN_ID] ,
							[OC_NO] ,
							[INIT_FLOW],
							CASE WHEN CPO_GAC_DATE = @mindate THEN NULL ELSE CPO_GAC_DATE END,
							CASE WHEN PL_ACTUAL_GAC = @mindate THEN NULL ELSE PL_ACTUAL_GAC END,
                            [VPO_ORDER_TYPE],
                            [VPO_SPEED_IND], 
							CASE WHEN PL_SHIP_DATE = @mindate THEN NULL ELSE PL_SHIP_DATE END,
                            [CPO_STYLE_NO],
                            [REQUEST_NO],
                            [LADING_POINT],
                            [TECH_DESIGNER], 
							[CPO_DEPT],
							[FINAL_QTY] * [GROSS_SELL_PRICE],
							CASE WHEN [FINAL_QTY] > [SHIPPED_QTY]  THEN [FINAL_QTY] - [SHIPPED_QTY] ELSE 0 END
							--[CPO_ORDER_QTY] - [SHIPPED_QTY]
							--ORDER_NO + '-' + CONVERT(VARCHAR(6),@ROWID)
							,ISNULL(PROFOMA_PO,'')+ '|'+ISNULL(ORDER_NO,'')+ '|'+ISNULL(PROD_OFFICE,'')+ '|'+ISNULL(SUPPLIER,'')+ '|'+ISNULL(VENDOR_NAME,'')
							,ISNULL(MANUFACTURER,'')+ '|'+ISNULL(FACTORY_NAME,'')+ '|'+ISNULL(ORIGIN_CNTRY,'')+ '|'+ISNULL(ORIGIN_COUNTRY,'')+ '|'+ISNULL(BRAND,'')
							,ISNULL(DEPT,'')+ '|'+ISNULL(CATEGORY,'')+ '|'+ISNULL(BUYER,'')+ '|'+ISNULL(STYLE_NO,'')+ '|'+ISNULL(STYLE_DESC,'')+ '|'+ISNULL(COLOR_CODE,'')
							,ISNULL(PLAN_ID,'')+ '|'+ISNULL(PRODUCT_TYPE,'') --+ '|'+ISNULL(PROGRAM_NAME,'')
							,ISNULL(GENDER,'')  +  '|' --+ISNULL(pe.CPO_SHIP_MODE,'') --+ '|' +ISNULL(SEASON,'')
							,'!' --ISNULL(SALES_ORDER_NO,'')+ '|'+ISNULL(CPO_COLOR_DESC,'')				
							from #tmppsr pe
							where not exists (select 1 from PSR_ORDER po  where					
									CK1=ISNULL(pe.PROFOMA_PO,'')+ '|'+ISNULL(pe.ORDER_NO,'')+ '|'+ISNULL(pe.PROD_OFFICE,'')+ '|'+ISNULL(pe.SUPPLIER,'')+ '|'+ISNULL(pe.VENDOR_NAME,'')
								AND	CK2=ISNULL(pe.MANUFACTURER,'')+ '|'+ISNULL(pe.FACTORY_NAME,'')+  '|' +  pe.ORIGIN_CNTRY +  '|'+ISNULL(pe.ORIGIN_COUNTRY,'')+'|'+ISNULL(pe.BRAND,'') 
								AND	CK3=ISNULL(pe.DEPT,'')+ '|'+ISNULL(pe.CATEGORY,'')+ '|'+ISNULL(pe.BUYER,'')+ '|'+ISNULL(pe.STYLE_NO,'')+ '|'+ISNULL(pe.STYLE_DESC,'')+ '|'+ISNULL(pe.COLOR_CODE,'') 
								AND	CK4=ISNULL(pe.PLAN_ID,'')+ '|'+ISNULL(pe.PRODUCT_TYPE,'') --+ '|'+ISNULL(pe.PROGRAM_NAME,'') 
								AND	CK5=ISNULL(pe.GENDER,'')  +  '|' -- +ISNULL(pe.CPO_SHIP_MODE,'') --+ '|'+ ISNULL(pe.SEASON,'')
								AND CK6='|' ) --ISNULL(pe.SALES_ORDER_NO,'')+ '|'+ISNULL(pe.CPO_COLOR_DESC,''))	
								AND  PE.RowID = @RowId

							SET @INSERTED = @INSERTED + 1

						END

				fetch next from pCur into @RowId		
			end

--   Fix PSR_EVENTS WITH PL_ACTUAL_GAC FOR E000 FROM ITS OWN ORDER RECORD AND NOT FROM OC PARENT  -- Ganesh 09/17/2019
			update e set e.ACTUAL_DATE = O.PL_ACTUAL_GAC
			from psr_events e 
			inner join psr_order o on o.PSR_ORDER_ID = e.PSR_ORDER_ID and e.EVENT_CODE = 'E000' 
			where PL_ACTUAL_GAC is not null and isnull(PL_ACTUAL_GAC,'1900-01-01') <> isnull(e.ACTUAL_DATE,'1900-01-01')
--   End PSR_EVENTS E00 ACTUAL_DATE FIX UP LOGIC   -- Ganesh 09/17/2019

		  -- select psr_order_id from @MyTableVar WHERE OldOCNO <> NewOCNO
		  -- SELECT * INTO TEMPHOLD FROM #TMPPSR
      -- interim method to prevent record missing, which is to move 10 min from the time when this sp execute, long time fix need to get the datetime of the SCHEDULER start time.
      UPDATE PSR_LASTRUN SET OrderSynchLastRun = dateadd(minute, -10, @LASTRUN)
		  SELECT @MESSAGE = 'Order Synch completed. Number of rows inserted = ' + convert(varchar(10),@INSERTED) + ' Updated = ' + convert(varchar(10),@UPDATED) 
		  insert into psr_eventslog (logtext, createddate)
		  select @MESSAGE , getdate()
		  PRINT	@MESSAGE
		  SELECT @MESSAGE RESULT  
END